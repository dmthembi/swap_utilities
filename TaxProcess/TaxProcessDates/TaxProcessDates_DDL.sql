if object_id('TaxProcessDates') is not null
drop table TaxProcessDates
GO
Create table TaxProcessDates(Tax_Year int, 
							 Begin_Date datetime,
							 End_Date datetime,
							 Bi_Annual bit DEFAULT 0)
GO
