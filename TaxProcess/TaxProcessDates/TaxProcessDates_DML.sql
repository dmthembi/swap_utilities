SET NOCOUNT ON
TRUNCATE TABLE TaxProcessDates
GO
DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit

select @Tax_Year = 2017,
       @Bi_Annual  = 0    --1: true. 0: false.
	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date   
from TaxYearBeginDates
where Tax_Year = @Tax_Year

if(@Bi_Annual = 1)
 select @End_Date = dateadd(dd, -datepart(dd,@End_Date),dateadd(mm, -5, @End_Date))
 

 INSERT INTO TaxProcessDates
 SELECT @Tax_Year, @Start_Date, @End_Date, @Bi_Annual
 
 IF(@Bi_Annual = 1)
 BEGIN
 SELECT 'TaxProcessDates INITIALIZED FOR BI-ANNUAL RUN'
 END
 ELSE
 BEGIN
  SELECT 'TaxProcessDates INITIALIZED FOR ANNUAL RUN'
 END
 
  SELECT 'Tax_Year' , @Tax_Year
 SELECT 'Start_Date' , @Start_Date
SELECT 'End_Date' , @End_Date

GO