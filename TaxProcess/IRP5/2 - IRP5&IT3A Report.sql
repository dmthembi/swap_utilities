IF OBJECT_ID('WORKSPACE..IT3aReport') IS NOT NULL DROP TABLE WORKSPACE..IT3aReport
IF OBJECT_ID('WORKSPACE..IRP5Report') IS NOT NULL DROP TABLE WORKSPACE..IRP5Report


GO

DECLARE @NonTax   CHAR,
	     @Adhoc BIT,
        @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit

	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Bi_Annual = Bi_Annual 
from TaxProcessDates

SELECT 	@NonTax 	= 'N',
	@Adhoc		= 0

--IF @NonTax = 'N'
SELECT
    Owner			AS 'EmpNumber',
    Income			AS 'Income',
    Non_Taxable_Income		AS 'NonTaxableIncome',
    Tax				AS 'Tax',
    Line_1			AS 'Line1',
    Line_2			AS 'Line2',
    ISNULL(Line_3, '')		AS 'Line3',
    ISNULL(Line_4, ISNULL(Line_5,''))		AS 'Line4',
    Postal_Code			AS 'Line5',
    Id				AS 'IDNumber',
    ISNULL(PassportNo, '')	AS 'Passport',
    ISNULL(TrustNo, '')		AS 'TrustNo',
    ISNULL(Tax_No, '')		AS 'TaxNumber',
    Commencement_Date		AS 'FromDate',
    To_Date			AS 'ToDate',
    cpy_TradingName		AS 'EmpTrading',
    cpy_IRP5Number		AS 'IRP5Number',
    cpy_Reference_Number	AS 'Reference',
    cpy_TaxYear			AS 'TaxYear',
    cpy_Address1		AS 'EmpLine1',
    cpy_Address2		AS 'EmpLine2',
    cpy_Address3		AS 'EmpLine3',
    cpy_Address4		AS 'EmpLine4',
    cpy_PostalCode		AS 'EmpPostal',
    PersonNature		AS 'Nature',
    Person_Surname		AS 'Trading',
    Person_Init			AS 'Initial',
    Person_FullNames		AS 'Names',
    Person_Birthdate		AS 'BirthDate',
    Person_PayPeriods		AS 'Period',
    12				AS 'PayPeriodsInTaxYear',
    Person_NoPayPeriods		AS 'PeriodWorked',
    ISNULL(FixedRateIncome, '')	AS 'FixedRate',
    ISNULL(Directive_Number,'')	AS 'DNumber',
    ISNULL(Selected_Rate, '')	AS 'Volt',
    ISNULL(cpy_Diplomatic, '')	AS 'Diplomatic'
INTO WORKSPACE..IRP5Report
FROM
    IRP5Details2001
WHERE 1=1
AND    ISNULL(Tax, 0) != 0
AND cpy_TaxYear = @TaxYear
--AND Adhoc = 0 --CASE @Adhoc WHEN 0 THEN 0 WHEN 1 THEN 1 ELSE Adhoc END

GO

DECLARE @TaxYear 	INT, 
	@NonTax   	CHAR(1),
	@Adhoc		INT

SELECT 	@TaxYear 	=2017, 
	@NonTax 	= 'N',
	@Adhoc		= 0

--IF @NonTax = 'N'
SELECT
    Owner			AS 'EmpNumber',
    Income			AS 'Income',
    Non_Taxable_Income		AS 'NonTaxableIncome',
    Tax				AS 'Tax',
    Line_1			AS 'Line1',
    Line_2			AS 'Line2',
    ISNULL(Line_3, '')		AS 'Line3',
    ISNULL(Line_4, '')		AS 'Line4',
    Postal_Code			AS 'Line5',
    Id				AS 'IDNumber',
    ISNULL(PassportNo, '')	AS 'Passport',
    ISNULL(TrustNo, '')		AS 'TrustNo',
    ISNULL(Tax_No, '')		AS 'TaxNumber',
    Commencement_Date		AS 'FromDate',
    To_Date			AS 'ToDate',
    cpy_TradingName		AS 'EmpTrading',
    cpy_IRP5Number		AS 'IRP5Number',
    cpy_Reference_Number	AS 'Reference',
    cpy_TaxYear			AS 'TaxYear',
    cpy_Address1		AS 'EmpLine1',
    cpy_Address2		AS 'EmpLine2',
    cpy_Address3		AS 'EmpLine3',
    cpy_Address4		AS 'EmpLine4',
    cpy_PostalCode		AS 'EmpPostal',
    PersonNature		AS 'Nature',
    Person_Surname		AS 'Trading',
    Person_Init			AS 'Initial',
    Person_FullNames		AS 'Names',
    Person_Birthdate		AS 'BirthDate',
    Person_PayPeriods		AS 'Period',
    12				AS 'PayPeriodsInTaxYear',
    Person_NoPayPeriods		AS 'PeriodWorked',
    ISNULL(FixedRateIncome, '')	AS 'FixedRate',
    ISNULL(Directive_Number,'')	AS 'DNumber',
    ISNULL(Selected_Rate, '')	AS 'Volt',
    ISNULL(cpy_Diplomatic, '')	AS 'Diplomatic'
INTO WORKSPACE..IT3aReport
FROM
    IRP5Details2001
WHERE 1=1
AND    ISNULL(Tax, 0) = 0
AND cpy_TaxYear = @TaxYear
--AND Adhoc = 0 --CASE @Adhoc WHEN 0 THEN 0 WHEN 1 THEN 1 ELSE Adhoc END


select count(*) from WORKSPACE..IRP5Report
select count(*) from WORKSPACE..IRP5Report

/*****************************************************************************/
-- End of script
/*****************************************************************************/


-- ************************************ WORKSPACE TABLES ***********************************

GO



--SELECT * FROM WORKSPACE..IT3aReport
--SELECT * FROM WORKSPACE..IRP5Report












