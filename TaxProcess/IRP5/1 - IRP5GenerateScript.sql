SET FLUSHMESSAGE ON
go
set nocount on
go


--- ****************This script Generates IRP and IT3A Certs ********************
declare @Start datetime
select @Start = getdate()
print	'Start: %1!', @Start
go
------***************Start Of IRP5 / IT3A Generation*********************

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit

	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Bi_Annual = Bi_Annual 
from TaxProcessDates


delete IRP5Details2001
where cpy_TaxYear = @Tax_Year

print 'Start Date  %1!', @Start_Date
print 'End Date  %1!', @End_Date

EXEC	GenerateIRP5 @Start_Date,@End_Date



------***************End Of IRP5 / IT3A Generation*********************
go
declare @End datetime
select @End = getdate()
print	'Start: %1!', @End

go
