use IMS_MAIN
go
-- ALTER TABLE IRP5Details2001 ADD FixedRateIncome	varchar(1) null
-- ALTER TABLE IRP5Details2001 ADD Directive_Number varchar(50) null
-- ALTER TABLE IRP5Details2001 ADD Selected_Rate varchar(1) null

IF OBJECT_ID('dbo.GenerateIRP5') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.GenerateIRP5
    IF OBJECT_ID('dbo.GenerateIRP5') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.GenerateIRP5 >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.GenerateIRP5 >>>'
END
go
 
CREATE PROCEDURE GenerateIRP5   
(
@StartDate datetime,
@EndDate   datetime,
@Owner     int = NULL
)
AS
--*********************************************************************************************************
--* Name            :   GenerateIRP5Details
--* Created By      :   Unknown
--* Date            :   Unknown
--* Description     :   Generate the IRP5 Details
--*********************************************************************************************************
--*********************************************************************************************************
--* Amendments
--*********************************************************************************************************
--*  Person         :   A.L. Raubenheimer
--*  Date           :   1 June 2002
--*  Description    :   (1) Standardized proc
--*                     (2) Changed reference to ILLA to include VIA - VIA is based on ILLA
--*  IT Issue       :   VIA Conversion Project - See reference point 'VIA Change'
--*********************************************************************************************************
--*  Person		:	Dale Mthembi
--*  Date		:	20 September 2002
--*  Description	:	(1) Change the logic to calculate tax year to accomodate the  		
--*				    'Generate Late Estate' Query tool.
--*				(2) The generation of IRP5 Numbers for IRP5's and IT3A's
--*				    will be mergerd into one sequence per product.
--***********************************************************************************************
--*  Person		:	Mario Andron
--*  Date		:	17 Feb 2003
--*  Description	:	Removed cursor logic
--***********************************************************************************************
--*  Person		:	Mario Andron
--*  Date		:	Mar 25 2003
--*  Description	:	Open ended Section 9(1)(g) clients now treated correctly
--*				Adhoc Certs will not be removed by a batch run
--***********************************************************************************************

BEGIN   
   --*************** Inserted by DBA's ********************
   exec DBA_ProcedureLog 'GenerateIRP5'
   --******************************************************

    -- D.M. Tax year calculation must be flexable across all end dates.
   DECLARE @TaxYear        INT
   SELECT  @TaxYear = CASE
                         WHEN DATEPART(MM,@EndDate) < 3 THEN DATEPART(YY,@EndDate)                

                         ELSE DATEPART(yy,@EndDate) + 1
                      END

   DECLARE @Tenant Varchar(10)
   SELECT  @Tenant = Name FROM Tenant

   --*************************************************************************
   --   Create temp tables
   --*************************************************************************

   CREATE TABLE #IRP5Details
   (
   Owner                          int             null,
   Product_Code                   varchar(5)      null,
   Income                         money           null,
   Non_Taxable_Income             money           null,
   Tax                            money           null,
   Underwriter                    varchar(50)     null,
   Owner_Name                     varchar(50)     null,
   Line_1                         varchar(35)     null,
   Line_2                         varchar(35)     null,
   Line_3                         varchar(35)     null,
   Line_4                         varchar(35)     null,
   Line_5                         varchar(30)     null,
   Postal_Code                    varchar(10)      null,
   Id                             varchar(21)     null,
   PassportNo                     varchar(21)     null,
   TrustNo                        varchar(21)     null,
   Tax_No                         varchar(21)     null,
   Tax_Office                     varchar(30)     null,
   Commencement_Date              datetime        null,
   To_Date                        datetime        null,
   cpy_TradingName                varchar(70)     null,
   cpy_IRP5Number                 int             null,
   cpy_Reference_Number           numeric(10)     null,
   cpy_TaxYear                    numeric(4)      null,
   cpy_Address1                   varchar(35)     null,
   cpy_Address2                   varchar(35)     null,
   cpy_Address3                   varchar(35)     null,
   cpy_Address4                   varchar(35)     null,
   cpy_PostalCode                 numeric(4)      null,
   cpy_Diplomatic                 varchar(1)      null,
   PersonNature                   varchar(1)      null,
   Person_Surname                 varchar(120)    null,
   Person_Init                    varchar(5)      null,
   Person_FullNames               varchar(90)     null,
   Person_Birthdate               varchar(8)      null,
   Person_PayPeriods              varchar(1)      null,
   Person_NoPayPeriods            int             null,
   FixedRateIncome                varchar(1)      null,
   Directive_Number               varchar(50)     null,
   Selected_Rate                  varchar(1)      null,
   Adhoc			  			  smallint	  null
   )

   CREATE TABLE #Deal
   (
   Deal_Xref                      int             null,
   Status                         varchar(1)      null,
   Product_Code                   varchar(5)      null,
   Owner                          int             null,
   Deal_Start_Date                datetime        null,
   Deal_End_Date                  datetime        null,
   Adhoc			  			  smallint	  null,
   )

   --*************************************************************************/
   --   Get Deals to process
   --*************************************************************************/

   IF @Owner IS NULL 
   begin
     
      INSERT INTO #Deal
      SELECT D.Deal_Xref, 
             D.Status,
             PG.Product_Code, 
             CD.Owner,  
             (
             SELECT MIN(DD.Date)
               FROM DealDates DD
              WHERE DD.Deal_Xref = D.Deal_Xref
                AND DD.Date_type = 'DC'                     
             ) Deal_Start_Date,
             (
             SELECT MAX(DS.Date)
               FROM DealStatus DS
              WHERE DS.Deal_Xref = D.Deal_Xref
                AND D.Status IN ('M', 'W', 'X', 'Y')
                AND DS.Status IN ('M', 'W', 'X', 'Y')
             ) Deal_End_Date,
	     Adhoc = 0
        FROM ContractDeal CD, 
             ProductGroup PG, 
             Deal D 
       WHERE CD.Product = PG.Product_Internal
         AND CD.Deal_Xref = D.Deal_Xref
         AND PG.Process IN ('EB', 'EK', 'EI')
         AND EXISTS (
                    SELECT *
                      FROM Transactions T
                     WHERE D.Deal_Xref = T.Deal_Xref
                       AND T.Date_Action BETWEEN @StartDate AND @EndDate
                       AND T.Transaction_Type = 'D'
                       AND T.Instrument_No = 45
                       AND T.Flow_Type IN ('I', '8')
                    )
          
     end
   ELSE
   
      INSERT INTO #Deal
      SELECT D.Deal_Xref, 
             D.Status,
             PG.Product_Code, 
             CD.Owner,  
             (
             SELECT MIN(DD.Date)
               FROM DealDates DD
              WHERE DD.Deal_Xref = D.Deal_Xref
                AND DD.Date_type = 'DC'                     
             ) Deal_Start_Date,
             (
             SELECT MAX(DS.Date)
               FROM DealStatus DS
              WHERE DS.Deal_Xref = D.Deal_Xref
                AND D.Status IN ('M', 'W', 'X', 'Y')
                AND DS.Status IN ('M', 'W', 'X', 'Y')
             ) Deal_End_Date,
             Adhoc = 1
        FROM ContractDeal CD, ProductGroup PG, Deal D 
       WHERE CD.Product = PG.Product_Internal
         AND CD.Deal_Xref = D.Deal_Xref
         AND PG.Process IN ('EB', 'EK', 'EI')
         AND EXISTS (
                     SELECT *
                       FROM Transactions T
                      WHERE D.Deal_Xref = T.Deal_Xref
                        AND T.Date_Action BETWEEN @StartDate AND @EndDate
                        AND T.Transaction_Type = 'D'
                        AND T.Instrument_No = 45
                        AND T.Flow_Type IN ('I', '8')
                    )
         AND CD.Owner = @Owner



	declare @PrevEndDate datetime


	select @PrevEndDate = End_Date
	from TaxYearBeginDates
	where Tax_Year = @TaxYear  - 1      


	create table #DealStatuses (Deal_Xref int, PreviousStatus char null, Status char null, PreviousDate datetime null, Date Datetime null)

	insert #DealStatuses (Deal_Xref)
	select distinct Deal_Xref
	from #Deal

	exec GetDealStatuses @PrevEndDate  , @EndDate

	delete #Deal
	from	#Deal D,
		#DealStatuses DS
	where D.Deal_Xref = DS.Deal_Xref
	and   DS.PreviousStatus = DS.Status
	and   DS.Status in ('X', 'T')


   --*************************************************************************/
   --   Create index on #Deal to speed up processing
   --*************************************************************************/

   create index deal_ndx1 on #Deal(Deal_Xref)

  
   --*************************************************************************
   --   Patch dates:
   --      1. Commencement_Date to the first day of the month
   --      2. To_Date to the last day of the month
   --*************************************************************************

   UPDATE #Deal
   SET  Deal_Start_Date = DATEADD(DD,1 - DATEPART(DD,Deal_Start_Date), Deal_Start_Date)
   WHERE  Deal_Start_Date IS NOT NULL

   UPDATE #Deal
   SET  Deal_End_Date = DATEADD(DD,-1,DATEADD(DD,1 - DATEPART(DD,Deal_End_Date),DATEADD(MM,1,Deal_End_Date)))
   WHERE  Deal_End_Date IS NOT NULL

   
      --*************************************************************************/
   --   Update Products !!!!!!
   --*************************************************************************/

If @TaxYear <= 2011
	BEGIN
			
			Update #Deal
			Set Product_Code = pg.Product_Code
			from #Deal D, WORKSPACE..ILLAVProductChange IPC, ProductGroup pg
			where D.Deal_Xref = IPC.Deal_Xref
            and IPC.SourceProduct = pg.Product_Internal
            and pg.Process IN ('EB', 'EK', 'EI')
			--and   D.Product_Code = IPC.DestProduct
	END

 --*************************************************************************/
   --   Don't re-issue certs that have already been issued
   --*************************************************************************/
	
   DELETE #Deal
     FROM #Deal			D,
	  IRP5Details2001	I
    WHERE D.Owner		= I.Owner
      AND D.Product_Code	= I.Product_Code
      AND I.cpy_TaxYear		= @TaxYear
      AND I.Adhoc		= 1


   --*************************************************************************/
   --   Summarise transactions for the tax year
   --*************************************************************************/

   IF @Owner IS NULL 
   BEGIN

   INSERT #IRP5Details 
          (Owner, Product_Code, Income, Non_Taxable_Income, Tax, Commencement_Date, To_Date, Adhoc,Person_PayPeriods,Person_NoPayPeriods)
   SELECT D.Owner, 
          D.Product_Code,
          SUM(ISNULL(T.Amount,0)), 
          0,
          SUM(ISNULL(T.Tax_Payable,0)), 
          CASE
              WHEN MIN(ISNULL(D.Deal_Start_Date, @StartDate)) < @StartDate THEN @StartDate
              ELSE MIN(ISNULL(D.Deal_Start_Date, @StartDate))
          END,
          CASE
              WHEN MAX(ISNULL(D.Deal_End_Date, @EndDate)) > @EndDate THEN @EndDate
              ELSE MAX(ISNULL(D.Deal_End_Date, @EndDate))
          END,
	  Adhoc,
	  'M',					----- Only in Months
	  count(distinct Date_Action)		----- Pay periods!!!!
     FROM Transactions T (index Transactions_ndx5), 
		#Deal D
    WHERE T.Deal_Xref = D.Deal_Xref
      AND T.Date_Action BETWEEN @StartDate AND @EndDate
      AND T.Transaction_Type = 'D'
      AND T.Instrument_No = 45
      AND T.Flow_Type IN ('I', '8')
   GROUP BY D.Owner, D.Product_Code, Adhoc

   END
   ELSE
  BEGIN
              INSERT #IRP5Details 
                  (Owner, Product_Code, Income, Non_Taxable_Income, Tax, Commencement_Date, To_Date, Adhoc,Person_PayPeriods,Person_NoPayPeriods)
           SELECT D.Owner, 
                  D.Product_Code,
                  SUM(ISNULL(T.Amount,0)), 
                  0,
                  SUM(ISNULL(T.Tax_Payable,0)), 
                  CASE
                      WHEN MIN(ISNULL(D.Deal_Start_Date, @StartDate)) < @StartDate THEN @StartDate
                      ELSE MIN(ISNULL(D.Deal_Start_Date, @StartDate))
                  END,
                  CASE
                      WHEN MAX(ISNULL(D.Deal_End_Date, @EndDate)) > @EndDate THEN @EndDate
                      ELSE MAX(ISNULL(D.Deal_End_Date, @EndDate))
                  END,
              Adhoc,
              'M',					----- Only in Months
              count(distinct Date_Action)		----- Pay periods!!!!
             FROM Transactions T , 
                #Deal D
            WHERE T.Deal_Xref = D.Deal_Xref
              AND T.Date_Action BETWEEN @StartDate AND @EndDate
              AND T.Transaction_Type = 'D'
              AND T.Instrument_No = 45
              AND T.Flow_Type IN ('I', '8')
           GROUP BY D.Owner, D.Product_Code, Adhoc

   END

   --*************************************************************************/
   --   Calculate non taxable income
   --*************************************************************************/

   UPDATE #IRP5Details
      SET Non_Taxable_Income = CONVERT(money, ROUND(#IRP5Details.Income * (100 - OT.Tax) / 100,2))
     FROM OwnerTax OT
    WHERE #IRP5Details.Owner = OT.Owner
      AND OT.Tax_Type in (3) -- = 3 GSP 17/11/2009 Log 49963 Tax Directive. DM TYP2010. Rollback as change was done in error.
      AND OT.Start_Date < @EndDate
      AND ISNULL(OT.End_Date, 'Feb 28 9999') >= @EndDate

   --*************************************************************************/
   --   Zero tax where ABS(Tax) < 1
   --*************************************************************************/

   UPDATE #IRP5Details
      SET Tax = 0
    WHERE ABS(Tax) < 1

   --*************************************************************************/
   --   Lookup Owner details
   --*************************************************************************/
   
   UPDATE #IRP5Details
      SET Owner_Name        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name),
          Person_Surname    = LTRIM(P.Name),
          Person_Init       = LTRIM(P.Initials),
          Person_FullNames  = LTRIM(P.First_Names),
          PersonNature      = 
              CASE 
                 WHEN P.Customer_Type IN ('Y','O','A','B') THEN 'A'
                 WHEN P.Customer_Type IN ('D','E','F')      THEN 'D'
                 WHEN P.Customer_Type IN ('G','H','I','J','K','L','M') THEN 'E'
		 WHEN P.Customer_Type  = 'N' THEN 'F'
                 WHEN P.Customer_Type IN ('P','Q','R','S','T','U','V','W','X') THEN 'H'
              END
     FROM People P
    WHERE #IRP5Details.Owner = P.People_Number

   --*************************************************************************/
   --   Lookup Owner address
   --*************************************************************************/

   UPDATE #IRP5Details
      SET Line_1 = LTRIM(PA.Line_1),
          Line_2 = LTRIM(PA.Line_2),
          Line_3 = LTRIM(PA.Line_3),
          Line_4 = LTRIM(PA.Line_4),
          Line_5 = LTRIM(PA.Line_5),
          Postal_Code = PA.Postal_Code
     FROM PeopleAddress PA
    WHERE #IRP5Details.Owner = PA.People_Number
      AND PA.Address_Type = 'POST'
      --AND ISNULL(PA.Line_1, '') <> '' -- TYEP 2006. There are valid address with either line1 or line2 empty.
      --AND ISNULL(PA.Line_2, '') <> ''

   UPDATE #IRP5Details
      SET Line_1 = LTRIM(PA.Line_1),
          Line_2 = LTRIM(PA.Line_2),
          Line_3 = LTRIM(PA.Line_3),
          Line_4 = LTRIM(PA.Line_4),
          Line_5 = LTRIM(PA.Line_5),
          Postal_Code = PA.Postal_Code
     FROM PeopleAddress PA
    WHERE #IRP5Details.Owner = PA.People_Number
      AND PA.Address_Type = 'PHYS'
	AND NOT EXISTS (SELECT * FROM PeopleAddress PA2 WHERE PA2.People_Number = PA.People_Number AND PA2.Address_Type = 'POST')
     

   --*************************************************************************/
   --   Lookup Owner id no
   --*************************************************************************/

   UPDATE #IRP5Details
      SET Id = PR.Reg_Value
     FROM PeopleRegistration PR
    WHERE #IRP5Details.Owner = PR.People_Number
      AND PR.Reg_Type = 1

   --*************************************************************************/
   --   Lookup Owner passport no
   --*************************************************************************/

   UPDATE #IRP5Details
      SET PassportNo = PR.Reg_Value
     FROM PeopleRegistration PR
    WHERE #IRP5Details.Owner = PR.People_Number
      AND PR.Reg_Type = 2

   --*************************************************************************/
   --   Lookup Owner trust no
   --*************************************************************************/

   UPDATE #IRP5Details
      SET TrustNo = PR.Reg_Value
     FROM PeopleRegistration PR
    WHERE #IRP5Details.Owner = PR.People_Number
      AND PR.Reg_Type = 12

   --*************************************************************************/
   --   Lookup Owner birthdate
   --*************************************************************************/

   UPDATE #IRP5Details
      SET Person_Birthdate = CONVERT(char(8), PD.Date, 112)
     FROM PeopleDates PD
    WHERE #IRP5Details.Owner = PD.People_Number
      AND Date_Type = 'B'    --* VIA Change - Added reference to Birth Date
   
   --*************************************************************************/
   --   Lookup Owner tax details
   --*************************************************************************/

   UPDATE #IRP5Details
      SET Tax_Office         = P.Name,
          Tax_No             = PT.Tax_Number
     FROM PeopleTax PT, People P
    WHERE #IRP5Details.Owner = PT.People_Number
      AND PT.Date_Effective  = (
                               SELECT MAX(Date_Effective)
                                 FROM PeopleTax PT2
                                WHERE PT2.People_Number = #IRP5Details.Owner
                               )
      AND PT.Tax_Office = P.People_Number

   --*************************************************************************/
   --   Update pay-period information
   --*************************************************************************/

   --UPDATE #IRP5Details
   --   SET Person_PayPeriods   = 'M',
   --       Person_NoPayPeriods = DATEDIFF(mm, Commencement_Date, To_Date) + 1

   UPDATE #IRP5Details
   SET Person_NoPayPeriods = 12
   WHERE Person_NoPayPeriods > 12
   
   --*************************************************************************/
   --   Update employer information
   --*************************************************************************/

   IF @Tenant = 'IMS' ----- Investec
   BEGIN
       UPDATE #IRP5Details
          SET cpy_TaxYear = @TaxYear,
              cpy_Reference_Number = 
                 CASE Product_Code
                   WHEN 'ILLA'  THEN 7230719157
                   WHEN 'VIA'   THEN 7230719157   --* VIA CHANGE - SAME AS FOR ILLA
                   WHEN 'FULA'  THEN 7330732613 ----TMA
                   WHEN 'IRAF2' THEN 7340732611 ----TMA
                   WHEN 'ILLAS' THEN 7490722032
                   WHEN 'ILRA2' THEN 7160721652
                   WHEN 'ILLAA' THEN 7490738442
                   WHEN 'ILLAV' THEN 7490738442 
                 END,
              cpy_TradingName = 'Investec IMS',
              cpy_Address1 = 
                 CASE Product_Code
                   WHEN 'ILLA'  THEN 'Fedsure ILLA Division'
                   WHEN 'VIA'   THEN 'Fedsure ILLA Division'  --* VIA CHANGE - SAME AS FOR ILLA
                   WHEN 'ILLAS' THEN 'Sanlam Investec ILLA Division'
                   WHEN 'FULA'  THEN 'Fedlife Universal Life Annuity Fund' ----TMA
                   WHEN 'IRAF2' THEN 'Independent Retirement Annuity Fund' ----TMA
                   WHEN 'ILRA2' THEN 'RCI Retirement Annuity'
                   WHEN 'ILLAA' THEN 'Investec ILLA Division'
               WHEN 'ILLAV' THEN 'Investec ILLA  Division'
                 END,
              cpy_Address2 = '100 Grayston Drive',
              cpy_Address3 = 'Sandown',
              cpy_Address4 = 'Sandton',
              cpy_PostalCode = 2146
   END

   IF @Tenant = 'RMB' ----- Ashburton
   BEGIN
   /*
       UPDATE #IRP5Details
          SET cpy_TaxYear = @TaxYear,
              cpy_Reference_Number = 
                 CASE Product_Code
                   WHEN 'LA'  THEN 7020781045
                 END,
              cpy_TradingName = 'Ashburton .....',
              cpy_Address1 = 
                 CASE Product_Code
                   WHEN 'LA'  THEN 'XXXXXXXXXXXXXXXXXXXX'
                 END,
              cpy_Address2 = 'XXXXXXXXXXXXXXXXXXXX',
              cpy_Address3 = 'XXXXXXXXXXXXXXXXXXXX',
              cpy_Address4 = 'XXXXXXXXXXXXXXXXXXXX',
              cpy_PostalCode = 9999
   */
        UPDATE      #IRP5Details
        SET         cpy_TaxYear = @TaxYear,
                    cpy_Reference_Number    = Convert(Numeric,PT.Tax_Number),
                    cpy_TradingName         = PP.Name,
                    cpy_Address1            = ISNULL(LTRIM(RTRIM(PA.Line_1)),''),
                    cpy_Address2            = ISNULL(LTRIM(RTRIM(PA.Line_2)),''),
                    cpy_Address3            = ISNULL(LTRIM(RTRIM(PA.Line_3)),''),
                    cpy_Address4            = ISNULL(LTRIM(RTRIM(PA.Line_4)),'') + ISNULL(LTRIM(RTRIM(PA.Line_5)),''),
                    cpy_PostalCode          = Convert(Numeric,PA.Postal_Code)
        FROM        #IRP5Details    IR
        INNER JOIN  Product PD
        ON          IR.Product_Code = PD.Product_Code
        INNER JOIN  People PP
        ON          PD.Product_Owner = PP.People_Number
        AND         PP.Language = 'E'
        INNER JOIN  PeopleTax PT
        ON          PP.People_Number = PT.People_Number
        INNER JOIN  PeopleAddress PA
        ON          PP.People_Number = PA.People_Number
        AND         PA.Address_Type = 'POST'
        WHERE       IR.cpy_TaxYear IS NULL
   END

   --*************************************************************************/
   --   Determine if a Deal_Xref has a tax directive
   --*************************************************************************/
      UPDATE #IRP5Details
         SET Selected_Rate = 'Y'
        FROM #IRP5Details i,
             OwnerTax p
       WHERE p.Owner      = i.Owner
         AND p.Start_Date BETWEEN @StartDate and @EndDate
         AND p.Tax_Type   = 1 -- Selected Tax Rate

      UPDATE #IRP5Details
         SET Selected_Rate = 'N'
        FROM #IRP5Details i
       WHERE Selected_Rate <> 'Y' OR Selected_Rate IS NULL

   --*************************************************************************/
   --   Determine if a Deal_Xref has a tax directive
   --*************************************************************************/
      UPDATE #IRP5Details
         SET FixedRateIncome = 'Y', Directive_Number = p.Reference
        FROM #IRP5Details i,
             OwnerTax p
       WHERE p.Owner      = i.Owner
         AND p.Start_Date BETWEEN @StartDate and @EndDate
         AND p.Tax_Type   = 2 -- Tax Directive

      UPDATE #IRP5Details
         SET FixedRateIncome = 'N'
        FROM #IRP5Details i
       WHERE FixedRateIncome <> 'Y' OR FixedRateIncome IS NULL

   --*************************************************************************/
   --   Generate Certificate Nos
   --*************************************************************************/

      --*************************************************************************/
      --   Get existing certificate Nos
      --*************************************************************************/

      UPDATE #IRP5Details
         SET cpy_IRP5Number = ID.cpy_IRP5Number
        FROM IRP5Details2001 ID
       WHERE #IRP5Details.Owner 	= ID.Owner
         AND #IRP5Details.Product_Code 	= ID.Product_Code
         AND #IRP5Details.cpy_TaxYear 	= ID.cpy_TaxYear

      --*************************************************************************/
      --   Generate new certificate nos
      --*************************************************************************/

        
	SELECT	ID_Cert_Id	= Identity(9),Cert_Id	= Convert(Numeric(9,0),NULL),
		*
	INTO	#IRP5DetailsID
	FROM	#IRP5Details
	ORDER BY
		Product_Code,	-- *****************************************
		cpy_IRP5Number	-- Important for the correct number sequence


      	SELECT	Product_Code,
		cpy_TaxYear,
		min(ID_Cert_Id) Start_Id
	INTO	#CertNum
	FROM	#IRP5DetailsID
	GROUP BY
		Product_Code,
		cpy_TaxYear

	-- ***** DM TYEP 2009.********************************************************************* 
	-- ***** Certificate numbers will now be sequenced with repurchase tax certificate numbers.	
	-- ****************************************************************************************
	
        SELECT  Product_Code , numRecs = count(*)
	INTO #Prods 
	FROM #IRP5Details 
	GROUP BY Product_Code

	DECLARE @ProductCode varchar(8),
		@CertificateNumber int,
		@numRecs int

	--SET IDENTITY_UPDATE #IRP5DetailsID ON

	WHILE EXISTS (SELECT 1 FROM #Prods)
	BEGIN
		set rowcount 1
		select @ProductCode = Product_Code, @numRecs = numRecs from #Prods
		set rowcount 0	 

		exec UpdateTaxCertificates @numRecs, @ProductCode , @TaxYear , @CertificateNumber output 
		
		select '@CertificateNumber = ', @CertificateNumber, ' @numRecs = ', @numRecs

		update #IRP5DetailsID
		set Cert_Id = ID_Cert_Id + ISNULL(@CertificateNumber,0)
		where Product_Code = @ProductCode 

		delete #Prods
		where Product_Code = @ProductCode
	END

	--SET IDENTITY_UPDATE #IRP5DetailsID OFF

	/****************************************************************************/
	-- Start issuing numbers after those allocated during the year
	/****************************************************************************/

	-- ***** DM TYEP 2009. Certificate numbers are already updated by the highest generated in TaxCertificates ***.

	--UPDATE	#CertNum
	--SET	Start_Id = Start_Id - isnull((	SELECT 	Max(cpy_IRP5Number)
	--				FROM	IRP5Details2001 I
	--				WHERE	C.Product_Code 	= I.Product_Code
	--				AND	C.cpy_TaxYear  	= I.cpy_TaxYear),0)
	--FROM	#CertNum	C

	/****************************************************************************/
	-- Generate numbers ...
	/****************************************************************************/
	-- For subquery in following statement
	CREATE CLUSTERED INDEX ID_Perf1 on #CertNum(Product_Code)

	--UPDATE	#IRP5DetailsID
	--SET	cpy_IRP5Number		= Cert_Id - (SELECT		Start_Id
	--				   FROM	#CertNum 	C
	--				   WHERE C.Product_Code	= I.Product_Code
	--				   AND	C.cpy_TaxYear	= I.cpy_TaxYear) + 1
	--FROM	#IRP5DetailsID I
	--WHERE	cpy_IRP5Number	is null -- Leave existing numbers

	UPDATE	#IRP5DetailsID
	SET	cpy_IRP5Number		= Cert_Id - (SELECT		Start_Id
					   FROM	#CertNum 	C
					   WHERE C.Product_Code	= I.Product_Code
					   AND	C.cpy_TaxYear	= I.cpy_TaxYear) + 1
	FROM	#IRP5DetailsID I
	WHERE	cpy_IRP5Number	is null -- Leave existing numbers

   --*************************************************************************/
   --   Commit changes to database
   --*************************************************************************/

   BEGIN TRAN

      DELETE IRP5Details2001
        FROM #IRP5DetailsID T
       WHERE IRP5Details2001.Owner 		= T.Owner
         AND IRP5Details2001.Product_Code 	= T.Product_Code
         AND IRP5Details2001.cpy_TaxYear 	= T.cpy_TaxYear
         AND IRP5Details2001.Adhoc	 	= 0 -- Don't delete adhoc certs!!!

      INSERT IRP5Details2001 
             (
             Owner,
             Product_Code,
             Income,
             Non_Taxable_Income,
             Tax,
             Underwriter,
             Owner_Name,
             Line_1,
             Line_2,
             Line_3,
             Line_4,
             Line_5,
             Postal_Code,
             Id,
             PassportNo,
             TrustNo,
             Tax_No,
             Tax_Office,
             Commencement_Date,
             To_Date,
             cpy_TradingName,
             cpy_IRP5Number,
             cpy_Reference_Number,
             cpy_TaxYear,
             cpy_Address1,
             cpy_Address2,
             cpy_Address3,
             cpy_Address4,
             cpy_PostalCode,
             cpy_Diplomatic,
             PersonNature,
             Person_Surname,
             Person_Init,
             Person_FullNames,
             Person_Birthdate,
             Person_PayPeriods,
             Person_NoPayPeriods,
             FixedRateIncome,
             Directive_Number,
             Selected_Rate,
	     Adhoc)
      SELECT Owner,
             Product_Code,
             Income,
             Non_Taxable_Income,
             Tax,
             Underwriter,
             Owner_Name,
             Line_1,
             Line_2,
             Line_3,
             Line_4,
             Line_5,
             Postal_Code,
             Id,
             PassportNo,
             TrustNo,
             Tax_No,
             Tax_Office,
             Commencement_Date,
             To_Date,
             cpy_TradingName,
             cpy_IRP5Number,
             cpy_Reference_Number,
             cpy_TaxYear,
             cpy_Address1,
             cpy_Address2,
             cpy_Address3,
             cpy_Address4,
             cpy_PostalCode,
             cpy_Diplomatic,
             PersonNature,
             Person_Surname,
             Person_Init,
             Person_FullNames,
             Person_Birthdate,
             Person_PayPeriods,
             Person_NoPayPeriods,
             FixedRateIncome,
             Directive_Number,
             Selected_Rate,
	     Adhoc
        FROM #IRP5DetailsID

   COMMIT TRAN
      
   --*************************************************************************/
   --   Clean-up
   --*************************************************************************/

   DROP TABLE #IRP5Details
   DROP TABLE #Deal
END
GO
GRANT EXEC ON GenerateIRP5 TO testers
GO
IF OBJECT_ID('dbo.GenerateIRP5') IS NOT NULL
    PRINT '<<< CREATED PROCEDURE dbo.GenerateIRP5 >>>'
ELSE
    PRINT '<<< FAILED CREATING PROCEDURE dbo.GenerateIRP5 >>>'
go



