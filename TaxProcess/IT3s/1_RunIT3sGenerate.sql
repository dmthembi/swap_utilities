IF OBJECT_ID('WORKSPACE..IT3SFinancialData') IS NOT NULL
DROP TABLE WORKSPACE..IT3SFinancialData
IF OBJECT_ID('WORKSPACE..IT3STransactionData') IS NOT NULL
DROP TABLE WORKSPACE..IT3STransactionData
IF OBJECT_ID('#IT3STransactionData') IS NOT NULL
DROP TABLE #IT3STransactionData
GO





DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates

SELECT Certificate_Key into #del
from IT3SCertificateHeader
where Tax_Year = @Tax_Year


delete IT3SCertificateDetail
where Certificate_Key in (select Certificate_Key from #del)

delete IT3SCertificateHeader
where Certificate_Key in (select Certificate_Key from #del)



exec IT3SGenerate @Start_Date , @End_Date    


select h.Owner as IT3_PERS_ID,
            h.Contract_Number, 
            d.Contribution,
            Opening_Balance = convert(money,0.00),
            h.Start_Date,
            Closing_Balance =  convert(money,0.00),
            h.End_Date,
            NROI_Source_Code = convert(char(4),'4239'),
            NROI = d.Interest + d.Dividends + d.Gain,
            Interest_Source_Code =  convert(char(4),'4241'),
            d.Interest,
            Dividend_Source_code =  convert(char(4),'4242'),
            d.Dividends,
            Gain_Source_Code =  convert(char(4),'4243'),
            d.Gain,
            d.Market_Value
    into WORKSPACE..IT3SFinancialData
    from IT3SCertificateHeader h inner join IT3SCertificateDetail d
    on h.Certificate_Key = d.Certificate_Key
	WHERE h.Tax_Year = @Tax_Year 
    order by h.Owner
    
    select h.Owner as IT3_PERS_ID,
            h.Contract_Number, 
            Transaction_Value = d.Contribution,
            Transaction_Type = convert(char(2),'01'),
            Assigned_Source_Code = convert(varchar(4),'4219')
          
    into #IT3STransactionData
    from IT3SCertificateHeader h inner join IT3SCertificateDetail d
    on h.Certificate_Key = d.Certificate_Key
	WHERE h.Tax_Year = @Tax_Year 
    order by h.Owner

    insert into #IT3STransactionData
    select h.Owner as IT3_PERS_ID,
            h.Contract_Number, 
            Transaction_Value = d.Withdrawals,
            Transaction_Type = convert(char(2),'04'),
            Assigned_Source_Code = ''
    from IT3SCertificateHeader h inner join IT3SCertificateDetail d
    on h.Certificate_Key = d.Certificate_Key
    WHERE d.Withdrawals > 0
	and  h.Tax_Year = @Tax_Year 
    order by h.Owner


    select *
    into WORKSPACE..IT3STransactionData
    from #IT3STransactionData
    order by IT3_PERS_ID


update  WORKSPACE..IT3SFinancialData
set NROI_Source_Code = '4240'
where NROI < 0

update  WORKSPACE..IT3SFinancialData
set Interest_Source_Code = ''
where Interest is null

update  WORKSPACE..IT3SFinancialData
set Dividend_Source_code = ''
where Dividends is null

update  WORKSPACE..IT3SFinancialData
set Gain_Source_Code = '4244'
where Gain < 0

GO