/************************************************************************
* Name of Proc           : IT3B_Get_Income
* Date Written           : 2:14 PM 05/09/2000
* Author                 : Phillip Strauss
* Link                   : IT3B_All
* Called By              : Executed after the IT3B_Get_People	 
* Purpose / Description  : To get the Detail information for certain clients
*                          going by Contract_Number
* 
* 
* Input Parameters       :  @Start_Date' datetime,   --  start of period(same as previous proc)
*                           @End_Date   datetime    --  end of period(same as previous proc)
* Output Parameters      : [output param with comment]   
* Tables                 : 
* Return Status          : 
* Error Codes            : 
* 
* ----------------------------------------------------------------------
* Modifications          : [state what changed and update description]
* Programmer             : Mario Andron
* Date                   : Feb 17 2003
* Changes                : Froeign Interest
* ______________________________________________________________________
**************************************************************************/

--**	********************************************************************
--**	CONFIRM THE DROPPING OF THE PROC ***********************************
--**	********************************************************************
        IF OBJECT_ID('IT3B_Get_Income') IS NOT NULL
        BEGIN
             DROP proc IT3B_Get_Income
             PRINT '<<< DROPPED PROCEDURE IT3B_Get_Income >>>'
        END
        ELSE
             PRINT '<<< PROCEDURE IT3B_Get_Income DOES NOT EXIST >>>'
GO

--**	********************************************************************
--**	CONFIRM THE DROPPING OF ANY HASH TABLES ****************************
--**	********************************************************************
        IF OBJECT_ID('#TCHD') IS NOT NULL
        BEGIN
             DROP TABLE #TCHD
             PRINT '<<< DROPPED HASH TABLE #TCHD >>>'
        END
GO

 IF OBJECT_ID('WORKSPACE..IT3B_Get_Income') IS NOT NULL
        BEGIN
            DROP TABLE WORKSPACE..IT3B_Get_Income
 PRINT '<<< DROPPED HASH TABLE #TCHD >>>'
 END
go



CREATE TABLE WORKSPACE..IT3B_Get_Income
(	Certificate_Key         numeric(9,0),
    Certificate_Number      int,
    Owner               	INT             NULL,
    Product                 varchar(6)      NULL,
	Contract_Number         int             NULL, 
	Instrument_Class    	VARCHAR(5)      NULL,
	Income_Source_Code	    char(4)         NULL,
    Value			        FLOAT           NULL,
	Branch_Code         	INT             NULL,
	Start_Date          	DATETIME        NULL,
	Start_Value         	NUMERIC(18,2)   NULL,
	End_Date            	DATETIME        NULL,
	End_Value           	NUMERIC(18,2)   NULL,
	WithHoldingTax		    MONEY           NULL,
	Loc_WH_Interest_TBT	    MONEY           NULL,
	LWHI_WHT_Tax		    MONEY           NULL,
    March_Credits           NUMERIC(18,2)   NULL,
    Apr_Credits             NUMERIC(18,2)   NULL,
    May_Credits             NUMERIC(18,2)   NULL,
    June_Credits            NUMERIC(18,2)   NULL,
    Jul_Credits             NUMERIC(18,2)   NULL,
    Aug_Credits             NUMERIC(18,2)   NULL,
    Sep_Credits             NUMERIC(18,2)   NULL,
    Oct_Credits             NUMERIC(18,2)   NULL,
    Nov_Credits             NUMERIC(18,2)   NULL,
    Dec_Credits             NUMERIC(18,2)   NULL,
    Jan_Credits             NUMERIC(18,2)   NULL,
    Feb_Credits             NUMERIC(18,2)   NULL,
    March_Debits            NUMERIC(18,2)   NULL,
    Apr_Debits              NUMERIC(18,2)   NULL,
    May_Debits              NUMERIC(18,2)   NULL,
    June_Debits             NUMERIC(18,2)   NULL,
    Jul_Debits              NUMERIC(18,2)   NULL,
    Aug_Debits              NUMERIC(18,2)   NULL,
    Sep_Debits              NUMERIC(18,2)   NULL,
    Oct_Debits              NUMERIC(18,2)   NULL,
    Nov_Debits              NUMERIC(18,2)   NULL,
    Dec_Debits              NUMERIC(18,2)   NULL,
    Jan_Debits              NUMERIC(18,2)   NULL,
    Feb_Debits              NUMERIC(18,2)   NULL,
    SA_Resident             Int             Null
)
go
create index it3b_idx1 on WORKSPACE..IT3B_Get_Income(Certificate_Key)
create index it3b_idx2 on WORKSPACE..IT3B_Get_Income(Owner)
create index it3b_idx3 on WORKSPACE..IT3B_Get_Income(Contract_Number)
create index it3b_idx4 on WORKSPACE..IT3B_Get_Income(Instrument_Class)
go



--**  =================================================================================
--**  ==  :::BEFORE THIS PROC GETS EXECUTED:::                                       ==
--**  ==  make sure that at this time the IT3B_Get_People has run inorder to get the ==
--**  ==  the header files for all the detail you want to create.                    ==
--**  ==  Secondly you must create copy February's DealMonthEndBalances over to the  ==
--**  ==  WORKSPACE database and call it (WORKSPACE..IT3B_DealMonthEndBalances)      ==
--**  =================================================================================

---- IT3B_Get_Income    '1 March 2012', '28 Feb 2013', 2013
--**	------------------
--**	CREATE STATEMENT -
--**	------------------
CREATE PROCEDURE IT3B_Get_Income    @Start_Date datetime,
                                    @End_Date   datetime,
                                    @Tax_Year int

AS
BEGIN

--**	For DBA's to monitor Procs 
--**	---------------------------------------------------
	exec DBA_ProcedureLog 'IT3B_Get_Income'
--**	---------------------------------------------------

--**    test execute statement
--**    exec IT3B_Get_Income 'Mar 01 1999', 'Feb 29 2000'

--Income Source Code	Description
--4201 			Interest/Dividends from a property
--4202 			Dividends/Dividends from a unit trust dividends from a foreign source
--4210 			Rental income
--4212 			Royalties income
--4216 			Dividends from a foreign source
--4218 			Interest from a foreign source


--declare @Tax_Year int
--select @Tax_Year = 2013

--declare @Start_Date datetime, @End_Date   datetime
--select  @Start_Date ='1 March 2012', @End_Date='28 Feb 2013'
      
select  distinct 
	TCH.Certificate_Key,
	TCH.Certificate_Number,
        TCH.Owner,
        TCH.Product,
        TCH.Contract_Number,
        Year,
        TCH.Start_Date,
        TCH.End_Date,
        Branch_Code = 0,
        Instrument_Class,
        Income_Source_Code = '4201',
        --Value =Interest,
        Value = ISNULL(Interest,0) + ISNULL(TCD.Loc_WH_Interest_TBT,0),
        WithHoldingTax=convert(money,null),
		Loc_WH_Interest_TBT = TCD.Loc_WH_Interest_TBT,
		LWHI_WHT_Tax = TCD.LWHI_WHT_Tax
into    #TCHD 
from    TaxCertificateHeader TCH,
        TaxCertificateDetail TCD
where   TCH.Certificate_Key = TCD.Certificate_Key
--and     (TCH.Start_Date between @Start_Date and @End_Date or TCH.End_Date between @Start_Date and @End_Date)
and     TCH.Year = @Tax_Year
--and     TCH.Owner = 134719
and     Interest > 0
and     Instrument_Class <> 'FS'
select @@rowcount,'4201 Source Codes '


insert  into #TCHD 
select  distinct 
        TCH.Certificate_Key,
        TCH.Certificate_Number,
        TCH.Owner,
        TCH.Product,
        TCH.Contract_Number,
        Year,
        TCH.Start_Date,
        TCH.End_Date,
        Branch_Code = 0,
        Instrument_Class,
        '4202',
        Value =NonTaxDividends,
        null,
		null,
		null
from    TaxCertificateHeader TCH,
        TaxCertificateDetail TCD  
where   TCH.Certificate_Key = TCD.Certificate_Key
--and     (TCH.Start_Date between @Start_Date and @End_Date or TCH.End_Date between @Start_Date and @End_Date)
and     TCH.Year = @Tax_Year
and     NonTaxDividends > 0


insert  into #TCHD 
select  distinct 
	TCH.Certificate_Key,
	TCH.Certificate_Number,
        TCH.Owner,
        TCH.Product,
        TCH.Contract_Number,
        Year,
        TCH.Start_Date,
        TCH.End_Date,
        Branch_Code = 0,
        Instrument_Class,
	'4216',
	Value =TaxForDividends,
	WithholdingTax,
		null,
		null
from    TaxCertificateHeader TCH,
        TaxCertificateDetail TCD    
where   TCH.Certificate_Key = TCD.Certificate_Key
--and     (TCH.Start_Date between @Start_Date and @End_Date or TCH.End_Date between @Start_Date and @End_Date)
and     TCH.Year = @Tax_Year
and     TaxForDividends > 0


insert  into #TCHD 
select  distinct 
        TCH.Certificate_Key,
        TCH.Certificate_Number,
        TCH.Owner,
        TCH.Product,
        TCH.Contract_Number,
        Year,
        TCH.Start_Date,
        TCH.End_Date,
        Branch_Code = 0,
        Instrument_Class,
        '4218',
        Value =Taxable_For_Int,
        Withholding_Tax_For_Int,
		null,
		null
from    TaxCertificateHeader TCH,
        TaxCertificateDetail TCD   
where   TCH.Certificate_Key = TCD.Certificate_Key
--and     (TCH.Start_Date between @Start_Date and @End_Date or TCH.End_Date between @Start_Date and @End_Date)
and     TCH.Year = @Tax_Year
and     Taxable_For_Int > 0

----- REIT Section (Start)
/*
IF OBJECT_ID('#REIT_Instruments') IS NOT NULL DROP TABLE #REIT_Instruments

SELECT      DISTINCT Txn.Deal_Xref, Txn.Instrument_No, IX.Instrument_Number
INTO        #REIT_Instruments
FROM        TaxCertificateHeader TCH
INNER JOIN  TaxCertificateDealXrefs TCD
ON          TCH.Certificate_Key = TCD.Certificate_Key
INNER JOIN  Transactions Txn
ON          TCD.Deal_Xref = Txn.Deal_Xref
AND         Txn.Transact_Type = 359
AND         Txn.Flow_Type in ('Z')
INNER JOIN  DistributionInfoHeader DIH
ON          Txn.Deal_Xref = DIH.Deal_Xref
AND         DIH.Dist_Date BETWEEN @Start_Date AND @End_Date
INNER JOIN  Deal D
ON          D.Deal_Xref = Txn.Deal_Xref
AND         D.Status      NOT IN ('Q', 'A', 'C', 'Z')
INNER JOIN  ContractDeal CD
ON          D.Deal_Xref = CD.Deal_Xref
--AND         CD.Owner = @WHTOwner
--AND         CD.Contract_Number = @WHTContract_Number
INNER JOIN  ProductGroup PG
ON          CD.Product = PG.Product_Internal
AND         PG.Process = 'AX'
--AND         PG.Product_Code = @WHTProduct
INNER JOIN  InstrumentXref IX
ON          Txn.Instrument_No = IX.Referenced_Instrument
AND         IX.Reference_Type = 2   -- Distribution Tax
WHERE       TCH.Year = @Tax_Year
AND         Txn.Instrument_No Not in (10998,20998)

IF OBJECT_ID('#REIT_Values') IS NOT NULL DROP TABLE #REIT_Values

SELECT      CD.Owner, 
            Citizen = 'L'
INTO        #REIT_Values
FROM        DistributionInfoHeader DH
INNER JOIN  #REIT_Instruments I
ON          DH.Instrument_Number = I.Instrument_Number
AND         DH.Deal_Xref = I.Deal_Xref
INNER JOIN  InstrumentDistributionHistor IDH
ON          DH.Instrument_Number = IDH.Instrument_Number
AND         DH.Dist_Date = IDH.Dist_Date
--AND         IDH.Reinvest_Date BETWEEN @Start_Date AND @End_Date
AND         IDH.Dist_Date BETWEEN @Start_Date AND @End_Date
INNER JOIN  DistributionInformation DI
ON          DH.Dist_Info_No = DI.Dist_Info_No
INNER JOIN  ContractDeal CD
ON          DH.Deal_Xref = CD.Deal_Xref
--AND         CD.Owner = @WHTOwner
--AND         CD.Contract_Number = @WHTContract_Number
INNER JOIN  ProductGroup PG
ON          CD.Product = PG.Product_Internal
AND         PG.Process = 'AX'
--AND         PG.Product_Code = @WHTProduct
INNER JOIN  Deal D
ON          I.Deal_Xref = D.Deal_Xref
AND         D.Status      NOT IN ('Q', 'A', 'C', 'Z')
GROUP BY    CD.Owner
HAVING      SUM(DI.REIT_WHT_Value) = 0
*/

insert  into #TCHD 
select  distinct 
        TCH.Certificate_Key,
        TCH.Certificate_Number,
        TCH.Owner,
        TCH.Product,
        TCH.Contract_Number,
        Year,
        TCH.Start_Date,
        TCH.End_Date,
        Branch_Code = 0,
        Instrument_Class,
        '4238',
        Value =Loc_REIT_TBT,
        Withholding_Tax_For_Int,
		null,
		null
from    TaxCertificateHeader TCH,
        TaxCertificateDetail TCD   
where   TCH.Certificate_Key = TCD.Certificate_Key
and     TCH.Year = @Tax_Year
AND     Loc_REIT_TBT > 0
--AND     REIT_WHT_Tax = 0

----- REIT Section (End)
SELECT distinct 
       CD.Deal_Xref
       INTO #Deals
       FROM   ContractDeal CD,
       ProductGroup PG,
       Deal D,
       TaxCertificateHeader C
WHERE  CD.Product    = PG.Product_Internal
AND    PG.Process    = 'AX'  
AND    CD.Deal_Xref  = D.Deal_Xref
AND    D.Status      NOT IN ('Q', 'A', 'C', 'Z')
AND    C.Owner 	= CD.Owner
AND    C.Product 	= PG.Product_Code
AND	C.Year		= @Tax_Year

create index D_IDX on #Deals(Deal_Xref)

---------add contractNumer 
select distinct Owner, Certificate_Key ,Instrument_Class ,Start_Date, End_Date
into #Sums
from #TCHD 


create index CertKey_Idx on #Sums(Owner, Certificate_Key)


set forceplan on

select  TCHD.Certificate_Key,
        TCHD.Owner,
        Instrument_Class,
        Balance_Type,
        Start_Value = sum(PP.Value)
into    #MM_StartSum 
from    #Sums 			TCHD,
        TaxCertificateDealXrefs TCDX,
        DealInstrumentPosition PP (index DealInstrumentPosition_PK),
        Instrument I,
        #Deals D
where   TCHD.Certificate_Key = TCDX.Certificate_Key
and     TCDX.Deal_Xref = PP.Deal_Xref
and     PP.Instrument_Number = I.Instrument_Number
and     Balance_Type = 2
and     Instrument_Class = 'MM'
and     I.Instrument_Type in ('CALL','FIX','GCAL','NOT','FEED')
and     Refreshed = TCHD.Start_Date
AND     D.Deal_Xref  =  TCDX.Deal_Xref
group by TCHD.Certificate_Key,
         TCHD.Owner,
         Instrument_Class,
         Balance_Type

select  TCHD.Certificate_Key,
        TCHD.Owner,
        Instrument_Class = 'UT',
        Balance_Type,
        Start_Value = sum(PP.Value)
into    #UT_StartSum
from    #Sums TCHD,
        TaxCertificateDealXrefs TCDX,
        DealInstrumentPosition PP (index DealInstrumentPosition_PK),
        Instrument I,
        #Deals D
where   TCHD.Certificate_Key = TCDX.Certificate_Key
and     TCDX.Deal_Xref = PP.Deal_Xref
and     PP.Instrument_Number = I.Instrument_Number
and     Balance_Type = 2
and     Instrument_Class in ('UT') --,'FS')
and     I.Instrument_Type not in ('CALL','FIX','GCAL','NOT','FEED')
and     Refreshed = TCHD.Start_Date
AND     D.Deal_Xref  =  TCDX.Deal_Xref
group by TCHD.Certificate_Key,
         TCHD.Owner,
         Instrument_Class,
         Balance_Type

select  TCHD.Certificate_Key,
        TCHD.Owner,
        Instrument_Class,
        Balance_Type,
        End_Value = sum(DMEB.Value)
into    #MM_EndSum
from    #Sums TCHD,
        TaxCertificateDealXrefs TCDX,
        DealInstrumentPosition DMEB (index DealInstrumentPosition_PK),
        Instrument I,
        #Deals D
where   TCHD.Certificate_Key = TCDX.Certificate_Key
and     TCDX.Deal_Xref = DMEB.Deal_Xref
and     DMEB.Instrument_Number = I.Instrument_Number
and     Balance_Type = 2
and     Instrument_Class = 'MM'
and     I.Instrument_Type in ('CALL','FIX','GCAL','NOT','FEED')
and     Refreshed = TCHD.End_Date
AND     D.Deal_Xref  =  TCDX.Deal_Xref
group by TCHD.Certificate_Key,
         TCHD.Owner,
         Instrument_Class,
         Balance_Type


select TCHD.Certificate_Key,
        TCHD.Owner,
        Instrument_Class='UT',
        Balance_Type,
        End_Value = sum(DMEB.Value)
into    #UT_EndSum 
from    #Sums TCHD,
        TaxCertificateDealXrefs TCDX,
        DealInstrumentPosition DMEB (index DealInstrumentPosition_PK),
        Instrument I,
	    #Deals D
where   TCHD.Certificate_Key = TCDX.Certificate_Key
and     TCDX.Deal_Xref = DMEB.Deal_Xref
and     DMEB.Instrument_Number = I.Instrument_Number
and     Balance_Type = 2
and     Instrument_Class in ('UT') --,'FS')
and     I.Instrument_Type not in ('CALL','FIX','GCAL','NOT','FEED')
and     Refreshed = TCHD.End_Date
AND     D.Deal_Xref  =  TCDX.Deal_Xref
group by TCHD.Certificate_Key,
         TCHD.Owner,
         Instrument_Class,
         Balance_Type



set forceplan off
/******************************************/

create index ndx1 on #MM_StartSum (Certificate_Key, Instrument_Class)
create index ndx1 on #MM_EndSum (Certificate_Key, Instrument_Class)

create index ndx1 on #UT_StartSum (Certificate_Key, Instrument_Class)
create index ndx1 on #UT_EndSum (Certificate_Key, Instrument_Class)


select  distinct Certificate_Key,
        Owner,
        Instrument_Class,
        Balance_Type = convert(smallint,null),
        Start_Value = convert(money,null),
        End_Value = convert(money,null)
into #MM
from #TCHD
where Instrument_Class = 'MM'


select  distinct Certificate_Key,
        Owner,
        Instrument_Class='UT',
        Balance_Type = convert(smallint,null),
        Start_Value = convert(money,null),
        End_Value = convert(money,null)
into #UT
from #TCHD
where Instrument_Class <> 'MM'



update #MM
set Balance_Type = SS.Balance_Type,
	Start_Value = SS.Start_Value
from #MM MM, #MM_StartSum SS
where MM.Certificate_Key = SS.Certificate_Key

update #MM
set Balance_Type = ES.Balance_Type,
	End_Value = ES.End_Value
from #MM MM, #MM_EndSum ES
where MM.Certificate_Key = ES.Certificate_Key


update #UT
set Balance_Type = SS.Balance_Type,
	Start_Value = SS.Start_Value
from #UT UT, #UT_StartSum SS
where UT.Certificate_Key = SS.Certificate_Key

update #UT
set Balance_Type = ES.Balance_Type,
	End_Value = ES.End_Value
from #UT UT, #UT_EndSum ES
where UT.Certificate_Key = ES.Certificate_Key


select  Certificate_Key,
        Owner,
        Balance_Type,
        Instrument_Class,
        Start_Value,
        End_Value    
into    #Total
from    #UT


insert  #Total
select  Certificate_Key,
        Owner,
        Balance_Type,
        Instrument_Class,
        Start_Value,
        End_Value    
from    #MM 

select      TCHD.Certificate_Key,
            TCHD.Certificate_Number ,
            TCHD.Owner ,
            TCHD.Product  ,
            TCHD.Contract_Number  ,
            Instrument_Class = case  when TCHD.Instrument_Class != 'MM' then 'UT' else 'MM' end,
            Income_Source_Code,
            Value = sum(TCHD.Value),
            Branch_Code,
            Start_Date = convert(char(12), Start_Date),
            Start_Value  = convert(money, null),
            End_Date   = convert(char(12), End_Date),
            End_Value = convert(money, null),
            WithHoldingTax = sum(TCHD.WithHoldingTax),
			Loc_WH_Interest_TBT = sum(Loc_WH_Interest_TBT),
			LWHI_WHT_Tax = sum(LWHI_WHT_Tax)
into        #TotalCert 
from        #TCHD TCHD
group by    TCHD.Certificate_Key,
            TCHD.Certificate_Number ,
            TCHD.Owner,
            TCHD.Product  ,
            TCHD.Contract_Number  ,
            case  when TCHD.Instrument_Class != 'MM' then 'UT' else 'MM' end , --TCHD.Instrument_Class,
	        Income_Source_Code,
	        Branch_Code,
	        convert(char(12), Start_Date),
	        convert(char(12), End_Date)

update #TotalCert 
set  Start_Value  = t.Start_Value,	 
	 End_Value =t.End_Value
from #TotalCert tc, #Total t
where tc.Owner  = t.Owner  
and tc.Instrument_Class = t.Instrument_Class
and tc.Certificate_Key= t.Certificate_Key


---select * into  WORKSPACE..IT3B_Get_IncomeX 
---from  #TotalCert 



truncate table WORKSPACE..IT3B_Get_Income

	    
insert  WORKSPACE..IT3B_Get_Income
                                    (Certificate_Key,
                                    Certificate_Number,
                                    Owner,
                                    Product,
                                    Contract_Number,
                                    Instrument_Class,
                                    Income_Source_Code,
                                    Value,
                                    Branch_Code ,
                                    Start_Date,
                                    Start_Value,
                                    End_Date,
                                    End_Value,
                                    WithHoldingTax,
									Loc_WH_Interest_TBT,
									LWHI_WHT_Tax)	 
select                              Certificate_Key,
                                    Certificate_Number,
                                    Owner,
                                    Product,
                                    Contract_Number,
                                    Instrument_Class,
                                    Income_Source_Code,
                                    Value,
                                    Branch_Code ,
                                    Start_Date,
                                    Start_Value,
                                    End_Date,
                                    End_Value,
                                    WithHoldingTax,
									Loc_WH_Interest_TBT,
									LWHI_WHT_Tax
from    #TotalCert 
order  by Owner

create unique index it3b_pk on WORKSPACE..IT3B_Get_Income(Certificate_Key,
                                    Certificate_Number,
                                    Owner,
                                    Product,
                                    Contract_Number,
                                    Instrument_Class,
                                    Income_Source_Code)

update WORKSPACE..IT3B_Get_Income  
set Start_Value = 0
where Start_Value  is null


update WORKSPACE..IT3B_Get_Income  
set End_Value = 0
where End_Value  is null

update WORKSPACE..IT3B_Get_Income  
set WithHoldingTax = 0
where WithHoldingTax  is null

--- Existing script to calculate 15%

update WORKSPACE..IT3B_Get_Income
set LWHI_WHT_Tax = Loc_WH_Interest_TBT * 0.15
where  LWHI_WHT_Tax <> 0

---- On top of 15%, to override the exemption rate
SELECT      E.Exemption_Rate, T.*
FROM        WORKSPACE..IT3B_Get_Income T
INNER JOIN  DividendTaxExemption E
ON          T.Owner = E.People_Number
AND         E.Exemption_Type = 'I'
AND         E.Tax_Type = 5
AND         E.End_Date IS NULL
WHERE       T.LWHI_WHT_Tax <> 0

UPDATE      WORKSPACE..IT3B_Get_Income
SET         LWHI_WHT_Tax = T.Loc_WH_Interest_TBT * (E.Exemption_Rate/100)
FROM        WORKSPACE..IT3B_Get_Income T
INNER JOIN  DividendTaxExemption E
ON          T.Owner = E.People_Number
AND         E.Exemption_Type = 'I'
AND         E.Tax_Type = 5
AND         E.End_Date IS NULL
WHERE       T.LWHI_WHT_Tax <> 0

    --  Updating SA Resident (Flag to identify Local or not) - Start
    UPDATE          WORKSPACE..IT3B_Get_Income
    SET             SA_Resident = 1
    FROM            WORKSPACE..IT3B_Get_Income W
    LEFT OUTER JOIN PeopleCountry PC
    ON              W.Owner = PC.People_Number
    AND             PC.Country_Type IN ('TR','TS','TT','TF','TG')
    WHERE           PC.People_Number IS NULL

    UPDATE          WORKSPACE..IT3B_Get_Income
    SET             SA_Resident = 1
    FROM            WORKSPACE..IT3B_Get_Income W
    INNER JOIN      PeopleCountry PC
    ON              W.Owner = PC.People_Number
    AND             PC.Country_Type IN ('TR','TS','TT','TF','TG')
    AND             PC.Code = 'ZA'
    --  Updating SA Resident (Flag to identify Local or not) - End

/***
select *
from WORKSPACE..IT3B_Get_Income
(Certificate_Key,Certificate_Number,Owner,Instrument_Class,Interest,Dividend,Product,Branch_Code,Start_Date
,Start_Value,End_Date,End_Value,Tax_For_Dividend,Taxable_For_Int,Withholding_Tax_For_Int)
***/

END
GO
GRANT EXECUTE ON IT3B_Get_Income TO testers
GO

--**	********************************************************************
--**	CONFIRM THE CREATION OF THE NEW PROC	****************************
--**	********************************************************************
	IF OBJECT_ID('IT3B_Get_Income') IS NOT NULL
    	BEGIN
		PRINT '<<< CREATED PROCEDURE IT3B_Get_Income >>>'
	END
	ELSE
	    	PRINT '<<< FAILED CREATING PROCEDURE IT3B_Get_Income >>>'
go
--**    THE END.............................................................................
