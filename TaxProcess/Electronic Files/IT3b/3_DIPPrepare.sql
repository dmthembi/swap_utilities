----- DIPPrepare.sql
USE IMS_MAIN
GO

IF OBJECT_ID('#CreateBalances') IS NOT NULL DROP TABLE #CreateBalances
IF OBJECT_ID('#IT3b_StartBalance') IS NOT NULL DROP TABLE #IT3b_StartBalance
IF OBJECT_ID('IT3b_StartBalance') IS NOT NULL DROP TABLE IT3b_StartBalance
IF OBJECT_ID('WORKSPACE..DIPBKP') IS NOT NULL DROP TABLE WORKSPACE..DIPBKP
GO

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit
	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year  = Tax_Year  
from TaxProcessDates


select  distinct Deal_Xref,
        Date = Start_Date
into    #CreateBalances
from    TaxCertificateDealXrefs TCD,  
        TaxCertificateHeader TCH 
where   TCD.Certificate_Key = TCH.Certificate_Key
and     Year = @Tax_Year

exec Balancing 'Y','Y','N','N','N','N',1

go

select 'Balancing 1 complete at ', getdate()

SELECT	DIP.Deal_Xref,
	DIP.Instrument_Number,
	DIP.Balance_Type,
	Refreshed,
	DIP.Value
INTO	#IT3b_StartBalance
FROM	DealInstrumentPosition	DIP,
	#CreateBalances		CB
WHERE	CB.Deal_Xref		= DIP.Deal_Xref
AND	CB.Date			= DIP.Refreshed

select * into IT3b_StartBalance from #IT3b_StartBalance

truncate table DealInstrumentPosition

update #CreateBalances
set Date = (select End_Date from TaxProcessDates)

select 'Deals to balance ', @@rowcount

exec Balancing 'Y','Y','N','N','N','N',1

go

select 'Balancing 2 complete at ', getdate()

INSERT	IT3b_StartBalance
SELECT	DIP.Deal_Xref,
	DIP.Instrument_Number,
	DIP.Balance_Type,
	DIP.Refreshed,
	DIP.Value
FROM	DealInstrumentPosition	DIP,
	#CreateBalances		CB
WHERE	CB.Deal_Xref		= DIP.Deal_Xref
AND	CB.Date			= DIP.Refreshed

--/////////////////////////////////////////////////////////
--**    Merge Opening balances with end balances

truncate table DealInstrumentPosition

select *
into WORKSPACE..DIPBKP
from IT3b_StartBalance

INSERT	DealInstrumentPosition
SELECT	Deal_Xref,
	Instrument_Number,
	Balance_Type,
	Refreshed,
	max(Value) Value
FROM	IT3b_StartBalance
GROUP BY
	Deal_Xref,
	Instrument_Number,
	Balance_Type,
	Refreshed
go


