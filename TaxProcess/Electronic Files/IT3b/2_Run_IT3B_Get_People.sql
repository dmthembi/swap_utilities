USE IMS_MAIN
GO

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit
	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year  = Tax_Year  
from TaxProcessDates
 
exec IT3B_Get_People @Start_Date,@End_Date,@Tax_Year 
go