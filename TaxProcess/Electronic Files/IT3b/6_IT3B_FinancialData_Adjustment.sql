
-------- From ASH_IT3B_AdjustmentFile
UPDATE WORKSPACE..IT3B_Get_Income
SET Start_Value = ISNULL(T2.Opening_Balance,0)
FROM WORKSPACE..IT3B_Get_Income T1
INNER JOIN WORKSPACE..IT3B_AdjustmentFile T2
ON T1.Owner = T2.Owner
AND T1.Product = T2.Product
WHERE T1.Instrument_Class = 'MM'
GO

UPDATE WORKSPACE..IT3B_Get_Income
SET End_Value = ISNULL(T2.Closing_Balance,0)
FROM WORKSPACE..IT3B_Get_Income T1
INNER JOIN WORKSPACE..IT3B_AdjustmentFile T2
ON T1.Owner = T2.Owner
AND T1.Product = T2.Product
WHERE T1.Instrument_Class = 'MM'
GO


----- IT3B_FinancialData_Cleanup.sql

----- IT3B_FinancialData_Cleanup_2013.sql

Insert  WORKSPACE..IT3B_Get_Income 
select  Certificate_Key, Certificate_Number, Owner,Product,Contract_Number,Instrument_Class,'4112',0,
        Branch_Code,Start_Date,Start_Value,End_Date,End_Value,abs(WithHoldingTax),
        ISNULL(Loc_WH_Interest_TBT,0), ISNULL(LWHI_WHT_Tax,0),
        March_Credits,Apr_Credits,May_Credits,June_Credits,Jul_Credits,Aug_Credits,Sep_Credits,Oct_Credits,Nov_Credits,Dec_Credits,Jan_Credits,Feb_Credits,
        March_Debits,Apr_Debits,May_Debits,June_Debits,Jul_Debits,Aug_Debits,Sep_Debits,Oct_Debits,Nov_Debits,Dec_Debits,Jan_Debits,Feb_Debits,
        SA_Resident
FROM    WORKSPACE..IT3B_Get_Income where Income_Source_Code = '4216' and abs(WithHoldingTax) > 0

GO
Insert  WORKSPACE..IT3B_Get_Income 
select  Certificate_Key, Certificate_Number, Owner,Product,Contract_Number,Instrument_Class,'4113',0,
        Branch_Code,Start_Date,Start_Value,End_Date,End_Value,abs(WithHoldingTax),
        ISNULL(Loc_WH_Interest_TBT,0), ISNULL(LWHI_WHT_Tax,0),
        March_Credits,Apr_Credits,May_Credits,June_Credits,Jul_Credits,Aug_Credits,Sep_Credits,Oct_Credits,Nov_Credits,Dec_Credits,Jan_Credits,Feb_Credits,
        March_Debits,Apr_Debits,May_Debits,June_Debits,Jul_Debits,Aug_Debits,Sep_Debits,Oct_Debits,Nov_Debits,Dec_Debits,Jan_Debits,Feb_Debits,
        SA_Resident
FROM    WORKSPACE..IT3B_Get_Income where Income_Source_Code = '4218' and  abs(WithHoldingTax) > 0
GO

Update  WORKSPACE..IT3B_Get_Income SET WithHoldingTax=0 where Income_Source_Code IN ('4218','4216')

GO

Update  WORKSPACE..IT3B_Get_Income 
SET     March_Credits=0,Apr_Credits=0,May_Credits=0,June_Credits=0,Jul_Credits=0,Aug_Credits=0,Sep_Credits=0,Oct_Credits=0,Nov_Credits=0,Dec_Credits=0,Jan_Credits=0,Feb_Credits=0,
        March_Debits=0,Apr_Debits=0,May_Debits=0,June_Debits=0,Jul_Debits=0,Aug_Debits=0,Sep_Debits=0,Oct_Debits=0,Nov_Debits=0,Dec_Debits=0,Jan_Debits=0,Feb_Debits=0

GO

update WORKSPACE..IT3B_Get_Income
set WithHoldingTax = 0
from WORKSPACE..IT3B_Get_Income
where Income_Source_Code = '4238'
GO

delete WORKSPACE..IT3B_Get_Income
where Income_Source_Code = '4238'
and isnull(SA_Resident,0) = 0
GO
