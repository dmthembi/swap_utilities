/************************************************************************
*
* Stored Procedure Name  : IT3B_Get_People
* Date Written           : 29 Feb 2000
* Author                 : Phillip Strauss
* Project / System       : IT3(b) Certificates
* SubProject / SubSystem : 
* Purpose / Description  : To extract PeopleInfo to send to the Receiver.
*                          Will be used to batch the detail and FTP to 
*                          the receiver in a 10000 batch format.
*                          This will be executed by a C++ program.   
*
* Input Parameters       : @Start_Date    - 
*                          @End_Year
*                          @Year          -  to determine DealXrefs to 
*                                                 balance
* Output Parameters      : none
* Tables                 : TaxCertificateHeader
*                          PeopleRegistration
*                          People
*                          PeopleDates
*                          PeopleAddress
*                          PeopleTax   
* Return Status          : 
* Error Codes            : 
* 
* ----------------------------------------------------------------------
* Modifications :
* Programmer    	Date        Changes
* ______________________________________________________________________
*
*************************************************************************/
IF OBJECT_ID('IT3B_Get_People') IS NOT NULL
   BEGIN
        DROP proc IT3B_Get_People
        PRINT '<<< DROPPED PROCEDURE IT3B_Get_People >>>'
   END
ELSE
    PRINT '<<< PROCEDURE IT3B_Get_People DOES NOT EXIST >>>'
go


--**  drop all hash tables that might have been created

IF OBJECT_ID('#PersonalDet') IS NOT NULL
   BEGIN
        DROP TABLE #PersonalDet
        PRINT '<<< DROPPED HASH TABLE #PersonalDet >>>'
   END
go



IF OBJECT_ID('#CreateBalances') IS NOT NULL
   BEGIN
        DROP TABLE #CreateBalances
        PRINT '<<< DROPPED HASH TABLE #CreateBalances >>>'
   END
go

IF OBJECT_ID('WORKSPACE..IT3B_Get_People') IS NOT NULL
   BEGIN
        DROP TABLE WORKSPACE..IT3B_Get_People
        PRINT '<<< DROPPED TABLE WORKSPACE..IT3B_Get_People >>>'
   END
go




CREATE TABLE WORKSPACE..IT3B_Get_People
(	Owner                       int           not null,
    Product                     varchar(6)    null,
	Contract_Number             int           null, 
	Tax_Number                  varchar(20)   null,
	Start_Date                  char(12)      null,
	End_Date                    char(12)      null,
	Reg_Type                    smallint      null,
    Reg_Value                   varchar(20)   null,
    Country_Code                char(3)       null,   -- Passport_Country_Of_Issue
    SA_Residence_Indicator      char(2)       null,   -- Account Holder South African residence indicator
	Name                        varchar(50)   null,
	Initials                    varchar(10)   null,
	First_Names                 varchar(50)   null,
	Date                        datetime      null,
    FICA_Status                 char          null,
	Post_Number                 int           null,
	Post_1                      varchar(35)   null,
	Post_2                      varchar(35)   null,
	Post_3                      varchar(35)   null,
	Post_4                      varchar(35)   null,
	Post_5                      varchar(35)   null,
	Post_Code                   varchar(10)   null,
	Phys_Number                 int           null,
	Phys_1                      varchar(35)   null,
	Phys_2                      varchar(35)   null,
	Phys_3                      varchar(35)   null,
	Phys_4                      varchar(35)   null,
	Phys_5                      varchar(35)   null,
	Phys_Code                   varchar(10)   null,
    Source                      char , 
    Nature_Of_Person int  null 
    )
go




/*	Create any external Hash Tables					*/

--**	------------------
--**	CREATE STATEMENT -
--**	------------------
CREATE PROCEDURE IT3B_Get_People  @Start_Date datetime,   --** start date of Period
                                  @End_Date   datetime,   --** end date of Period
                                  @Year       int
                                  
AS
-------------------------------------------------------
-- Modifications : Pieter Pieterse 22/26 June 2000
-- Select into's changed to insert
-------------------------------------------------------
BEGIN

--**    this is a test to see if the IT3(b) proc poulates the table correctly.
--**
--**    exec IT3B_Get_People  'Mar 01 1999', 'Feb 29 2000', 2000 -- input parameter expects a year
--**    select * from WORKSPACE..PP_Get_People
--**    drop table WORKSPACE..PP_Get_People



--**	Insert by the DBA's
--**	---------------------------------------------------
	exec DBA_ProcedureLog 'IT3B_Get_People'
--**	---------------------------------------------------





create table #PersonalDet
( Owner                         int           not null,
    Product                    varchar(6)    null,
	Contract_Number             int           null, 
  Tax_Number                    varchar(20)   null,
  Start_Date                    char(12)      null,
  End_Date                      char(12)      null,
  Reg_Type                      smallint      null,
  Reg_Value                     varchar(20)   null,
  Country_Code                  char(3)       null,   -- Passport_Country_Of_Issue
  SA_Residence_Indicator        char(2)       null,   -- Account Holder South African residence indicator
  Name                          varchar(50)   null,
  Initials                      varchar(10)   null,
  First_Names                   varchar(50)   null,
  Date                          char(12)      null,
  FICA_Status                   char          null,
  Post_Number                   int           null,
  Post_1                        varchar(35)   null,
  Post_2                        varchar(35)   null,
  Post_3                        varchar(35)   null,
  Post_4                        varchar(35)   null,
  Post_5                        varchar(35)   null,
  Post_Code                     varchar(10)   null,
  Phys_Number                   int           null,
  Phys_1                        varchar(35)   null,
  Phys_2                        varchar(35)   null,
  Phys_3                        varchar(35)   null,
  Phys_4                        varchar(35)   null,
  Phys_5                        varchar(35)   null,
  Phys_Code                     varchar(10)   null,
  Source                        char, 
  Nature_Of_Person int   null )



--**    Do a select into a hash table to get the Owner info
--**    Table queried is 
select  Owner,
        Product, 
		Contract_Number, 
        Tax_Number  = convert(varchar(20), null),
        Year,
        Start_Date, 
        End_Date,
        Reg_Type    = convert(smallint, null),
        Reg_Value   = convert(varchar(20),null),
        Country_Code  = 'ZAF',                -- Passport_Country_Of_Issue
        SA_Residence_Indicator = convert(char(2), null), --Account Holder South African residence indicator
        Name        = Name,
        Initials    = Initials,
        First_Names = First_Names,
        Date        = convert(datetime, null),
        FICA_Status = convert(char,'N'),
        Source = 'I',
        Nature_Of_Person  = convert(int,null)
into    #PeopleDetail
from    TaxCertificateHeader TCH,
        People P
where   TCH.Owner   = P.People_Number
and     Year   =  @Year
GROUP BY  Owner,
        Product, 
		Contract_Number, 
        Year,
        Start_Date, 
        End_Date,
        Initials,
        Name,
        First_Names
		
		select @@rowcount ," Records found from TaxCertificateHeader"
		
		

CREATE INDEX Perf1 on #PeopleDetail(Owner,Source)

/*-----------------*/
-- Data From Manual Adjustments from Ashburton

--**    update the table to get the info from PeopleRegistration    
--select * from PeopleRegistration where People_Number=10195
update  #PeopleDetail
set     Reg_Type  = PR.Reg_Type,
        Reg_Value = isnull(PR.Reg_Value,
			CASE PR.Reg_Type
				WHEN 1 THEN '9999999999999' 	-- id
				WHEN 2 THEN '' 			-- passport
				WHEN 3 THEN '999999999999' 	-- Company reg
				WHEN 12 THEN 'IT9999999999' 	-- Trust Reg
                WHEN 14 THEN 'IT9999999999' 	-- Trust Reg
				ELSE '9999999999999'
			END)
from    PeopleRegistration PR,
        #PeopleDetail PD
where   Owner = People_Number
and	PR.Reg_Type in (1,2,3,12,14,17,18)
and Source = 'I'

/* DM. TYEP 2013. 16/feb/2013. Comment out. ???
UPDATE	#PeopleDetail
SET	Reg_Type 	= 3,
	Reg_Value 	= 'IT9999/99'	
WHERE	Name like '%Trust%'
*/
--**    Update table to get the Owners Tax Number - If there's one

update  #PeopleDetail
set     Tax_Number   = convert(varchar(20), PT.Tax_Number ) --+ '0000000000') WHY???
from    PeopleTax PT,
        #PeopleDetail PD  
where   Owner = People_Number
AND	PT.Tax_Number  is not null   --- ???? why?.


--**    select * from #PeopleDetail
--**    Update table to get the Owners Birth Date - If there's one

update  #PeopleDetail
set     Date = PBD.Date
from    PeopleDates PBD,
        #PeopleDetail PD
where   Owner = People_Number
and     Date_Type = 'B'


--** FICA....  The FICATEST needs to be developed...



/*
update #PeopleDetail
set FICA_Status = 'Y'
from #PeopleDetail p, FICATEST ft
where p.Owner = ft.People_Number
and lower(ft.FICA_Status) like '%fica compliant%'

update #PeopleDetail
set FICA_Status = 'N'
from #PeopleDetail p, FICATEST ft
where p.Owner = ft.People_Number
and lower(ft.FICA_Status) like '%non compliant%'
*/

update #PeopleDetail
set FICA_Status = 'Y'
from #PeopleDetail pd, PeopleCompliance pc
where pd.Owner = pc.People_Number
and pc.Compliance_Type_Id IN (2,3,7) 
and current_date() between pc.Start_Date and ISNULL(pc.End_Date,'31 Dec 9999')


update #PeopleDetail
set FICA_Status = 'N'
where FICA_Status is null

--** Name (SurName) update based on FICA Status
UPDATE #PeopleDetail SET Name = 'UNKNOWN' WHERE FICA_Status IN ('N','E') AND LEN(RTRIM(LTRIM(Name))) = 0


--*** Pasport Country Of Issue....

update #PeopleDetail
set Country_Code = cm.SARS_CountryCode
from #PeopleDetail pd, PeopleCountry pc, CountryMap cm
where pd.Owner = pc.People_Number
and pc.Code			= cm.ISO_CountryCode
and pc.Country_Type = 'PA'


--- *** SA Residance indicator....
update #PeopleDetail
set    SA_Residence_Indicator = pc.Code
from   #PeopleDetail pd, PeopleCountry pc
where  pd.Owner = pc.People_Number
and    pc.Country_Type = 'TR'

----- **** Nature Of Person
update  #PeopleDetail
set     Nature_Of_Person=N.SARS_Code
from    #PeopleDetail PD ,
        WORKSPACE..NatureOfPersonMapping N,
        People P 
where   PD.Owner = P.People_Number 
AND     P.Customer_Type = N.Customer_Type

--**    Now go and populate two hash tables with the postal and physical addresses
--**    the 2 tables are 1.) #PostAddress   2.) #PhysAddress
select  distinct Owner,
        Post_Number   = Address_Number, 
        Post_1        = Line_1, 
        Post_2        = Line_2,     
        Post_3        = Line_3,     
        Post_4        = Line_4,      
        Post_5        = Line_5,      
        Post_Code     = Postal_Code
into    #PostAddress
from    #PeopleDetail PD,
        PeopleAddress PA
where   PD.Owner      = PA.People_Number
and     Address_Type  = 'POST'

select  Distinct Owner,
        Phys_Number   = Address_Number,
        Phys_1        = Line_1,      
        Phys_2        = Line_2,      
        Phys_3        = Line_3,      
        Phys_4        = Line_4,      
        Phys_5        = Line_5,      
        Phys_Code     = Postal_Code  
into    #PhysAddress
from    #PeopleDetail PD,
        PeopleAddress PA
where   PD.Owner      = PA.People_Number
and     Address_Type  = 'PHYS'


--**    now update the output hash table #PersonalDet
--**    First the Owner Details and then update with
--**    the addresses.    

insert  #PersonalDet
select  Owner,
        Product, 
		Contract_Number, 
        Tax_Number, 
        Start_Date  = convert(char(12), Start_Date, 112),
        End_Date    = convert(char(12), End_Date, 112),
        Reg_Type    = convert(smallint, isnull(Reg_Type,1)),  
        Reg_Value, 
        Country_Code,
        SA_Residence_Indicator,
        Name,      
        Initials,  
        First_Names,
        Date       ,    
        FICA_Status,
        null,
        null,
        null,     
        null,     
        null,     
        null,     
        null, 
        null,
        null,    
        null,    
        null,    
        null,  
        null,    
        null,
        Source,
        Nature_Of_Person
from    #PeopleDetail

--create clustered index ndx1 on #PersonalDet (Owner,Source)

update  #PersonalDet
set     Post_Number   = PA1.Post_Number,
        Post_1        = PA1.Post_1,      
        Post_2        = PA1.Post_2,      
        Post_3        = PA1.Post_3,      
        Post_4        = PA1.Post_4,      
        Post_5        = PA1.Post_5,      
        Post_Code     = PA1.Post_Code  
from    #PersonalDet PD,
        #PostAddress PA1
where   PD.Owner      = PA1.Owner

update  #PersonalDet
set     Phys_Number  = PA1.Phys_Number,
        Phys_1       = PA1.Phys_1,      
        Phys_2       = PA1.Phys_2,      
        Phys_3       = PA1.Phys_3,      
        Phys_4       = PA1.Phys_4,      
        Phys_5       = PA1.Phys_5,      
        Phys_Code    = PA1.Phys_Code
from    #PersonalDet PD,
        #PhysAddress PA1
where   PD.Owner     = PA1.Owner


--**    Update table to get the Owners Tax Number - If there's one



--**    select into this WORKSPACE table for testing purposes


truncate table WORKSPACE..IT3B_Get_People --PP2606


insert  WORKSPACE..IT3B_Get_People  --PP2206
select  *
from    #PersonalDet
select 'Deals copied to WORKSPACE ', @@rowcount

select 'Before Return'
return
select 'After Return'


END
go

GRANT EXECUTE ON IT3B_Get_People TO testers
go

IF OBJECT_ID('IT3B_Get_People') IS NOT NULL
    PRINT '<<< CREATED PROCEDURE IT3B_Get_People >>>'
ELSE
    PRINT '<<< FAILED CREATING PROCEDURE IT3B_Get_People >>>'
go
