set nocount on
go
select 'Remove Invalid characters from Initials'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
	@Person_Init        varchar(5),
        @Count          int


select Owner, Reg_Value, Initials
into  #Cursor
from WORKSPACE..IT3B_Get_People
WHERE  Reg_Type	= 1

--select *
--from WORKSPACE..IT3B_Get_People


select @Owner = Owner, @Person_Init = Initials
from  WORKSPACE..IT3B_Get_People
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin

   select @Count = 1
   while (@Count <= datalength(@Person_Init))
   begin
    -- select substring(@Person_Init, @Count, 1)
     if (((ascii(substring(@Person_Init, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Person_Init, @Count
       update WORKSPACE..IT3B_Get_People
       set Initials = stuff(Initials,@Count,1,null)
       where Owner           =   @Owner 

      
        
       select @Count = @Count - 1

       select @Person_Init = Initials
       from  WORKSPACE..IT3B_Get_People
       where Owner           =   @Owner 

      select @Owner, @Person_Init, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 


--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner,  @Person_Init = Initials
from  #Cursor
set rowcount 0

end
GO