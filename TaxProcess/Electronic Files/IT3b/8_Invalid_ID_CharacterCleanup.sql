set nocount on
go

IF OBJECT_ID('#Cursor') IS NOT NULL DROP TABLE #Cursor
go

declare @Owner          int, 
        @Reg_Value      varchar(20),
        @Id             varchar(20),
        @Count          int



select Owner, Reg_Value
into  #Cursor
from WORKSPACE..IT3B_Get_People
WHERE  Reg_Type	= 1

select @Owner = Owner, @Reg_Value = Reg_Value
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
   --select  @Owner, @Reg_Value , datalength(@Reg_Value)
   select @Count = 1
   while (@Count <= datalength(@Reg_Value))
   begin
     --select substring(@Id, @Count, 1)
       if ascii(substring(@Reg_Value, @Count, 1)) not between 48 and 57
       begin
         select  @Owner, @Reg_Value , datalength(@Reg_Value)
         update WORKSPACE..IT3B_Get_People
         set Reg_Value = stuff(Reg_Value,@Count,1,null)
         where Owner           =   @Owner 
         and   Reg_Type	     = 1
         select  @Reg_Value = Reg_Value
           from  WORKSPACE..IT3B_Get_People
           where Owner = @Owner
         select @Count = @Count - 1
         select  @Owner, @Reg_Value , datalength(@Reg_Value)
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 


select @Owner = Owner, @Reg_Value = Reg_Value
from  #Cursor

end
go
drop table #Cursor
go