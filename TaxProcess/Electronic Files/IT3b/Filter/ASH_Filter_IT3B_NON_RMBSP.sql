
TRUNCATE TABLE WORKSPACE..IT3B_Get_People
TRUNCATE TABLE WORKSPACE..IT3B_Get_Income
GO

INSERT INTO WORKSPACE..IT3B_Get_People
SELECT DISTINCT * FROM WORKSPACE..IT3B_Get_People_ALL WHERE Product <> 'RMBSP' AND Source = 'I'
GO

INSERT INTO WORKSPACE..IT3B_Get_People
SELECT DISTINCT * FROM WORKSPACE..IT3B_Get_People_ALL WHERE Product <> 'RMBSP' AND Source <> 'I' AND Owner NOT IN
(
    SELECT DISTINCT Owner FROM WORKSPACE..IT3B_Get_People
)
GO

INSERT INTO WORKSPACE..IT3B_Get_People
SELECT DISTINCT * FROM WORKSPACE..IT3B_Get_People_ALL WHERE Product = 'RMBSP' 
AND Owner IN (SELECT Owner FROM WORKSPACE..IT3B_Get_Income_ALL WHERE Product = 'RMBSP' AND Instrument_Class = 'UT')
AND Owner NOT IN (SELECT DISTINCT Owner FROM WORKSPACE..IT3B_Get_People)
AND Source = 'I'
GO

INSERT INTO WORKSPACE..IT3B_Get_People
SELECT DISTINCT * FROM WORKSPACE..IT3B_Get_People_ALL WHERE Product = 'RMBSP' 
AND Owner IN (SELECT Owner FROM WORKSPACE..IT3B_Get_Income_ALL WHERE Product = 'RMBSP' AND Instrument_Class = 'UT')
AND Owner NOT IN (SELECT DISTINCT Owner FROM WORKSPACE..IT3B_Get_People)
AND Source <> 'I'
GO


INSERT INTO WORKSPACE..IT3B_Get_Income
SELECT * FROM WORKSPACE..IT3B_Get_Income_ALL WHERE Product <> 'RMBSP'

INSERT INTO WORKSPACE..IT3B_Get_Income
SELECT 
Certificate_Key, Certificate_Number, Owner, Product, Contract_Number, Instrument_Class, Income_Source_Code, Value, Branch_Code,
Start_Date, Start_Value = 0.0 , End_Date, End_Value = 0.0, WithHoldingTax,
ISNULL(Loc_WH_Interest_TBT,0), ISNULL(LWHI_WHT_Tax,0),
March_Credits,Apr_Credits,May_Credits,June_Credits,Jul_Credits,Aug_Credits,Sep_Credits,Oct_Credits,Nov_Credits,Dec_Credits,Jan_Credits,Feb_Credits,March_Debits,
Apr_Debits,May_Debits,June_Debits,Jul_Debits,Aug_Debits,Sep_Debits,Oct_Debits,Nov_Debits,Dec_Debits,Jan_Debits,Feb_Debits,
SA_Resident
FROM WORKSPACE..IT3B_Get_Income_ALL WHERE Product = 'RMBSP' AND Instrument_Class = 'UT'
GO

-- Remove Owners for which there is no data in Financial section
--select Owner from WORKSPACE..IT3B_Get_People where Owner Not in (select Owner from WORKSPACE..IT3B_Get_Income)

DELETE FROM WORKSPACE..IT3B_Get_People WHERE Owner NOT IN(SELECT DISTINCT Owner FROM WORKSPACE..IT3B_Get_Income)
GO

INSERT INTO WORKSPACE..IT3B_Get_People
SELECT * FROM WORKSPACE..IT3B_Get_People_ALL WHERE Convert(Varchar,Owner) || Convert(Varchar, Contract_Number)
IN (SELECT Convert(Varchar,Owner) || Convert(Varchar, Contract_Number) FROM WORKSPACE..IT3B_Get_Income)
AND Convert(Varchar,Owner) || Convert(Varchar, Contract_Number)
NOT IN (SELECT Convert(Varchar,Owner) || Convert(Varchar, Contract_Number) FROM WORKSPACE..IT3B_Get_People)
GO

