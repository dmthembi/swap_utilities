USE IMS_MAIN
GO

TRUNCATE TABLE WORKSPACE..IT3B_Get_People
TRUNCATE TABLE WORKSPACE..IT3B_Get_Income
GO

INSERT INTO WORKSPACE..IT3B_Get_People
SELECT * FROM WORKSPACE..IT3B_Get_People_ALL WHERE Product = 'RMBSP' AND Owner IN 
    (SELECT Owner FROM WORKSPACE..IT3B_Get_Income_ALL WHERE Product = 'RMBSP' AND Instrument_Class = 'MM')
GO

INSERT INTO WORKSPACE..IT3B_Get_Income
SELECT * FROM WORKSPACE..IT3B_Get_Income_ALL WHERE Product = 'RMBSP' AND Instrument_Class = 'MM'
GO

UPDATE WORKSPACE..IT3B_Get_Income SET Loc_WH_Interest_TBT = 0 where Loc_WH_Interest_TBT IS NULL
UPDATE WORKSPACE..IT3B_Get_Income SET LWHI_WHT_Tax = 0 where LWHI_WHT_Tax IS NULL



-- Remove Owners for which there is no data in Financial section
--select Owner from WORKSPACE..IT3B_Get_People where Owner Not in (select Owner from WORKSPACE..IT3B_Get_Income)

DELETE FROM WORKSPACE..IT3B_Get_People WHERE Owner NOT IN(SELECT DISTINCT Owner FROM WORKSPACE..IT3B_Get_Income)
GO

DELETE FROM WORKSPACE..IT3B_Get_People WHERE Convert(varchar,Owner) || convert(varchar,Contract_Number) NOT IN(SELECT DISTINCT Convert(varchar,Owner) || convert(varchar,Contract_Number) FROM WORKSPACE..IT3B_Get_Income)
GO
