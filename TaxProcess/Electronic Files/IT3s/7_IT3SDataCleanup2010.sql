----- IT3bDataCleanup2010.sql
/*****************************************************************************/
-- IT3b/c Data Validation
/*****************************************************************************/
set nocount on
go

/*
select *
from RegistrationTypes

select * from WORKSPACE..IT3S_Get_People where Reg_Type = 12

update WORKSPACE..IT3S_Get_People
set Reg_Type = 3
where Reg_Type = 12
*/

/*
select * 
from WORKSPACE..IT3S_Get_People 
where Reg_Type	= 1
AND	isnull(First_Names,'')	= ''
and lower(Name) like '%trust%'
*/

update WORKSPACE..IT3S_Get_People
set Reg_Type	= 2
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
AND	isnull(First_Names,'')	= ''
GO

/*
select *
from WORKSPACE..IT3S_Get_People
where Reg_Type	= 1
AND	isnull(Initials,'')	= ''
*/

update WORKSPACE..IT3S_Get_People
set Initials = substring(First_Names,1,1)
where Reg_Type	= 1
AND	isnull(Initials,'')	= ''

/*
SELECT	'Invalid Company Registration'
SELECT	*
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type		= 3
AND	isnull(Reg_Value,'')	= ''
*/

update WORKSPACE..IT3S_Get_People
set Initials = 'G E'
where Owner = 144441

update WORKSPACE..IT3S_Get_People
set Initials = 'L'
where Owner = 174905

/*
SELECT	'Invalid trust or deed number'
select Reg_Value,  A.*
from WORKSPACE..IT3S_Get_People A
where Reg_Type	= 3
and datalength(Reg_Value) = 1
order by Reg_Value
*/

update WORKSPACE..IT3S_Get_People
set Reg_Value  =  '9999/999999/99'
from WORKSPACE..IT3S_Get_People A
where Reg_Type	= 3
and (datalength(Reg_Value) = 1
or Reg_Value  is null)

/*
select *
from WORKSPACE..IT3S_Get_People
where Reg_Type	= 3
and lower(Name) not like '%trust%'
and isnull(First_Names,'') != ''
*/

update WORKSPACE..IT3S_Get_People
set  Reg_Type	= 1
where Owner in (214457 , 204522 )
GO

/*
select *
from  WORKSPACE..IT3S_Get_People
where Owner in (214457 , 204522 )


SELECT	'Invalid Initials / Name for Company/Trust'
SELECT	*
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type		not in (1,2)
AND	(isnull(Initials,'')	!= ''
OR	isnull(First_Names,'')	!= '')

--select * from WORKSPACE..IT3S_Get_People_BKP where Owner =  214457
--select * from PeopleRegistration where People_Number =  214457

SELECT	'Invalid Initials / Name for Company/Trust'
SELECT	*
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type		not in (1,2)
AND	(isnull(Initials,'')	!= ''
OR	isnull(First_Names,'')	!= '')
*/

update WORKSPACE..IT3S_Get_People
set Initials = '',
    First_Names = ''
WHERE	Reg_Type		not in (1,2)
AND	(isnull(Initials,'')	!= ''
OR	isnull(First_Names,'')	!= '')



/*
SELECT	'Invalid Birth Date'
SELECT	'19' + substring(Reg_Value, 1, 6), Date, *
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	in (1) --,2)
AND	datalength(isnull(Date,'')) != 8
*/

update WORKSPACE..IT3S_Get_People
set Date = '19' + substring(Reg_Value, 1, 6)
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	in (1)
AND	datalength(isnull(Date,'')) != 8


update WORKSPACE..IT3S_Get_People
set Date = '19800101'
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	in (1)
AND	datalength(isnull(Date,'')) != 8



go


update WORKSPACE..IT3S_Get_People
set 	Reg_Value   =  stuff(Reg_Value, 1, 6, substring(Date,3,8))
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
AND	(substring(Reg_Value,1,6) != substring(Date,3,6)
OR	datalength(Reg_Value)	!= 13)



update 	WORKSPACE..IT3S_Get_People
set 	Reg_Value   =  substring(Reg_Value,1,13)
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
and      datalength(Reg_Value)	> 13


update 	WORKSPACE..IT3S_Get_People
set 	Reg_Value   =  Reg_Value + replicate(substring(Reg_Value,datalength(Reg_Value),1), 13 - datalength(Reg_Value))
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
and      datalength(Reg_Value)	< 13





update WORKSPACE..IT3S_Get_People
set Post_1  =  '100 Grayston Drive',
    Post_2='Sandown', 
    Post_3= 'Sandton', 
    Post_4='',Post_5='',
    Post_Code='2146'
where isnull(Post_1,'')  =  ''



/*****************************************************************************/
-- End of script
/*****************************************************************************/
GO
