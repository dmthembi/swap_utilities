----- IT3DataCleanup_2014.sql
----- IT3B Data Clean Up 2014 

SELECT '<<<<<<<<<< MORE STUPID UPDATES ON WORKSPACE..IT3S_Get_People >>>>>>>>>>>>>>>'

UPDATE  WORKSPACE..IT3S_Get_People  
SET     Post_1 = STR_REPLACE(Post_1,'&','and'),
        Post_2 = STR_REPLACE(Post_2,'&','and'),
        Post_3 = STR_REPLACE(Post_3,'&','and'),
        Post_4 = STR_REPLACE(Post_4,'&','and'),
        Post_5 = STR_REPLACE(Post_5,'&','and')

GO

UPDATE  WORKSPACE..IT3S_Get_People  
SET     Name = STR_REPLACE(Name,'  ',' ')
GO

UPDATE  WORKSPACE..IT3S_Get_People  
SET     Name = STR_REPLACE(Name,'&','and')
GO


IF EXISTS(SELECT 1 FROM Tenant WHERE Name = 'RMB')
BEGIN

    SELECT 'Ashburton Clean Up'
    
    -- Defaulting FICA Status to N for Foreign passport and ID nos.
    UPDATE  WORKSPACE..IT3S_Get_People
    SET     FICA_Status = 'N'
    WHERE   Reg_Type = 1 
    AND     Reg_Value IS NULL
    AND     Owner IN
        (
        SELECT People_Number FROM PeopleRegistration WHERE Reg_Type IN (15,16)
        )

    -- Defaulting FICA Status to N where ID and Date of Birth is null and FICA Status is Y
    UPDATE  WORKSPACE..IT3S_Get_People
    SET     FICA_Status ='N' 
    WHERE   FICA_Status = 'Y' 
    AND     Reg_Value = NULL 
    AND     Date IS NULL

    UPDATE  WORKSPACE..IT3S_Get_People
    SET     Reg_Type = 12
    WHERE   lower(Name) LIKE '%trust%' 
    AND     Reg_Type <> 12 
    AND     Product = 'RMBSP' 
    AND     Source = 'U'

   
END
GO




IF EXISTS(SELECT 1 FROM Tenant WHERE Name = 'IMS')
BEGIN
    SELECT 'IMS Clean Up'

    UPDATE      WORKSPACE..IT3S_Get_People
    SET         Reg_Type = R.Reg_Type
    FROM        WORKSPACE..IT3S_Get_People W
    INNER JOIN  PeopleRegistration R
    ON          W.Owner = R.People_Number
    WHERE       R.Reg_Type IN (1,2,3,12,14,17,18)

    UPDATE      WORKSPACE..IT3S_Get_People
    SET         Reg_Value = R.Reg_Value
    FROM        WORKSPACE..IT3S_Get_People W
    INNER JOIN  PeopleRegistration R
    ON          W.Owner = R.People_Number
    AND         W.Reg_Type = R.Reg_Type
    WHERE       R.Reg_Type IN (1,2,3,12,14,17,18)

    UPDATE WORKSPACE..IT3S_Get_People SET FICA_Status = 'N' 

    UPDATE      WORKSPACE..IT3S_Get_People
    SET         FICA_Status = 'Y'
    FROM        WORKSPACE..IT3S_Get_People P 
    INNER JOIN  PeopleCompliance PC 
    ON          PC.People_Number = P.Owner
    AND         CURRENT_DATE() Between PC.Start_Date AND ISNULL(PC.End_Date,'31 Dec 9999')
    AND         PC.Compliance_Type_Id IN (2,3,7)

    UPDATE      WORKSPACE..IT3S_Get_People
    SET         FICA_Status = 'N'
    FROM        WORKSPACE..IT3S_Get_People W
    INNER JOIN  People P
    ON          W.Owner = P.People_Number
    AND         P.Language = 'E'
    INNER JOIN  RegistrationTypes R
    ON          W.Reg_Type = R.Reg_Type
    WHERE       W.FICA_Status = 'Y'
    AND         (W.Reg_Value IS NULL OR W.Reg_Value = '')
    AND         W.Date IS NULL

    UPDATE WORKSPACE..IT3S_Get_People SET FICA_Status = 'N'  WHERE (Reg_Value IS NULL OR Reg_Value = '')

    UPDATE  WORKSPACE..IT3S_Get_People 
    SET     FICA_Status = 'N',
            Reg_Type = 12,
            Reg_Value = NULL
    WHERE   lower(Name) LIKE '%trust%' 
    AND     Reg_Type NOT IN (12,18)

    --** Name (SurName) update based on FICA Status
    UPDATE WORKSPACE..IT3S_Get_People SET Name = 'UNKNOWN' WHERE FICA_Status IN ('N','E') AND LEN(RTRIM(LTRIM(Name))) = 0

    -- Update Date of Birth
    UPDATE      WORKSPACE..IT3S_Get_People
    SET         Date = D.Date
    FROM        WORKSPACE..IT3S_Get_People P 
    INNER JOIN  PeopleDates D 
    ON          D.People_Number = P.Owner
    AND         D.Date_Type = 'B'


            
    UPDATE  WORKSPACE..IT3S_Get_People
    SET     FICA_Status = 'N'
    WHERE   FICA_Status ='Y' 
    AND     Reg_Type = 100




END
SELECT '<<<<<<<<<< DONE WITH MORE STUPID UPDATES ON WORKSPACE..IT3S_Get_People >>>>>>>>>>>>>>>'
GO

