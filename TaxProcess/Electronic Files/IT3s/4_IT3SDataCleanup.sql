----- IT3DataCleanup.sql

SELECT 'UPDATING Reg_Type AND DATE IN WORKSPACE..IT3S_Get_People '

update WORKSPACE..IT3S_Get_People
set Reg_Type = 12
where Reg_Type in (1,2)
and Date is null
and lower(Name) like '%trust%'
and Reg_Type <> 12
GO


update WORKSPACE..IT3S_Get_People
set Reg_Type = 3
where Reg_Type in (1,2)
and isnull(Initials,'') = ''
and Date is null
GO

update WORKSPACE..IT3S_Get_People
set Date = substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-' + substring(Reg_Value,1,2 )
where Reg_Type in (1,2)
and Date is null
and isnull(Reg_Value,'') <> ''
and datalength(Reg_Value) = 13
and datediff(yy,substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-' + substring(Reg_Value,1,2 ),getdate()) > 0
GO

update WORKSPACE..IT3S_Get_People
set Date = substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-19' + substring(Reg_Value,1,2 )
where Reg_Type in (1,2)
and Date is null
and isnull(Reg_Value,'') <> ''
and datalength(Reg_Value) = 13
and datediff(yy,substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-' + substring(Reg_Value,1,2 ),getdate()) <0
GO

-- ** Exceptions **
update WORKSPACE..IT3S_Get_People
set Date = '01 jan 1900'
where Reg_Type in (1,2)
and Date is null
GO


SELECT '<<<<<<<<<<<<<<<<DONE>>>>>>>>>>>>>>>>>>>>>'

GO

