----- IT3bDataCleanup2010.sql
/*****************************************************************************/
-- IT3b/c Data Validation
/*****************************************************************************/
set nocount on
go

SELECT '<<<<MORE UPDATES ON WORKSPACE..IT3S_Get_People >>>>>>'

update WORKSPACE..IT3S_Get_People
set Reg_Type	= 2
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
AND	isnull(First_Names,'')	= ''
GO


update WORKSPACE..IT3S_Get_People
set Initials = substring(First_Names,1,1)
where Reg_Type	= 1
AND	isnull(Initials,'')	= ''



update WORKSPACE..IT3S_Get_People
set Reg_Value  =  '9999/999999/99'
from WORKSPACE..IT3S_Get_People A
where Reg_Type	= 3
and (datalength(Reg_Value) = 1
or Reg_Value  is null)



update WORKSPACE..IT3S_Get_People
set  Reg_Type	= 1
where Owner in (214457 , 204522 )
GO



update WORKSPACE..IT3S_Get_People
set Initials = '',
    First_Names = ''
WHERE	Reg_Type		not in (1,2)
AND	(isnull(Initials,'')	!= ''
OR	isnull(First_Names,'')	!= '')




update WORKSPACE..IT3S_Get_People
set Date = '19' + substring(Reg_Value, 1, 6)
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	in (1)
AND	datalength(isnull(Date,'')) != 8


update WORKSPACE..IT3S_Get_People
set Date = '19800101'
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	in (1)
AND	datalength(isnull(Date,'')) != 8


go


set nocount on
select 'Remove Invalid characters from Name'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int


select Owner, Reg_Value, Name
into  #Cursor
from WORKSPACE..IT3S_Get_People
WHERE  Reg_Type	= 1

--select *
--from WORKSPACE..IT3S_Get_People


select @Owner = Owner, @Surname = Name
from  WORKSPACE..IT3S_Get_People
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
      if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
        select @Owner, @Surname, @Count
        update WORKSPACE..IT3S_Get_People
        set Name = stuff(Name,@Count,1,null)
        where Owner           =   @Owner 

        select @Owner, @Surname, @Count
         
        select @Count = @Count - 1

        select @Surname = Name
         from  WORKSPACE..IT3S_Get_People
         where Owner           =   @Owner 
     
       select @Owner, @Surname, @Count
       end

     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 


--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner,  @Surname = Name
from  #Cursor
set rowcount 0

end





drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from First Names'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int


select Owner, Reg_Value, First_Names
into  #Cursor
from WORKSPACE..IT3S_Get_People
WHERE  Reg_Type	= 1

--select *
--from WORKSPACE..IT3S_Get_People


select @Owner = Owner, @Surname = First_Names
from  WORKSPACE..IT3S_Get_People
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
   
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Surname, @Count
       update WORKSPACE..IT3S_Get_People
       set First_Names = stuff(First_Names,@Count,1,null)
       where Owner           =   @Owner 

       
        
       select @Count = @Count - 1

       select @Surname = First_Names
       from  WORKSPACE..IT3S_Get_People
       where Owner           =   @Owner 

        select @Owner, @Surname, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 


--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner,  @Surname = First_Names
from  #Cursor
set rowcount 0

end






drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from Initials'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
	@Person_Init        varchar(5),
        @Count          int


select Owner, Reg_Value, Initials
into  #Cursor
from WORKSPACE..IT3S_Get_People
WHERE  Reg_Type	= 1

--select *
--from WORKSPACE..IT3S_Get_People


select @Owner = Owner, @Person_Init = Initials
from  WORKSPACE..IT3S_Get_People
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin

   select @Count = 1
   while (@Count <= datalength(@Person_Init))
   begin
    
     if (((ascii(substring(@Person_Init, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Person_Init, @Count
       update WORKSPACE..IT3S_Get_People
       set Initials = stuff(Initials,@Count,1,null)
       where Owner           =   @Owner 

      
        
       select @Count = @Count - 1

       select @Person_Init = Initials
       from  WORKSPACE..IT3S_Get_People
       where Owner           =   @Owner 

      select @Owner, @Person_Init, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 


--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner,  @Person_Init = Initials
from  #Cursor
set rowcount 0

end

GO

update WORKSPACE..IT3S_Get_People
set 	Reg_Value   =  stuff(Reg_Value, 1, 6, substring(Date,3,8))
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
AND	(substring(Reg_Value,1,6) != substring(Date,3,6)
OR	datalength(Reg_Value)	!= 13)



update 	WORKSPACE..IT3S_Get_People
set 	Reg_Value   =  substring(Reg_Value,1,13)
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
and      datalength(Reg_Value)	> 13


update 	WORKSPACE..IT3S_Get_People
set 	Reg_Value   =  Reg_Value + replicate(substring(Reg_Value,datalength(Reg_Value),1), 13 - datalength(Reg_Value))
FROM	WORKSPACE..IT3S_Get_People
WHERE	Reg_Type	= 1
and      datalength(Reg_Value)	< 13



update WORKSPACE..IT3S_Get_People
set Initials = 'JR',
    First_Names  = 'McHugh'
where Owner in (95064)

update WORKSPACE..IT3S_Get_People
set Initials = 'R S',
    First_Names  = 'Welsh'
where Owner in (11647)


update WORKSPACE..IT3S_Get_People
set Post_1  =  '100 Grayston Drive',
    Post_2='Sandown', 
    Post_3= 'Sandton', 
    Post_4='',Post_5='',
    Post_Code='2146'
where isnull(Post_1,'')  =  ''


/*****************************************************************************/
SELECT '<<<<<<<<<<<<<<<<<<<-- End of script -->>>>>>>>>>>>>>>>>>>>>'
/*****************************************************************************/
GO
