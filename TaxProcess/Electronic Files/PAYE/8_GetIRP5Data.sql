
if object_id('GetIRP5Data') is not null
drop proc GetIRP5Data
go


create proc GetIRP5Data 
@Prod varchar(6) ,
@PAYEReferenceNumber numeric(10,0)
AS
BEGIN
				
SELECT * 
FROM EmployerPAYE 
where Product_Code = @Prod 
and PAYE_Reference_Number = convert(numeric(10,0),@PAYEReferenceNumber) 
and TaxCertificateType = 'IRP5'


SELECT
     Owner,
     Tax,
     EP.Product_Code,
     cpy_IRP5Number,
     TaxRef,
     PersonNature,
     cpy_TaxYear,
	 Person_Surname,
	 Person_FullNames,
     Person_Init,
	 Country_Code,
     Line_1,
     Line_2,
     Line_3,
     Postal_Code,
	 IT.Phys_Addr_Country_Code,
     Person_Birthdate,
     Tax_No,
     Person_Email,
	 Person_Home_Tel,
	 Person_Cellphone,
     Id,
     PassportNo,
	 Owner,
     Commencement_Date=convert(varchar(20),Commencement_Date,112),
     To_Date=convert(varchar(20),To_Date,112),
     Person_NoPayPeriods ,
	 Directive_Number,
     Person_Acc_Type,
	 Person_Acc_No,
     Person_Branch_No,
     Person_Acc_Holder,
     BankBranchName='',
     Income=convert(int, Income),
     Non_Taxable_Income=convert(int, isnull(Non_Taxable_Income,0))
	from 	IncomeTax2008 IT,EmployerPAYE EP
	where 	EP.Product_Code	= @Prod
    and IT.cpy_Reference_Number = EP.PAYE_Reference_Number
    and EP.PAYE_Reference_Number = convert(numeric(10,0),@PAYEReferenceNumber)
    and TaxCertificateType = 'IRP5'

	SELECT	sum(CASE Tax WHEN 0 THEN 0 ELSE 1 END) IRP5Count,
    sum(CASE Tax WHEN 0 THEN 1 ELSE 0 END) IT3aCount
    from IncomeTax2008 IT,EmployerPAYE EP
    where 	EP.Product_Code	= @Prod
    and IT.cpy_Reference_Number = EP.PAYE_Reference_Number
    and EP.PAYE_Reference_Number = convert(numeric(10,0),@PAYEReferenceNumber)
    and EP.TaxCertificateType = 'IRP5'


END

GO
