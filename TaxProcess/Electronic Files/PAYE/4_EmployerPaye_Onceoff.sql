TRUNCATE TABLE EmployerPAYE
GO



DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit,
		@Month char(2),
		@Year char(4)


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates
 
 
 select @Year = convert(char(4), datepart(yy,@End_Date))


 if(datepart(MM,@End_Date) <= 9)
 begin
    select @Month = '0' + convert(char,datepart(MM,@End_Date))
 end
 else
  begin
    select @Month = convert(char,datepart(MM,@End_Date))
 end

 
IF((select Name from Tenant) = 'IMS')
 BEGIN
		INSERT INTO EmployerPAYE
		select 'ILLAA',
			   'Investec ILLA Division',
			   'TEST',
			   7490738442,
			   '',
			   '',
			   'Praan Pachai',
			   '0113024208',
			   'ppachai@silica.net',
			   '',
			   @Tax_Year,
			   convert(int,@Year + @Month), 
			   '64300',
			   '',
			   2522,
			   '',
			   '',
			   '',
			   '128 Peter Road',
			   'Sandown',
			   'Sandton',
			   '2146',
			   'ZA',
			   'IRP5'


		INSERT INTO EmployerPAYE
		select 'ILLAS',
			   'Sanlam Investment Linked Life Annuity (Gtee)',
			   'TEST',
			   7490722032,
			   '',
			   '',
			   'Praan Pachai',
			   '0113024208',
			   'ppachai@silica.net',
			   '',
			   @Tax_Year,
			   convert(int,@Year + @Month), 
			   '64300',
			   '',
			   2522,
			   '',
			   '',
			   '',
			   '128 Peter Road',
			   'Sandown',
			   'Sandton',
			   '2146',
			   'ZA',
			   'IRP5'
			   
			   --***Repurchase Data***
insert into EmployerPAYE
select distinct 
		r.Product_Code, 
		i.Product_Name,
		'TEST',
		r.cpy_Reference_Number,
		'',
		'',
		'Praan Pachai',
		'0113024208',
		'ppachai@silica.net',
		'',
		@Tax_Year,
		convert(int,@Year + @Month), 
		'64300',
		'',
		2522,
		'',
		'',
		'',
		'128 Peter Road',
		'Sandown',
		'Sandown',
		'2146',
		'ZA',
		'Repo'
from IncomeTax_RePurchase r, ProductDescription i
where r.Product_Code = i.Product_Code
and i.Language = 'E'

END
ELSE
BEGIN

INSERT INTO EmployerPAYE
select 'LA',
       'Ashburton Living Annuity',
       'TEST',
       7760783938,
       '',
       '',
       'Praan Pachai',
       '0113024208',
       'ppachai@silica.net',
       '',
	   @Tax_Year,
	   convert(int,@Year + @Month), 
       '64300',
       '',
       2522,
       '',
       '',
       '',
       '128 Peter Road',
       'Sandown',
       'Sandton',
       '2146',
	  'ZA',
       'IRP5'


INSERT INTO EmployerPAYE
select 'LAF',
       'Ashburton Living Annuity',
       'TEST',
       7760783938,
       '',
       '',
       'Praan Pachai',
       '0113024208',
       'ppachai@silica.net',
       '',
	   @Tax_Year,
	   convert(int,@Year + @Month), 
       '64300',
       '',
       2522,
       '',
       '',
       '',
       '128 Peter Road',
       'Sandown',
       'Sandton',
       '2146',
	  'ZA',
       'IRP5'


--***Repurchase Data***
insert into EmployerPAYE
select distinct 
		r.Product_Code, 
		i.Product_Name,
		'TEST',
		r.cpy_Reference_Number,
		'',
		'',
		'Praan Pachai',
		'0113024208',
		'ppachai@silica.net',
		'',
		@Tax_Year,
		convert(int,@Year + @Month), 
		'64300',
		'',
		2522,
		'',
		'',
		'',
		'128 Peter Road',
		'Sandown',
		'Sandown',
		'2146',
		'ZA',
		'Repo'
from IncomeTax_RePurchase r, ProductDescription i
where r.Product_Code = i.Product_Code
and i.Language = 'E'



update EmployerPAYE
set PAYE_Reference_Number = 7950783961
where Product_Code = 'RA'


update EmployerPAYE
set PAYE_Reference_Number = 7910783960
where Product_Code = 'PNP'


update EmployerPAYE
set PAYE_Reference_Number = 7240783979
where Product_Code = 'PRP'


update EmployerPAYE
set PAYE_Reference_Number = 7760783938
where Product_Code = 'LA'



update EmployerPAYE
set PAYE_Reference_Number = 7920789460
where Product_Code = 'LAF'



END



GO




