if object_id('EmployerPAYE') is not null 
drop table EmployerPAYE
go

  create table EmployerPAYE(
  Product_Code	varchar(5),/* Added by Abhishek*/
  Trading_Name                  varchar(90),     
  TEST_LIVE_indicator           varchar(4),     
  PAYE_Reference_Number         NUMERIC(10,0),     
  SDL_Reference_Number          varchar(10),     
  UIF_Reference_Number          varchar(10),     
  Employer_Contact_Person       varchar(30),     
  Employer_Contact_Number       varchar(11),     
  Employer_Email_address        varchar(70),     
  Payroll_Software              varchar(12),     
  Transaction_Year              int,     
  Period_of_Reconciliation      int,    
  Employer_SIC7_Code            varchar(5),     
  Employer_SEZ_Code             varchar(3),     
  Employer_Trade_Classification int,     
  Phys_Addr_Unit_Number         varchar(8),     
  Pys_Addr_Complex             varchar(26),     
  Phys_Addr_Street_Number       varchar(8),     
  Phys_Addr_Street_Name_of_Farm varchar(26),     
  Phys_Addr_Suburb_District     varchar(33),     
  Phys_Addr_City_Town           varchar(21),     
  Phys_Addr_Postal_Code         varchar(4),
  Phys_Addr_Country_Code        varchar(4), 
  TaxCertificateType varchar(4)/* IRP5/RP*/
)  
GO

