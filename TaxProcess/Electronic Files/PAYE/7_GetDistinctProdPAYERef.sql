IF(OBJECT_ID('GetDistinctProdPAYERef') IS NOT NULL)
drop proc GetDistinctProdPAYERef
go


create proc GetDistinctProdPAYERef
as
begin
SELECT distinct Product_Code, PAYE_Reference_Number
FROM EmployerPAYE
end
go