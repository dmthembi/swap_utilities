IF OBJECT_ID('#RepurchaseDetails') IS NOT NULL  drop table #RepurchaseDetails
IF OBJECT_ID('#TaxAmount') IS NOT NULL  drop table #TaxAmount
IF OBJECT_ID('Repurchase2009') IS NOT NULL  drop table Repurchase2009
IF OBJECT_ID('IncomeTax_RePurchase') IS NOT NULL  drop table IncomeTax_RePurchase
IF OBJECT_ID('#Test') IS NOT NULL drop table #Test 
IF OBJECT_ID('#banktls') IS NOT NULL drop table #banktls
IF OBJECT_ID('#S') IS NOT NULL drop table #S
IF OBJECT_ID('#dups') IS NOT NULL drop table #dups
GO
  --*************************************************************************/
   --   Create a Structure of the final output table same as IRP5Details2001
   --*************************************************************************/
   
CREATE TABLE #RepurchaseDetails
(
Owner                          int             null,
Product_Code                   varchar(5)      null,
Instruction_Number             int             NULL, -- additional Column for better understand
Income		               money           null,
Tax		               money           null,
Underwriter                    varchar(50)     null,
Owner_Name                     varchar(50)     null,
Line_1                         varchar(35)     null,
Line_2                         varchar(35)     null,
Line_3                         varchar(35)     null,
Line_4                         varchar(35)     null,
Line_5                         varchar(30)     null,
Postal_Code                    varchar(10)      null,
Country                        varchar(21)     null,
Phys_Addr_Country_Code         varchar(21)     null,
Id                             varchar(21)     null,
PassportNo                     varchar(21)     null,
Country_Code                   varchar(21)     null,
TrustNo                        varchar(21)     null,
Tax_No                         varchar(21)     null,
Tax_Office                     varchar(30)     null,
Commencement_Date              datetime        null,
To_Date                        datetime        null,
cpy_TradingName                varchar(70)     null,
cpy_IRP5Number                 int             null,
cpy_Reference_Number           numeric(10)     null,
cpy_TaxYear                    numeric(4)      null,
cpy_Address1                   varchar(45)     null,
cpy_Address2                   varchar(35)     null,
cpy_Address3                   varchar(35)     null,
cpy_Address4                   varchar(35)     null,
cpy_PostalCode                 numeric(4)      null,
cpy_Diplomatic                 varchar(1)      null,
PersonNature                   varchar(1)      null,
Person_Surname                 varchar(120)    null,
Person_Init                    varchar(5)      null,
Person_FullNames               varchar(90)     null,
Person_Birthdate               varchar(8)      null,
Person_Email				   varchar(120)    null,
Person_Home_Tel				   varchar(120)   		   null,
Person_Cellphone			   varchar(120)    		   null,
Person_Acc_Type				   Varchar(2)      null,			
Person_Acc_No				   Varchar(20)             null,
Person_Branch_No			   Varchar(20)             null,
Account_Holder_No              int             null,
Person_Acc_Holder              Varchar(120)             null,
Person_PayPeriods              varchar(1)      null,
Person_NoPayPeriods            int             null,
Printable                      int             null, 
Date_Action                    datetime        null,
Process                        varchar(2)      null,
JobID                          int             null,
Non_Taxable_Income             money           null,
FixedRateIncome                varchar(1)      null,
Directive_Number               varchar(50)     null,
Selected_Rate                  varchar(1)      null,
Adhoc                          smallint	  null,
Source_Code                     varchar(10)      null
)


DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates

select *
into #TaxYear
from TaxYearBeginDates
where Tax_Year = @Tax_Year


select distinct cd.Deal_Xref, People_Number = Owner, it.Instruction_Number, Product, Tax_Amount = PAYE_Amount + Tax_On_Interest,
Cash_Portion = RT.Amount, Interest_Earned,Tax_Certificate_Number, Source_Code
into #TaxAmount
from        RepurchaseTax rt,
            InstructionTransaction  it,
            Transactions t,
            ContractDeal cd,  
            RepurchaseTotals RT, 
            ProductTax Pt
where rt.Instruction_Number =  it.Instruction_Number 
and   RT.Instruction_Number =  it.Instruction_Number   
and it.Transaction_No = t.Transaction_No
and t.Deal_Xref  = cd.Deal_Xref
and rt.Capture_Date between @Start_Date and @End_Date
and rt.Tax_Status = 3
order by Product,Tax_Certificate_Number, Source_Code
-- 3305 record(s) affected 

UPDATE	#TaxAmount
SET	    People_Number = DP.People_Number
FROM	#TaxAmount D,
    	DealParticipants DP
WHERE	D.Deal_Xref = DP.Deal_Xref
AND	    DP.Role_Id = 1
AND	    DP.Member_Type = 'S'
-- 1346 record(s) affected 




 --*************************************************************************/
   --  Data pushing into Final output table
   --*************************************************************************/

insert into #RepurchaseDetails (Owner ,
Product_Code , 
Instruction_Number , 
cpy_IRP5Number, 
Tax,
Income, Source_Code
) 
select distinct People_Number, Product,Instruction_Number,  convert(int,Tax_Certificate_Number), Tax_Amount,
Cash_Portion + Interest_Earned, Source_Code from   #TaxAmount

--Update Remaining Columns in #RepurchaseDetails

  --*************************************************************************/
   --   Lookup Owner details
   --*************************************************************************/
   
     UPDATE #RepurchaseDetails
      SET Owner_Name        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name),
          Person_Surname    = LTRIM(P.Name),
          Person_Init       = LTRIM(P.Initials),
          Person_FullNames  = LTRIM(P.First_Names),
          PersonNature      = 'N'
     FROM People P
    WHERE #RepurchaseDetails.Owner = P.People_Number


   --*************************************************************************/
   --   Lookup Owner address
   --*************************************************************************/

   UPDATE #RepurchaseDetails
      SET Line_1 = LTRIM(PA.Line_1),
          Line_2 = LTRIM(PA.Line_2),
          Line_3 = LTRIM(PA.Line_3),
          Line_4 = LTRIM(PA.Line_4),
          Line_5 = LTRIM(PA.Line_5),
          Postal_Code = PA.Postal_Code
     FROM PeopleAddress PA
    WHERE #RepurchaseDetails.Owner = PA.People_Number
      AND PA.Address_Type = 'POST'
      --AND ISNULL(PA.Line_1, '') <> '' -- TYEP 2006. There are valid address with either line1 or line2 empty.
      --AND ISNULL(PA.Line_2, '') <> ''

   UPDATE #RepurchaseDetails
      SET Line_1 = LTRIM(PA.Line_1),
          Line_2 = LTRIM(PA.Line_2),
          Line_3 = LTRIM(PA.Line_3),
          Line_4 = LTRIM(PA.Line_4),
          Line_5 = LTRIM(PA.Line_5),
          Postal_Code = PA.Postal_Code
     FROM PeopleAddress PA
    WHERE #RepurchaseDetails.Owner = PA.People_Number
      AND PA.Address_Type = 'PHYS'
	AND NOT EXISTS (SELECT * FROM PeopleAddress PA2 WHERE PA2.People_Number = PA.People_Number AND PA2.Address_Type = 'POST')
     

   --*************************************************************************/
   --   Lookup Owner id no
   --*************************************************************************/

   UPDATE #RepurchaseDetails
      SET Id = PR.Reg_Value
     FROM PeopleRegistration PR
    WHERE #RepurchaseDetails.Owner = PR.People_Number
      AND PR.Reg_Type = 1

   --*************************************************************************/
   --   Lookup Owner passport no
   --*************************************************************************/

   UPDATE #RepurchaseDetails
      SET PassportNo = PR.Reg_Value
     FROM PeopleRegistration PR
    WHERE #RepurchaseDetails.Owner = PR.People_Number
      AND PR.Reg_Type = 2

   --*************************************************************************/
   --   Lookup Owner trust no
   --*************************************************************************/

   UPDATE #RepurchaseDetails
      SET TrustNo = PR.Reg_Value
     FROM PeopleRegistration PR
    WHERE #RepurchaseDetails.Owner = PR.People_Number
      AND PR.Reg_Type = 12

   --*************************************************************************/
   --   Lookup Owner birthdate
   --*************************************************************************/

   UPDATE #RepurchaseDetails
      SET Person_Birthdate = CONVERT(char(8), PD.Date, 112)
     FROM PeopleDates PD
    WHERE #RepurchaseDetails.Owner = PD.People_Number
      AND Date_Type = 'B'    --* VIA Change - Added reference to Birth Date
   
   --*************************************************************************/
   --   Lookup Owner tax details
   --*************************************************************************/

   UPDATE #RepurchaseDetails
      SET Tax_Office         = P.Name,
          Tax_No             = PT.Tax_Number
     FROM PeopleTax PT, People P
    WHERE #RepurchaseDetails.Owner = PT.People_Number
      AND PT.Date_Effective  = (
                               SELECT MAX(Date_Effective)
                                 FROM PeopleTax PT2
                                WHERE PT2.People_Number = #RepurchaseDetails.Owner
                               )
      AND PT.Tax_Office = P.People_Number

   --*************************************************************************/
   --   Update Commencement_Date information
   --*************************************************************************/
   UPDATE #RepurchaseDetails
    SET Commencement_Date   = '01 Mar 2014'

  --*************************************************************************/
   --   Update ADHOC information
   --*************************************************************************/
   UPDATE #RepurchaseDetails
    SET Adhoc   = 1

    --*************************************************************************/
   --   Update Directive_Number information
   --*************************************************************************/

   UPDATE #RepurchaseDetails
    SET   Directive_Number = R.Directive_Number
    from #RepurchaseDetails I, RepurchaseTax R
    where convert(int,isnull(R.Tax_Certificate_Number,null)) = isnull(I.cpy_IRP5Number,null)
    and   R.Instruction_Number = I.Instruction_Number
    and   R.Tax_Status  = 3
    and  isnumeric(R.Tax_Certificate_Number) <> 0

    --*************************************************************************/
   --   Update To_Date information
   --*************************************************************************/
   UPDATE #RepurchaseDetails
    SET   To_Date  = R.Capture_Date
     from #RepurchaseDetails I, RepurchaseTax R
	where convert(int,isnull(R.Tax_Certificate_Number,null)) = isnull(I.cpy_IRP5Number,null)
    and   R.Instruction_Number = I.Instruction_Number
    and   R.Tax_Status  = 3
and  isnumeric(R.Tax_Certificate_Number) <> 0
 

   --*************************************************************************/
   --   Update pay-period information
   --*************************************************************************/


   UPDATE #RepurchaseDetails
      SET Person_PayPeriods   = 'M',
          Person_NoPayPeriods = DATEDIFF(mm, Commencement_Date, To_Date) + 1  
   --*************************************************************************/
   --   Update employer information
   --*************************************************************************/

   
   
  
IF ((SELECT Name FROM Tenant) = 'RMB')
BEGIN
 UPDATE #RepurchaseDetails
      SET cpy_TaxYear = (select Tax_Year from #TaxYear),
          cpy_Reference_Number = 
             CASE Product_Code
				    WHEN  'LA'  	THEN 	7760783938     
					WHEN  'LAF'  	THEN 	7920789460    					
					WHEN  'RA'      THEN 	7950783961                    
					WHEN  'PNP' 	THEN 	7910783960              
					WHEN  'PRP' 	THEN 	7240783979    
             END,
          cpy_TradingName = 'Ashburton Investor Services (Pty) Ltd',
          cpy_Address1 = 
             CASE Product_Code
				    WHEN  'LA'  	THEN 	'XXXXXXXXXXXXXXXXXXXX'
					WHEN  'LAF'  	THEN 	'XXXXXXXXXXXXXXXXXXXX'
					WHEN  'RA'      THEN 	'XXXXXXXXXXXXXXXXXXXX'                    
					WHEN  'PNP' 	THEN 	'XXXXXXXXXXXXXXXXXXXX'
					WHEN  'PRP' 	THEN 	'XXXXXXXXXXXXXXXXXXXX'
             END,
          cpy_Address2 = '2 Merchant Place',
          cpy_Address3 = '1 Fredman Drive',
          cpy_Address4 = 'Sandton',
          cpy_PostalCode = 2196


END
ELSE
BEGIN
   
   UPDATE #RepurchaseDetails
      SET cpy_TaxYear = (select Tax_Year from #TaxYear),
          cpy_Reference_Number = 
             CASE Product_Code
				    WHEN  'VIA'  	THEN 	7230719157          
					WHEN  'FULA' 	THEN 	7330732613                    
					WHEN  'ILLA' 	THEN 	7230719157              
					WHEN  'ILPF' 	THEN 	7760719833    
					WHEN  'ILPP' 	THEN 	7730719830        
					WHEN  'IPPF' 	THEN 	7740719838 
					WHEN  'IPPP' 	THEN 	7750719835 
					WHEN  'IRAF' 	THEN 	7340732611         
					WHEN  'ILLAA' 	THEN  	7490738442             
					WHEN  'ILLAB'   THEN 	7230719157                  
					WHEN  'ILLAG' 	THEN 	7490722032                            
					WHEN  'ILLAI' 	THEN 	7490722032                         
					WHEN  'ILLAS' 	THEN 	7490722032                        
					WHEN  'ILLAV' 	THEN 	7490738442           
					WHEN  'ILPFT' 	THEN 	7380732612   
					WHEN  'ILPPT' 	THEN 	7410732616        
					WHEN  'INVLA' 	THEN 	7490738442         
					WHEN  'INVPF' 	THEN 	7740719838
					WHEN  'INVPP' 	THEN 	7750719835                  
					WHEN  'INVRA' 	THEN 	7340732611             
					WHEN  'IRAF2' 	THEN 	7340732611                  
					WHEN  'WPPPF' 	THEN 	7530760950  
             END,
          cpy_TradingName = 'Investec IMS',
          cpy_Address1 = 
             CASE Product_Code
				WHEN 'ILLA' 	THEN 'INVESTEC LINKED LIFE ANNUITY (FEDLIFE)'
				WHEN 'VIA'  	THEN 'INVESTEC LINKED LIFE ANNUITY (FEDLIFE)'
				WHEN 'ILLAB' 	THEN 'INVESTEC LINKED LIFE ANNUITY (FEDLIFE)'
				WHEN 'FULA' 	THEN 'FEDLIFE UNIVERSAL LIFE ANNUITY'
				WHEN 'IRAF' 	THEN 'INDEPENDENT RETIREMENT ANNUITY FUND'
				WHEN 'IRAF' 	THEN 'INDEPENDENT RETIREMENT ANNUITY FUND'
				WHEN 'INVRA' 	THEN 'INDEPENDENT RETIREMENT ANNUITY FUND'
				WHEN 'ILPPT'   	THEN 'INDEPENDENT PENSION FUND'  --* VIA CHANGE - SAME AS FOR ILLA
               	WHEN 'ILPFT' 	THEN 'INDEPENDENT PROVIDENT FUND'
               	WHEN 'ILLAS' 	THEN 'INVESTEC LINKED LIFE ANNUITY (SANLAM)'
               	WHEN 'ILLAG' 	THEN 'INVESTEC LINKED LIFE ANNUITY (SANLAM)'
               	WHEN 'ILLAI' 	THEN 'INVESTEC LINKED LIFE ANNUITY (SANLAM)'
               	WHEN 'ILLAV' 	THEN 'INVESTEC LINKED LIFE ANNUITY'
               	WHEN 'ILLAA' 	THEN 'INVESTEC LINKED LIFE ANNUITY'
               	WHEN 'INVLA' 	THEN 'INVESTEC LINKED LIFE ANNUITY'
               	WHEN 'WPPPF' 	THEN 'WEALTH PROTECTOR PRESERVATION PROVIDENT FUND'
               	WHEN 'ILPP' 	THEN 'INVESTEC LINKED PENSION PLAN'
               	WHEN 'IPPF'  	THEN 'INVESTEC LINKED PRESERVATION PROVIDENT FUND' ----TMA
               	WHEN 'INVPF'  	THEN 'INVESTEC LINKED PRESERVATION PROVIDENT FUND' ----TMA
               	WHEN 'IPPP' 	THEN 'INVESTEC LINKED PRESERVATION PENSION PLAN' ----TMA
               	WHEN 'INVPP' 	THEN 'INVESTEC LINKED PRESERVATION PENSION PLAN' ----TMA
               	WHEN 'ILPF'  	THEN 'INVESTEC LINKED PROVIDENT FUND'
             END,
          cpy_Address2 = '100 Grayston Drive',
          cpy_Address3 = 'Sandown',
          cpy_Address4 = 'Sandton',
          cpy_PostalCode = 2146
END


   --*************************************************************************/
   --   Determine if a Deal_Xref has a tax directive
   --*************************************************************************/


      UPDATE #RepurchaseDetails
         SET Selected_Rate = 'Y'
        FROM #RepurchaseDetails i,
             OwnerTax p,
              #TaxYear t
       WHERE p.Owner      = i.Owner
         AND p.Start_Date BETWEEN t.Begin_Date and t.End_Date
         AND p.Tax_Type   = 1 -- Selected Tax Rate

      UPDATE #RepurchaseDetails
         SET Selected_Rate = 'N'
        FROM #RepurchaseDetails i
       WHERE Selected_Rate <> 'Y' OR Selected_Rate IS NULL

   --*************************************************************************/
   --    --   Determine if a FixedRateIncome has a tax directive
   --*************************************************************************/


      UPDATE #RepurchaseDetails
         SET FixedRateIncome = 'N'
        FROM #RepurchaseDetails i
       WHERE FixedRateIncome <> 'Y' OR FixedRateIncome IS NULL

   --*************************************************************************/
   --    --   Determine if a Country_Cd
   --*************************************************************************/

/****
      UPDATE #RepurchaseDetails
         SET Country_Code = Case 
                            when Id is null and PassportNo is not null then 'ZNC'
                            END
****/						

	update #RepurchaseDetails
	set Country_Code = pc.Code
	from PeopleCountry pc inner join #RepurchaseDetails i
	on i.Owner = pc.People_Number	
	WHERE pc.Country_Type = 'PA'	


update #RepurchaseDetails 
	set Country_Code = 'ZA'
	where Country_Code in ('ZAR','ZAF')   -- DM. 06 Sep 2016. No consistancy in PeopleCountry table...
	
	update #RepurchaseDetails
	set Country_Code = 'ZA'
	where Country_Code is null
	
	update #RepurchaseDetails
   set PassportNo = ''
   where Country_Code = 'ZA'


	update #RepurchaseDetails
	set Country_Code = cm.SARS_CountryCode
	from #RepurchaseDetails it inner join CountryMap cm
	on cm.ISO_CountryCode = it.Country_Code
	

update #RepurchaseDetails
set PassportNo = null
where Country_Code = ''
and Phys_Addr_Country_Code = 'ZA'	


	

--select * from #RepurchaseDetails
 --*************************************************************************/
   --    --   Account Details
   --*************************************************************************/

Select Start_Date, Owner 
into #Test 
from PeopleBank C, #RepurchaseDetails D 
where C.People_Number = D.Owner 
and C.People_Number in (select Owner from #RepurchaseDetails)

 select         T.Start_Date, R.Owner, Account_Type = CASE Account_Type
               WHEN 'C'  THEN '1'
               WHEN 'S'   THEN '2'  --* VIA CHANGE - SAME AS FOR ILLA
               WHEN 'T' THEN '3'
               WHEN 'E'  THEN '4' ----TMA
               WHEN 'I' THEN '5'----TMA
               END,
         Person_Acc_No      = Account,
         Person_Branch_No  =  Branch,
         Account_Holder_No   = Account_Holder 
    into #banktls
      from #RepurchaseDetails R, PeopleBank B, #Test T
      where  B.People_Number = R.Owner
      AND   R.Owner = T.Owner
      And B.Start_Date  =  T.Start_Date
     
select Start_Date = max(Start_Date), Owner into #S from #banktls B group by Owner

      UPDATE #RepurchaseDetails
         SET Person_Acc_Type =   Account_Type,
         Person_Acc_No      = B.Person_Acc_No,
         Person_Branch_No  =  B.Person_Branch_No,
         Account_Holder_No   = B.Account_Holder_No
      from #RepurchaseDetails R, #banktls B, #S S
      where B.Owner = R.Owner
      AND   B.Owner = S.Owner
      AND B.Start_Date = S.Start_Date

--select * from   #banktls
 UPDATE #RepurchaseDetails
      SET Person_Acc_Holder        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name)
        FROM People P
    WHERE #RepurchaseDetails.Owner = P.People_Number

--select * from #RepurchaseDetails
 --*************************************************************************/
   --    --   Update Phone and Email Details
   --*************************************************************************/

      UPDATE #RepurchaseDetails
         SET Person_Email = Phone_Number
      from #RepurchaseDetails R, PeoplePhone B
      where Owner = B.People_Number
      AND Phone_Type = 'EM'
      
      UPDATE #RepurchaseDetails
         SET Person_Home_Tel = Phone_Number
      from #RepurchaseDetails R, PeoplePhone B
      where Owner = B.People_Number
      AND Phone_Type = 'HM'
      
      UPDATE #RepurchaseDetails
         SET Person_Cellphone = Phone_Number
      from #RepurchaseDetails R, PeoplePhone B
      where Owner = B.People_Number
      AND Phone_Type = 'C'

   --*************************************************************************/
   --   Output
   --*************************************************************************/
   
   
update #RepurchaseDetails
set    Country = pa.Line_5
from #RepurchaseDetails i inner join PeopleAddress pa on i.Owner = pa.People_Number
and pa.Address_Type = 'POST'
and isnull(pa.Line_5,'') <> ''

update #RepurchaseDetails
set Phys_Addr_Country_Code = ISO_CountryCode
from #RepurchaseDetails i inner join CountryMap cm on i.Country = cm.ISO_Descrpition
   
   
 
update #RepurchaseDetails
set Phys_Addr_Country_Code = pc.Code
from #RepurchaseDetails i inner join PeopleCountry pc
on i.Owner = pc.People_Number
where pc.Country_Type in ('RE','RR','RT','SR','FR')
and i.Phys_Addr_Country_Code is null 


update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%london%' 


update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%u.k%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%australia%' 
and Phys_Addr_Country_Code is null


update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%united%kingdom%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%united%kingdom%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%england%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where ltrim(rtrim(lower(Country))) = 'uk' 
and Phys_Addr_Country_Code is null


update #RepurchaseDetails
set Phys_Addr_Country_Code = 'GB'
where ltrim(rtrim(lower(Country))) = 'uk' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'US'
where ltrim(rtrim(lower(Country))) like 'usa' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'US'
where ltrim(rtrim(lower(Country))) like '%united%states%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'US'
where ltrim(rtrim(lower(Country))) like '%united%states%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'NA'
where ltrim(rtrim(lower(Country))) like '%namib%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'TW'
where ltrim(rtrim(lower(Country))) like '%taiwan%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'CA'
where ltrim(rtrim(lower(Country))) like '%canad%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'TZ'
where ltrim(rtrim(lower(Country))) like '%tanzania%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'IE'
where ltrim(rtrim(lower(Country))) like '%ireland%' 
and Phys_Addr_Country_Code is null

update #RepurchaseDetails
set Phys_Addr_Country_Code = 'AE'
where ltrim(rtrim(lower(Country))) like '%arab%' 
and Phys_Addr_Country_Code is null


update #RepurchaseDetails
set Phys_Addr_Country_Code = 'ZA'
where  Phys_Addr_Country_Code is null  




--drop table IncomeTax2008_Re
 select Owner,
   Product_Code,
   Income,
   Tax,
   Underwriter,
   Owner_Name,
   Line_1,
   Line_2,
   Line_3, 
   Line_4,
   Line_5,
   Postal_Code,
   Country,
   Phys_Addr_Country_Code,
   Id, 
   PassportNo, 
   Country_Code,
   TrustNo,
   Tax_No, 
   Tax_Office, 
   Commencement_Date,
   To_Date,
   cpy_TradingName,
   cpy_IRP5Number,
   cpy_Reference_Number, 
   cpy_TaxYear, 
   cpy_Address1, 
   cpy_Address2,  
   cpy_Address3,     
   cpy_Address4,     
   cpy_PostalCode,     
   cpy_Diplomatic,     
   PersonNature,
   Person_Surname,                 
   Person_Init,     
   Person_FullNames,             
   Person_Birthdate,  
   Person_Acc_Type, 
   Person_Acc_No,
   Person_Branch_No,   
   Person_Acc_Holder, 
   Person_PayPeriods,     
   Person_NoPayPeriods, 
   Printable, 
   Date_Action, 
   Process, 
   JobID,
   Non_Taxable_Income, 
  FixedRateIncome,     
  Directive_Number,
   Selected_Rate,     
   Adhoc, 
   Source_Code, 
   Person_Email,	
   Person_Home_Tel,
   Person_Cellphone,
   TaxRef = convert(char(10),null) , 
   Instruction_Number, 
   DuplicateTaxNumber = convert(bit,'0'),
   DupNumber = convert(int,null)
into     Repurchase2009 
from  #RepurchaseDetails



select num = identity(10), cpy_IRP5Number,  Product_Code, cnt = count(*)
into #dups
from Repurchase2009
group by  cpy_IRP5Number,  Product_Code
having count(*) > 1
order by cpy_IRP5Number

/**
select *
from #dups
order by cpy_IRP5Number


select r.DuplicateTaxNumber, r.DupNumber, d.num, r.cpy_IRP5Number, r.Product_Code, Instruction_Number, r.*
from Repurchase2009 r, #dups d
where r.cpy_IRP5Number = d.cpy_IRP5Number
and r.Product_Code = d.Product_Code
order by r.cpy_IRP5Number
**/

update Repurchase2009
set DuplicateTaxNumber = 1,
    DupNumber = num
from Repurchase2009 r, #dups d
where r.cpy_IRP5Number = d.cpy_IRP5Number
and r.Product_Code = d.Product_Code    


select * into IncomeTax_RePurchase 
from Repurchase2009 

update IncomeTax_RePurchase
set TaxRef = 'IT3(a)' 
where Tax = 0

update IncomeTax_RePurchase
set TaxRef = 'IRP5' 
where Tax <>  0

/**
update IncomeTax_RePurchase
set PersonNature = 'N'
**/

Update IncomeTax_RePurchase
set Person_Init = str_replace(Person_Init,' ',NULL)

 UPDATE IncomeTax_RePurchase
      SET Owner_Name        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name),
          Person_Surname    = LTRIM(P.Name),
          Person_Init       = LTRIM(P.Initials),
          Person_FullNames  = LTRIM(P.First_Names),
          PersonNature      = 'N'
     FROM People P
    WHERE IncomeTax_RePurchase.Owner = P.People_Number

Update IncomeTax_RePurchase
set Person_Init = str_replace(Person_Init,' ',NULL)

GO


--select * from IncomeTax_RePurchase
--order by Product_Code, cpy_IRP5Number
