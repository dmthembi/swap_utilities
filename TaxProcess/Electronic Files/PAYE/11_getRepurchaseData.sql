

if object_id('getRepurchaseData') is not null
drop proc getRepurchaseData
go

create proc getRepurchaseData @cpy_Reference_Number numeric(10,0) , @Prod varchar(6) 
AS
BEGIN
				
SELECT * FROM EmployerPAYE where Product_Code = @Prod and TaxCertificateType = 'Repo'


  	
SELECT
        Owner,
        ITR.Product_Code,
        Income = round(Income,0),
        Tax,
        Underwriter,
        Owner_Name,
        Line_1,
        Line_2,
        Line_3,
        Line_4,
        Line_5,
        Postal_Code,
        Id,
        PassportNo,
		ITR.Phys_Addr_Country_Code,
        Country_Code,
        TrustNo,Tax_No,
        Tax_Office,
        Commencement_Date = '20' + convert(varchar(22),Commencement_Date,12),
        To_Date = '20' + convert(varchar(22),To_Date,12),
        cpy_TradingName,
        cpy_IRP5Number,
        cpy_Reference_Number,
        cpy_TaxYear,
        cpy_Address1,
        cpy_Address2,
        cpy_Address3,
        cpy_Address4,
        cpy_PostalCode,
        cpy_Diplomatic,
        PersonNature,
        Person_Surname,
        Person_Init,
        Person_FullNames,
        Person_Birthdate,
        Person_Acc_Type,
        Person_Acc_No,
        Person_Branch_No,
        Person_Acc_Holder,
        Person_PayPeriods = 1,
        Person_NoPayPeriods = 1,
        Printable,
        Date_Action,Process,
        JobID,
        Non_Taxable_Income,
        FixedRateIncome,
        Directive_Number,
        Selected_Rate,
        Adhoc,
        Source_Code,
        Person_Email,
        Person_Home_Tel,
        Person_Cellphone,
        TaxRef,
        DuplicateTaxNumber,
        DupNumber    
FROM 	IncomeTax_RePurchase ITR,EmployerPAYE EP
WHERE 	ITR.Product_Code	= @Prod
AND ITR.cpy_Reference_Number = @cpy_Reference_Number
AND EP.Product_Code = ITR.Product_Code
AND ITR.cpy_Reference_Number = EP.PAYE_Reference_Number
AND EP.TaxCertificateType  = 'Repo'

end
GO
























