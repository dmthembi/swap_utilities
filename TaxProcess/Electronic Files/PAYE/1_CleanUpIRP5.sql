

IF OBJECT_ID('IncomeTax2008') IS NOT NULL  Drop table IncomeTax2008  
GO


DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates



select *, 
TaxRef = convert(char(10),null),
Country_Code = convert(varchar(21),null),
Country = convert(varchar(21),'South Africa'),
Phys_Addr_Country_Code = convert(varchar(21),null),
Residential_Addr_Country_Code = convert(varchar(21),null),
Person_Email= convert(varchar(120),null),
Person_Home_Tel= convert(varchar(120),null),
Person_Cellphone= convert(varchar(120),null),
Person_Acc_Type= convert(varchar(2),null),	
Person_Acc_No= convert(varchar(20),null),
Person_Branch_No= convert(varchar(20),null),
Account_Holder_No= convert(int,0),
Person_Acc_Holder = convert(varchar(120),null)
into IncomeTax2008 
from IRP5Details2001 
where cpy_TaxYear = @Tax_Year
GO



update IncomeTax2008
set    Country = pa.Line_5
from IncomeTax2008 i inner join PeopleAddress pa on i.Owner = pa.People_Number
and pa.Address_Type = 'POST'
and isnull(pa.Line_5,'') <> ''

update IncomeTax2008
set Phys_Addr_Country_Code = ISO_CountryCode
from IncomeTax2008 i inner join CountryMap cm on i.Country = cm.ISO_Descrpition





update IncomeTax2008
set Phys_Addr_Country_Code = pc.Code
from IncomeTax2008 i inner join PeopleCountry pc
on i.Owner = pc.People_Number
where pc.Country_Type in ('RE','RR','RT','SR','FR')
and i.Phys_Addr_Country_Code is null 


update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%london%' 


update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%u.k%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%australia%' 
and Phys_Addr_Country_Code is null


update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%united%kingdom%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%united%kingdom%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where lower(Country) like '%england%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where ltrim(rtrim(lower(Country))) = 'uk' 
and Phys_Addr_Country_Code is null


update IncomeTax2008
set Phys_Addr_Country_Code = 'GB'
where ltrim(rtrim(lower(Country))) = 'uk' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'US'
where ltrim(rtrim(lower(Country))) like 'usa' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'US'
where ltrim(rtrim(lower(Country))) like '%united%states%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'US'
where ltrim(rtrim(lower(Country))) like '%united%states%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'NA'
where ltrim(rtrim(lower(Country))) like '%namib%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'TW'
where ltrim(rtrim(lower(Country))) like '%taiwan%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'CA'
where ltrim(rtrim(lower(Country))) like '%canad%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'TZ'
where ltrim(rtrim(lower(Country))) like '%tanzania%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'IE'
where ltrim(rtrim(lower(Country))) like '%ireland%' 
and Phys_Addr_Country_Code is null

update IncomeTax2008
set Phys_Addr_Country_Code = 'AE'
where ltrim(rtrim(lower(Country))) like '%arab%' 
and Phys_Addr_Country_Code is null


update IncomeTax2008
set Phys_Addr_Country_Code = 'ZA'
where  Phys_Addr_Country_Code is null


update IncomeTax2008
set TaxRef = 'IT3(a)' 
where Tax = 0

update IncomeTax2008
set TaxRef = 'IRP5' 
where Tax <>  0

update IncomeTax2008
set PersonNature = 'N'

Update IncomeTax2008
set Person_Init = str_replace(Person_Init,' ',NULL)

 UPDATE IncomeTax2008
      SET Owner_Name        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name),
          Person_Surname    = LTRIM(P.Name),
          Person_Init       = LTRIM(P.Initials),
          Person_FullNames  = LTRIM(P.First_Names),
          PersonNature      = 'N'
     FROM People P
    WHERE IncomeTax2008.Owner = P.People_Number

Update IncomeTax2008
set Person_Init = str_replace(Person_Init,' ',NULL)


select '**********TEST*****************'

  --*************************************************************************/
   --    --   Determine if a Country_Cd
   --*************************************************************************/


   --   UPDATE IncomeTax2008
   --      SET Country_Code = Case 
   --                         when Id is null and PassportNo is not null then 'ZNC'
   --                         END
							
	update IncomeTax2008
	set Country_Code = pc.Code
	from PeopleCountry pc inner join IncomeTax2008 i
	on i.Owner = pc.People_Number	
	WHERE pc.Country_Type = 'PA'
	
	update IncomeTax2008 
	set Country_Code = 'ZA'
	where Country_Code in ('ZAR','ZAF')   -- DM. 06 Sep 2016. No consistancy in PeopleCountry table...
	
	update IncomeTax2008
	set Country_Code = 'ZA'
	where Country_Code is null
	
	update IncomeTax2008
   set PassportNo = ''
   where Country_Code = 'ZA'


	update IncomeTax2008
	set Country_Code = cm.SARS_CountryCode
	from IncomeTax2008 it inner join CountryMap cm
	on cm.ISO_CountryCode = it.Country_Code
	




update IncomeTax2008
set PassportNo = null
where Country_Code = ''
and Phys_Addr_Country_Code = 'ZA'	

--select * from #RepurchaseDetails
 --*************************************************************************/
   --    --   Account Details
   --*************************************************************************/
/**

      UPDATE IncomeTax2008
         SET Person_Acc_Type = 
         CASE Account_Type
               WHEN 'C'  THEN '1'
               WHEN 'S'   THEN '2'  --* VIA CHANGE - SAME AS FOR ILLA
               WHEN 'T' THEN '3'
               WHEN 'E'  THEN '4' ----TMA
               WHEN 'I' THEN '5'----TMA
               END,
         Person_Acc_No	     = Account,
         Person_Branch_No	 =		Branch,
         Account_Holder_No   = Account_Holder
      from IncomeTax2008 R, PeopleBank B, DealActiveBank DB, ContractDeal T 
      where B.Bank_Number = DB.Bank_Number 
      AND   DB.Deal_Xref = T.Deal_Xref
      AND   R.Owner      = T.Owner
      AND   B.People_Number = R.Owner
      --AND People_Number = 4137
**/      
      --UPDATE #RepurchaseDetails
 --        SET Person_Acc_Type = 




--DM. 2015 Oct 14. This is rubish.....
 Select Start_Date, Owner
 into #Test 
 from PeopleBank C, IncomeTax2008 D 
 where C.People_Number = D.Owner 
 and C.People_Number in (select Owner from IncomeTax2008)


 
 select        T.Start_Date, R.Owner, Account_Type = CASE Account_Type
               WHEN 'A'  THEN '2'    --* Call Acount is like savings account
               WHEN 'C'  THEN '1'
               WHEN 'S'   THEN '2'  --* VIA CHANGE - SAME AS FOR ILLA
               WHEN 'T' THEN '3'
               WHEN 'E'  THEN '4' ----TMA
               WHEN 'F' THEN  '2' ---- Fixed Accountis Savings account...
               WHEN 'I' THEN '5'----TMA
               WHEN 'J' THEN '2'
               WHEN 'H' THEN '0'
               WHEN 'N' THEN '2' -- 33 DAY NOTICE CAN ONLY BE SAVINGS ACCOUNT
               WHEN 'Z' THEN '0'  --Unknown can only be zero...
               END,
         Person_Acc_No      = Account,
         Person_Branch_No  =  Branch,
         Account_Holder_No   = Account_Holder 
      into #banktls
      from IncomeTax2008 R, PeopleBank B, #Test T
      where  B.People_Number = R.Owner
      AND   R.Owner = T.Owner
      And B.Start_Date  =  T.Start_Date



--select * from   #banktls

select Start_Date = max(Start_Date), Owner into #S from #banktls B group by Owner

      UPDATE IncomeTax2008
         SET Person_Acc_Type =   Account_Type,
         Person_Acc_No      = B.Person_Acc_No,
         Person_Branch_No  =  B.Person_Branch_No,
         Account_Holder_No   = B.Account_Holder_No
      from IncomeTax2008 R, #banktls B, #S S
      where B.Owner = R.Owner
      AND   B.Owner = S.Owner
      AND B.Start_Date = S.Start_Date
      and B.Account_Type is not null


update  IncomeTax2008
         SET Person_Acc_Type = '0' --No paid by electronic bank transfer
where Person_Acc_Type is null


 UPDATE IncomeTax2008
      SET Person_Acc_Holder        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name)
        FROM People P
    WHERE IncomeTax2008.Owner = P.People_Number

--select * from #RepurchaseDetails
 --*************************************************************************/
   --    --   Update Phone and Email Details
   --*************************************************************************/

      UPDATE IncomeTax2008
         SET Person_Email = Phone_Number
      from IncomeTax2008 R, PeoplePhone B
      where Owner = B.People_Number
      AND Phone_Type = 'EM'
      
      UPDATE IncomeTax2008
         SET Person_Home_Tel = Phone_Number
      from IncomeTax2008 R, PeoplePhone B
      where Owner = B.People_Number
      AND Phone_Type = 'HM'
      
      UPDATE IncomeTax2008
         SET Person_Cellphone = Phone_Number
      from IncomeTax2008 R, PeoplePhone B
      where Owner = B.People_Number
      AND Phone_Type = 'C'


--select * from IncomeTax2008
-- Clean Up



update IncomeTax2008
set TrustNo = substring(TrustNo,1, datalength(TrustNo) - 1)
where ascii(substring(TrustNo,datalength(TrustNo),1)) not between 33 and 126

GO

select 'Update Person Nature. Assumption : A'


update IncomeTax2008
set PersonNature    =  'A'
where PersonNature    is null


go



set nocount on
select 'Remove Non Integers from ID numbers'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Id             varchar(20),
        @Count          int



select Owner, Product_Code, cpy_IRP5Number, Id
into  #Cursor
from IncomeTax2008
WHERE  PersonNature  IN ('A','C')


select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Id = Id
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Id , datalength(@Id)
   select @Count = 1
   while (@Count <= datalength(@Id))
   begin
    -- select substring(@Id, @Count, 1)
     if ascii(substring(@Id, @Count, 1)) not between 48 and 57
       begin
       select @Owner, @Id, @Count
      update IncomeTax2008
       set Id = stuff(Id,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number

       and   PersonNature  IN ('A','C')
        
       select @Count = @Count - 1

       select @Id = Id
       from  IncomeTax2008
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Id = Id
from  #Cursor
set rowcount 0

end




select 'Name contains invalid characters'


drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from Surname'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int



select Owner, Product_Code, cpy_IRP5Number, Person_FullNames, Person_Surname
into  #Cursor
from IncomeTax2008
WHERE PersonNature  IN ('A','B','C')


select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Surname = Person_Surname
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
    -- select substring(@Surname, @Count, 1)
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Surname, @Count
       update IncomeTax2008
       set Person_Surname = stuff(Person_Surname,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','B','C')
       select @Owner, @Surname, @Count
        
       select @Count = @Count - 1

       select @Surname = Person_Surname
       from  IncomeTax2008
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number

     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Surname = Person_Surname
from  #Cursor
set rowcount 0

end


drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from FullNames'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Person_FullNames        varchar(20),
        @Count          int



select Owner, Product_Code, cpy_IRP5Number, Person_FullNames, Person_Surname
into  #Cursor
from IncomeTax2008
WHERE PersonNature  IN ('A','B','C')


select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_FullNames = Person_FullNames
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Person_FullNames , datalength(@Person_FullNames)
   select @Count = 1
   while (@Count <= datalength(@Person_FullNames))
   begin
    -- select substring(@Person_FullNames, @Count, 1)
     if (((ascii(substring(@Person_FullNames, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Person_FullNames, @Count

       update IncomeTax2008
       set Person_FullNames = stuff(Person_FullNames,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','B','C')

       select @Owner, @Person_FullNames, @Count
        
       select @Count = @Count - 1

       select @Person_FullNames = Person_FullNames
       from  IncomeTax2008
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_FullNames = Person_FullNames
from  #Cursor
set rowcount 0

end


drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from Person_Init'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Person_Init        varchar(5),
        @Count          int



select Owner, Product_Code, cpy_IRP5Number, Person_Init
into  #Cursor
from IncomeTax2008
WHERE PersonNature  IN ('A','B','C')


select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_Init = Person_Init
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, datalength(@Person_Init)
   select @Count = 1
   while (@Count <= datalength(@Person_Init))
   begin
    -- select substring(@Person_Init, @Count, 1)
     if (((ascii(substring(@Person_Init, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Person_Init, @Count

       update IncomeTax2008
       set Person_Init = stuff(Person_Init,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','B','C')

       select @Owner, @Person_Init, @Count
        
       select @Count = @Count - 1

       select @Person_Init = Person_Init
       from  IncomeTax2008
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_Init = Person_Init
from  #Cursor
set rowcount 0

end



SELECT 'Append the last ID character to the remaining missing characters'



update IncomeTax2008
set Id = Id + replicate(substring(Id,datalength(Id),1), 13 - datalength(Id))
WHERE PersonNature  IN ('A','C')
and datalength(Id) < 13


SELECT 'IF ID is null, replace with DBO + 1111111'



update IncomeTax2008
set Id  =  substring(Person_Birthdate,3,8) + '1111111'
where   PersonNature     in ('A', 'C') 
and  Id               is null
and  Person_Birthdate is not null

go


SELECT 'IF no DBO, assume Person Nature D'



UPDATE IncomeTax2008
SET PersonNature  =  'D'
WHERE PersonNature  IN ('A', 'B', 'C')
and  Person_Birthdate is null

GO

SELECT 'Invalid month in Id date'



UPDATE IncomeTax2008
SET Id   =   stuff(Id, 3, 2, substring(Person_Birthdate,5,2))
WHERE convert(int,substring(Id, 3,2)) > 12
and  PersonNature in ('A','C')


SELECT 'Invalid day in Id date'


UPDATE IncomeTax2008
SET Id   =   stuff(Id, 5, 2, substring(Person_Birthdate,7,2))
where convert(int,substring(Id, 5,2)) > 31
and  PersonNature in ('A','C')


SELECT 'ID does not CORRELATE with DOB'

SELECT ' YY '



UPDATE IncomeTax2008
SET Person_Birthdate = stuff(Person_Birthdate, 3, 2, substring(Id,1,2))
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
and    convert(char(2),substring(Id,1,2)) <> convert(char(2),substring(Person_Birthdate,3,2) )  -- YY



UPDATE IncomeTax2008
SET Person_Birthdate = stuff(Person_Birthdate, 5, 2, substring(Id,3,2))
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
and  convert(char(2),substring(Id,3,2)) <> convert(char(2),substring(Person_Birthdate,5,2) )   --MM



UPDATE IncomeTax2008
SET Person_Birthdate = stuff(Person_Birthdate, 7, 2, substring(Id,5,2))
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
and   convert(char(2),substring(Id,5,2)) <> convert(char(2),substring(Person_Birthdate,7,2))   --DD



SELECT 'Invalid DOB'


SELECT 'Update the remaining ID Numbers'



UPDATE IncomeTax2008
SET Id   =   stuff(Id, 1, 6, substring(Person_Birthdate,3,8))
WHERE PersonNature  IN ('A','C')
AND  ( ( convert(char(2),substring(Id,1,2)) <> convert(char(2),substring(Person_Birthdate,3,2) ) ) -- YY
    OR (convert(char(2),substring(Id,3,2)) <> convert(char(2),substring(Person_Birthdate,5,2) ))   --MM
    OR (convert(char(2),substring(Id,5,2)) <> convert(char(2),substring(Person_Birthdate,7,2)) ))  --DD

go

SELECT 'ID Number greater than 13 characters'


update IncomeTax2008
set Id = substring(Id, 1, datalength(Id) - 1)
WHERE  PersonNature  IN ('A','C')
and datalength(Id) > 13



update IncomeTax2008
set Person_Birthdate = stuff(Person_Birthdate, 7, 2, '01'),
    Id = stuff(Id, 5,2,'01')
where	convert(int,substring(Person_Birthdate,7,2)) not between 1 and 31
and 	PersonNature = 'A'




update IncomeTax2008
set Person_Birthdate = stuff(Person_Birthdate,5, 2, '01'),
    Id = stuff(Id, 3,2,'01')
where convert(int,substring(Person_Birthdate,5,2)) not between 1 and 12
and 	PersonNature = 'A'

go



SELECT 'NO POSTAL ADDRESS'


update IncomeTax2008
set Line_1  =  '100 Grayston Drive',
    Postal_Code = '2146'
where Line_1 is null


go




--update IncomeTax2008
--set Postal_Code = '0000'
--where Postal_Code is null

go


update IncomeTax2008
set Person_Surname = 'UnSpecified'
where Person_Surname  is null

go



SELECT 'Full Names not specified for PersonNature  in (A, B,C)'





update IncomeTax2008
set Person_FullNames = 'UnSpecified'
where PersonNature    in ('A', 'B','C')


go


select 'No Registration'



update IncomeTax2008
set TrustNo  =  'IT9999/99'
from	IncomeTax2008
where	isnull(TrustNo,'') = ''
and 		PersonNature in( 'D','E','H','K')

go


update IncomeTax2008
set Id = NULL,
    PassportNo = NULL
where	(isnull(Id,'') != ''
or	isnull(PassportNo,'') != '')
and 	PersonNature in ( 'B','D', 'E','F','G','H','K')



go


select 'Inappropriate Initials'



update IncomeTax2008
set Person_Init = isnull(substring(Person_Surname, 32,4), 'A A')
where 	isnull(Person_Init,'') = ''
    and PersonNature in ('A','B','C')


update IncomeTax2008
set Person_Init = ''
where 	isnull(Person_Init,'') = ''
    and PersonNature not in ('A','B','C')



go


select  'Inappropriate First Names'




update IncomeTax2008
set Person_FullNames = null
where	isnull(Person_FullNames,'') != ''
and 	PersonNature not in ('A','B','C')


select 'Invalid Initials for person of nature D'



update IncomeTax2008
set Person_Init = ''
where	isnull(Person_Init,'') != ''
and 		PersonNature = 'D'


select 'Invalid ID / Passport no for Trusts'



update IncomeTax2008
set Id = '',
	PassportNo = ''
where (isnull(Id,'') != ''
or	isnull(PassportNo,'') != '')
and 	PersonNature in ( 'B','D', 'E','F','G','H','K')


Update IncomeTax2008
set Person_Init = str_replace(Person_Init,' ',NULL)

 UPDATE IncomeTax2008
      SET Owner_Name        = LTRIM(P.Title + ' ') + LTRIM(P.Initials + ' ') + LTRIM(P.Name),
          Person_Surname    = LTRIM(P.Name),
          Person_Init       = LTRIM(P.Initials),
          Person_FullNames  = LTRIM(P.First_Names),
          PersonNature      = 'N'
     FROM People P
    WHERE IncomeTax2008.Owner = P.People_Number

Update IncomeTax2008
set Person_Init = str_replace(Person_Init,' ',NULL)

-- select distinct Product_Code from IncomeTax2008
--select * from IncomeTax2008 where Product_Code = 'ILLAS'
go