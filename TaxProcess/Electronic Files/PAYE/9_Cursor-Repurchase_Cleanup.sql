-- IncomeTax_RePurchase  
/**
select * into IncomeTax_RePurchaseBackUp from IncomeTax_RePurchase
**/
select TrustNo
from IncomeTax_RePurchase
where ascii(substring(TrustNo,datalength(TrustNo),1)) not between 33 and 126

update IncomeTax_RePurchase
set TrustNo = substring(TrustNo,1, datalength(TrustNo) - 1)
where ascii(substring(TrustNo,datalength(TrustNo),1)) not between 33 and 126


GO
select 'Update Person Nature. Assumption : A'

select Id, Person_Birthdate, PersonNature,Person_Surname,Person_Init,
Person_FullNames,
Person_Birthdate,  *
from IncomeTax_RePurchase
where  PersonNature    is null


update IncomeTax_RePurchase
set PersonNature    =  'A'
where PersonNature    is null


go

set nocount on
select 'Remove Non Integers from ID numbers'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Id             varchar(20),
        @Count          int

select Owner, Product_Code, cpy_IRP5Number, Id
into  #Cursor
from IncomeTax_RePurchase
WHERE PersonNature  IN ('A','C')


select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Id = Id
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Id , datalength(@Id)
   select @Count = 1
   while (@Count <= datalength(@Id))
   begin
    -- select substring(@Id, @Count, 1)
     if ascii(substring(@Id, @Count, 1)) not between 48 and 57
       begin
       select @Owner, @Id, @Count
      update IncomeTax_RePurchase
       set Id = stuff(Id,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','C')
        
       select @Count = @Count - 1

       select @Id = Id
       from  IncomeTax_RePurchase
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Id = Id
from  #Cursor
set rowcount 0

end

select count(*)
from IncomeTax_RePurchase
WHERE PersonNature  IN ('A','C')

select 'Name contains invalid characters'

select	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Id,
    Person_Birthdate,
	PassportNo,
	TrustNo	
from	IncomeTax_RePurchase
where 	isnull(Person_Init,'') = ''
and PersonNature in ('A','B','C')

drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from Surname'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int


select Owner, Product_Code, cpy_IRP5Number, Person_FullNames, Person_Surname
into  #Cursor
from IncomeTax_RePurchase
WHERE PersonNature  IN ('A','B','C','N')

select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Surname = Person_Surname
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
    -- select substring(@Surname, @Count, 1)
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Surname, @Count
       update IncomeTax_RePurchase
       set Person_Surname = stuff(Person_Surname,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','B','C','N')
       select @Owner, @Surname, @Count
        
       select @Count = @Count - 1

       select @Surname = Person_Surname
       from  IncomeTax_RePurchase
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number

     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Surname = Person_Surname
from  #Cursor
set rowcount 0

end

drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from FullNames'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Person_FullNames        varchar(20),
        @Count          int

select Owner, Product_Code, cpy_IRP5Number, Person_FullNames, Person_Surname
into  #Cursor
from IncomeTax_RePurchase
WHERE PersonNature  IN ('A','B','C','N')

select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_FullNames = Person_FullNames
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Person_FullNames , datalength(@Person_FullNames)
   select @Count = 1
   while (@Count <= datalength(@Person_FullNames))
   begin
    -- select substring(@Person_FullNames, @Count, 1)
     if (((ascii(substring(@Person_FullNames, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Person_FullNames, @Count

       update IncomeTax_RePurchase
       set Person_FullNames = stuff(Person_FullNames,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','B','C','N')

       select @Owner, @Person_FullNames, @Count
        
       select @Count = @Count - 1

       select @Person_FullNames = Person_FullNames
       from  IncomeTax_RePurchase
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_FullNames = Person_FullNames
from  #Cursor
set rowcount 0

end

drop table #Cursor
go

set nocount on
select 'Remove Invalid characters from Person_Init'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Person_Init        varchar(5),
        @Count          int

select Owner, Product_Code, cpy_IRP5Number, Person_Init
into  #Cursor
from IncomeTax_RePurchase
WHERE PersonNature  IN ('A','B','C','N')

select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_Init = Person_Init
from  #Cursor

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, datalength(@Person_Init)
   select @Count = 1
   while (@Count <= datalength(@Person_Init))
   begin
    -- select substring(@Person_Init, @Count, 1)
     if (((ascii(substring(@Person_Init, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Person_Init, @Count

       update IncomeTax_RePurchase
       set Person_Init = stuff(Person_Init,@Count,1,null)
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
       and   PersonNature  IN ('A','B','C','N')

       select @Owner, @Person_Init, @Count
        
       select @Count = @Count - 1

       select @Person_Init = Person_Init
       from  IncomeTax_RePurchase
       where Owner           =   @Owner 
       and   Product_Code    =   @Product_Code
       and   cpy_IRP5Number  =   @cpy_IRP5Number
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 
and   Product_Code    =   @Product_Code
and   cpy_IRP5Number  =   @cpy_IRP5Number

--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner, @Product_Code = Product_Code, @cpy_IRP5Number = cpy_IRP5Number, @Person_Init = Person_Init
from  #Cursor
set rowcount 0

end
GO
