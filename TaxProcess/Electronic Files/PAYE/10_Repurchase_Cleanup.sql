
update IncomeTax_RePurchase
set Source_Code = '3915'
where Source_Code = '3921'



SELECT 'Append the last ID character to the remaining missing characters'

select datalength(Id),
        substring(Id,datalength(Id),1), Id,
        Id + replicate(substring(Id,datalength(Id),1), 13 - datalength(Id))
from IncomeTax_RePurchase
WHERE PersonNature  IN ('A','C','N')
and datalength(Id) < 13

update IncomeTax_RePurchase
set Id = Id + replicate(substring(Id,datalength(Id),1), 13 - datalength(Id))
WHERE PersonNature  IN ('A','C','N')
and datalength(Id) < 13

SELECT 'IF ID is null, replace with DBO + 1111111'

select PersonNature     , Id, Person_Birthdate, *
from IncomeTax_RePurchase
where  PersonNature     in ('A', 'C','N') 
and  Id               is null
and  Person_Birthdate is not null

update IncomeTax_RePurchase
set Id  =  substring(Person_Birthdate,3,8) + '1111111'
where  PersonNature     in ('A', 'C','N') 
and  Id               is null
and  Person_Birthdate is not null

go

SELECT 'IF no DBO, assume Person Nature D'

SELECT Owner_Name, PersonNature , Person_Birthdate, Id, *
FROM IncomeTax_RePurchase
WHERE PersonNature  IN ('A', 'B', 'C','N')
and  Person_Birthdate is null

UPDATE IncomeTax_RePurchase
SET PersonNature  =  'D'
WHERE PersonNature  IN ('A', 'B', 'C','N')
and  Person_Birthdate is null

GO

SELECT 'Invalid month in Id date'

select convert(int,substring(Id, 3,2)), Id, Person_Birthdate, convert(int,substring(Person_Birthdate, 5,2)),
       stuff(Id, 3, 2, substring(Person_Birthdate,5,2))
from IncomeTax_RePurchase
where convert(int,substring(Id, 3,2)) > 12
and  PersonNature in ('A','C','N')


UPDATE IncomeTax_RePurchase
SET Id   =   stuff(Id, 3, 2, substring(Person_Birthdate,5,2))
WHERE convert(int,substring(Id, 3,2)) > 12
and  PersonNature in ('A','C','N')

SELECT 'Invalid day in Id date'

select convert(int,substring(Id, 5,2)), Id, Person_Birthdate, convert(int,substring(Person_Birthdate, 7,2)),
        stuff(Id, 5, 2, substring(Person_Birthdate,7,2))
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and convert(int,substring(Id, 5,2)) > 31
and  PersonNature in ('A','C','N')

UPDATE IncomeTax_RePurchase
SET Id   =   stuff(Id, 5, 2, substring(Person_Birthdate,7,2))
where cpy_TaxYear = cpy_TaxYear
and convert(int,substring(Id, 5,2)) > 31
and  PersonNature in ('A','C','N')

SELECT 'ID does not CORRELATE with DOB'

SELECT ' YY '

select cpy_IRP5Number, Id, Person_Birthdate,stuff(Person_Birthdate, 3, 2, substring(Id,1,2)), *
from IncomeTax_RePurchase
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
AND   convert(char(2),substring(Id,1,2)) <> convert(char(2),substring(Person_Birthdate,3,2) )  -- YY

UPDATE IncomeTax_RePurchase
SET Person_Birthdate = stuff(Person_Birthdate, 3, 2, substring(Id,1,2))
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
AND   convert(char(2),substring(Id,1,2)) <> convert(char(2),substring(Person_Birthdate,3,2) )  -- YY

SELECT ' MM '
select cpy_IRP5Number, Id, Person_Birthdate, stuff(Person_Birthdate, 5, 2, substring(Id,3,2)), *
from IncomeTax_RePurchase
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
AND convert(char(2),substring(Id,3,2)) <> convert(char(2),substring(Person_Birthdate,5,2) )   --MM

UPDATE IncomeTax_RePurchase
SET Person_Birthdate = stuff(Person_Birthdate, 5, 2, substring(Id,3,2))
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)

AND convert(char(2),substring(Id,3,2)) <> convert(char(2),substring(Person_Birthdate,5,2) )   --MM

SELECT ' DD '
select cpy_IRP5Number, Id, Person_Birthdate , stuff(Person_Birthdate, 7, 2, substring(Id,5,2)), *
from IncomeTax_RePurchase
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
AND   convert(char(2),substring(Id,5,2)) <> convert(char(2),substring(Person_Birthdate,7,2))   --DD

UPDATE IncomeTax_RePurchase
SET Person_Birthdate = stuff(Person_Birthdate, 7, 2, substring(Id,5,2))
where 1=1 --cpy_IRP5Number in (725, 781, 1526, 1795, 3383, 339, 1062)
AND   convert(char(2),substring(Id,5,2)) <> convert(char(2),substring(Person_Birthdate,7,2))   --DD

SELECT 'Invalid DOB'

SELECT Id, Person_Birthdate, stuff(Id, 1, 6, substring(Person_Birthdate,3,8))
FROM IncomeTax_RePurchase
WHERE cpy_TaxYear = cpy_TaxYear
AND  PersonNature  IN ('A','C','N')
and datalength(Person_Birthdate) <> 8

SELECT 'Update the remaining ID Numbers'
--SELECT cpy_IRP5Number, stuff(Person_Birthdate, 3, 8, substring(Id,1,6)), Id, Person_Birthdate, *
--SELECT cpy_IRP5Number, stuff(Person_Birthdate, 3, 8, substring(Id,1,6)), Id, Person_Birthdate, *
SELECT Id, Person_Birthdate, stuff(Id, 1, 6, substring(Person_Birthdate,3,8))
FROM IncomeTax_RePurchase
WHERE cpy_TaxYear = cpy_TaxYear
AND  PersonNature  IN ('A','C','N')
AND  ( ( convert(char(2),substring(Id,1,2)) <> convert(char(2),substring(Person_Birthdate,3,2) ) ) -- YY
    OR (convert(char(2),substring(Id,3,2)) <> convert(char(2),substring(Person_Birthdate,5,2) ))   --MM
    OR (convert(char(2),substring(Id,5,2)) <> convert(char(2),substring(Person_Birthdate,7,2)) ))  --DD

UPDATE IncomeTax_RePurchase
SET Id   =   stuff(Id, 1, 6, substring(Person_Birthdate,3,8))
WHERE cpy_TaxYear = cpy_TaxYear
AND  PersonNature  IN ('A','C','N')
AND  ( ( convert(char(2),substring(Id,1,2)) <> convert(char(2),substring(Person_Birthdate,3,2) ) ) -- YY
    OR (convert(char(2),substring(Id,3,2)) <> convert(char(2),substring(Person_Birthdate,5,2) ))   --MM
    OR (convert(char(2),substring(Id,5,2)) <> convert(char(2),substring(Person_Birthdate,7,2)) ))  --DD

go



SELECT 'ID Number greater than 13 characters'

select Id, Person_Birthdate, substring(Id, 1, datalength(Id) - 1), *
from IncomeTax_RePurchase
WHERE cpy_TaxYear = cpy_TaxYear
AND  PersonNature  IN ('A','C','N')
and datalength(Id) > 13

update IncomeTax_RePurchase
set Id = substring(Id, 1, datalength(Id) - 1)
WHERE cpy_TaxYear = cpy_TaxYear
AND  PersonNature  IN ('A','C','N')
and datalength(Id) > 13


SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Id,
	PassportNo,
	TrustNo,
	Person_Birthdate,
    stuff(Person_Birthdate, 7, 2, '01'),  stuff(Id, 5,2,'01')
from	IncomeTax_RePurchase
where	convert(int,substring(Person_Birthdate,7,2)) not between 1 and 31
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature = 'N'


update IncomeTax_RePurchase
set Person_Birthdate = stuff(Person_Birthdate, 7, 2, '01'),
    Id = stuff(Id, 5,2,'01')
where	convert(int,substring(Person_Birthdate,7,2)) not between 1 and 31
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature = 'N'



SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Id,
	PassportNo,
	TrustNo,
	Person_Birthdate,
    stuff(Person_Birthdate,5, 2, '01'),   stuff(Id, 3,2,'01')
from	IncomeTax_RePurchase
where convert(int,substring(Person_Birthdate,5,2)) not between 1 and 12
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature = 'N'


update IncomeTax_RePurchase
set Person_Birthdate = stuff(Person_Birthdate,5, 2, '01'),
    Id = stuff(Id, 3,2,'01')
where convert(int,substring(Person_Birthdate,5,2)) not between 1 and 12
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature = 'N'

go



SELECT 'NO POSTAL ADDRESS'

select *
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and Line_1 is null

update IncomeTax_RePurchase
set Line_1  =  '100 Grayston Drive',
    Postal_Code = '2146'
where cpy_TaxYear = cpy_TaxYear
and Line_1 is null


go

select *
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and Postal_Code is null

update IncomeTax_RePurchase
set Postal_Code = '0000'
where cpy_TaxYear = cpy_TaxYear
and Postal_Code is null

go


SELECT 'Name not available'
select *
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and Person_Surname  is null


update IncomeTax_RePurchase
set Person_Surname = 'UnSpecified'
where cpy_TaxYear = cpy_TaxYear
and Person_Surname  is null

go

SELECT 'Full Names not specified for PersonNature  in (A, B,C,N)'

select *
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and Person_FullNames is null
and  PersonNature    in ('A', 'B','C','N')

update IncomeTax_RePurchase
set Person_FullNames = 'UnSpecified'
where cpy_TaxYear = cpy_TaxYear
and Person_FullNames  is null
and  PersonNature    in ('A', 'B','C','N')

SELECT 'ID does not exist for PersonNature    in (A, C)'
select *
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and Id is null
and  PersonNature    in ('A', 'C','N')

go

select 'No Registration'

SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Person_FullNames,
	Id,
	PassportNo,
	TrustNo
from	IncomeTax_RePurchase
where	isnull(TrustNo,'') = ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in( 'D','E','H','K')

update IncomeTax_RePurchase
set TrustNo  =  'IT9999/99'
from	IncomeTax_RePurchase
where	isnull(TrustNo,'') = ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in( 'D','E','H','K')

go

SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Person_FullNames,
	Id,
	PassportNo,
	TrustNo
from	IncomeTax_RePurchase
where	(isnull(Id,'') != ''
or	isnull(PassportNo,'') != '')
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in ( 'B','D', 'E','F','G','H','K')

update IncomeTax_RePurchase
set Id = NULL,
    PassportNo = NULL
where	(isnull(Id,'') != ''
or	isnull(PassportNo,'') != '')
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in ( 'B','D', 'E','F','G','H','K')

select *
from IncomeTax_RePurchase
where cpy_TaxYear = cpy_TaxYear
and isnull(cpy_Address1,'') = ''

go


select 'Inappropriate Initials'

select	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Id,
    Person_Birthdate,
	PassportNo,
	TrustNo	
from	IncomeTax_RePurchase
where 	isnull(Person_Init,'') = ''
    and cpy_TaxYear = cpy_TaxYear 
    and PersonNature in ('A','B','C','N')

select	datalength(Person_Init),Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Id,
    Person_Birthdate,
	PassportNo,
	TrustNo	
from	IncomeTax_RePurchase
where 	isnull(Person_Init,'') = ''
    and cpy_TaxYear = cpy_TaxYear 
    and PersonNature not in ('A','B','C','N')

update IncomeTax_RePurchase
set Person_Init = isnull(substring(Person_Surname, 32,4), 'A A')
where 	isnull(Person_Init,'') = ''
    and cpy_TaxYear = cpy_TaxYear 
    and PersonNature in ('A','B','C','N')

update IncomeTax_RePurchase
set Person_Init = ''
where 	isnull(Person_Init,'') = ''
    and cpy_TaxYear = cpy_TaxYear 
    and PersonNature not in ('A','B','C','N')

go

select  'Inappropriate First Names'

SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Person_FullNames,
	Id,
	PassportNo,
	TrustNo
from	IncomeTax_RePurchase
where	isnull(Person_FullNames,'') = ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in ('A', 'B','C')


select  'Inappropriate First Names'

SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Person_FullNames,
	Id,Person_Birthdate,
	PassportNo,
	TrustNo
from	IncomeTax_RePurchase
where	isnull(Person_FullNames,'') != ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature not in ('A','B','C','N')


update IncomeTax_RePurchase
set Person_FullNames = null
where	isnull(Person_FullNames,'') != ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature not in ('A','B','C','N')


select 'Invalid Initials for person of nature D'

select	cpy_Reference_Number,cpy_IRP5Number, Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Id,
	PassportNo,
	TrustNo	
from	IncomeTax_RePurchase
where	isnull(Person_Init,'') != ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature = 'D'

update IncomeTax_RePurchase
set Person_Init = ''
where	isnull(Person_Init,'') != ''
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature = 'D'

select 'Invalid ID / Passport no for Trusts'

SELECT	Owner,
	PersonNature,
	Person_Surname,
	Person_Init,
	Person_FullNames,
	Id,
	PassportNo,
	TrustNo
from	IncomeTax_RePurchase
where	(isnull(Id,'') != ''
or	isnull(PassportNo,'') != '')
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in ( 'B','D', 'E','F','G','H','K')

update IncomeTax_RePurchase
set Id = '',
	PassportNo = ''
where (isnull(Id,'') != ''
or	isnull(PassportNo,'') != '')
and 	cpy_TaxYear = cpy_TaxYear
and	PersonNature in ( 'B','D', 'E','F','G','H','K')


--delete data where cpy_IRP5Number is null.
--The certificates were never issued......



delete IncomeTax_RePurchase
where cpy_IRP5Number is null


GO


---Checks....



update IncomeTax_RePurchase
set Person_Acc_Type  = '0'
where isnumeric(Person_Acc_No) = 0



update  IncomeTax_RePurchase
set Person_Acc_Type  = '0'
where Person_Acc_Type is null



update IncomeTax_RePurchase
set Person_Acc_Type = '0'
where Person_Acc_No = ''


update IncomeTax_RePurchase
set Line_2 = 'SandDown'
where isnull(Line_2,'') = ''


update IncomeTax_RePurchase
set Tax_No = '0' + Tax_No 
where substring( Tax_No,1,1) not in ('0','1','2','3')
and Tax_No <> 'Unknown'



select id = identity(10), Owner, cpy_IRP5Number, Product_Code
into #dupUPD
from IncomeTax_RePurchase
where DuplicateTaxNumber = 1
group by  Owner, cpy_IRP5Number, Product_Code


update IncomeTax_RePurchase
set DupNumber =  id 
from IncomeTax_RePurchase i, #dupUPD d
where DuplicateTaxNumber = 1
and i.Owner = d.Owner
and i.cpy_IRP5Number = d.cpy_IRP5Number
and i.Product_Code = d.Product_Code


GO
