

drop table WORKSPACE..CGTReport
go


DECLARE @TaxYear    INT

SELECT @TaxYear = 2016

/*****************************************************************************/
SELECT	Certificate_Number,
	C.People_Number,
	C.Contract_Number,
	Product_Code,
	convert(varchar,C.People_Number) + ' \ ' + convert(varchar,Contract_Number) Account,
	CASE WHEN Net_Gain > 0 
		THEN '6506'
		ELSE '6507'
	END AS 'SARS_CODE',
	Product_Name,
	Tax_Year,
	NatureOfPerson,
        P.Title as Disignation,
	Surname,
	FirstTwoNames,
	C.Initials,
	IdentityNumber,
	PassportNumber,
	convert(varchar(11),DateOfBirth,111) DateOfBirth,
	CompCCTrustNumber,
	SAResident,
	IncomeTaxNumber,
	EmployeeNumber,
	convert(varchar(11),PeriodFrom,111) PeriodFrom,
	convert(varchar(11),PeriodTo,111) PeriodTo,
	StatementDate,
	Line_1,
	Line_2,
	Line_3,
	Line_4,
	Line_5,
	Postal_Code,
	Instrument_Description,
	Manco_Description, 
	Units_Disposed,
	Gross_Proceeds, 
	Net_Gain, 
	Units_Held, 
	WAC, 
    	Gross_Proceeds - Net_Gain AS 'AUC',
    	WAC * Units_Held/100 AS 'CHeld'
INTO    WORKSPACE..CGTReport
FROM	CGTIT3CCertificate		C,
	CGTIT3CInstrumentCertificate	I,
        People                          P
WHERE	Tax_Year		= @TaxYear
        and C.People_Number    =    P.People_Number
AND	C.Certificate_Key	= I.Certificate_Key
and     Net_Gain <> 0
ORDER BY
	Product_Code, 
	Certificate_Number,
	Manco_Description,
	Instrument_Description



go

/****************************** JUNK JUNK JUNK JUNK JUNK JUNK ****************************************

drop table #CertNumbers

select distinct Certificate_Number
into #CertNumbers
from WORKSPACE..CGTReport



select count(*) from #CertNumbers



select * into #buffer from #CertNumbers where 1=2


select count(*) from #CertNumbers
select count(*) from #buffer


set rowcount 8000
insert #buffer select * from #CertNumbers
set rowcount 0

select c.*
from WORKSPACE..CGTReport c, #buffer b
where c.Certificate_Number = b.Certificate_Number


delete #CertNumbers
from #CertNumbers c, #buffer b
where c.Certificate_Number = b.Certificate_Number

truncate table #buffer



bcp WORKSPACE..CGTReport out CGTReport12Apr2012B.csv -S imsqa2 -U system_application -P system_application -c  -t|


select *
from WORKSPACE..CGTReport
where Certificate_Number = 3323

update WORKSPACE..CGTReport
set IncomeTaxNumber = 'Unknown'

************************************************************************************************************************/