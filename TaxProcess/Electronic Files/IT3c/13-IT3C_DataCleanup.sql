----- IT3B & IT3C Data Cleanup
USE IMS_MAIN
GO

UPDATE  WORKSPACE..IT3C_Get_People  
SET     Post_1 = STR_REPLACE(Post_1,'&','and'),
        Post_2 = STR_REPLACE(Post_2,'&','and'),
        Post_3 = STR_REPLACE(Post_3,'&','and'),
        Post_4 = STR_REPLACE(Post_4,'&','and'),
        Post_5 = STR_REPLACE(Post_5,'&','and')

GO

UPDATE  WORKSPACE..IT3C_Get_People  
SET     Name = STR_REPLACE(Name,'  ',' ')
GO

UPDATE  WORKSPACE..IT3C_Get_People  
SET     Name = STR_REPLACE(Name,'&','and')
GO


IF EXISTS(SELECT 1 FROM Tenant WHERE Name = 'RMB')
BEGIN

    SELECT 'Ashburton Clean Up'
    
    -- Defaulting FICA Status to N for Foreign passport and ID nos.
    UPDATE  WORKSPACE..IT3C_Get_People
    SET     FICA_Status = 'N'
    WHERE   Reg_Type = 1 
    AND     Reg_Value IS NULL
    AND     Owner IN
        (
        SELECT People_Number FROM PeopleRegistration WHERE Reg_Type IN (15,16)
        )

    -- Defaulting FICA Status to N where ID and Date of Birth is null and FICA Status is Y
    UPDATE  WORKSPACE..IT3C_Get_People
    SET     FICA_Status ='N' 
    WHERE   FICA_Status = 'Y' 
    AND     Reg_Value = NULL 
    AND     Date IS NULL

    UPDATE  WORKSPACE..IT3C_Get_People
    SET     Reg_Type = 12
    WHERE   lower(Name) LIKE '%trust%' 
    AND     Reg_Type <> 12 
    AND     Product = 'RMBSP' 
    AND     Source = 'U'

END
GO



IF EXISTS(SELECT 1 FROM Tenant WHERE Name = 'IMS')
BEGIN
    SELECT 'IMS Clean Up'

    UPDATE      WORKSPACE..IT3C_Get_People
    SET         Reg_Type = R.Reg_Type
    FROM        WORKSPACE..IT3C_Get_People W
    INNER JOIN  PeopleRegistration R
    ON          W.Owner = R.People_Number
    WHERE       R.Reg_Type IN (1,2,3,12,14,17,18)

    UPDATE      WORKSPACE..IT3C_Get_People
    SET         Reg_Value = R.Reg_Value
    FROM        WORKSPACE..IT3C_Get_People W
    INNER JOIN  PeopleRegistration R
    ON          W.Owner = R.People_Number
    AND         W.Reg_Type = R.Reg_Type
    WHERE       R.Reg_Type IN (1,2,3,12,14,17,18)

    UPDATE WORKSPACE..IT3C_Get_People SET FICA_Status = 'N' 

    UPDATE      WORKSPACE..IT3C_Get_People
    SET         FICA_Status = 'Y'
    FROM        WORKSPACE..IT3C_Get_People P 
    INNER JOIN  PeopleCompliance PC 
    ON          PC.People_Number = P.Owner
    AND         CURRENT_DATE() Between PC.Start_Date AND ISNULL(PC.End_Date,'31 Dec 9999')
    AND         PC.Compliance_Type_Id IN (2,3,7)

    UPDATE      WORKSPACE..IT3C_Get_People
    SET         FICA_Status = 'N'
    FROM        WORKSPACE..IT3C_Get_People W
    INNER JOIN  People P
    ON          W.Owner = P.People_Number
    AND         P.Language = 'E'
    INNER JOIN  RegistrationTypes R
    ON          W.Reg_Type = R.Reg_Type
    WHERE       W.FICA_Status = 'Y'
    AND         (W.Reg_Value IS NULL OR W.Reg_Value = '')
    AND         W.Date IS NULL

    UPDATE WORKSPACE..IT3C_Get_People SET FICA_Status = 'N'  WHERE (Reg_Value IS NULL OR Reg_Value = '')

    UPDATE  WORKSPACE..IT3C_Get_People 
    SET     FICA_Status = 'N',
            Reg_Type = 12,
            Reg_Value = NULL
    WHERE   lower(Name) LIKE '%trust%' 
    AND     Reg_Type NOT IN (12,18)

    --** Name (SurName) update based on FICA Status
    UPDATE WORKSPACE..IT3C_Get_People SET Name = 'UNKNOWN' WHERE FICA_Status IN ('N','E') AND LEN(RTRIM(LTRIM(Name))) = 0

    -- Update Date of Birth
    UPDATE      WORKSPACE..IT3C_Get_People
    SET         Date = D.Date
    FROM        WORKSPACE..IT3C_Get_People P 
    INNER JOIN  PeopleDates D 
    ON          D.People_Number = P.Owner
    AND         D.Date_Type = 'B'

    UPDATE  WORKSPACE..IT3C_Get_People
    SET     FICA_Status = 'N'
    WHERE   FICA_Status ='Y' 
    AND     Reg_Type = 100

END
GO


-- IT3B :  Cleanup -- Reg Type Vs Nature of Person
UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 1 
AND     Nature_Of_Person NOT IN (1,12)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 3 
AND     Nature_Of_Person NOT IN (2)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 12 
AND     Nature_Of_Person NOT IN (3)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 2 
AND     Nature_Of_Person NOT IN (14)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 15 
AND     Nature_Of_Person NOT IN (13)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 16 
AND     Nature_Of_Person NOT IN (13)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 17 
AND     Nature_Of_Person NOT IN (14)

UPDATE  WORKSPACE..IT3C_Get_People 
SET     FICA_Status = 'N',
        Reg_Type = 100
WHERE   Reg_Type = 18 
AND     Nature_Of_Person NOT IN (3)

GO

update WORKSPACE..IT3C_Get_People
set Post_Code = '0000'
where isnull(Post_Code,'') = ''
go

UPDATE WORKSPACE..IT3C_Get_People 
SET FICA_Status = 'N' 
WHERE Reg_Type IN (3,14) 
and isnull(Reg_Value,'') = ''
AND FICA_Status = 'Y'  

UPDATE WORKSPACE..IT3C_Get_People 
SET FICA_Status = 'N' 
WHERE Reg_Value = '9999/999999/99'
GO

--ONCE OFF UPDATES FOR 2016 TAX YEAR...

update WORKSPACE..IT3C_Get_People
set FICA_Status = 'N'
from WORKSPACE..IT3C_Get_People
where Owner in ( 11105, 15050, 15273, 18762, 24172, 25243, 34077)

GO
