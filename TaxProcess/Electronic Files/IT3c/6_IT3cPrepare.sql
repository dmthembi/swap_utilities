
 IF OBJECT_ID('WORKSPACE..IT3C_Get_Detail') IS NOT NULL
        BEGIN
            DROP TABLE WORKSPACE..IT3C_Get_Detail
 PRINT '<<< DROPPED TABLE WORKSPACE..IT3C_Get_Detail >>>'
 END

IF OBJECT_ID('#RE_InstrumentRatesHistory') IS NOT NULL
        BEGIN
            DROP TABLE #RE_InstrumentRatesHistory
 PRINT '<<< #RE_InstrumentRatesHistory >>>'
 END

go


CREATE TABLE WORKSPACE..IT3C_Get_Detail
(	Owner            	INT             NULL,
    Contract_Number     INT             NULL,
    Product_Code          varchar(6),
	Source_Code             VARCHAR(4)      NULL,
    Instrument_Number        INT             NULL,
	Instrument_Description  VARCHAR(30)     NULL,
	Units_Disposed          float null, --NUMERIC(20,8) 	NULL,
	WAC                     float null, --NUMERIC(20,8)   NULL,
    Rate                    float null, --NUMERIC(20,8)   NULL,
	Gross_Proceeds          float null, --NUMERIC(20,8)   NULL,
	Net_Gain                float null, --NUMERIC(20,8)   NULL,
	Unit_Balance            float null, --NUMERIC(20,8)   NULL,
	Unit_Value             float null) -- NUMERIC(20,8)   NULL)
go


DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit

	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Bi_Annual = Bi_Annual , @Tax_Year = Tax_Year
from TaxProcessDates


INSERT	INTO WORKSPACE..IT3C_Get_Detail
SELECT	C.People_Number,
    C.Contract_Number,
    C.Product_Code,
    case when Net_Gain > 0 and  C.Product_Code NOT IN('GIB','GAPU') then '4250' 
      when Net_Gain < 0 and  C.Product_Code NOT IN('GIB','GAPU')    then '4251' 
      when Net_Gain > 0 and  C.Product_Code IN('GIB','GAPU')        then '4252' 
      else '4253' end ,	
    Instrument_Number,
	Instrument_Description,
	round(Units_Disposed,3),
	round(WAC ,3),
    NULL,
	round(Gross_Proceeds,3),
	round(Net_Gain,3),
	round(Units_Held,3),
	round( WAC * Units_Held / 100.0,3)
FROM	CGTIT3CInstrumentCertificate	CI,
	CGTIT3CCertificate		C
WHERE	C.Certificate_Key		= CI.Certificate_Key
AND	C.Tax_Year			= @Tax_Year
AND	Net_Gain			!= 0
AND C.Product_Code NOT IN ('GIB')


-- GIB Products from SARS_GIB tables
INSERT	INTO WORKSPACE..IT3C_Get_Detail
SELECT	C.People_Number,
     C.Contract_Number,
    C.Product_Code,
    case when Net_Gain > 0 and  C.Product_Code NOT IN('GIB','GAPU') then '4250' 
      when Net_Gain < 0 and  C.Product_Code NOT IN('GIB','GAPU')    then '4251' 
      when Net_Gain > 0 and  C.Product_Code IN('GIB','GAPU')        then '4252' 
      else '4253' end ,	
     Instrument_Number,
	Instrument_Description,
	round(Units_Disposed,3),
	round(WAC,3),
    NULL,
	round(Gross_Proceeds,3),
	round(Net_Gain,3),
	round(Units_Held,3),
	round(WAC * Units_Held / 100.0,3)
FROM	SARS_CGTIT3CInstCert	CI,
	SARS_CGTIT3CCertificate		C
WHERE	C.Certificate_Key		= CI.Certificate_Key
AND	C.Tax_Year			= @Tax_Year
AND	Net_Gain			!= 0
AND C.Product_Code IN ('GIB')



select *
into #RE_InstrumentRatesHistory
from RE_InstrumentRatesHistory r1
where Date = (select max(Date) from RE_InstrumentRatesHistory r2 where r1.Instrument_Number = r2.Instrument_Number and r2.Date <= @End_Date)


update WORKSPACE..IT3C_Get_Detail
set Rate = r.Buy
from WORKSPACE..IT3C_Get_Detail w, #RE_InstrumentRatesHistory r
where w.Instrument_Number = r.Instrument_Number

GO

UPDATE	WORKSPACE..IT3C_Get_Detail SET WAC = ABS(WAC) WHERE WAC	< 0
UPDATE	WORKSPACE..IT3C_Get_Detail SET Unit_Balance = 0, Unit_Value = 0 WHERE Unit_Balance < 0
UPDATE	WORKSPACE..IT3C_Get_Detail SET Unit_Value = 0 WHERE Unit_Value < 0


delete WORKSPACE..IT3C_Get_Detail
from WORKSPACE..IT3C_Get_Detail w, Instruments i
where w.Instrument_Number = i.Instrument_Number
and i.Instrument_Type = 'PSP'

GO

