set nocount on
go


set nocount on
select 'Remove Invalid characters from Name'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int


select Owner, Reg_Value, Name
into  #Cursor
from WORKSPACE..IT3C_Get_People
WHERE  Reg_Type	= 1

--select *
--from WORKSPACE..IT3B_Get_People


select @Owner = Owner, @Surname = Name
from  WORKSPACE..IT3C_Get_People
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
     select substring(@Surname, @Count, 1)
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
        select @Owner, @Surname, @Count
        update WORKSPACE..IT3C_Get_People
        set Name = stuff(Name,@Count,1,null)
        where Owner           =   @Owner 

        select @Owner, @Surname, @Count
         
        select @Count = @Count - 1

        select @Surname = Name
         from  WORKSPACE..IT3C_Get_People
         where Owner           =   @Owner 
     
       select @Owner, @Surname, @Count
       end

     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 


--select 'Rows left from #cursor ',count(*)
--from #Cursor
--select @@rowcount 
set rowcount 1
select @Owner = Owner,  @Surname = Name
from  #Cursor
set rowcount 0

end
go