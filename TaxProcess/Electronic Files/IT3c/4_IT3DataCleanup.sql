----- IT3DataCleanup.sql

SELECT 'UPDATING Reg_Type AND DATE IN WORKSPACE..IT3C_Get_People '

update WORKSPACE..IT3C_Get_People
set Reg_Type = 12
where Date is null
and lower(Name) like '%trust%'
and Reg_Type <> 12



update WORKSPACE..IT3C_Get_People
set Reg_Type = 3
where Reg_Type in (1,2)
and isnull(Initials,'') = ''
and Date is null


update WORKSPACE..IT3C_Get_People
set Date = substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-' + substring(Reg_Value,1,2 )
where Reg_Type in (1,2)
and Date is null
and isnull(Reg_Value,'') <> ''
and datalength(Reg_Value) = 13
and datediff(yy,substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-' + substring(Reg_Value,1,2 ),getdate()) > 0


update WORKSPACE..IT3C_Get_People
set Date = substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-19' + substring(Reg_Value,1,2 )
where Reg_Type in (1,2)
and Date is null
and isnull(Reg_Value,'') <> ''
and datalength(Reg_Value) = 13
and datediff(yy,substring(Reg_Value,3,2 ) + '-' + substring(Reg_Value,5,2 ) + '-' + substring(Reg_Value,1,2 ),getdate()) <0


-- ** Exceptions **
update WORKSPACE..IT3C_Get_People
set Date = '01 jan 1900'
where Reg_Type in (1,2)
and Date is null


update WORKSPACE..IT3C_Get_People
set Post_1  =  '100 Grayston Drive',
    Post_2='Sandown', 
    Post_3= 'Sandton', 
    Post_4='',Post_5='',
    Post_Code='2146'
where isnull(Post_1,'')  =  ''



SELECT '<<<<MORE UPDATES ON WORKSPACE..IT3C_Get_People >>>>>>'

update WORKSPACE..IT3C_Get_People
set Reg_Type	= 2
FROM	WORKSPACE..IT3C_Get_People
WHERE	Reg_Type	= 1
AND	isnull(First_Names,'')	= ''
GO


update WORKSPACE..IT3C_Get_People
set Initials = substring(First_Names,1,1)
where Reg_Type	= 1
AND	isnull(Initials,'')	= ''



update WORKSPACE..IT3C_Get_People
set Reg_Value  =  '9999/999999/99'
from WORKSPACE..IT3C_Get_People A
where Reg_Type	= 3
and (datalength(Reg_Value) = 1
or Reg_Value  is null)



update WORKSPACE..IT3C_Get_People
set Initials = '',
    First_Names = ''
WHERE	Reg_Type		not in (1,2)
AND	(isnull(Initials,'')	!= ''
OR	isnull(First_Names,'')	!= '')




update WORKSPACE..IT3C_Get_People
set Date = '19' + substring(Reg_Value, 1, 6)
FROM	WORKSPACE..IT3C_Get_People
WHERE	Reg_Type	in (1)
AND	datalength(isnull(Date,'')) != 8



SELECT '<<<<<<<<<<<<<<<<DONE>>>>>>>>>>>>>>>>>>>>>'

GO

