

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates


exec IT3C_Get_People  @Start_Date ,   --** start date of Period
                      @End_Date   ,   --** end date of Period
                      @Tax_Year      

GO 