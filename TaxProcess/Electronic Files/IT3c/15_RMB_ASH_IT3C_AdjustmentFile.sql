USE IMS_MAIN
GO

UPDATE      WORKSPACE..IT3C_Get_Detail
SET         Unit_Value      = ISNULL(T2.Balance_Value,0)
FROM        WORKSPACE..IT3C_Get_Detail T1
INNER JOIN  WORKSPACE..IT3C_AdjustmentFile T2
ON          T1.Owner = T2.Owner
AND         T1.Product_Code = T2.Product
GO


UPDATE      WORKSPACE..IT3C_Get_Detail
SET         Unit_Value      = ISNULL(T2.Balance_Value,0)
FROM        WORKSPACE..IT3C_Get_Detail T1
INNER JOIN  WORKSPACE..IT3C_AdjustmentFile T2
ON          T1.Owner = T2.Owner
AND         T1.Product_Code = T2.Product
AND         T1.Contract_Number = T2.Contract_Number
AND         Convert(Numeric(18,4),T2.Balance_Unit) = Convert(Numeric(18,4),T1.Unit_Balance)
GO
