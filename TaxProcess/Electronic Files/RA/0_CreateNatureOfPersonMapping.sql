IF OBJECT_ID('WORKSPACE..NatureOfPersonMapping') IS NOT NULL
BEGIN
		DROP TABLE WORKSPACE..NatureOfPersonMapping
END
GO

Create table WORKSPACE..NatureOfPersonMapping 
(
    Customer_Type varchar(3) null,                                     
    SARS_Code int,       
    SARS_Description varchar(255),                                      
    CONSTRAINT NatureOfPersonMapping_FK1 FOREIGN KEY(Customer_Type) REFERENCES PeopleType(Customer_Type)
)
GO
Insert into WORKSPACE..NatureOfPersonMapping values ('A',1,'Individual')
Insert into WORKSPACE..NatureOfPersonMapping values ('C',1,'Individual')
Insert into WORKSPACE..NatureOfPersonMapping values ('G',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('H',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('J',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('M',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('O',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('I',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('L',2,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('D',3,'Intervivos_Trust')
Insert into WORKSPACE..NatureOfPersonMapping values ('E',3,'Intervivos_Trust')
Insert into WORKSPACE..NatureOfPersonMapping values ('F',3,'Intervivos_Trust')
Insert into WORKSPACE..NatureOfPersonMapping values ('U',4,'Public/Local authorty & Municipality')
Insert into WORKSPACE..NatureOfPersonMapping values ('V',4,'Public/Local authorty & Municipality')
Insert into WORKSPACE..NatureOfPersonMapping values ('W',4,'Public/Local authorty & Municipality')
Insert into WORKSPACE..NatureOfPersonMapping values ('X',4,'Public/Local authorty & Municipality')
Insert into WORKSPACE..NatureOfPersonMapping values ('Q',5,'Club')
Insert into WORKSPACE..NatureOfPersonMapping values ('R',5,'Club')
Insert into WORKSPACE..NatureOfPersonMapping values ('N',6,'Partnership')
Insert into WORKSPACE..NatureOfPersonMapping values ('Z',7,'Association not for gain')
Insert into WORKSPACE..NatureOfPersonMapping values ('P',8,'Welfare Organisation')
Insert into WORKSPACE..NatureOfPersonMapping values (null,9,'Corporate: Estate/Liquidation')
Insert into WORKSPACE..NatureOfPersonMapping values (null,10,'Stockvel')
Insert into WORKSPACE..NatureOfPersonMapping values (null,11,'Society')
Insert into WORKSPACE..NatureOfPersonMapping values (null,12,'Individual/ Estates (including Estate Lates)')
Insert into WORKSPACE..NatureOfPersonMapping values ('B',13,'Individual')
Insert into WORKSPACE..NatureOfPersonMapping values ('K',14,'Company/CC')
Insert into WORKSPACE..NatureOfPersonMapping values ('S',15,'Other')
Insert into WORKSPACE..NatureOfPersonMapping values ('T',15,'Other')
Insert into WORKSPACE..NatureOfPersonMapping values ('Y',15,'Other')
Insert into WORKSPACE..NatureOfPersonMapping values ('AA',15,'Other')
GO
