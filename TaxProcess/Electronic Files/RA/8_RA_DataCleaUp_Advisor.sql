set nocount on
select 'Remove Invalid characters from First Names'

declare @Broker_Number          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int


select Broker_Number, First_Names
into  #Cursor
from WORKSPACE..RAAdvisor_Data

select @Broker_Number = Broker_Number, @Surname = First_Names
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Broker_Number, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
    -- select substring(@Surname, @Count, 1)
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Broker_Number, @Surname, @Count
       update WORKSPACE..RAAdvisor_Data
       set First_Names = stuff(First_Names,@Count,1,null)
       where Broker_Number           =   @Broker_Number 

       select @Count = @Count - 1

       select @Surname = First_Names
       from  WORKSPACE..RAAdvisor_Data
       where Broker_Number           =   @Broker_Number 

        select @Broker_Number, @Surname, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Broker_Number           =   @Broker_Number 

set rowcount 1
select @Broker_Number = Broker_Number,  @Surname = First_Names
from  #Cursor
set rowcount 0

end
GO
drop table #Cursor
go

-----------------------------------------------------------------------------------------------------------------

select 'Remove Invalid characters from Name'

declare @Broker_Number          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int


select Broker_Number, Name
into  #Cursor
from WORKSPACE..RAAdvisor_Data

select @Broker_Number = Broker_Number, @Surname = Name
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Broker_Number, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
     
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       
        update WORKSPACE..RAAdvisor_Data
        set Name = stuff(Name,@Count,1,null)
        where Broker_Number           =   @Broker_Number 

       
         
        select @Count = @Count - 1

        select @Surname = Name
         from  WORKSPACE..RAAdvisor_Data
         where Broker_Number           =   @Broker_Number 
     
       select @Broker_Number, @Surname, @Count
       end

     select @Count = @Count + 1
   end

delete #Cursor
where Broker_Number           =   @Broker_Number 

set rowcount 1
select @Broker_Number = Broker_Number,  @Surname = Name
from  #Cursor
set rowcount 0

end

drop table #Cursor
go

-----------------------------------------------------------------------------------------------------------------

set nocount on
select 'Remove Invalid characters from Initials'

declare @Broker_Number          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
	@Person_Init        varchar(5),
        @Count          int

select Broker_Number,  Initials
into  #Cursor
from WORKSPACE..RAAdvisor_Data

select @Broker_Number = Broker_Number, @Person_Init = Initials
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 

while ((select count(*) from #Cursor) > 0 )
begin

   select @Count = 1
   while (@Count <= datalength(@Person_Init))
   begin
    -- select substring(@Person_Init, @Count, 1)
     if (((ascii(substring(@Person_Init, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
      
       update WORKSPACE..RAAdvisor_Data
       set Initials = stuff(Initials,@Count,1,null)
       where Broker_Number           =   @Broker_Number 

       select @Count = @Count - 1

       select @Person_Init = Initials
       from  WORKSPACE..RAAdvisor_Data
       where Broker_Number           =   @Broker_Number 

      select @Broker_Number, @Person_Init, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Broker_Number           =   @Broker_Number 

set rowcount 1
select @Broker_Number = Broker_Number,  @Person_Init = Initials
from  #Cursor
set rowcount 0

end


drop table #Cursor
go




---END



Update WORKSPACE..RAAdvisor_Data  
Set    Post_Code = '0000' 
where  Post_Code = null OR LEN(ltrim(rtrim(Post_Code)))  <= 1 OR LEN(Post_Code)  = 1

Update  WORKSPACE..RAAdvisor_Data  
Set     Post_Code = str_replace(Post_Code, '-','')
where   isnumeric(Post_Code) =0

Update  WORKSPACE..RAAdvisor_Data  
Set     Post_Code = str_replace(Post_Code, '>','')
where   isnumeric(Post_Code) =0
ANd     Post_Code LIKE '%>%'

Update  WORKSPACE..RAAdvisor_Data 
SET     Post_Code = '0000' 
where   Post_Code NOT LIKE '%[0-9]%' 
and     isnumeric(Post_Code) =0

Update  WORKSPACE..RAAdvisor_Data 
SET     Post_Code = str_replace(ltrim(rtrim(Post_Code)),' ',  null) 
where Post_Code like '% %'

Update WORKSPACE..RAAdvisor_Data 
SET Nature_Of_Person ='15' where Nature_Of_Person is null

UPDATE WORKSPACE..RAMember_Data
SET    Nature_Of_Person ='12'  
FROM   WORKSPACE..RAAdvisor_Data P, PeopleDates PD 
where  P.Broker_Number = PD.People_Number
AND    P.Nature_Of_Person ='1'
AND    PD.Date_Type = 'D'

UPDATE  WORKSPACE..RAAdvisor_Data 
SET Passport_No = NULL  WHERE Passport_No IS NOT NULL

IF EXISTS(SELECT 1 FROM Tenant WHERE Name = 'IMS')
BEGIN
    update WORKSPACE..RAAdvisor_Data
    set Post_1  =  '100 Grayston Drive',
        Post_2='Sandown', 
        Post_3= 'Sandton', 
        Post_4='',
        Post_5='',
        Post_Code='2146'
    where isnull(Post_1,'')  =  ''
END

IF EXISTS(SELECT 1 FROM Tenant WHERE Name = 'RMB')
BEGIN
    update WORKSPACE..RAAdvisor_Data
    set Post_1  =   '2 Merchant Place',
        Post_2  =   '1 Fredman Drive', 
        Post_3  =   'Sandton', 
        Post_4  =   '',
        Post_5  =   'South Africa',
        Post_Code=  '2196'
    where isnull(Post_1,'')  =  ''
END

UPDATE WORKSPACE..RAAdvisor_Data
SET Post_1 = str_replace(Post_1,',',' ')
WHERE Post_1  Like '%,%'
GO

--2016 once off updates...

update WORKSPACE..RAAdvisor_Data
set Name = p.Name
from WORKSPACE..RAAdvisor_Data w, People p
where w.Broker_Number = p.People_Number
and isnull(w.Name,'') = ''

update WORKSPACE..RAAdvisor_Data
set Name = 'Neethling'
where Owner = 101707

go

