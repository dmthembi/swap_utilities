
set nocount on
select 'Remove Invalid characters from Addresses'

declare @Broker_Number          int, 
        @Product_Code   	varchar(5),
        @Address_Line	varchar(35),
	   @Line_1        	varchar(35),
	   @Line_2        	varchar(35),
	   @Line_3        	varchar(35),
	   @Line_4        	varchar(35),
	   @Line_5        	varchar(35),
	   @PostalCode      varchar(10),
	   @pointer        	int,
	   @updated		bit ,
        @Count          	int

 

select Broker_Number,  Post_1, Post_2, Post_3, Post_4, Post_5,  Post_Code
into  #Cursor
from WORKSPACE..RAAdvisor_Data

set rowcount 1
select @Broker_Number = Broker_Number, @Line_1 = Post_1, @Line_2 = Post_2, @Line_3 = Post_3, @Line_4 = Post_4, @Line_5 = Post_5,  @PostalCode = Post_Code
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
set rowcount 0


select ' Broker : ', @Broker_Number 

while ((select count(*) from #Cursor) > 0 )
begin

select @updated = 0

select @Address_Line = Post_1
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		
		update WORKSPACE..RAAdvisor_Data
		set Post_1 = case when @Address_Line = '' then '100 Grayston Drive' else @Address_Line end
		where Broker_Number           =   @Broker_Number 
		and @updated = 1
	

select @Address_Line = Post_2
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		
		update WORKSPACE..RAAdvisor_Data
		set Post_2 = case when @Address_Line = '' then 'Sandown' else @Address_Line end
		where Broker_Number           =   @Broker_Number 
		and @updated = 1

		
select @Address_Line = Post_3
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
			
		update WORKSPACE..RAAdvisor_Data
		set Post_3 = case when @Address_Line = '' then 'Sandown' else @Address_Line end
		where Broker_Number           =   @Broker_Number 
		and @updated = 1
		
	

select @Address_Line = Post_4
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		
		update WORKSPACE..RAAdvisor_Data
		set Post_4 = @Address_Line
		where Broker_Number           =   @Broker_Number 
		and @updated = 1
		




select @Address_Line = Post_5
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		update WORKSPACE..RAAdvisor_Data
		set Post_5 = @Address_Line
		where Broker_Number           =   @Broker_Number 
		and @updated = 1
		



select @Address_Line = Post_Code
from  WORKSPACE..RAAdvisor_Data
where Broker_Number = @Broker_Number 
		
		select @pointer = patindex('%[^0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^0-9]%',@Address_Line)
		end
		
	
		update WORKSPACE..RAAdvisor_Data
		set Post_Code = case when @Address_Line = '' then '2146' else @Address_Line end
		where Broker_Number           =   @Broker_Number 
		and @updated = 1

		
		if @updated = 1
		select 'updated Broker ' , @Broker_Number 
		--else
		--select 'Broker ' , @Broker_Number ,' Clean '
  

delete #Cursor
where Broker_Number           =   @Broker_Number 


--select count(*), ' records left to process' from #Cursor

set rowcount 1
select @Broker_Number = Broker_Number
from  #Cursor
set rowcount 0

end


drop table #Cursor
go