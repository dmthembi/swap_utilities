set nocount on

if object_id('WORKSPACE..RAMember_Data') is not null
drop table WORKSPACE..RAMember_Data
if object_id('#yearTotal') is not null
drop table #yearTotal
if object_id('#dealTotal') is not null
drop table #dealTotal
if object_id('#contractTotal') is not null
drop table #contractTotal
if object_id('#Owners ') is not null
drop table  #Owners 
if object_id('#Batch') is not null
drop table #Batch
if object_id('#Owner_List') is not null
drop table #Owner_List

go

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates


select distinct Owner into #Owners from RAContributionCert where Year = @Tax_Year

select * into #Batch from #Owners where 1=2

set rowcount 0
insert #Batch select * from #Owners 
set rowcount 0

CREATE TABLE #Owner_List
( 
  Unique_Number INT IDENTITY,
  Section_Identifier CHAR(1),
  Record_Type CHAR(3),
  Record_Status CHAR(1),
  Row_Number INT NULL,
  Unique_Membership_No VARCHAR(50) NULl,
  Nature_Of_Person CHAR(2) NULL,
  Comm_Date DATETIME NULL,
  Name          varchar(50)   null,
  Initials      varchar(10)   null,
  First_Names   varchar(50)   null,
  Identity_No Varchar(50) NULL,
  Passport_No Varchar(50) NULL,
  Country_Of_Issue Varchar(5) NULL,
  IT_Ref_No INT NULL,
  Other_Reg_No   Varchar(50) NULL,
  DOB DATETIME NULL,
  Post_1		varchar(35) null,
  Post_2		varchar(35) null,
  Post_3		varchar(35) null,
  Post_4		varchar(35) null,
  Post_5		varchar(35) null,
  Post_Code	varchar(35) null,
  Financial_Adv_Idicator  CHAR(1) NULL,
  Owner INT,
  Product Varchar(20)
)

  
INSERT INTO #Owner_List
SELECT DISTINCT
    'B' as Section_Identifier,
    'MDD' as Record_Type,
    'A' as Record_Status,
     NULL as Row_Number,
     NULL as Unique_Membership_No,
     NULL as Nature_Of_Person,
     NULL as Comm_Date,
     NULL as Name,
     NULL as Initials,
     NULL as First_Names,
     NULL as DOB,
     NULL as Identity_No,
     NULL as Passport_No,
     NULL as Country_Of_Issue,
     NULL as IT_Ref_No,
     NULL as Other_Reg_No,
	 Post_1		=	convert(varchar(35),null),
	 Post_2		=	convert(varchar(35),null),
	 Post_3		=	convert(varchar(35),null),
	 Post_4		=	convert(varchar(35),null),
	 Post_5		=	convert(varchar(35),null),
	 Post_Code	=	convert(varchar(10),null),
     NULL as Financial_Adv_Idicator,
	 Owner,
	 Product
from	RAContributionCert ra 
where	ra.Year		=	@Tax_Year 
and     ra.Owner in (select Owner from #Batch)

----- **** Row_Number
UPDATE #Owner_List
SET Row_Number = Unique_Number

----- ****Unique_Membership_No
UPDATE #Owner_List
SET Unique_Membership_No = convert(varchar(20),Owner)

----- **** Nature Of Person
update  #Owner_List
set     Nature_Of_Person=CONVERT(CHAR(2),N.SARS_Code)
from    #Owner_List PD ,
        WORKSPACE..NatureOfPersonMapping N,
        People P 
where   PD.Owner = P.People_Number 
AND     P.Customer_Type = N.Customer_Type

----- **** Name
UPDATE #Owner_List
SET PD.Name = P.Name,
PD.Initials =  P.Initials,
PD.First_Names = P.First_Names
FROM #Owner_List PD,
     People P 
WHERE PD.Owner = P.People_Number 

----- ****  Commensment Date
update #Owner_List
set Comm_Date = convert(varchar(8), Date,112)
from #Owner_List w,
    ContractDeal CD,
	DealDates DD
where w.Owner = CD.Owner
AND CD.Deal_Xref = DD.Deal_Xref
and DD.Date_type = 'DC'

update #Owner_List
set Comm_Date = convert(varchar(8), Date,112)
from #Owner_List w,
    ContractDeal CD,
	DealDates DD
where w.Owner = CD.Holder
AND CD.Deal_Xref = DD.Deal_Xref
and Comm_Date is null
and DD.Date_type = 'DC'

----- ****   Date of Birth
update #Owner_List
set DOB = convert(varchar(8), Date,112)
from #Owner_List w,
	PeopleDates pd
where w.Owner = pd.People_Number
and pd.Date_Type = 'B'

----- ****   Postal Address
update #Owner_List
set Post_1 = p.Line_1,
Post_2 = p.Line_2,
Post_3 = p.Line_3,
Post_4 = p.Line_4,
Post_5 = p.Line_5,
Post_Code = Postal_Code
from    #Owner_List ra,
	PeopleAddress		    p
where ra.Owner	=	p.People_Number
and p.Address_Type = 'POST'

----- ****   Financial_Adv_Idicator
update #Owner_List
SET Financial_Adv_Idicator = CASE WHEN Secondary_Person IS NOT NULL THEN  'Y'
ELSE 'N' END
FROM  #Owner_List OL,PeopleRelationships PR 
WHERE Relation_Type = 1
AND EndDate IS NULL
AND PR.Primary_Person = OL.Owner 

----- ****   Identity_No
update #Owner_List
SET Identity_No = Reg_Value
FROM  #Owner_List OL,PeopleRegistration PR 
WHERE Reg_Type = 1
AND PR.People_Number = OL.Owner 

----- ****   Identity_No
SELECT * INTO WORKSPACE..RAMember_Data FROM #Owner_List
GO
