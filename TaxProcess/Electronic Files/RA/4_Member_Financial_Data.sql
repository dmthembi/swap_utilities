SET NOCOUNT ON 

if object_id('WORKSPACE..RA_Member_Financial_Data') is not null
drop table WORKSPACE..RA_Member_Financial_Data
if object_id('#yearTotal') is not null
drop table #yearTotal
if object_id('#TaxYearBeginDates') is not null
drop table #TaxYearBeginDates
if object_id('#dealTotal') is not null
drop table #dealTotal
if object_id('#contractTotal') is not null
drop table #contractTotal
if object_id('#Owners ') is not null
drop table  #Owners 
if object_id('#Batch') is not null
drop table #Batch
if object_id('#Financial_Data') is not null
drop table #Financial_Data
if object_id('#Temp') is not null
drop table #Temp
if object_id('#Months') is not null
drop table #Months
if object_id('#Total') is not null
drop table #Total

go






DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates

select Tax_Year = @Tax_Year, Begin_Date, End_Date
into #TaxYearBeginDates
from TaxYearBeginDates
where Tax_Year = @Tax_Year

--update #TaxYearBeginDates
--set End_Date = '31 aug 2015'


select distinct Owner 
into #Owners 
from RAContributionCert r, #TaxYearBeginDates t
where r.Year = t.Tax_Year



select * into #Batch from #Owners where 1=2

set rowcount 0
insert #Batch select * from #Owners 
set rowcount 0


CREATE TABLE #Financial_Data
( 
  Unique_Number INT IDENTITY,
  Section_Identifier CHAR(1),
  Record_Type CHAR(6),
  Record_Status CHAR(1),
  Row_Number INT NULL,
  Unique_Membership_No VARCHAR(50) NULl,
  Current_Ret_Ann_Fund INT NULL,
  Owner INT,
  Product Varchar(20),
  March FLOAT NULL,
  April FLOAT NULL,
  May FLOAT NULL,
  June FLOAT NULL,
  July FLOAT NULL,
  August FLOAT NULL,
  September FLOAT NULL,
  October FLOAT NULL,
  November FLOAT NULL,
  December FLOAT NULL, 
  January FLOAT NULL,
  February FLOAT NULL,
  Total FLOAT NULL
)

  
INSERT INTO #Financial_Data
SELECT 
    'B' as Section_Identifier,
    'MFD' as Record_Type,
    'A' as Record_Status,
     NULL as Row_Number,
     Unique_Membership_No as Unique_Membership_No,
     4006 as Current_Ret_Ann_Fund,
	 Owner,
	 Product,
	 convert(float,0.00) as March,
	 convert(float,0.00) as April,
	 convert(float,0.00) as May,
	 convert(float,0.00) as June,
	 convert(float,0.00) as July,
	 convert(float,0.00) as August,
	 convert(float,0.00) as September,
	 convert(float,0.00) as October,
	 convert(float,0.00) as November,
	 convert(float,0.00) as December,
	 convert(float,0.00) as January,
	 convert(float,0.00) as February,
     convert(float,0.00) as Total
from	WORKSPACE..RAMember_Data ra 


----- **** Row_Number
UPDATE #Financial_Data
SET Row_Number = Unique_Number




select w.*
into #RAContributionDetail
from WORKSPACE..RAContributionDetail w, #TaxYearBeginDates t
where w.Tax_Year = t.Tax_Year
and w.Amount > 0




update #RAContributionDetail
set Date_Action = t.End_Date
from #RAContributionDetail w, #TaxYearBeginDates t
where w.Tax_Year = t.Tax_Year
and w.Date_Action > t.End_Date


select  Owner,
        Product,
        Month=datepart(mm,Date_Action), 
        Amount = sum(convert(float,Amount))
into #Months
from #RAContributionDetail 
group by Owner ,Product  , datepart(mm,Date_Action)


select Owner, Product,  Amount = sum(convert(float,w.Amount))
into #Total
from #RAContributionDetail w
group by Owner, Product  


UPDATE #Financial_Data
SET March = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 3 -- March

UPDATE #Financial_Data
SET April = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 4 -- April

UPDATE #Financial_Data
SET May = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 5 -- May

UPDATE #Financial_Data
SET June = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 6 -- June

UPDATE #Financial_Data
SET July = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 7 -- July

UPDATE #Financial_Data
SET August = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 8 -- August

UPDATE #Financial_Data
SET September = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 9 -- September


UPDATE #Financial_Data
SET October = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 10 -- October

UPDATE #Financial_Data
SET November = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 11 -- November

UPDATE #Financial_Data
SET December = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 12 -- December

UPDATE #Financial_Data
SET January = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 1 -- January

UPDATE #Financial_Data
SET February = M.Amount
FROM #Financial_Data F, #Months M
WHERE F.Owner = M.Owner
and F.Product = M.Product
AND M.Month = 2 -- February


UPDATE #Financial_Data
SET Total = Amount
FROM #Financial_Data F, #Total T
WHERE F.Owner = T.Owner
and   F.Product = T.Product
go
select *
from #Total


SELECT * INTO  WORKSPACE..RA_Member_Financial_Data from #Financial_Data where Total > 0
go


select *
from WORKSPACE..RA_Member_Financial_Data

