set nocount on

if object_id('WORKSPACE..Fund_Entity') is not null
drop table WORKSPACE..Fund_Entity
if object_id('#yearTotal') is not null
drop table #yearTotal
if object_id('#dealTotal') is not null
drop table #dealTotal
if object_id('#contractTotal') is not null
drop table #contractTotal
if object_id('#Owners ') is not null
drop table  #Owners 
if object_id('#Batch') is not null
drop table #Batch
if object_id('#Fund_Entity') is not null
drop table #Fund_Entity

go

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates



CREATE TABLE #Fund_Entity
( 
  Unique_Number INT IDENTITY,
  Section_Identifier CHAR(1),
  Record_Type CHAR(6),
  Row_Number INT NULL,
  Year INT,
  StartDate VARCHAR(10) ,
  EndDate VARCHAR(10) ,
  Name_Of_Fund_Entity VARCHAR(200) NULL,
  Fund_Entity_Tax VARCHAR(200) NULL,
  Reg_Insurnce_No VARCHAr(20) NULL,
  Member_Start_Year INT NULL,
  Member_During_Year INT NULL,
  Member_Resigned INT NULL,
  Member_End_Year INT NULL,
  Member_Premiums NUMERIC(18,2) NULL,
  Total_Premiums NUMERIC(18,2) NULL
)

INSERT INTO #Fund_Entity
SELECT 
    'B' as Section_Identifier,
    'FE' as Record_Type,
     NULL as Row_Number,
     @Tax_Year as Year,
     str_replace(convert(varchar(11), @Start_Date,102),'.','-') as StartDate,
     str_replace(convert(varchar(11), @End_Date,102),'.','-') as EndDate,
      NULL as Name_Of_Fund_Entity,
      Null as Fund_Entity_Tax ,
      Null as Reg_Insurnce_No    ,
	  NULL as Member_Start_Year,
	  NULL as Member_During_Year,
	  NULL as Member_Resigned,
	  NULL as Member_End_Year,
      NULL AS Member_Premiums,
      NULL AS Total_Premiums

----- **** Row_Number
UPDATE #Fund_Entity
SET Row_Number = Unique_Number

----- **** Name_Of_Fund_Entity
IF EXISTS(SELECT 1 FROM Tenant Where Name = 'IMS')
BEGIN
    UPDATE #Fund_Entity
    SET Name_Of_Fund_Entity = Name
    from People where People_Number = 449065

    ----- **** Fund_Entity_Tax
    UPDATE #Fund_Entity
    SET Fund_Entity_Tax = NULL
    from PeopleTax where People_Number = 449065

    ----- **** Reg_Insurnce_No
    UPDATE #Fund_Entity
    SET Reg_Insurnce_No = '1282622'
    from PeopleRegistration where People_Number = 449065 

END

IF EXISTS(SELECT 1 FROM Tenant Where Name = 'RMB')
BEGIN
    UPDATE  #Fund_Entity
    SET     Name_Of_Fund_Entity = Name,
            Fund_Entity_Tax = NULL,
            Reg_Insurnce_No = '38102'
    from People where People_Number = 10569

END

----- ****Member_Start_Year
DECLARE  @Member_Start_Year INT
SELECT  @Member_Start_Year =  Count( DISTINCT Owner) FROM RAContributionCert WHERE  Year = @Tax_Year

UPDATE #Fund_Entity
SET Member_Start_Year = @Member_Start_Year

----- ****Member_During_Year

---Calculate total Deal that commenced in 2014 Tax Year.
DECLARE  @Member_During_Year INT,@Common INT

SELECT  @Member_During_Year =  COUNT(DISTINCT Owner) FROM ContractDeal CD,DealDates DD
WHERE Product IN  ('IRAF','INVRA','RA')
AND CD.Deal_Xref = DD.Deal_Xref
and Date_type = 'DC'
AND 
DD.Date BEtween @Start_Date and @End_Date

---Calculate If any of the Owner is in Previous Tax year.

SELECT DISTINCT Owner 
into #Owner
FROM ContractDeal CD,DealDates DD
WHERE Product IN  ('IRAF','INVRA','RA')
AND CD.Deal_Xref = DD.Deal_Xref
and Date_type = 'DC'
AND 
DD.Date BEtween @Start_Date and @End_Date



SELECT  @Common = COUNT(DISTINCT Owner) FROM RAContributionCert WHERE  Year = @Tax_Year and Owner in (select Owner from #Owner)


SELECT @Member_During_Year = @Member_During_Year  - @Common

UPDATE #Fund_Entity
SET Member_During_Year = @Member_During_Year

----- ****Member_Resigned
DECLARE  @Member_Resigned INT

SELECT DISTINCT Owner 
into #Owner2
FROM RAContributionCert 
WHERE  Year = @Tax_Year

SELECT  @Member_Resigned = Count(DISTINCT Owner) FROM ContractDeal CD,Deal  D
WHERE CD.Deal_Xref = D.Deal_Xref
AND Owner IN (select Owner from #Owner2)

AND Product IN  ('IRAF','INVRA','RA')
AND D.Status = 'X'

UPDATE #Fund_Entity
SET Member_Resigned = @Member_Resigned

----- ****Member_End_Year
DECLARE  @Member_End_Year INT
SELECT  @Member_End_Year =  Count(DISTINCT Owner) FROM RAContributionCert WHERE  Year = @Tax_Year

UPDATE #Fund_Entity
SET Member_End_Year = @Member_End_Year

----- ****Member_Premiums
DECLARE  @Member_Premiums NUMERIC(18,2)
SELECT @Member_Premiums  = SUM(Contribution) FROM RAContributionCert WHERE  Year = @Tax_Year

UPDATE #Fund_Entity
SET Member_Premiums = @Member_Premiums

----- ****Total_Premiums
UPDATE #Fund_Entity
SET Total_Premiums = @Member_Premiums

SELECT * INTO WORKSPACE..Fund_Entity FROM  #Fund_Entity
GO