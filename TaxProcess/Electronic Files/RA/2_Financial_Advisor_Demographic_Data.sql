set nocount on

if object_id('WORKSPACE..RAAdvisor_Data') is not null
drop table WORKSPACE..RAAdvisor_Data
if object_id('#yearTotal') is not null
drop table #yearTotal
if object_id('#dealTotal') is not null
drop table #dealTotal
if object_id('#contractTotal') is not null
drop table #contractTotal
if object_id('#Owners ') is not null
drop table  #Owners 
if object_id('#Batch') is not null
drop table #Batch
if object_id('#Fin_Adv_List') is not null
drop table #Fin_Adv_List

go

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates


select distinct Owner into #Owners from RAContributionCert where Year = @Tax_Year

select * into #Batch from #Owners where 1=2

set rowcount 0
insert #Batch select * from #Owners 
set rowcount 0

CREATE TABLE #Fin_Adv_List
( 
  Unique_Number INT IDENTITY,
  Section_Identifier CHAR(1),
  Record_Type CHAR(6),
  Record_Status CHAR(1),
  Row_Number INT NULL,
  Unique_Membership_No VARCHAR(50) NULl,
  Registration_Unique_No VARCHAR(50) NULL,
  Nature_Of_Person CHAR(2) NULL,
  Name          varchar(50)   null,
  Initials      varchar(10)   null,
  First_Names   varchar(50)   null,
  Identity_No Varchar(50) NULL,
  Passport_No Varchar(50) NULL,
  Country_Of_Issue Varchar(5) NULL,
  IT_Ref_No INT NULL,
  Other_Reg_No   Varchar(50) NULL,
  DOB DATETIME NULL,
  Post_1		varchar(35) null,
  Post_2		varchar(35) null,
  Post_3		varchar(35) null,
  Post_4		varchar(35) null,
  Post_5		varchar(35) null,
  Post_Code	varchar(35) null,
  Owner INT,
  Broker_Number INT NULL
)

  
INSERT INTO #Fin_Adv_List
SELECT 
    'B' as Section_Identifier,
    'MFADD' as Record_Type,
    'A' as Record_Status,
     NULL as Row_Number,
     Unique_Membership_No as Unique_Membership_No,
     NULL as Registration_Unique_No,
     '01' as Nature_Of_Person,
     NULL as Name,
     NULL as Initials,
     NULL as First_Names,
     NULL as DOB,
     NULL as Identity_No,
     NULL as Passport_No,
     NULL as Country_Of_Issue,
     NULL as IT_Ref_No,
     NULL as Other_Reg_No,
	 Post_1		=	convert(varchar(35),null),
	 Post_2		=	convert(varchar(35),null),
	 Post_3		=	convert(varchar(35),null),
	 Post_4		=	convert(varchar(35),null),
	 Post_5		=	convert(varchar(35),null),
	 Post_Code	=	convert(varchar(10),null),
	 Owner,
     Null as Broker_Number
from	WORKSPACE..RAMember_Data ra 

----- **** Row_Number
UPDATE #Fin_Adv_List
SET Row_Number = Unique_Number

----- **** Broker_Number
update #Fin_Adv_List
SET Broker_Number = Secondary_Person
FROM  #Fin_Adv_List OL,PeopleRelationships PR 
WHERE Relation_Type = 1
AND EndDate IS NULL
AND PR.Primary_Person = OL.Owner 

----- **** Name
UPDATE #Fin_Adv_List
SET PD.Name = P.Name,
PD.Initials =  P.Initials,
PD.First_Names = P.First_Names
FROM #Fin_Adv_List PD,
     People P 
WHERE PD.Broker_Number = P.People_Number 

----- ****   Date of Birth
update #Fin_Adv_List
set DOB = convert(varchar(8), Date,112)
from #Fin_Adv_List w,
	PeopleDates pd
where w.Broker_Number = pd.People_Number
and pd.Date_Type = 'B'

----- ****   Postal Address
update #Fin_Adv_List
set Post_1 = p.Line_1,
Post_2 = p.Line_2,
Post_3 = p.Line_3,
Post_4 = p.Line_4,
Post_5 = p.Line_5,
Post_Code = Postal_Code
from    #Fin_Adv_List ra,
	PeopleAddress		    p
where ra.Broker_Number	=	p.People_Number
and p.Address_Type = 'POST'

----- ****  Registration_Unique_No
update #Fin_Adv_List
SET Registration_Unique_No = Reg_Value
FROM #Fin_Adv_List FA,BrokerCorporate BC,PeopleRegistration PR
WHERE FA.Broker_Number = BC.Broker_Number
AND PR.People_Number = BC.Corporate_Number
AND Reg_Type = 11



----- **** Get The Other Broker
update #Fin_Adv_List
SET Broker_Number = Secondary_Person
FROM  #Fin_Adv_List OL,PeopleRelationships PR 
WHERE Relation_Type = 1
AND EndDate IS NULL
AND PR.Primary_Person = OL.Owner 
and OL.Registration_Unique_No is null
and OL.Broker_Number <> PR.Secondary_Person

----- ****  Get the other Registration_Unique_No

update #Fin_Adv_List
SET Registration_Unique_No = Reg_Value
FROM #Fin_Adv_List FA,BrokerCorporate BC,PeopleRegistration PR
WHERE FA.Broker_Number = BC.Broker_Number
AND PR.People_Number = BC.Corporate_Number
AND Reg_Type = 11
and Registration_Unique_No is null


update #Fin_Adv_List
set Registration_Unique_No = '0000'
where Registration_Unique_No is null

----- ****   Identity_No
update #Fin_Adv_List
SET Identity_No = Reg_Value
FROM  #Fin_Adv_List OL,PeopleRegistration PR 
WHERE Reg_Type = 1
AND PR.People_Number = OL.Broker_Number 

----- ****   Identity_No
update #Fin_Adv_List
SET Passport_No = Reg_Value
FROM  #Fin_Adv_List OL,PeopleRegistration PR 
WHERE Reg_Type In(2,16)
AND PR.People_Number = OL.Broker_Number 

update #Fin_Adv_List
set Country_Of_Issue = cm.SARS_CountryCode
from #Fin_Adv_List pd, PeopleCountry pc, CountryMap cm
where pd.Broker_Number = pc.People_Number
and pc.Code			= cm.ISO_CountryCode
and pc.Country_Type = 'PA'

SELECT * INTO WORKSPACE..RAAdvisor_Data FROM  #Fin_Adv_List
GO
