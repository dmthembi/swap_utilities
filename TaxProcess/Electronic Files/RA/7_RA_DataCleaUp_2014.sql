-- RA Data Cleanup - 2014
-- IMS

DELETE FROM WORKSPACE..RAMember_Data WHERE Owner IN (SELECT People_Number FROM People WHERE Name = 'Client')
DELETE FROM WORKSPACE..RAAdvisor_Data where Owner IN (SELECT People_Number FROM People WHERE Name = 'Client')
DELETE FROM WORKSPACE..RA_Member_Financial_Data where Owner IN (SELECT People_Number FROM People WHERE Name = 'Client')
GO

UPDATE  WORKSPACE..RAMember_Data
SET     Financial_Adv_Idicator = 'N'
WHERE   Owner In
        (
        SELECT 
        DISTINCT    Owner 
        FROM        WORKSPACE..RAAdvisor_Data 
        WHERE       Identity_No IS NULL 
        AND         Passport_No IS NULL
        AND         DOB IS NULL
        )
GO

IF EXISTS (SELECT 1 FROM Tenant WHERE Name = 'IMS')
BEGIN
    UPDATE  WORKSPACE..RAAdvisor_Data 
    SET     Post_1  =  '100 Grayston Drive',
            Post_2='Sandown', 
            Post_3= 'Sandton', 
            Post_4='',
            Post_5='',
            Post_Code='2146'
    WHERE   (Post_1 IS NULL OR Post_1 = '')
    AND     (Post_2 IS NULL OR Post_2 = '')
    AND     (Post_3 IS NULL OR Post_3 = '')
    AND     (Post_4 IS NULL OR Post_4 = '')
    AND     (Post_5 IS NULL OR Post_5 = '')
    AND     (Post_Code IS NULL OR Post_Code  = '')
	
	    UPDATE  WORKSPACE..RAMember_Data 
    SET     Post_1  =  '100 Grayston Drive',
            Post_2='Sandown', 
            Post_3= 'Sandton', 
            Post_4='',
            Post_5='',
            Post_Code='2146'
    WHERE   (Post_1 IS NULL OR Post_1 = '')
    AND     (Post_2 IS NULL OR Post_2 = '')
    AND     (Post_3 IS NULL OR Post_3 = '')
    AND     (Post_4 IS NULL OR Post_4 = '')
    AND     (Post_5 IS NULL OR Post_5 = '')
   
END

IF EXISTS (SELECT 1 FROM Tenant WHERE Name = 'RMB')
BEGIN
    UPDATE  WORKSPACE..RAAdvisor_Data 
    SET     Post_1  =   '2 Merchant Place',
            Post_2  =   '1 Fredman Drive', 
            Post_3  =   'Sandton', 
            Post_4  =   '',
            Post_5  =   'South Africa',
            Post_Code=  '2196'
    WHERE   (Post_1 IS NULL OR Post_1 = '')
    AND     (Post_2 IS NULL OR Post_2 = '')
    AND     (Post_3 IS NULL OR Post_3 = '')
    AND     (Post_4 IS NULL OR Post_4 = '')
    AND     (Post_5 IS NULL OR Post_5 = '')
    AND     (Post_Code IS NULL OR Post_Code  = '')
END

GO

UPDATE  WORKSPACE..RAAdvisor_Data   
SET     Post_1 = STR_REPLACE(Post_1,',',' '),
        Post_2 = STR_REPLACE(Post_2,',',' '),
        Post_3 = STR_REPLACE(Post_3,',',' '),
        Post_4 = STR_REPLACE(Post_4,',',' '),
        Post_5 = STR_REPLACE(Post_5,',',' ')
GO


UPDATE  WORKSPACE..RAMember_Data
SET     Financial_Adv_Idicator = 'N'
WHERE   Owner In
        (
        SELECT 
        DISTINCT    Owner 
        FROM        WORKSPACE..RAAdvisor_Data 
        WHERE       Registration_Unique_No IS NULL
        )
GO

UPDATE  WORKSPACE..RAMember_Data
SET     Financial_Adv_Idicator = 'N'
WHERE   Owner In
        (
        SELECT 
        DISTINCT    Owner 
        FROM        WORKSPACE..RAAdvisor_Data 
        WHERE       Initials IS NULL 
        OR          Initials = ''
        )
GO

UPDATE  WORKSPACE..RAMember_Data
SET     Financial_Adv_Idicator = 'N'
WHERE   Owner In
        (
        SELECT 
        DISTINCT    Owner 
        FROM        WORKSPACE..RAAdvisor_Data 
        WHERE       First_Names IS NULL 
        OR          First_Names = ''
        )
GO

UPDATE  WORKSPACE..RAMember_Data
SET     Financial_Adv_Idicator = 'N'
WHERE   Owner In
        (
        SELECT 
        DISTINCT    Owner 
        FROM        WORKSPACE..RAAdvisor_Data 
        WHERE       Passport_No IS NOT NULL
        AND         Country_Of_Issue IS NULL
        )
GO

UPDATE  WORKSPACE..RA_Member_Financial_Data
SET     March       = CASE WHEN March       < 0 THEN 0 ELSE March       END,
        April       = CASE WHEN April       < 0 THEN 0 ELSE April       END,
        May         = CASE WHEN May         < 0 THEN 0 ELSE May         END, 
        June        = CASE WHEN June        < 0 THEN 0 ELSE June        END,
        July        = CASE WHEN July        < 0 THEN 0 ELSE July        END,
        August      = CASE WHEN August      < 0 THEN 0 ELSE August      END,
        September   = CASE WHEN September   < 0 THEN 0 ELSE September   END,
        October     = CASE WHEN October     < 0 THEN 0 ELSE October     END,
        November   = CASE WHEN November   < 0 THEN 0 ELSE November   END,
        December    = CASE WHEN December    < 0 THEN 0 ELSE December    END,
        January     = CASE WHEN January     < 0 THEN 0 ELSE January     END,
        February    = CASE WHEN February    < 0 THEN 0 ELSE February    END
GO

