
set nocount on
select 'Remove Invalid characters from Addresses'

declare @Unique_Membership_No          varchar(20), 
        @Product_Code   	varchar(5),
        @Address_Line	varchar(35),
	   @Line_1        	varchar(35),
	   @Line_2        	varchar(35),
	   @Line_3        	varchar(35),
	   @Line_4        	varchar(35),
	   @Line_5        	varchar(35),
	   @PostalCode      varchar(10),
	   @pointer        	int,
	   @updated		bit ,
        @Count          	int

 

select Unique_Membership_No,  Post_1, Post_2, Post_3, Post_4, Post_5,  Post_Code
into  #Cursor
from WORKSPACE..RAMember_Data

set rowcount 1
select @Unique_Membership_No = Unique_Membership_No, @Line_1 = Post_1, @Line_2 = Post_2, @Line_3 = Post_3, @Line_4 = Post_4, @Line_5 = Post_5,  @PostalCode = Post_Code
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
set rowcount 0


select ' Broker : ', @Unique_Membership_No 

while ((select count(*) from #Cursor) > 0 )
begin

select @updated = 0

select @Address_Line = Post_1
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		select @Address_Line = isnull(@Address_Line,'')
		
		update WORKSPACE..RAMember_Data
		set Post_1 = case when ltrim(rtrim(@Address_Line)) = '' then '100 Grayston Drive' else @Address_Line end
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1
		
		update WORKSPACE..RAMember_Data
		set Post_1 = case when @Address_Line = ' ' then '100 Grayston Drive' else @Address_Line end
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1
	

select @Address_Line = Post_2
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		select @Address_Line = isnull(@Address_Line,'')
		update WORKSPACE..RAMember_Data
		set Post_2 = case when @Address_Line = '' then 'Sandown' else @Address_Line end
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1

		
select @Address_Line = Post_3
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		select @Address_Line = isnull(@Address_Line,'')	
		update WORKSPACE..RAMember_Data
		set Post_3 = case when @Address_Line = '' then 'Sandown' else @Address_Line end
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1
		
	

select @Address_Line = Post_4
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		select @Address_Line = isnull(@Address_Line,'')
		update WORKSPACE..RAMember_Data
		set Post_4 = @Address_Line
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1
		




select @Address_Line = Post_5
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
		
		select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^ a-zA-Z0-9]%',@Address_Line)
		end
		
		select @Address_Line = isnull(@Address_Line,'')
		update WORKSPACE..RAMember_Data
		set Post_5 = @Address_Line
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1
		



select @Address_Line = Post_Code
from  WORKSPACE..RAMember_Data
where Unique_Membership_No = @Unique_Membership_No 
		
		select @pointer = patindex('%[^0-9]%',@Address_Line)
		while (@pointer > 0)
		begin
		 select @updated = 1
		 select @Address_Line = str_replace(@Address_Line,substring(@Address_Line, @pointer, 1),null)
		 select @pointer = patindex('%[^0-9]%',@Address_Line)
		end
		
	    select @Address_Line = isnull(@Address_Line,'')
		update WORKSPACE..RAMember_Data
		set Post_Code = case when @Address_Line = '' then '2146' else @Address_Line end
		where Unique_Membership_No           =   @Unique_Membership_No 
		and @updated = 1

		
		if @updated = 1
		select 'updated Owner ' , @Unique_Membership_No 
		--else
		--select 'Broker ' , @Unique_Membership_No ,' Clean '
  

delete #Cursor
where Unique_Membership_No           =   @Unique_Membership_No 


--select count(*), ' records left to process' from #Cursor

set rowcount 1
select @Unique_Membership_No = Unique_Membership_No
from  #Cursor
set rowcount 0

end


drop table #Cursor
go