
set nocount on
select 'Remove Invalid characters from First Names'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int

select Owner, First_Names
into  #Cursor
from WORKSPACE..RAMember_Data

UPDATE WORKSPACE..RAMember_Data
SET Nature_Of_Person = '01'
WHERE Nature_Of_Person = '1'

select @Owner = Owner, @Surname = First_Names
from  WORKSPACE..RAMember_Data
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
    -- select substring(@Surname, @Count, 1)
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
       select @Owner, @Surname, @Count
       update WORKSPACE..RAMember_Data
       set First_Names = stuff(First_Names,@Count,1,null)
       where Owner           =   @Owner 

       select @Count = @Count - 1

       select @Surname = First_Names
       from  WORKSPACE..RAMember_Data
       where Owner           =   @Owner 

        select @Owner, @Surname, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 

set rowcount 1
select @Owner = Owner,  @Surname = First_Names
from  #Cursor
set rowcount 0

end
GO
drop table #Cursor
go

-----------------------------------------------------------------------------------------------------------------

select 'Remove Invalid characters from Name'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
        @Surname        varchar(20),
        @Count          int

select Owner, Name
into  #Cursor
from WORKSPACE..RAMember_Data

select @Owner = Owner, @Surname = Name
from  WORKSPACE..RAMember_Data
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin
  --select  @Owner, @Product_Code , @cpy_IRP5Number, @Surname , datalength(@Surname)
   select @Count = 1
   while (@Count <= datalength(@Surname))
   begin
    
     if (((ascii(substring(@Surname, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
        
        update WORKSPACE..RAMember_Data
        set Name = stuff(Name,@Count,1,null)
        where Owner           =   @Owner 

       
         
        select @Count = @Count - 1

        select @Surname = Name
         from  WORKSPACE..RAMember_Data
         where Owner           =   @Owner 
     
       select @Owner, @Surname, @Count
       end

     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 

set rowcount 1
select @Owner = Owner,  @Surname = Name
from  #Cursor
set rowcount 0

end

drop table #Cursor
go

-----------------------------------------------------------------------------------------------------------------
set nocount on
select 'Remove Invalid characters from Initials'

declare @Owner          int, 
        @Product_Code   varchar(5),
        @cpy_IRP5Number int,
	@Person_Init        varchar(5),
        @Count          int


select Owner,  Initials
into  #Cursor
from WORKSPACE..RAMember_Data

select @Owner = Owner, @Person_Init = Initials
from  WORKSPACE..RAMember_Data
where Owner = @Owner 

while ((select count(*) from #Cursor) > 0 )
begin

   select @Count = 1
   while (@Count <= datalength(@Person_Init))
   begin
    -- select substring(@Person_Init, @Count, 1)
     if (((ascii(substring(@Person_Init, @Count, 1))))  in (42, 35, 37, 64, 33, 36, 94, 38))
       begin
     
       update WORKSPACE..RAMember_Data
       set Initials = stuff(Initials,@Count,1,null)
       where Owner           =   @Owner 

       select @Count = @Count - 1

       select @Person_Init = Initials
       from  WORKSPACE..RAMember_Data
       where Owner           =   @Owner 

      select @Owner, @Person_Init, @Count
     
       end
     select @Count = @Count + 1
   end

delete #Cursor
where Owner           =   @Owner 

set rowcount 1
select @Owner = Owner,  @Person_Init = Initials
from  #Cursor
set rowcount 0

end


drop table #Cursor
go
-------------------------------------------------------------------------
Update WORKSPACE..RAMember_Data  
Set    Post_Code = '0000' 
where  Post_Code = null OR LEN(ltrim(rtrim(Post_Code)))  <= 1 OR LEN(Post_Code)  = 1

Update  WORKSPACE..RAMember_Data  
Set     Post_Code = str_replace(Post_Code, '-','')
where   isnumeric(Post_Code) =0

Update  WORKSPACE..RAMember_Data  
Set     Post_Code = str_replace(Post_Code, '>','')
where   isnumeric(Post_Code) =0
ANd     Post_Code LIKE '%>%'

Update  WORKSPACE..RAMember_Data 
SET     Post_Code = '0000' 
where   Post_Code NOT LIKE '%[0-9]%' 
and     isnumeric(Post_Code) =0

Update  WORKSPACE..RAMember_Data 
SET     Post_Code = str_replace(ltrim(rtrim(Post_Code)),' ',  null) 
where Post_Code like '% %'

-----------------------------------------------------------------------------------------
Update WORKSPACE..RAMember_Data 
SET Nature_Of_Person ='15' where Nature_Of_Person is null

UPDATE WORKSPACE..RAMember_Data
SET    Nature_Of_Person ='12'  
FROM   WORKSPACE..RAMember_Data P, PeopleDates PD 
where  P.Owner = PD.People_Number
AND    Nature_Of_Person ='1'
AND    PD.Date_Type = 'D'

UPDATE WORKSPACE..RAMember_Data
SET Post_1 = str_replace(Post_1,',',' ')
WHERE Post_1  Like '%,%'

-----------------------------------------------
/*
UPDATE  WORKSPACE..RAAdvisor_Data 
SET Nature_Of_Person = '15'
WHERE Broker_Number in (SELECT Broker_Number FROM #TEMP)
*/

SELECT Owner  INTO #Owner from WORKSPACE..RAAdvisor_Data  WHERE Nature_Of_Person = '15'

SELECT * FROM  WORKSPACE..RAMember_Data WHERE Owner IN (SELECT Owner FROM #Owner)

UPDATE WORKSPACE..RAMember_Data
SET Financial_Adv_Idicator = 'N'
 WHERE Owner IN (SELECT Owner FROM #Owner)
GO
