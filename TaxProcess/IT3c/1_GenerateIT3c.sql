USE IMS_MAIN
GO

--- ****************This script Generates IT3c Certs for the 2004 Tax Year********************

set  arithabort arith_overflow off


SELECT	'Start:', convert(varchar,getdate(),109)
go
------***************Start Of IT3c Generation*********************

if object_id('CGTWACError') is not null
drop table CGTWACError
go
CREATE TABLE CGTWACError
		(Deal_Xref int,
		 TotalCumUnits float) 

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates





EXEC	IT3cGenerate @Start_Date,@End_Date, @Tax_Year ,50--***, 79647, 66777


------***************End Of IT3c Generation*********************
go
set  arithabort arith_overflow on

SELECT	'End:', convert(varchar,getdate(),109)
go