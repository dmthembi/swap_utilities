IF OBJECT_ID('WORKSPACE..IT3C_AdjustmentFile') IS NOT NULL DROP TABLE WORKSPACE..IT3C_AdjustmentFile
GO

CREATE TABLE WORKSPACE..IT3C_AdjustmentFile
(
    Client_Code     Varchar(50)     NULL,
    Deal_Xref       INT             NULL,
    Owner           INT             NULL,
    Contract_Number INT             NULL, 
    Source_Code     INT             NULL,
    Product         VARCHAR(6)      NULL,
    Product_Desc    VARCHAR(50)     NULL,
    Units_sold      NUMERIC(22,10)   NULL,
    Base_Cost       NUMERIC(22,10)   NULL,
    Proceeds        NUMERIC(22,10)   NULL,
    Gain_Loss       NUMERIC(22,10)   NULL,
    Balance_Unit    NUMERIC(22,10)   NULL,
    Balance_Value   NUMERIC(22,10)   NULL
)
GO
