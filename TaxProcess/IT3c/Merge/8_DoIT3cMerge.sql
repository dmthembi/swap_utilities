USE IMS_MAIN
GO



SELECT	'Start:', convert(varchar,getdate(),109)
go
------***************Start Of IT3c Generation*********************



DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates



select count(*), ' Before' from CGTIT3CCertificate where Tax_Year = @Tax_Year

EXEC CGTIT3CCertificate_Merge @Start_Date,@End_Date, @Tax_Year

select count(*) , ' After' from CGTIT3CCertificate where Tax_Year = @Tax_Year

------***************End Of IT3c Generation*********************
go
set  arithabort arith_overflow on

SELECT	'End:', convert(varchar,getdate(),109)
go