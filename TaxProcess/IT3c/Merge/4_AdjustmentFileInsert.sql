

insert into WORKSPACE..IT3C_AdjustmentFile(Client_Code,Source_Code,Product_Desc,Units_sold,Base_Cost,Proceeds,Gain_Loss,Balance_Unit,Balance_Value)
select ClientCode,convert(int,IncomeSource),AssetDescription,UnitsSold,BaseCost,Proceeds,GainLoss,BalanceOfUnits,BalanceOfUnitsValue
from WORKSPACE..IT3cStatingFile
select @@rowcount, 'Records inserted in WORKSPACE..IT3C_AdjustmentFile'

GO