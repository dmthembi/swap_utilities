USE IMS_MAIN
GO

UPDATE      WORKSPACE..IT3C_AdjustmentFile
SET         Deal_Xref       =   T2.Deal_Xref,
            Owner           =   T3.Owner,
            Contract_Number =   T3.Contract_Number,
            Product         =   T3.Product
FROM        WORKSPACE..IT3C_AdjustmentFile T1
INNER JOIN  DealNote T2
ON          RTRIM(LTRIM(T1.Client_Code)) = RTRIM(LTRIM(T2.External_Logical_Note_Id))
INNER JOIN  ContractDeal T3
ON          T2.Deal_Xref = T3.Deal_Xref

select Deal_Xref, Owner, Contract_Number, Product
from WORKSPACE..IT3C_AdjustmentFile
GO
