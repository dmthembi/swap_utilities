/**************************************************************************
!  Procedure    : CGTIT3CCertificate_Merge
!  Description  : Merges Existing CGTIT3CCertificate tables with that of
!                 data (XL file) received from Ashburton
! ------------------------------------------------------------------------
! Modifications :
! Programmer    Date        Changes
! ________________________________________________________________________
! Kesavan       10 Mar 2014 Creation
!*************************************************************************/

USE IMS_MAIN
GO

IF EXISTS(SELECT 1 FROM dbo.sysobjects WHERE name='CGTIT3CCertificate_Merge' AND type='P')
BEGIN
    DROP PROC dbo.CGTIT3CCertificate_Merge
    PRINT '<<<<<<<<<<<<<<<<<<<CGTIT3CCertificate_Merge DROPPED>>>>>>>>>>>>>>>>>>>>>>>'
END
GO

IF OBJECT_ID('WORKSPACE..IT3C_AdjustmentFile_Process') IS NOT NULL DROP TABLE WORKSPACE..IT3C_AdjustmentFile_Process
IF OBJECT_ID('#InstrumentCheck') IS NOT NULL DROP TABLE #InstrumentCheck
IF OBJECT_ID('#CertNum') IS NOT NULL DROP TABLE #CertNum
IF OBJECT_ID('#ProdNum') IS NOT NULL DROP TABLE #ProdNum
GO

CREATE PROCEDURE CGTIT3CCertificate_Merge
(	
    @StartDate 	DATETIME, 
	@EndDate 	DATETIME, 
	@TaxYear 	INT,
    @Product    Varchar(5) = 'RMBSP'
)
AS
BEGIN
    DECLARE @LastCert INT
    DECLARE @StartCert INT
    DECLARE @Client_Code Varchar(50)

    SELECT @LastCert = ISNULL(MAX(Certificate_Number),0) + 1 FROM CGTIT3CCertificate WHERE Tax_Year = @TaxYear AND Product_Code = 'RMBSP'
    SELECT @StartCert = @LastCert

    IF OBJECT_ID('WORKSPACE..IT3C_AdjustmentFile_Process') IS NOT NULL DROP TABLE WORKSPACE..IT3C_AdjustmentFile_Process

    SELECT *, Flag = 'N' INTO WORKSPACE..IT3C_AdjustmentFile_Process FROM WORKSPACE..IT3C_AdjustmentFile

    WHILE EXISTS(SELECT 1 FROM WORKSPACE..IT3C_AdjustmentFile_Process WHERE Flag = 'N')
    BEGIN
        SET ROWCOUNT 1
        SELECT @Client_Code = Client_Code FROM WORKSPACE..IT3C_AdjustmentFile_Process WHERE Flag = 'N'
        SET ROWCOUNT 0

        INSERT CGTIT3CCertificate  
        (       
                People_Number,
                Contract_Number,
                Product_Code,
                Tax_Year
        )
        SELECT	Owner,
                Contract_Number,
                Product,
                @TaxYear
        FROM	WORKSPACE..IT3C_AdjustmentFile
        WHERE   Client_Code = @Client_Code

        IF OBJECT_ID('#InstrumentCheck') IS NOT NULL DROP TABLE #InstrumentCheck

        SELECT      Certificate_Key,
                    RM.Instrument_Number
        INTO        #InstrumentCheck
        FROM        WORKSPACE..IT3C_AdjustmentFile T1
        INNER JOIN  CGTIT3CCertificate T2
        ON          T1.Owner    = T2.People_Number
        AND         T1.Contract_Number = T2.Contract_Number
        AND         T1.Product = T2.Product_Code
        AND         T2.Certificate_Number IS NULL
        INNER JOIN  DealNote DN
        ON          T1.Client_Code = DN.External_Logical_Note_Id
        INNER JOIN  RSPIdMap RM
        ON          DN.Note_Id = RM.Note_Id
        INNER JOIN  Instruments I
        ON          RM.Instrument_Number = I.Instrument_Number
        INNER JOIN  People P
        ON          I.People_Number = P.People_Number
        AND         P.Language = 'E'
        WHERE       T2.Tax_Year         = @TaxYear
        AND         T2.Product_Code     = @Product
        AND         T1.Client_Code      = @Client_Code
        GROUP BY    Certificate_Key,
                    RM.Instrument_Number

        IF (SELECT Count(*) FROM #InstrumentCheck) > 1
        BEGIN

            INSERT INTO CGTIT3CInstrumentCertificate
            (
                Certificate_Key,
                Instrument_Number,
                Instrument_Description,
                Manco_Description,
                Units_Disposed,
                Gross_Proceeds,
                Net_Gain,
                Units_Held,
                WAC
            )
            SELECT      T2.Certificate_Key,
                        1005,               --  Instrument Number
                        'RMB Growth PLIA',  --  Instrument Description
                        'MANCO',
                        CONVERT(Numeric(18,4),T1.Units_sold),
                        CONVERT(Numeric(18,4),T1.Proceeds),
                        CONVERT(Numeric(18,4),T1.Gain_Loss),
                        CONVERT(Numeric(18,4),T1.Balance_Unit),
                        CONVERT(Numeric(18,4),T1.Base_Cost)
            FROM        WORKSPACE..IT3C_AdjustmentFile T1
            INNER JOIN  CGTIT3CCertificate T2
            ON          T1.Owner    = T2.People_Number
            AND         T1.Contract_Number = T2.Contract_Number
            AND         T1.Product = T2.Product_Code
            AND         T2.Certificate_Number IS NULL
            AND         T1.Client_Code = @Client_Code
            INNER JOIN  People P
            ON          T1.Owner = P.People_Number
            AND         P.Language = 'E'
            WHERE       T2.Tax_Year         = @TaxYear
            AND         T2.Product_Code     = @Product
        END
        ELSE
        BEGIN
            INSERT	CGTIT3CInstrumentCertificate
            (       Certificate_Key,
                    Instrument_Number,
                    Instrument_Description,
                    Manco_Description,
                    Units_Disposed,
                    Gross_Proceeds,
                    Net_Gain,
                    Units_Held,
                    WAC)
            SELECT      DISTINCT
                        Certificate_Key,
                        RM.Instrument_Number, -- Instrument_Number,
                        I.Description, -- Instrument_Description,
                        P.Name, --Manco_Description,
                        Convert(Numeric(18,4),T1.Units_sold), --Units_Disposed,
                        Convert(Numeric(18,4),T1.Proceeds), -- Gross_Proceeds,
                        Convert(Numeric(18,4),T1.Gain_Loss), --Net_Gain,
                        Convert(Numeric(18,4),T1.Balance_Unit), --Units_Held,
                        Convert(Numeric(18,4),T1.Base_Cost) --WAC
            FROM        WORKSPACE..IT3C_AdjustmentFile T1
            INNER JOIN  CGTIT3CCertificate T2
            ON          T1.Owner    = T2.People_Number
            AND         T1.Contract_Number = T2.Contract_Number
            AND         T1.Product = T2.Product_Code
            AND         T2.Certificate_Number IS NULL
            INNER JOIN  DealNote DN
            ON          T1.Client_Code = DN.External_Logical_Note_Id
            INNER JOIN  RSPIdMap RM
            ON          DN.Note_Id = RM.Note_Id
            INNER JOIN  Instruments I
            ON          RM.Instrument_Number = I.Instrument_Number
            INNER JOIN  People P
            ON          I.People_Number = P.People_Number
            AND         P.Language = 'E'
            WHERE       T2.Tax_Year         = @TaxYear
            AND         T2.Product_Code     = @Product
            AND         T1.Client_Code      = @Client_Code
            
        END

        UPDATE      CGTIT3CCertificate
        SET         Certificate_Number = @LastCert
        WHERE       Tax_Year = @TaxYear
        AND         Product_Code     = @Product
        AND         Certificate_Number IS NULL

        SELECT @LastCert = @LastCert + 1

        UPDATE      WORKSPACE..IT3C_AdjustmentFile_Process 
        SET         Flag = 'Y'
        WHERE       Client_Code = @Client_Code

        DELETE  WORKSPACE..IT3C_AdjustmentFile_Process WHERE Client_Code = @Client_Code
    END

    -- Update Manco Description
    UPDATE      CGTIT3CInstrumentCertificate
    SET         Manco_Description = P.Name
    FROM        CGTIT3CInstrumentCertificate C
    INNER JOIN  Instrument I
    ON          C.Instrument_Number = I.Instrument_Number
    INNER JOIN  People P
    ON          I.People_Number = P.People_Number
    INNER JOIN  CGTIT3CCertificate CH
    ON          C.Certificate_Key = CH.Certificate_Key
    WHERE       CH.Tax_Year              = @TaxYear
    AND         CH.Product_Code          = @Product
    AND         CH.Certificate_Number    >= @StartCert

    /*****************************************************************************/
    -- Update static data for certificate
    /*****************************************************************************/

    UPDATE      CGTIT3CCertificate
    SET         c.Surname = p.Name,
                c.FirstTwoNames = p.First_Names,
                c.Initials = p.Initials
    FROM        CGTIT3CCertificate c
    INNER JOIN  People p
    ON          p.People_Number = c.People_Number
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON Surname'
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : Surname DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.Product_Name = p.Product_Name
    FROM        CGTIT3CCertificate c
    INNER JOIN  ProductDescription p
    ON          p.Product_Code = c.Product_Code
    AND         p.Language = 'E'
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON Product_Name'
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : Product_Name DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.IdentityNumber = pr.Reg_Value
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleRegistration pr
    ON          c.People_Number = pr.People_Number
    AND         pr.Reg_Type = 1 --SA Id
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON IdentityNumber'
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : IdentityNumber DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.PassportNumber = pr.Reg_Value
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleRegistration pr
    ON          c.People_Number = pr.People_Number
    AND         pr.Reg_Type = 2 --Passport
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON PassportNumber'
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : PassportNumber DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.CompCCTrustNumber = pr.Reg_Value
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleRegistration pr
    ON          c.People_Number = pr.People_Number
    AND         pr.Reg_Type = 3 --Company Registration
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON CompCCTrustNumber3 '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : CompCCTrustNumber3  DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.CompCCTrustNumber = pr.Reg_Value
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleRegistration pr
    ON          c.People_Number = pr.People_Number
    AND         pr.Reg_Type = 12 --Trust Registration
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON CompCCTrustNumber12 '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : CompCCTrustNumber12 DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.DateOfBirth = pd.Date
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleDates pd
    ON          c.People_Number = pd.People_Number
    AND         pd.Date_Type = 'B' --BirthDate
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert
 
    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON DateOfBirth '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : DateOfBirth DONE'

    UPDATE      CGTIT3CCertificate
    SET         StatementDate = GETDATE(),
                PeriodFrom = @StartDate,
                PeriodTo = @EndDate
    WHERE       Tax_Year = @TaxYear
    AND         Product_Code    = @Product
    AND         Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON StatementDate '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : StatementDate DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.Line_1 = pa.Line_1,
                c.Line_2 = pa.Line_2,
                c.Line_3 = pa.Line_3,
                c.Line_4 = pa.Line_4,
                c.Line_5 = pa.Line_5,
                c.Postal_Code = pa.Postal_Code
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleAddress pa
    ON          c.People_Number = pa.People_Number
    AND         pa.Address_Type = 'POST'
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON POST Address '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : POST Address DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.Line_1 = pa.Line_1,
                c.Line_2 = pa.Line_2,
                c.Line_3 = pa.Line_3,
                c.Line_4 = pa.Line_4,
                c.Line_5 = pa.Line_5,
                c.Postal_Code = pa.Postal_Code
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleAddress pa
    ON          c.People_Number = pa.People_Number
    AND         pa.Address_Type = 'PHYS'
    WHERE       c.Line_1 IS NULL
    AND         c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON PHYS Address '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : PHYS Address DONE'

    UPDATE      CGTIT3CCertificate
    SET         SAResident = 'Y'
    FROM        CGTIT3CCertificate c
    WHERE       c.IdentityNumber IS NOT NULL
    AND         c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON SAResident1 '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : SAResident1 DONE'

    UPDATE  CGTIT3CCertificate
    SET     SAResident = 'Y'
    FROM    CGTIT3CCertificate c
    WHERE   c.IdentityNumber IS NULL
    AND     c.PassportNumber IS NOT NULL
    AND     c.Tax_Year = @TaxYear
    AND     c.Product_Code    = @Product
    AND     c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON SAResident2 '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : SAResident2 DONE'

    UPDATE      CGTIT3CCertificate
    SET         c.IncomeTaxNumber = p.Tax_Number
    FROM        CGTIT3CCertificate c
    INNER JOIN  PeopleTax p
    ON          c.People_Number = p.People_Number
    WHERE       c.Tax_Year = @TaxYear
    AND         c.Product_Code    = @Product
    AND         c.Certificate_Number >= @StartCert

    IF @@ERROR <> 0
    BEGIN
      SELECT 'ERROR OCCURED WHILE UPDATING CGTIT3CCertificate ON IncomeTaxNumber '
      GOTO END_RUN
    END
    ELSE
    SELECT 'UPDATE ON CGTIT3CCertificate : IncomeTaxNumber DONE'

    END_RUN:
END
GO

IF OBJECT_ID('CGTIT3CCertificate_Merge') <> NULL
BEGIN
    PRINT '<<<<<<<<<<<<<<<<<<<CGTIT3CCertificate_Merge CREATED>>>>>>>>>>>>>>>>>>>>>>>'
END

