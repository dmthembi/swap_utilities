
if object_id('WORKSPACE..IT3cStatingFile') is not null
drop table WORKSPACE..IT3cStatingFile
GO





create table WORKSPACE..IT3cStatingFile(ClientCode varchar(50),
                             IncomeSource varchar(4),
                             AssetDescription varchar(70),
                             UnitsSold float,
                             BaseCost float,
                             Proceeds float,
                             GainLoss float,
                             BalanceOfUnits float,
                             BalanceOfUnitsValue float)




GO