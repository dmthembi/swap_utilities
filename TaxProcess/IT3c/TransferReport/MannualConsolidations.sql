set nocount on


if object_id('MannualConsReport') is not null 
drop table MannualConsReport
if object_id('#XferLog') is not null 
drop table #XferLog
if object_id('#SXferLog') is not null
drop table #SXferLog
if object_id('#DXferLog') is not null
drop table #DXferLog
if object_id('#Transactions') is not null
drop table #Transactions
if object_id('#TransData') is not null
drop table #TransData
if object_id('#TransData1') is not null
drop table #TransData1
if object_id('#TransData2') is not null
drop table #TransData2
if object_id('#TransData3') is not null
drop table #TransData3
if object_id('#SourceDeals') is not null
drop table #SourceDeals
if object_id('#DestDeals') is not null
drop table #DestDeals 
if object_id('#Report') is not null
drop table #Report
if object_id('#cnt') is not null
drop table #cnt
if object_id('#SourceDealsTMP') is not null
drop table #SourceDealsTMP
if object_id('#DestDealsTMP') is not null
drop table #DestDealsTMP


go


CREATE TABLE #SourceDealsTMP ( 
    Date_Action    	datetime NULL,
    Instrument_No  	int NOT NULL,
    Description    	varchar(50) NULL,
    Amount         	float NULL,
    Units          	float NULL,
    Rollover       	money NULL,
    Deal_Xref      	int NOT NULL,
    Owner          	int NULL,
    Product        	varchar(5) NULL,
    Name           	varchar(101) NULL,
    Age            	int NULL,
    Flow_No        	int NULL,
    RealInstrument 	int NULL,
    RealDescription	varchar(50) NULL,
    row            	int NOT NULL 
    )

CREATE TABLE #DestDealsTMP ( 
    Date_Action    	datetime NULL,
    Instrument_No  	int NOT NULL,
    Description    	varchar(50) NULL,
    Amount         	float NULL,
    Product        	varchar(5) NULL,
    Units          	float NULL,
    Rollover       	money NULL,
    Name           	varchar(101) NULL,
    Age            	int NULL,
    Deal_Xref      	int NOT NULL,
    Owner          	int NULL,
    Flow_No        	int NULL,
    RealInstrument 	int NULL,
    RealDescription	varchar(50) NULL,
    row            	int NOT NULL 
    )

go

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates

if(@Bi_Annual = 1)
begin
 select 'BI_Annual Consolidations Transferes Report generation for'
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 else
 begin
 select 'Annual Consolidations Transferes Report generation for '
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 

select distinct l.*
into #XferLog
from TransferLog l
where Xfer_Date >= @Start_Date
and Xfer_Date <= @End_Date
select @@rowcount,' Records transfers found in TransferLog'


update #XferLog
set  Xfer_Date = convert(varchar(11), Xfer_Date)

select distinct Source_Deal, Xfer_Date
into #SXferLog
from #XferLog

select distinct Destination_Deal, Xfer_Date
into #DXferLog
from #XferLog



select *
into #Transactions
from Transactions
where Date_Action between @Start_Date and @End_Date
and Flow_Type = 'X'
select @@rowcount,' Records Transactions found'



delete #Transactions
from #Transactions t, #XferLog x
where t.Deal_Xref = x.Source_Deal
and t.Date_Action = x.Xfer_Date
select @@rowcount,' Source Records from #Transactions'


delete #Transactions
from #Transactions t, #XferLog x
where t.Deal_Xref = x.Destination_Deal
and t.Date_Action = x.Xfer_Date
select @@rowcount,' Destination Records from #Transactions'




select T.*
into #TransData
from
        #Transactions T,
		ContractDeal	CD, 
		ProductGroup 	PG
	WHERE 	CD.Deal_Xref 	= T.Deal_Xref
	AND	CD.Product 	= PG.Product_Internal
	AND	PG.Process 	in ('EM', 'EP')




select Rollover = convert(money,null), *
into #TransData1
from #TransData

update #TransData1
set Rollover = c.NWAC
from #TransData1 t, CGTRollover c
where t.Flow_No = c.Flow_Number




select Instrument_Type, i.Description, t.*
into #TransData2
from #TransData1 t, Instruments i
where t.Instrument_No = i.Instrument_Number


select cd.Owner, cd.Contract_Number, cd.Deal_Number, cd.Product, t.*
into #TransData3
from #TransData2 t, Instruments i, ContractDeal cd
where Rollover is null
and t.Instrument_Type not in ('CALL','FEED','XFER','CASH')
and Transaction_Type = 'S'
and t.Instrument_No = i.Instrument_Number
and t.Deal_Xref  = cd.Deal_Xref
order by Run_Number, Flow_No




select  Date_Action, 
        Instrument_No, 
        i.Description,
        Amount, 
        Units, 
        Rollover = convert(money,null),
        t.Deal_Xref, 
        cd.Owner, 
        cd.Product, 
        Name = p.Name + ' ' + p.First_Names,
        Age = convert(int,null),
        Flow_No, 
        RealInstrument = convert(int,null),
        RealDescription = convert(varchar(50),null),
        row = identity(10)
into #SourceDeals
from #TransData2 t, Instruments i, ContractDeal cd, ProductGroup pg, People p
where Date_Action between @Start_Date and @End_Date
and Flow_Type = 'X'
and Transaction_Type = 'D'
and t.Instrument_No = i.Instrument_Number
and i.Instrument_Type = 'XFER'
and t.Deal_Xref = cd.Deal_Xref
and cd.Product = pg.Product_Internal
and pg.Process 	in ('EM', 'EP')
and cd.Owner = p.People_Number
select @@rowcount,' source data found'


update #SourceDeals
set RealInstrument = t.Instrument_No
from #SourceDeals sd, Transactions t
where sd.Flow_No = t.Flow_No
and t.Transaction_Type = 'S'

update #SourceDeals
set RealDescription = i.Description
from #SourceDeals dd, Instruments i
where dd.RealInstrument = i.Instrument_Number

update #SourceDeals
set Age = datediff(yy,pd.Date, getdate())
from #SourceDeals sd, PeopleDates pd
where sd.Owner = pd.People_Number
and pd.Date_Type = 'B'

update #SourceDeals
set Rollover = c.NWAC
from #SourceDeals sd, CGTRollover c
where sd.Flow_No = c.Flow_Number



select Date_Action, 
        Instrument_No, 
        Description,
        Amount, 
        cd.Product,  
        Units,         
         Rollover = convert(money,null),
        Name = p.Name + ' ' + p.First_Names,
        Age = convert(int,null),
        td.Deal_Xref, 
        cd.Owner,
        Flow_No, 
        RealInstrument = convert(int,null),
        RealDescription = convert(varchar(50),null),
        row = identity(10)
into #DestDeals 
from #TransData2 td, ContractDeal cd, People p
where 1=1
and Transaction_Type = 'S'
and td.Instrument_Type = 'XFER'
and td.Deal_Xref = cd.Deal_Xref
and cd.Owner = p.People_Number
order by Flow_No
select @@rowcount,' source data found'


update #DestDeals
set RealInstrument = t.Instrument_No
from #DestDeals sd, Transactions t
where sd.Flow_No = t.Flow_No
and t.Transaction_Type = 'D'


update #DestDeals
set RealDescription = i.Description
from #DestDeals dd, Instruments i
where dd.RealInstrument = i.Instrument_Number

update #DestDeals
set Age = datediff(yy,pd.Date, getdate())
from #DestDeals sd, PeopleDates pd
where sd.Owner = pd.People_Number
and pd.Date_Type = 'B'

update #DestDeals
set Rollover = c.NWAC
from #DestDeals sd, CGTRollover c
where sd.Flow_No = c.Flow_Number

select SourceDeal = sd.Deal_Xref,
       DestinationDeal = dd.Deal_Xref,
       SourceProduct = sd.Product,
       DestinationProduct = dd.Product,
       SourceOwner = sd.Owner,
       DestOwner = dd.Owner,
       SourceName = sd.Name,
       DestName = dd.Name,
       SourceAge = sd.Age,
       DestAge = dd.Age,
       SourceDateAction = sd.Date_Action,   
       DestDateAction = dd.Date_Action,
       SourceInst = sd.RealInstrument,
       DestInstInst = dd.RealInstrument, 
       SourceInstDesc = sd.RealDescription,
       DestInstDesc = dd.RealDescription,
       SourceUnits = sd.Units,
       DestUnits = dd.Units,
       SourceRollover = sd.Rollover,
       DestRollover = dd.Rollover,
       SourceFlow = sd.Flow_No,
       DestinationFlow = dd.Flow_No
into #Report
from #SourceDeals sd, #DestDeals dd
where abs(sd.Amount - dd.Amount) < 1
and sd.RealInstrument = dd.RealInstrument
and sd.Date_Action = dd.Date_Action




truncate table #Report



select row = 1 into #cnt

while exists (select 1 from #SourceDeals)
begin


 insert into #SourceDealsTMP
 select s.* from #SourceDeals s, #cnt c
 where s.row = c.row

 set rowcount 1
 insert into #DestDealsTMP
 select dd.*
 from #SourceDealsTMP sd, #DestDeals dd
 where abs(sd.Amount - dd.Amount) < 1
 and sd.RealInstrument = dd.RealInstrument
 and sd.Date_Action = dd.Date_Action
 set rowcount 0



 insert into #Report
 select 
       SourceDeal = sd.Deal_Xref,
       DestinationDeal = dd.Deal_Xref,
       SourceProduct = sd.Product,
       DestinationProduct = dd.Product,
       SourceOwner = sd.Owner,
       DestOwner = dd.Owner,
       SourceName = sd.Name,
       DestName = dd.Name,
       SourceAge = sd.Age,
       DestAge = dd.Age,
       SourceDateAction = sd.Date_Action,   
       DestDateAction = dd.Date_Action,
       SourceInst = sd.RealInstrument,
       DestInstInst = dd.RealInstrument, 
       SourceInstDesc = sd.RealDescription,
       DestInstDesc = dd.RealDescription,
       SourceUnits = sd.Units,
       DestUnits = dd.Units,
       SourceRollover = sd.Rollover,
       DestRollover = dd.Rollover,
       SourceFlow = sd.Flow_No,
       DestinationFlow = dd.Flow_No

from #SourceDealsTMP sd, #DestDealsTMP dd
where abs(sd.Amount - dd.Amount) < 1
and sd.RealInstrument = dd.RealInstrument
and sd.Date_Action = dd.Date_Action


 update #cnt set row = row + 1

delete #SourceDeals
from #SourceDeals s, #SourceDealsTMP t
where s.row = t.row

delete #DestDeals
from #DestDeals s, #DestDealsTMP t
where s.row = t.row

    truncate table #SourceDealsTMP
    truncate table #DestDealsTMP


end

select * 
into MannualConsReport
from #Report order by SourceOwner
select @@rowcount,' records inserted into MannualConsReport'

--bcp IMS_MAIN..MannualConsReport out MannualConsReport.csv -SIMSQA2 -Usystem_application -Psystem_application -c -t,

go

