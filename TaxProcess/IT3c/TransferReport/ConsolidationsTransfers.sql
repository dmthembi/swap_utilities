
---*** ///
set nocount on


if object_id('ConsolTransReport') is not null drop table ConsolTransReport

if object_id('#Trans') is not null drop table #Trans
if object_id('#XferLog') is not null drop table #XferLog
if object_id('#SwitchTransfers') is not null drop table #SwitchTransfers
if object_id('#RepeatingSourcedeals') is not null drop table #RepeatingSourcedeals
if object_id('#backup') is not null drop table #backup
if object_id('#SourceTrans') is not null drop table #SourceTrans
if object_id('#CompleteSwitchXFer') is not null drop table #CompleteSwitchXFer
if object_id('#DestTrans') is not null drop table #DestTrans
if object_id('#RepeatingSFlow') is not null drop table #RepeatingSFlow
if object_id('#SXferLog') is not null drop table #SXferLog
if object_id('#DXferLog') is not null drop table #DXferLog
if object_id('#RelTypes') is not null drop table #RelTypes
if object_id('#Report') is not null drop table #Report
if object_id('#totalSource') is not null drop table #totalSource
if object_id('#totalDest') is not null drop table #totalDest

go



DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates

if(@Bi_Annual = 1)
begin
 select 'BI_Annual Consolidations Transferes Report generation for'
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 else
 begin
 select 'Annual Consolidations Transferes Report generation for '
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 
 

-- // All Transfers where 

select distinct l.*
into #XferLog
from TransferLog l
where Xfer_Date >= @Start_Date
and Xfer_Date <= @End_Date
and Xfer_Type  = 'C'
select @@rowcount, ' records found in #XferLog'

update #XferLog
set  Xfer_Date = convert(varchar(11), Xfer_Date)

select distinct Source_Deal, Xfer_Date
into #SXferLog
from #XferLog


select distinct Destination_Deal, Xfer_Date
into #DXferLog
from #XferLog


select  distinct
    Deal='SourceDeal',
    t.Deal_Xref,
	cd.Owner,
	cd.Product,
	t.Date_Action,
    totalAmount = convert(money,null),
	t.Amount,
	t.Units,
	t.Rate,
	t.Instrument_No,
	WAC = convert(money,null),
    Rollover = convert(money,null),
	Instrument_Type = i.Instrument_Type ,
    i.Description,
	Opposite_Instrument_Type = convert(varchar(5),null),
	Flow_No
	
into #SourceTrans
from Transactions t, #SXferLog c, Instruments i, ContractDeal cd
where t.Deal_Xref = c.Source_Deal
and t.Date_Action = c.Xfer_Date
and t.Flow_Type     = 'X'
and t.Instrument_No = i.Instrument_Number
and t.Deal_Xref = cd.Deal_Xref
and t.Transaction_Type = 'S'
and i.Instrument_Type <> 'XFER'
select @@rowcount, ' records inserted into #SourceTrans'




select Owner, Date_Action , totalAmount = sum(Amount)
into #totalSource
from #SourceTrans
group by Owner, Date_Action



update #SourceTrans
set totalAmount = t.totalAmount
from #SourceTrans s, #totalSource t
where s.Owner = t.Owner
and s.Date_Action = t.Date_Action 


update #SourceTrans
set Rollover = NWAC
from #SourceTrans s, CGTRollover c
where s.Flow_No = c.Flow_Number




update #SourceTrans
set Opposite_Instrument_Type  = i.Instrument_Type
FROM #SourceTrans s, Transactions t, Instruments i
where s.Flow_No = t.Flow_No
and t.Transaction_Type = 'D'
and t.Instrument_No = i.Instrument_Number  



delete #SourceTrans
where Opposite_Instrument_Type  <> 'XFER'              


select count(*) from #SourceTrans

-- ////****** NOW GET THE DESTINATIONS ******\\\\ --



select distinct
     Deal='DestinationDeal',
    t.Deal_Xref,
	cd.Owner,
	cd.Product,
	t.Date_Action,
    totalAmount = convert(money,null),
	t.Amount,
	t.Units,
	t.Rate,
	t.Instrument_No,
	WAC = convert(money,null),
    Rollover = convert(money,null),
	Instrument_Type = i.Instrument_Type ,
    i.Description,
	Opposite_Instrument_Type = convert(varchar(5),null),
	t.Flow_No
	
into #DestTrans
from #DXferLog s, Transactions t, Instruments i, ContractDeal cd
where s.Destination_Deal	=	t.Deal_Xref
and t.Transaction_Type		=	'D'
and t.Date_Action		=	s.Xfer_Date
and t.Flow_Type			=	'X'
and t.Instrument_No		=	i.Instrument_Number
and t.Deal_Xref			=	cd.Deal_Xref
select @@rowcount, ' records inserted into #DestTrans'



select Owner, Date_Action , totalAmount = sum(Amount)
into #totalDest
from #DestTrans
group by Owner, Date_Action




update #DestTrans
set totalAmount = t.totalAmount
from #DestTrans s, #totalDest t
where s.Owner = t.Owner
and s.Date_Action = t.Date_Action 


update #DestTrans
set Opposite_Instrument_Type = i.Instrument_Type
from #DestTrans d, Transactions t, Instruments i
where d.Flow_No = t.Flow_No
and t.Transaction_Type = 'S'
and t.Instrument_No = i.Instrument_Number

update #DestTrans
set Rollover = NWAC
from #DestTrans s, CGTRollover c
where s.Flow_No = c.Flow_Number



delete #DestTrans
where Opposite_Instrument_Type  <> 'XFER'              

select count(*) from #DestTrans

-- construct comprihensive report.....




select *
into #Report
from #SourceTrans
UNION
SELECT *
FROM #DestTrans


select *
into ConsolTransReport
from #Report
order by Owner
select @@rowcount, 'Records inserted into ConsolTransReport'

GO

/*****


CREATE TABLE #CGTNewTransactions
(	Deal_Xref		int		null,
	Start_Date		datetime	null,
	End_Date		datetime	null,
	Flow_No			int		null,
	Transaction_No		numeric(18,0)		null,
	Transaction_Type	char(1)		null,
	Instrument_No		int		null,
	Date_Action		datetime	null,
	Flow_Type		varchar(1)	null,
	Amount			money		null,
	Rate			numeric(14,6)	null,
	Units			numeric(18,4)	null)

CREATE TABLE #CGTExtractDetail
(	Deal_Xref		int		null,
	Start_Date		datetime	null,
	End_Date		datetime	null,
	Transaction_No		numeric(18,0)		null,
	Instrument_No		int		null,
	Date			datetime	null,
	Rate			numeric(14,6)	null,
	WAC			numeric(14,6)	null,
	Cumulative_Gain		money		null,
	Gain			money		null,
	Cumulative_Units	numeric(18,4)	null,
	Units			numeric(18,4)	null,
	Amount			money		null,
	WAC_Rollover		smallint	null
)


CREATE TABLE #CGTExtractSummary
(	Deal_Xref		int		null,
	Start_Date		datetime	null,
	End_Date		datetime	null,
	Instrument_No		int		null,
	Units_Disposed		numeric(18,4)	null,
	Aggregate_Unit_Cost	numeric(14,6)	null,
	Gross_Proceeds		money		null,
	Net_Gain		money		null,
	Units_Held		numeric(18,4)	null,
	WAC			numeric(14,6)	null
)


CREATE TABLE #CGTLossCarryOver
(	Deal_Xref		int		null,
	Start_Date		datetime	null,
	End_Date		datetime	null,
	Tax_Year		int		null,
	Gain			money		null)

CREATE TABLE #RE_Sell
(Instrument_Number   int                null,
 Rate_Type           char(1)            null,
 Date                datetime           null,
 Rate_Date           datetime           null,
 Buy                 numeric(14,6)      null,
 Sell                numeric(14,6)      null,
 Basic               numeric(14,6)      null,
 Adjusted            numeric(14,6)      null,
 TimeStamp           varbinary(8)       null)




CREATE TABLE #CGTExtractIn
(	Deal_Xref		int		null,
	Start_Date		datetime	null,
	End_Date		datetime	null)

select *
from #SourceTrans
where Product = 'GAPU'


INSERT #CGTExtractIn
SELECT DISTINCT Deal_Xref, '01 march 2015', Date_Action
from #SourceTrans s, ProductGroup pg
where Rollover is null
and Instrument_Type not in ('CALL','FEED','FSCAL','PSP')
and s.Product = pg.Product_Internal
and pg.Process = 'EP'


select *
from #CGTExtractIn

exec CGTExtract



update #SourceTrans
set WAC = c.WAC
from #CGTExtractDetail c, #SourceTrans s, Transactions t
where c.Transaction_No = t.Transaction_No
and t.Flow_No = s.Flow_No
and t.Transaction_Type = 'S'


select s.*
from #SourceTrans s, ProductGroup pg
where 1=1 --Rollover is null
and Instrument_Type not in ('CALL','FEED','FSCAL','PSP')
and s.Product = pg.Product_Internal
and pg.Process = 'EP'





select 'insert into CGTRollover(Flow_Number, NWAC) select ' + convert(varchar(20), Flow_No) + ','+convert(varchar(20), WAC) + char(10) +
       ' where not exists ( select 1 from CGTRollover where Flow_Number = ' + convert(varchar(20), Flow_No)+ char(10) +
       ' GO '
from #SourceTrans s, #XferLog x
where WAC is not null
and Rollover is null
and s.Deal_Xref = x.Source_Deal
order by Destination_Deal, Description



select  'insert into CGTRollover(Flow_Number, NWAC) select ' + convert(varchar(20), d.Flow_No) + ','+convert(varchar(20), s.WAC) + char(10) +
       ' where not exists ( select 1 from CGTRollover where Flow_Number = ' + convert(varchar(20), d.Flow_No)+ char(10) +
       ' GO '
from #SourceTrans s, #XferLog x, #DestTrans d
where s.WAC is not null
and s.Rollover is null
and s.Deal_Xref = x.Source_Deal
and x.Destination_Deal = d.Deal_Xref
and s.Date_Action = d.Date_Action
and s.Instrument_No = d.Instrument_No
order by Destination_Deal, d.Description


select *
from CGTRollover
where Flow_Number in (164301074,
166525121,
166525122,
166525123,
166525124,
166525125,
170527397)


select *
from  #XferLog 
where Destination_Deal = 2054512777

 Source_Deal     Destination_Deal     Xfer_Type     Xfer_Date             
 --------------  -------------------  ------------  --------------------- 
 2054355430      2054512777           C             5/14/2012 12:00:00 AM 
 2054508849      2054512777           C             5/14/2012 12:00:00 AM 

select *
from #SourceTrans
where Deal_Xref in (2054355430, 2054508849)


update #DestTrans
set WAC = s.WAC
from #DestTrans d, #SourceTrans s
where s.Flow_No = d.Source_Flow_No


--drop table CGTExtractDetail

select *
into CGTExtractDetail
from #CGTExtractDetail




select Flow_No, count(*)
from #SourceTrans
group by Flow_No
having count(*) > 1

select Flow_No, count(*)
from #DestTrans
group by Flow_No
having count(*) > 1





select cd.Contract_Number, s.*, i.Description
from #SourceTrans s, ProductGroup pg, Instruments i, ContractDeal cd
where s.Product = pg.Product_Internal
and Process = "EP"
and s.Instrument_No  = i.Instrument_Number
and s.Deal_Xref = cd.Deal_Xref



select SourcePeopleNo = cd.Owner,
	SourceContractNo = cd.Contract_Number,
	SourceDealNumber = cd.Deal_Number,
	SourceFundDescrition = i.Description,
	SourceAmount = s.Amount,
	SourceUnits= s.Units,
	SourceRate = s.Rate,
	SourceWAC = s.WAC,
	SourceFlowNo = s.Flow_No
	--cd.Contract_Number, s.*, i.Description
into #Report1
from #SourceTrans s, ProductGroup pg, Instruments i, ContractDeal cd
where s.Product = pg.Product_Internal
and Process = "EP"
and s.Instrument_No  = i.Instrument_Number
and s.Deal_Xref = cd.Deal_Xref


select 	DestPeopleNo = cd.Owner,
	DestContractNo = cd.Contract_Number,
	DestDealNumber = cd.Deal_Number,
	DestFundDescrition = i.Description,
	DestAmount = s.Amount,
	DestUnits= s.Units,
	DestRate = s.Rate,
	DestWAC = s.WAC,
	DestFlowNo = s.Flow_No,
	Source_Flow_No
	--cd.Contract_Number,s.*, i.Description
into #Report2
from #DestTrans s, ProductGroup pg, Instruments i, ContractDeal cd
where s.Product = pg.Product_Internal
and Process = "EP"
and s.Instrument_No  = i.Instrument_Number
and s.Deal_Xref = cd.Deal_Xref



**/



