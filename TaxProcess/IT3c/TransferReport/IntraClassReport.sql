


---*** ///
set nocount on


if object_id('IntraClassReport') is not null drop table IntraClassReport

if object_id('#Report') is not null drop table #Report


go
--client name, People number, Contract, Source People Number, Destination People number, Source Fund,  Destination fund, Value Date and Values
DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates

if(@Bi_Annual = 1)
begin
 select 'BI_Annual Consolidations Transferes Report generation for'
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 else
 begin
 select 'Annual Consolidations Transferes Report generation for '
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 

select CGTRollover = convert(float,null),
       t1.Deal_Xref,
       cd.Owner,
       cd.Product,
       cd.Deal_Number,
       cd.Contract_Number,
       t1.Date_Action,
       Flow_Type = ft.Description,
       Source_Instrument_No = i1.Instrument_Number,
       Source_Instrument_Desc = i1.Description,
       Source_Instrument_Type = i1.Instrument_Type,
       Dest_Instrument_No = i2.Instrument_Number,
       Dest_Instrument_Desc = i2.Description,
       t1.Flow_No
into #Report      
from Transactions t1 inner join Transactions t2 on t1.Flow_No = t2.Flow_No
inner join Instruments i1 on i1.Instrument_Number = t1.Instrument_No 
inner join Instruments i2 on i2.Instrument_Number = t2.Instrument_No 
inner join FlowTypes ft on t1.Flow_Type = ft.Flow_Type
inner join ContractDeal cd on t1.Deal_Xref = cd.Deal_Xref
inner join People p on cd.Owner = p.People_Number
inner join ProductGroup pg on cd.Product = pg.Product_Internal
where t1.Date_Action between @Start_Date  and @End_Date
and t1.Flow_Type in ('T','W')
and t1.Transaction_Type = 'S'
and t1.Transaction_Type <> t2.Transaction_Type
and pg.Process in ('AO')--('EM','EO')
select @@rowcount, ' records found from Transactions'


update #Report
set CGTRollover = c.NWAC
from CGTRollover c, #Report r
where c.Flow_Number = r.Flow_No




select r.*
into IntraClassReport
from #Report r
select @@rowcount, ' records inserted into IntraClassReport'

--bcp IMS_MAIN..IntraClassReport out IntraClassReport.csv -SIMSQA2 -Usystem_application -Psystem_application -c -t,


GO