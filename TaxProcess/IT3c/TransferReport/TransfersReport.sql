

set nocount on


if object_id('TransfersReport') is not null drop table TransfersReport
if object_id('#RelTypes') is not null drop table #RelTypes

if object_id('#Trans') is not null drop table #Trans
if object_id('#XferLog') is not null drop table #XferLog
if object_id('#SXferLog') is not null drop table #SXferLog
if object_id('#SwitchTransfers') is not null drop table #SwitchTransfers
if object_id('#RepeatingSourcedeals') is not null drop table #RepeatingSourcedeals
if object_id('#backup') is not null drop table #backup
if object_id('#SourceTrans') is not null drop table #SourceTrans
if object_id('#CompleteSwitchXFer') is not null drop table #CompleteSwitchXFer
if object_id('#DestTrans') is not null drop table #DestTrans
if object_id('#RepeatingSFlow') is not null drop table #RepeatingSFlow
if object_id('#SXferLog') is not null drop table #SXferLog
if object_id('#DXferLog') is not null drop table #DXferLog


if object_id('#InterProdXfer') is not null drop table #InterProdXfer
if object_id('#SameProdXfer1') is not null drop table #SameProdXfer1

if object_id('#Report') is not null drop table #Report
if object_id('#totalSource') is not null drop table #totalSource
if object_id('#totalDest') is not null drop table #totalDest

IF OBJECT_ID('#CGTExtractIn') <> NULL
DROP TABLE #CGTExtractIn
IF OBJECT_ID('#CGTExtractDetail') <> NULL
DROP TABLE #CGTExtractDetail
IF OBJECT_ID('#CGTExtractSummary') <> NULL
DROP TABLE #CGTExtractSummary
IF OBJECT_ID('#CGTLossCarryOver') <> NULL
DROP TABLE #CGTLossCarryOver
IF OBJECT_ID('#CGTNewTransactions') <> NULL
DROP TABLE #CGTNewTransactions
IF OBJECT_ID('#RE_Sell') <> NULL
DROP TABLE #RE_Sell

go





DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates


if(@Bi_Annual = 1)
begin
 select 'BI_Annual Transferes Report generation for'
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 else
 begin
 select 'Annual Transferes Report generation for '
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 
 

 
select distinct l.*
into #XferLog
from TransferLog l
where Xfer_Date >= @Start_Date
and Xfer_Date <= @End_Date
and Xfer_Type  = 'T'
select @@rowcount, ' records found in #XferLog'


update #XferLog
set  Xfer_Date = convert(varchar(11), Xfer_Date)

select distinct Source_Deal, Xfer_Date
into #SXferLog
from #XferLog

select distinct Destination_Deal, Xfer_Date
into #DXferLog
from #XferLog




select distinct 
        rt.Description,
	x.*, 
	SourceInit = p1.Initials,
	SourceName = p1.Name,
	SourcePeopleNumber = cd1.Owner,  
	SourceContractNumber = cd1.Contract_Number,
	SourceProductCode = cd1.Product,
	SourceDealNumber= cd1.Deal_Number,
	DestInit = p2.Initials,
	DestName = p2.Name,
	DestPeopleNumber = cd2.Owner,  
	DestContractNumber = cd2.Contract_Number,
	DestProductCode = cd2.Product,
	DestDealNumber= cd2.Deal_Number
	
	
into #RelTypes
from #XferLog x, ContractDeal cd1, ContractDeal cd2, People p1, People p2, PeopleRelationships pr, RelationshipTypes rt, ProductGroup pg
where x.Source_Deal = cd1.Deal_Xref
and x.Destination_Deal = cd2.Deal_Xref
and Xfer_Date between isnull(pr.StartDate,'01 jan 1900') and isnull(pr.EndDate,'31 dec 9999')
and cd1.Owner = pr.Primary_Person
and cd1.Owner = p1.People_Number
and cd2.Owner = pr.Secondary_Person
and cd2.Owner = p2.People_Number
and pr.Relation_Type = rt.Relation_Type
and cd1.Product = pg.Product_Internal
and pg.Process = 'AO' --'EP'
order by x.Source_Deal
select @@rowcount, ' records inserted into #RelTypes'


select  distinct
    Deal='SourceDeal',
 Relationship_Type = convert(varchar(20),null),
    t.Deal_Xref,
	cd.Owner,
	cd.Product,
	t.Date_Action,
    totalAmount = convert(money,null),
	t.Amount,
	t.Units,
	t.Rate,
	t.Instrument_No,
	WAC = convert(money,null),
    Rollover = convert(money,null),
	Instrument_Type = i.Instrument_Type ,
    i.Description,
	Opposite_Instrument_Type = convert(varchar(5),null),
	Flow_No,
    Opposite_Deal = convert(int,null) --c.Destination_Deal
	
into #SourceTrans
from Transactions t, #SXferLog c, Instruments i, ContractDeal cd, ProductGroup pg
where t.Deal_Xref = c.Source_Deal
and t.Date_Action = c.Xfer_Date
and t.Flow_Type     = 'X'
and t.Instrument_No = i.Instrument_Number
and t.Deal_Xref = cd.Deal_Xref
and t.Transaction_Type = 'S'
and i.Instrument_Type <> 'XFER'
and  cd.Product = pg.Product_Internal
and pg.Process = 'EP'
select @@rowcount, 'Source transaction records found'



update #SourceTrans
set Opposite_Deal = Destination_Deal
from #XferLog x, #SourceTrans s
where x.Source_Deal = s.Deal_Xref




select Owner, Date_Action , totalAmount = sum(Amount)
into #totalSource
from #SourceTrans
group by Owner, Date_Action


update #SourceTrans
set totalAmount = t.totalAmount
from #SourceTrans s, #totalSource t
where s.Owner = t.Owner
and t.Date_Action = t.Date_Action 

update #SourceTrans
set Rollover = NWAC
from #SourceTrans s, CGTRollover c
where s.Flow_No = c.Flow_Number




update #SourceTrans
set Opposite_Instrument_Type  = i.Instrument_Type
FROM #SourceTrans s, Transactions t, Instruments i
where s.Flow_No = t.Flow_No
and t.Transaction_Type = 'D'
and t.Instrument_No = i.Instrument_Number  



delete #SourceTrans
where Opposite_Instrument_Type  <> 'XFER'              




select distinct
     Deal='DestinationDeal',
     Relationship_Type = convert(varchar(20),null),
    t.Deal_Xref,
	cd.Owner,
	cd.Product,
	t.Date_Action,
    totalAmount = convert(money,null),
	t.Amount,
	t.Units,
	t.Rate,
	t.Instrument_No,
	WAC = convert(money,null),
    Rollover = convert(money,null),
	Instrument_Type = i.Instrument_Type ,
    i.Description,
	Opposite_Instrument_Type = convert(varchar(5),null),
	t.Flow_No,
    Opposite_Deal =t.Deal_Xref
	
into #DestTrans
from #DXferLog s, Transactions t, Instruments i, ContractDeal cd, ProductGroup pg
where s.Destination_Deal	=	t.Deal_Xref
and t.Transaction_Type		=	'D'
and t.Date_Action		=	s.Xfer_Date
and t.Flow_Type			=	'X'
and t.Instrument_No		=	i.Instrument_Number
and t.Deal_Xref			=	cd.Deal_Xref
and  cd.Product = pg.Product_Internal
and pg.Process = 'EP'
select @@rowcount, 'Destination transaction records found'



select Owner, Date_Action , totalAmount = sum(Amount)
into #totalDest
from #DestTrans
group by Owner, Date_Action


update #DestTrans
set totalAmount = t.totalAmount
from #DestTrans s, #totalDest t
where s.Owner = t.Owner
and t.Date_Action = t.Date_Action 


update #DestTrans
set Opposite_Instrument_Type = i.Instrument_Type
from #DestTrans d, Transactions t, Instruments i
where d.Flow_No = t.Flow_No
and t.Transaction_Type = 'S'
and t.Instrument_No = i.Instrument_Number

update #DestTrans
set Rollover = NWAC
from #DestTrans s, CGTRollover c
where s.Flow_No = c.Flow_Number



delete #DestTrans
where Opposite_Instrument_Type  <> 'XFER'              


-- construct comprihensive report.....

update #SourceTrans
set Relationship_Type = r.Description
from #SourceTrans s,  #RelTypes r
where s.Deal_Xref = r.Source_Deal


update #DestTrans
set Relationship_Type = r.Description
from #DestTrans d,  #RelTypes r
where d.Deal_Xref = r.Destination_Deal




select p.Name, p.First_Names, DOB = convert(datetime,null), age = convert(smallint,null), ageGap =  convert(smallint,null), OppRecCount = convert(int,null),s.*
into #Report
from #SourceTrans s, People p
where s.Owner = p.People_Number
UNION
SELECT  p.Name, p.First_Names, DOB = convert(datetime,null), age = convert(smallint,null), ageGap =  convert(smallint,null), OppRecCount = convert(int,null),d.*
FROM #DestTrans d, People p
where d.Owner = p.People_Number

select * 
into TransfersReport
from #Report 
order by Opposite_Deal
select @@rowcount, 'Records inserted into TransfersReport'


update TransfersReport
set DOB = pd. Date
from  TransfersReport r, PeopleDates pd
where r.Owner = pd.People_Number


update TransfersReport
set age = datediff(yy,DOB,getdate())

GO

--bcp IMS_MAIN..TransfersReport out TransfersReport.csv -SIMSQA2 -Usystem_application -Psystem_application -c -t,




