if object_id('DupRollover') is not null
drop table DupRollover
go
select Flow_Number, NWAC, cnt=count(*)
into #del
from CGTRollover
group by Flow_Number, NWAC
having count(*) >1
select @@rowcount,' duplicates found in CGTRollover Table'


while exists (select 1 from #del where cnt > 1)
begin

set rowcount 1

delete CGTRollover
from CGTRollover c, #del
where c.Flow_Number = #del.Flow_Number
and cnt > 1

set rowcount 0

update #del
set cnt = cnt -1

end

select Flow_Number, cnt=count(*)
into #dupFlows
from CGTRollover
group by Flow_Number
having count(*) >1
select @@rowcount,' duplicates found in CGTRollover Table where NWAC is different'



select '********Please report this to the Tax Team.************'

select i.Description, t.Date_Action, t.Flow_No, t.Flow_Type, cd.Owner, cd.Contract_Number, cd.Product, t.Rate, c.NWAC
INTO DupRollover
from CGTRollover c, #dupFlows d, ContractDeal cd, Instruments i, Transactions t
where c.Flow_Number = d.Flow_Number
and c.Flow_Number = t.Flow_No
and t.Deal_Xref = cd.Deal_Xref
and t.Instrument_No = i.Instrument_Number
order by t.Flow_No

select * from DupRollover
GO