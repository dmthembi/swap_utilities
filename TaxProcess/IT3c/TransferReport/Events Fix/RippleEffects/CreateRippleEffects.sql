--Transfers.....
IF OBJECT_ID('#Flows') IS NOT NULL
drop table #Flows
IF OBJECT_ID('#TransferLog') IS NOT NULL
drop table #TransferLog
IF OBJECT_ID('#StartFlows') IS NOT NULL
drop table #StartFlows
IF OBJECT_ID('#NextTransFers1') IS NOT NULL
drop table #NextTransFers1
IF OBJECT_ID('#Dates') IS NOT NULL
drop table #Dates
IF OBJECT_ID('#InsertScript') IS NOT NULL
drop table #InsertScript
IF OBJECT_ID('#XferLog') IS NOT NULL
drop table #XferLog
IF OBJECT_ID('#data1') IS NOT NULL
drop table #data1
IF OBJECT_ID('#data2') IS NOT NULL
drop table #data2
IF OBJECT_ID('#data3') IS NOT NULL
drop table #data3
IF OBJECT_ID('#StartFlowsTMP') IS NOT NULL
drop table #StartFlowsTMP
IF OBJECT_ID('#TransferLogTMP') IS NOT NULL
drop table #TransferLogTMP
IF OBJECT_ID('#TransferLogTMP2') IS NOT NULL
drop table #TransferLogTMP2
IF OBJECT_ID('#newSourceDeals') IS NOT NULL
drop table #newSourceDeals
IF OBJECT_ID('#nextDestDeals') IS NOT NULL
drop table  #nextDestDeals
go

create table #InsertScript(sqlData varchar(100))

IF OBJECT_ID('#CGTExtractIn') is not null
	DROP TABLE #CGTExtractIn
	IF OBJECT_ID('#CGTExtractDetail') is not null
	DROP TABLE #CGTExtractDetail
	IF OBJECT_ID('#CGTExtractSummary') is not null
	DROP TABLE #CGTExtractSummary
	IF OBJECT_ID('#CGTLossCarryOver') is not null
	DROP TABLE #CGTLossCarryOver
	IF OBJECT_ID('#CGTNewTransactions') is not null
	DROP TABLE #CGTNewTransactions
	IF OBJECT_ID('#RE_Sell') is not null
	DROP TABLE #RE_Sell
	IF OBJECT_ID('#FxRates') is not null
	DROP TABLE #FxRates

	GO

	CREATE TABLE #CGTExtractIn
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null)


	CREATE TABLE #CGTNewTransactions
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Flow_No			int		null,
		Transaction_No		numeric(18,0)		null,
		Transaction_Type	char(1)		null,
		Instrument_No		int		null,
		Date_Action		datetime	null,
		Flow_Type		varchar(1)	null,
		Amount			money		null,
		Rate			numeric(14,6)	null,
		Units			numeric(18,4)	null)

	CREATE TABLE #CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)


	CREATE TABLE #CGTExtractSummary
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Instrument_No		int		null,
		Units_Disposed		numeric(18,4)	null,
		Aggregate_Unit_Cost	numeric(14,6)	null,
		Gross_Proceeds		money		null,
		Net_Gain		money		null,
		Units_Held		numeric(18,4)	null,
		WAC			numeric(14,6)	null
	)


	CREATE TABLE #CGTLossCarryOver
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Tax_Year		int		null,
		Gain			money		null)

	CREATE TABLE #RE_Sell
	(Instrument_Number   int                null,
	 Rate_Type           char(1)            null,
	 Date                datetime           null,
	 Rate_Date           datetime           null,
	 Buy                 numeric(14,6)      null,
	 Sell                numeric(14,6)      null,
	 Basic               numeric(14,6)      null,
	 Adjusted            numeric(14,6)      null,
	 TimeStamp           varbinary(8)       null)



	
	
declare @Start_Date datetime,@End_Date datetime, @Tax_Year int


select @Start_Date = Begin_Date, @End_Date = End_Date, @Tax_Year = Tax_Year
from TaxProcessDates

--#TransferLog: To be used in many places....
select  Source_Deal,Destination_Deal,Xfer_Type,Xfer_Date  = convert(varchar(11),Xfer_Date) 
into #TransferLogTMP
from TransferLog
where Xfer_Date >= @Start_Date
and Xfer_Date <= @End_Date


--CGTRolloverManualXfers
--Get all the Flow_Number affected..
select distinct Flow_Number,NWAC 
into #Flows
from DAILY..CGTRollover_Log
where Action_Date > ''  -- When did we run DML Scripts??...
and Action_Type in ('I')



select distinct tl.*
into #TransferLogTMP2
from #Flows f, Transactions t, #TransferLogTMP tl
where f.Flow_Number = t.Flow_No
and t.Deal_Xref = tl.Source_Deal
and t.Transaction_Type = 'S'

--We need all the Source Deals
select distinct l.Source_Deal,l.Destination_Deal,l.Xfer_Type,Xfer_Date  = convert(varchar(11),l.Xfer_Date) 
into #TransferLog
from #TransferLogTMP2 t, TransferLog l
where t.Destination_Deal = l.Destination_Deal




-- get all the STARTING TransferFlows


select distinct tl.*, Date_Action, t.Instrument_No , Flow_Number = t.Flow_No, NWAC = convert(float,null)
into #StartFlows
from Transactions t, #TransferLog tl, Instruments i
where t.Flow_Type = 'X' 
and t.Deal_Xref = tl.Source_Deal
and t.Date_Action = tl.Xfer_Date
and t.Transaction_Type = 'S'
and i.Instrument_Type <> 'XFER'
and t.Instrument_No = i.Instrument_Number



update #StartFlows
set NWAC = c.NWAC
from #StartFlows sf, CGTRollover c
where sf.Flow_Number = c.Flow_Number


--Get A complete list...



select distinct s.Destination_Deal
into #newSourceDeals
from #StartFlows s, ContractDeal cd
where s.Source_Deal = cd.Deal_Xref





select distinct t.Destination_Deal
into #nextDestDeals
from #newSourceDeals ns, #TransferLogTMP t
where ns.Destination_Deal = t.Source_Deal




select distinct tl.*, t.Date_Action, t.Instrument_No, t.Flow_No, OldWAC = c.NWAC, NWAC = convert(money,null)
into #NextTransFers1
from Transactions t, #TransferLogTMP tl, CGTRollover c, #nextDestDeals nd
where t.Deal_Xref  = tl.Source_Deal
and tl.Destination_Deal = nd.Destination_Deal
and t.Date_Action = tl.Xfer_Date
and t.Flow_No = c.Flow_Number
and tl.Xfer_Date = t.Date_Action
--and t.Date_Action >= sf.Date_Action  not sure if this should be added...
and Transaction_Type = 'S'



-- First Consolidations and Transfers.....:::#NextTransFers1

delete CGTRollover
where Flow_Number in (select Flow_No from #NextTransFers1)



insert into #InsertScript
select 'delete CGTRollover where Flow_Number = ' + convert(char(10), Flow_No)
from #NextTransFers1



select minDate =  min(Date_Action),
       Date_Action = min(Date_Action),
       maxDate = max(Date_Action)
into #Dates
from #NextTransFers1



while exists (select 1 from #Dates where maxDate >= Date_Action)
begin



  insert into #CGTExtractIn
  select distinct Source_Deal, '01 march 2016', n.Date_Action
  from #NextTransFers1 n, #Dates d
  where d.Date_Action = n.Date_Action
  
  exec CGTExtract


  update #NextTransFers1
  set NWAC = c.WAC
  from #CGTExtractDetail c, #NextTransFers1 nt, Transactions t
  where c.Transaction_No = t.Transaction_No
  and t.Flow_No = nt.Flow_No


    INSERT INTO CGTRollover
    select Flow_No, null, NWAC
    from #NextTransFers1 n, #Dates d
   where n.Date_Action = d.Date_Action



insert into #InsertScript
select  'insert CGTRollover select '+ convert(char(10), Flow_No) + ',null,'  + convert(char(15),NWAC)
 from #NextTransFers1 n, #Dates d
    where n.Date_Action = d.Date_Action

	TRUNCATE TABLE #CGTExtractIn
	TRUNCATE TABLE #CGTExtractDetail
	TRUNCATE TABLE #CGTExtractSummary
	TRUNCATE TABLE #CGTLossCarryOver
	TRUNCATE TABLE #CGTNewTransactions
	TRUNCATE TABLE #RE_Sell
	--TRUNCATE TABLE #FxRates

 update  #Dates
 set Date_Action = dateadd(dd,1,Date_Action)

end



--select distinct *
--into #XferLog
--from TransferLog
--where Destination_Deal in (select distinct Destination_Deal from #NextTransFers1)

drop table #XferLog
GO
select distinct Source_Deal, Destination_Deal, Xfer_Date
into #XferLog
from #NextTransFers1



update #XferLog
set Xfer_Date = convert(varchar(11),Xfer_Date)



select  x.Source_Deal, 
        x.Destination_Deal , 
        t.Instrument_No, 
        t.Date_Action,
        SourceFlow = t.Flow_No,
        SourceUnits =  t.Units, 
        SourceRollover = convert(money,null),
        DestUnits = convert(money,null), 
        DestFlow = convert(int,null),
        DestRollover = convert(money,null)
into #data1
from #XferLog x, Transactions t
where x.Source_Deal = t.Deal_Xref
and x.Xfer_Date = t.Date_Action
and t.Flow_Type = 'X'
and t.Transaction_Type = 'S'





update #data1
set SourceRollover = c.NWAC
from #data1 d, CGTRollover c
where d.SourceFlow = c.Flow_Number



select Destination_Deal, Instrument_No, Date_Action, totalDestUnits = sum(SourceUnits)
into #data2
from #data1
group by Destination_Deal, Instrument_No, Date_Action




update #data1
set DestUnits = d2.totalDestUnits
from #data1 d1, #data2 d2
where d1.Destination_Deal = d2.Destination_Deal
and d1.Instrument_No = d2.Instrument_No
and d1.Date_Action = d2.Date_Action




update #data1
set DestFlow = t.Flow_No
from #data1 d1, Transactions t
where d1.Destination_Deal = t.Deal_Xref
and d1.Instrument_No = t.Instrument_No
and d1.Date_Action = t.Date_Action
--and abs(d1.DestUnits - t.Units) < 1
and t.Flow_Type = 'X'
and t.Transaction_Type = 'D'



select  DestFlow, DestUnits, Cost = sum(SourceUnits*SourceRollover), DestRollover = convert(money,null)
into #data3
from #data1
group by DestFlow, DestUnits

update #data3
set DestRollover = Cost/DestUnits



delete CGTRollover
where Flow_Number in (select DestFlow from #data3 where DestFlow is not null
and DestRollover is not null)

insert into #InsertScript
select 'delete CGTRollover where Flow_Number = ' + convert(char(15),DestFlow)
from #data3 
where DestFlow is not null
and DestRollover is not null

insert into #InsertScript
select 'insert CGTRollover select '+ convert(char(15), DestFlow) +  ',null,'  + convert(char(15),DestRollover)
from #data3
where DestFlow is not null
and DestRollover is not null

SELECT * FROM #InsertScript

GO


