SET NOCOUNT ON
if object_id('#Flows') is not null
drop table #Flows
if object_id('#XferLog') is not null
drop table #XferLog
if object_id('#SourceDest') is not null
drop table #SourceDest

GO
create table #Flows(Direction varchar(20),
                    Flow_Number int,
                    Deal_Xref int,
                    InstrDisc varchar(50),
                    NWAC float null)
					
select *
into #XferLog
from TransferLog
where Xfer_Date >= (SELECT Begin_Date FROM TaxProcessDates)

GO

insert into #Flows select 'SourceDeal',20115874,244,'Ashburton {RF} MultiMgr Prud Flex Fund B1',null
insert into #Flows select 'DestinationDeal',20115873,37507,'Ashburton {RF} MultiMgr Prud Flex Fund B1',null
insert into #Flows select 'SourceDeal',20115876,244,'Ashburton {RF} Targeted Return Fund B4',null
insert into #Flows select 'DestinationDeal',20115875,37507,'Ashburton {RF} Targeted Return Fund B4',null
insert into #Flows select 'DestinationDeal',19408963,36010,'Ashburton {RF} Targeted Return Fund B4',null
insert into #Flows select 'SourceDeal',19408962,453,'Ashburton {RF} Targeted Return Fund B4',null

GO



update #XferLog
set Xfer_Date = convert(varchar(11),Xfer_Date) 




select distinct x.*, SourceFlow = t.Flow_No, DestFlow = convert(int,null), WAC = convert(float,null), GAIN = convert(money,null)
into #SourceDest
from #Flows f, Transactions t, #XferLog x, Instruments i
where f.Flow_Number = t.Flow_No
and t.Deal_Xref = x.Source_Deal
and t.Instrument_No = i.Instrument_Number
and i.Instrument_Type = 'XFER'
and t.Transaction_Type = 'D' -- 'D' or 'S' makes no difference
AND  f.Direction = 'SourceDeal'



update #SourceDest
set DestFlow = td.Flow_No
from #SourceDest sd, Transactions ts, #Flows f, Instruments i, Transactions td, #Flows f2
where sd.Source_Deal = ts.Deal_Xref
and sd.Xfer_Date = ts.Date_Action
and ts.Flow_No = sd.SourceFlow
and ts.Flow_No = f.Flow_Number
and ts.Instrument_No = i.Instrument_Number
and i.Instrument_Type <> 'XFER'
and ts.Transaction_Type = 'S'
and sd.Destination_Deal = td.Deal_Xref
and sd.Xfer_Date = td.Date_Action
and td.Flow_No = f2.Flow_Number
and td.Transaction_Type = 'D'
and ts.Instrument_No = td.Instrument_No



delete #SourceDest
where Xfer_Type = 'C'

delete #SourceDest
where DestFlow  is null



CREATE TABLE #CGTExtractIn
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null)


	CREATE TABLE #CGTNewTransactions
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Flow_No			int		null,
		Transaction_No		numeric(18,0)		null,
		Transaction_Type	char(1)		null,
		Instrument_No		int		null,
		Date_Action		datetime	null,
		Flow_Type		varchar(1)	null,
		Amount			money		null,
		Rate			numeric(14,6)	null,
		Units			numeric(18,4)	null)

	CREATE TABLE #CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)


	CREATE TABLE #CGTExtractSummary
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Instrument_No		int		null,
		Units_Disposed		numeric(18,4)	null,
		Aggregate_Unit_Cost	numeric(14,6)	null,
		Gross_Proceeds		money		null,
		Net_Gain		money		null,
		Units_Held		numeric(18,4)	null,
		WAC			numeric(14,6)	null
	)


	CREATE TABLE #CGTLossCarryOver
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Tax_Year		int		null,
		Gain			money		null)

	CREATE TABLE #RE_Sell
	(Instrument_Number   int                null,
	 Rate_Type           char(1)            null,
	 Date                datetime           null,
	 Rate_Date           datetime           null,
	 Buy                 numeric(14,6)      null,
	 Sell                numeric(14,6)      null,
	 Basic               numeric(14,6)      null,
	 Adjusted            numeric(14,6)      null,
	 TimeStamp           varbinary(8)       null)



insert into #CGTExtractIn
select distinct Deal_Xref, tpd.Begin_Date, tpd.End_Date
from #SourceDest sd, Transactions t, TaxProcessDates tpd
where sd.SourceFlow = t.Flow_No


exec CGTExtract



update #SourceDest
   set WAC = c.WAC,
        GAIN = c.Gain
from #CGTExtractDetail c, Transactions t, #SourceDest sd
where c.Transaction_No = t.Transaction_No
and t.Flow_No = sd.SourceFlow




select 'delete CGTRollover where Flow_Number =  ' + convert(char(15), SourceFlow) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest
where DestFlow is not null


select 'delete CGTRollover where Flow_Number =  ' + convert(char(15), DestFlow) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest
where DestFlow is not null


select 'insert into CGTRollover select ' + convert(char(15), SourceFlow) + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest
where DestFlow is not null

select 'insert into CGTRollover select ' + convert(char(15), DestFlow) + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest
where DestFlow is not null
GO
