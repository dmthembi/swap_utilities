set nocount on
IF OBJECT_ID('#CGTExtractIn') <> NULL
DROP TABLE #CGTExtractIn
IF OBJECT_ID('#CGTExtractDetail') <> NULL
DROP TABLE #CGTExtractDetail
IF OBJECT_ID('#CGTExtractSummary') <> NULL
DROP TABLE #CGTExtractSummary
IF OBJECT_ID('#CGTLossCarryOver') <> NULL
DROP TABLE #CGTLossCarryOver
IF OBJECT_ID('#CGTNewTransactions') <> NULL
DROP TABLE #CGTNewTransactions
IF OBJECT_ID('#RE_Sell') <> NULL
DROP TABLE #RE_Sell
IF OBJECT_ID('#FxRates') <> NULL
DROP TABLE #FxRates

if object_id('#Flows') is not null
drop table #Flows
if object_id('#XferLog') is not null
drop table #XferLog
if object_id('#SourceDest') is not null
drop table #SourceDest
if object_id('#SourceTotal') is not null
drop table #SourceTotal


go
create table #Flows(Direction varchar(20),
                    Flow_Number int,
                    InstrDisc varchar(50),
                    NWAC float null)
go
insert into #Flows select 'SourceDeal',241416843,'Coronation Balanced Defensive Fund B4',null
insert into #Flows select 'SourceDeal',241416846,'Coronation Balanced Defensive Fund B4',null
insert into #Flows select 'DestinationDeal',241416847,'Coronation Balanced Defensive Fund B4',null
insert into #Flows select 'SourceDeal',241416844,'Allan Gray Stable Fund C',null








select *
into #XferLog
from TransferLog
where Xfer_Date >= (SELECT Begin_Date FROM TaxProcessDates)
and Xfer_Type = 'C'


update #XferLog
set Xfer_Date = convert(varchar(11),Xfer_Date) 




select distinct x.*, 
                SourceFlow = t.Flow_No,     
                DestFlow = convert(int,null), 
                WAC = convert(float,null), 
                GAIN = convert(money,null), 
                Cost = convert(float,null), 
                i.Instrument_Number,
                i.Description,
                i.Instrument_Type,
                SourceUnits = convert(money,null), 
                DestUnits = convert(money,null), 
                totalDestUnits = convert(money,null), 
                DestRollover = convert(money,null)
into #SourceDest
from #Flows f, Transactions t, #XferLog x, Instruments i
where f.Flow_Number = t.Flow_No
and t.Deal_Xref = x.Source_Deal
and t.Instrument_No = i.Instrument_Number
and t.Transaction_Type = 'S'



update #SourceDest
set DestFlow = td.Flow_No
from #SourceDest sd, Transactions ts, #Flows f, Instruments i, Transactions td, #Flows f2
where sd.Source_Deal = ts.Deal_Xref
and sd.Xfer_Date = ts.Date_Action
and ts.Flow_No = sd.SourceFlow
and ts.Flow_No = f.Flow_Number
and ts.Instrument_No = i.Instrument_Number
and i.Instrument_Type <> 'XFER'
and ts.Transaction_Type = 'S'
and sd.Destination_Deal = td.Deal_Xref
and sd.Xfer_Date = td.Date_Action
and td.Flow_No = f2.Flow_Number
and td.Transaction_Type = 'D'
and ts.Instrument_No = td.Instrument_No




CREATE TABLE #CGTExtractIn
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null)


	CREATE TABLE #CGTNewTransactions
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Flow_No			int		null,
		Transaction_No		numeric(18,0)		null,
		Transaction_Type	char(1)		null,
		Instrument_No		int		null,
		Date_Action		datetime	null,
		Flow_Type		varchar(1)	null,
		Amount			money		null,
		Rate			numeric(14,6)	null,
		Units			numeric(18,4)	null)

	CREATE TABLE #CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)


	CREATE TABLE #CGTExtractSummary
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Instrument_No		int		null,
		Units_Disposed		numeric(18,4)	null,
		Aggregate_Unit_Cost	numeric(14,6)	null,
		Gross_Proceeds		money		null,
		Net_Gain		money		null,
		Units_Held		numeric(18,4)	null,
		WAC			numeric(14,6)	null
	)


	CREATE TABLE #CGTLossCarryOver
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Tax_Year		int		null,
		Gain			money		null)

	CREATE TABLE #RE_Sell
	(Instrument_Number   int                null,
	 Rate_Type           char(1)            null,
	 Date                datetime           null,
	 Rate_Date           datetime           null,
	 Buy                 numeric(14,6)      null,
	 Sell                numeric(14,6)      null,
	 Basic               numeric(14,6)      null,
	 Adjusted            numeric(14,6)      null,
	 TimeStamp           varbinary(8)       null)





delete CGTRollover
from #SourceDest sd, CGTRollover c
where sd.SourceFlow = c.Flow_Number

declare @Start_Date datetime,@End_Date datetime, @Tax_Year int

select @Tax_Year = (SELECT Tax_Year FROM TaxProcessDates)

select @Start_Date = Begin_Date, @End_Date = End_Date, @Tax_Year = Tax_Year
from TaxProcessDates



insert into #CGTExtractIn
select distinct Deal_Xref, @Start_Date, Date_Action
from #SourceDest sd, Transactions t
where sd.SourceFlow = t.Flow_No


exec CGTExtract



update #SourceDest
   set WAC = c.WAC,
        GAIN = c.Gain
from #CGTExtractDetail c, Transactions t, #SourceDest sd
where c.Transaction_No = t.Transaction_No
and t.Flow_No = sd.SourceFlow


-- Just in case WAC is already in CGTRollover..

        update #SourceDest
        set WAC = r.NWAC
        from #SourceDest sd, CGTRollover r
        where sd.SourceFlow = r.Flow_Number


update #SourceDest
set SourceUnits = t.Units
from #SourceDest sd, Transactions t
where sd.SourceFlow = t.Flow_No
and t.Transaction_Type = 'S'

update #SourceDest
set DestUnits = t.Units
from #SourceDest sd, Transactions t
where sd.DestFlow = t.Flow_No
and t.Transaction_Type = 'D'


select Destination_Deal, Instrument_Number, Xfer_Date, totalDestUnits = sum(SourceUnits)
into #SourceTotal
from #SourceDest
group by Destination_Deal, Instrument_Number, Xfer_Date

update #SourceDest
set totalDestUnits = st.totalDestUnits
from #SourceDest sd,  #SourceTotal st
where sd.Destination_Deal = st.Destination_Deal
and sd.Instrument_Number = st.Instrument_Number


select  DestFlow, DestUnits, Cost = sum(SourceUnits*WAC), DestRollover = convert(money,null)
into #Cost
from #SourceDest
group by DestFlow, DestUnits

update #Cost
set DestRollover = Cost/DestUnits



update #SourceDest
set DestRollover = c.DestRollover
from #SourceDest sd, #Cost c
where sd.DestFlow = c.DestFlow




select distinct 'delete CGTRollover where Flow_Number =  ' + convert(char(15), SourceFlow) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest


select distinct 'delete CGTRollover where Flow_Number =  ' + convert(char(15), DestFlow) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest


select distinct 'insert into CGTRollover select ' + convert(char(15), SourceFlow) + ',null,   ' + convert(char(15),convert(money, WAC))
from #SourceDest


select distinct 'insert into CGTRollover select ' + convert(char(15), DestFlow) + ',null,   ' + convert(char(15),convert(money, DestRollover))
from #SourceDest

GO