set nocount on
IF OBJECT_ID('#CGTExtractIn') is not null
DROP TABLE #CGTExtractIn
IF OBJECT_ID('#CGTExtractDetail') is not null
DROP TABLE #CGTExtractDetail
IF OBJECT_ID('#CGTExtractSummary') is not null
DROP TABLE #CGTExtractSummary
IF OBJECT_ID('#CGTLossCarryOver') is not null
DROP TABLE #CGTLossCarryOver
IF OBJECT_ID('#CGTNewTransactions') is not null
DROP TABLE #CGTNewTransactions
IF OBJECT_ID('#RE_Sell') is not null
DROP TABLE #RE_Sell
IF OBJECT_ID('#FxRates') is not null
DROP TABLE #FxRates
IF OBJECT_ID('CGTExtractDetail') is not null
DROP TABLE CGTExtractDetail
IF OBJECT_ID('#CGTExtractIn_BKP') is not null
DROP TABLE #CGTExtractIn_BKP
if object_id('#Flows') is not null
drop table #Flows
if object_id('#XferLog') is not null
drop table #XferLog
if object_id('#SourceDest') is not null
drop table #SourceDest
if object_id('#SourceTotal') is not null
drop table #SourceTotal


go
create table #Flows(Flow_Number int, NWAC float null, GAIN money null, Deal_Xref int)
go


insert into #Flows select 243882742,null,null,2054272885
insert into #Flows select 243882742,null,null,2054272885
insert into #Flows select 243882741,null,null,515015259
insert into #Flows select 243882743,null,null,2054389446
insert into #Flows select 243882744,null,null,2054424146
insert into #Flows select 243646198,null,null,2054650320
insert into #Flows select 243723810,null,null,2054653509
insert into #Flows select 243723794,null,null,2054654002
insert into #Flows select 243885906,null,null,2054571891
insert into #Flows select 243886037,null,null,2054586416
insert into #Flows select 243886040,null,null,2054586421
insert into #Flows select 243885907,null,null,2054621060
insert into #Flows select 243886043,null,null,2054624944
insert into #Flows select 243885908,null,null,2054629478
insert into #Flows select 243885909,null,null,2054631984
insert into #Flows select 243885910,null,null,2054635804


GO



CREATE TABLE #CGTExtractIn
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null)


	CREATE TABLE #CGTNewTransactions
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Flow_No			int		null,
		Transaction_No		numeric(18,0)		null,
		Transaction_Type	char(1)		null,
		Instrument_No		int		null,
		Date_Action		datetime	null,
		Flow_Type		varchar(1)	null,
		Amount			money		null,
		Rate			numeric(14,6)	null,
		Units			numeric(18,4)	null)

	CREATE TABLE #CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)

	CREATE TABLE CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)


	CREATE TABLE #CGTExtractSummary
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Instrument_No		int		null,
		Units_Disposed		numeric(18,4)	null,
		Aggregate_Unit_Cost	numeric(14,6)	null,
		Gross_Proceeds		money		null,
		Net_Gain		money		null,
		Units_Held		numeric(18,4)	null,
		WAC			numeric(14,6)	null
	)


	CREATE TABLE #CGTLossCarryOver
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Tax_Year		int		null,
		Gain			money		null)

	CREATE TABLE #RE_Sell
	(Instrument_Number   int                null,
	 Rate_Type           char(1)            null,
	 Date                datetime           null,
	 Rate_Date           datetime           null,
	 Buy                 numeric(14,6)      null,
	 Sell                numeric(14,6)      null,
	 Basic               numeric(14,6)      null,
	 Adjusted            numeric(14,6)      null,
	 TimeStamp           varbinary(8)       null)




declare @Start_Date datetime,@End_Date datetime, @Tax_Year int


select @Start_Date = Begin_Date, @End_Date = End_Date, @Tax_Year = Tax_Year
from TaxProcessDates



insert into #CGTExtractIn
select distinct t.Deal_Xref, @Start_Date, Date_Action
from #Flows sd, Transactions t
where sd.Flow_Number = t.Flow_No




select *
into #CGTExtractIn_BKP
from #CGTExtractIn

truncate table #CGTExtractIn


while exists (select 1 from #CGTExtractIn_BKP)
begin

set rowcount 1
insert into #CGTExtractIn
select * from #CGTExtractIn_BKP
set rowcount 0


exec CGTExtract


delete #CGTExtractIn_BKP
from #CGTExtractIn_BKP c1, #CGTExtractIn c2
where c1.Deal_Xref = c2.Deal_Xref


insert into CGTExtractDetail
select * from #CGTExtractDetail

truncate table #CGTExtractIn

truncate table #CGTLossCarryOver
truncate table #CGTExtractSummary
truncate table #CGTExtractDetail
truncate table #CGTNewTransactions

end



INSERT INTO #CGTExtractDetail
SELECT * FROM CGTExtractDetail



update #Flows
   set NWAC = c.WAC, GAIN = c.Gain
from #CGTExtractDetail c, Transactions t, #Flows sd
where c.Transaction_No = t.Transaction_No
and t.Flow_No = sd.Flow_Number





select distinct 'delete CGTRollover where Flow_Number =  ' + convert(char(15), Flow_Number) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #Flows
where isnull(GAIN,0.00) > 0.00


select distinct 'insert into CGTRollover select ' + convert(char(15), Flow_Number) + ',null,   ' + convert(char(15),convert(money, NWAC))
from #Flows
where isnull(GAIN,0.00) > 0.00

GO