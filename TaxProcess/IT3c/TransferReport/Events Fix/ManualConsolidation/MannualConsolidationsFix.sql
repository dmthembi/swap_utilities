--***
set nocount on
IF OBJECT_ID('#CGTExtractIn') is not null
DROP TABLE #CGTExtractIn
IF OBJECT_ID('#CGTExtractDetail') is not null
DROP TABLE #CGTExtractDetail
IF OBJECT_ID('#CGTExtractSummary') is not null
DROP TABLE #CGTExtractSummary
IF OBJECT_ID('#CGTLossCarryOver') is not null
DROP TABLE #CGTLossCarryOver
IF OBJECT_ID('#CGTNewTransactions') is not null
DROP TABLE #CGTNewTransactions
IF OBJECT_ID('#RE_Sell') is not null
DROP TABLE #RE_Sell
IF OBJECT_ID('#FxRates') is not null
DROP TABLE #FxRates
IF OBJECT_ID('CGTExtractDetail') is not null
DROP TABLE CGTExtractDetail
IF OBJECT_ID('#CGTExtractIn_BKP') is not null
DROP TABLE #CGTExtractIn_BKP
if object_id('#Flows') is not null
drop table #Flows
if object_id('#XferLog') is not null
drop table #XferLog
if object_id('#SourceDest') is not null
drop table #SourceDest
if object_id('#SourceTotal') is not null
drop table #SourceTotal

create table #SourceDest (SourceFlow int, DestFlow int)
go

insert into #SourceDest select 17412765,17412791
insert into #SourceDest select 17412753,17412790
insert into #SourceDest select 17412736,17412785
insert into #SourceDest select 17412752,17412789
insert into #SourceDest select 17412738,17412787
insert into #SourceDest select 17412737,17412786
insert into #SourceDest select 17412735,17412779
insert into #SourceDest select 17412751,17412788
insert into #SourceDest select 17412778,17412792
insert into #SourceDest select 16955916,16955917
insert into #SourceDest select 21622395,21622405
insert into #SourceDest select 21622394,21622404
insert into #SourceDest select 21622378,21622398
insert into #SourceDest select 21622379,21622399
insert into #SourceDest select 21622391,21622401
insert into #SourceDest select 21622380,21622400
insert into #SourceDest select 21622377,21622397
insert into #SourceDest select 21622392,21622402
insert into #SourceDest select 21622396,21622406
insert into #SourceDest select 16213541,16213542
insert into #SourceDest select 16213543,16213544
insert into #SourceDest select 16213777,16213785
insert into #SourceDest select 16213776,16213784
insert into #SourceDest select 16213775,16213783
insert into #SourceDest select 16213773,16213781
insert into #SourceDest select 16213772,16213780
insert into #SourceDest select 16213774,16213782
insert into #SourceDest select 16213779,16213787
insert into #SourceDest select 16213778,16213786
insert into #SourceDest select 15707388,15707406
insert into #SourceDest select 15707387,15707405
insert into #SourceDest select 15707377,15707392
insert into #SourceDest select 15707383,15707403
insert into #SourceDest select 15707385,15707404
insert into #SourceDest select 15707380,15707396
insert into #SourceDest select 15707379,15707394
insert into #SourceDest select 15707381,15707402
insert into #SourceDest select 15707391,15707408
insert into #SourceDest select 15707389,15707407
insert into #SourceDest select 15252974,15252984
insert into #SourceDest select 15252973,15252983
insert into #SourceDest select 15252967,15252977
insert into #SourceDest select 15252971,15252981
insert into #SourceDest select 15252972,15252982
insert into #SourceDest select 15252968,15252978
insert into #SourceDest select 15252970,15252980
insert into #SourceDest select 15252976,15252986
insert into #SourceDest select 15252975,15252985
insert into #SourceDest select 17054820,17054840
insert into #SourceDest select 17054819,17054834
insert into #SourceDest select 17054814,17054823
insert into #SourceDest select 17054818,17054833
insert into #SourceDest select 17054816,17054831
insert into #SourceDest select 17054815,17054829
insert into #SourceDest select 17054813,17054822
insert into #SourceDest select 17054817,17054832
insert into #SourceDest select 17054821,17054841
insert into #SourceDest select 18208271,18208274
insert into #SourceDest select 16294936,16294946
insert into #SourceDest select 16294927,16294944
insert into #SourceDest select 16294935,16294945
insert into #SourceDest select 17626821,17626822
insert into #SourceDest select 17626824,17626827
insert into #SourceDest select 17626825,17626826
insert into #SourceDest select 20511733,20511734
insert into #SourceDest select 20511731,20511732
insert into #SourceDest select 20511729,20511730
insert into #SourceDest select 20511725,20511726
insert into #SourceDest select 20511723,20511724
insert into #SourceDest select 20511721,20511722
insert into #SourceDest select 20511727,20511728
insert into #SourceDest select 20511735,20511736
insert into #SourceDest select 20511755,20511756
insert into #SourceDest select 20511753,20511754
insert into #SourceDest select 20511739,20511740
insert into #SourceDest select 20511741,20511742
insert into #SourceDest select 20511749,20511750
insert into #SourceDest select 20511745,20511746
insert into #SourceDest select 20511743,20511744
insert into #SourceDest select 20511737,20511738
insert into #SourceDest select 20511747,20511748
insert into #SourceDest select 20511757,20511758
insert into #SourceDest select 20511775,20511776
insert into #SourceDest select 20511773,20511774
insert into #SourceDest select 20511761,20511762
insert into #SourceDest select 20511763,20511764
insert into #SourceDest select 20511771,20511772
insert into #SourceDest select 20511767,20511768
insert into #SourceDest select 20511765,20511766
insert into #SourceDest select 20511759,20511760
insert into #SourceDest select 20511769,20511770
insert into #SourceDest select 20511777,20511778
insert into #SourceDest select 18464740,18464744
insert into #SourceDest select 18464741,18464745
insert into #SourceDest select 18464742,18464746
insert into #SourceDest select 18464743,18464747
insert into #SourceDest select 18140871,18141242
insert into #SourceDest select 18140870,18140885
insert into #SourceDest select 18140863,18140878
insert into #SourceDest select 18140868,18140883
insert into #SourceDest select 18140865,18140881
insert into #SourceDest select 18140864,18140879
insert into #SourceDest select 18140860,18140877
insert into #SourceDest select 18140867,18140882
insert into #SourceDest select 18140876,18141243
insert into #SourceDest select 17627054,17627055
insert into #SourceDest select 17627049,17627048
insert into #SourceDest select 17627051,17627050
insert into #SourceDest select 17412801,17412802
insert into #SourceDest select 17497387,17497388
insert into #SourceDest select 14416564,14416594
insert into #SourceDest select 14416563,14416593
insert into #SourceDest select 14416554,14416567
insert into #SourceDest select 14416561,14416580
insert into #SourceDest select 14416562,14416588
insert into #SourceDest select 14416559,14416576
insert into #SourceDest select 14416555,14416575
insert into #SourceDest select 14416560,14416578
insert into #SourceDest select 14416566,14416596
insert into #SourceDest select 14416565,14416595
insert into #SourceDest select 19060851,19060852
insert into #SourceDest select 19061138,19060847
insert into #SourceDest select 14681010,14681011
insert into #SourceDest select 16119313,16119337
insert into #SourceDest select 16119312,16119336
insert into #SourceDest select 16119306,16119316
insert into #SourceDest select 16119310,16119334
insert into #SourceDest select 16119311,16119335
insert into #SourceDest select 16119308,16119332
insert into #SourceDest select 16119307,16119317
insert into #SourceDest select 16119309,16119333
insert into #SourceDest select 16119315,16119339
insert into #SourceDest select 16119314,16119338
insert into #SourceDest select 17627007,17627006
insert into #SourceDest select 16360215,16360239
insert into #SourceDest select 16360214,16360232
insert into #SourceDest select 16360208,16360218
insert into #SourceDest select 16360212,16360230
insert into #SourceDest select 16360213,16360231
insert into #SourceDest select 16360210,16360228
insert into #SourceDest select 16360209,16360219
insert into #SourceDest select 16360211,16360229
insert into #SourceDest select 16360217,16360248
insert into #SourceDest select 16360216,16360240
insert into #SourceDest select 15501530,15501564
insert into #SourceDest select 15501563,15501580
insert into #SourceDest select 15501551,15501566
insert into #SourceDest select 15501545,15501565
insert into #SourceDest select 15501562,15501573
insert into #SourceDest select 16213739,16213749
insert into #SourceDest select 16213738,16213748
insert into #SourceDest select 16213732,16213742
insert into #SourceDest select 16213736,16213746
insert into #SourceDest select 16213737,16213747
insert into #SourceDest select 16213734,16213744
insert into #SourceDest select 16213733,16213743
insert into #SourceDest select 16213735,16213745
insert into #SourceDest select 16213741,16213751
insert into #SourceDest select 18208236,18208238
insert into #SourceDest select 18208235,18208237
insert into #SourceDest select 19408965,19408971
insert into #SourceDest select 19409015,19409017
insert into #SourceDest select 20919408,20919427
insert into #SourceDest select 19408967,19408973
insert into #SourceDest select 19408966,19408972
insert into #SourceDest select 19408968,19408976
insert into #SourceDest select 20919409,20919428
insert into #SourceDest select 19408978,19408984
insert into #SourceDest select 20919425,20919449
insert into #SourceDest select 20919424,20919440
insert into #SourceDest select 19408982,19408992
insert into #SourceDest select 19408980,19408986
insert into #SourceDest select 20919411,20919430
insert into #SourceDest select 19408979,19408985
insert into #SourceDest select 19408981,19409028
insert into #SourceDest select 20919423,20919431
insert into #SourceDest select 18831227,18831234
insert into #SourceDest select 18831228,18831235
insert into #SourceDest select 18831232,18831239
insert into #SourceDest select 18831230,18831237
insert into #SourceDest select 18831229,18831236
insert into #SourceDest select 18831231,18831238
insert into #SourceDest select 21153947,21153964
insert into #SourceDest select 21153948,21153965
insert into #SourceDest select 21153963,21153969
insert into #SourceDest select 21153950,21153967
insert into #SourceDest select 21153949,21153966
insert into #SourceDest select 21153962,21153968
insert into #SourceDest select 19479214,19479222
insert into #SourceDest select 19479213,19479221
insert into #SourceDest select 19479212,19479220
insert into #SourceDest select 19479210,19479218
insert into #SourceDest select 19479209,19479217
insert into #SourceDest select 19479208,19479216
insert into #SourceDest select 19479211,19479219
insert into #SourceDest select 19479215,19479223
insert into #SourceDest select 19479231,19479240
insert into #SourceDest select 19479230,19479239
insert into #SourceDest select 19479225,19479234
insert into #SourceDest select 19479229,19479238
insert into #SourceDest select 19479227,19479236
insert into #SourceDest select 19479226,19479235
insert into #SourceDest select 19479224,19479233
insert into #SourceDest select 19479228,19479237
insert into #SourceDest select 19479243,19479242
insert into #SourceDest select 17626855,17626857
insert into #SourceDest select 17626854,17626856
insert into #SourceDest select 14384889,14384890
insert into #SourceDest select 17156073,17156085
insert into #SourceDest select 17156072,17156082
insert into #SourceDest select 17156067,17156076
insert into #SourceDest select 17156071,17156081
insert into #SourceDest select 17156069,17156079
insert into #SourceDest select 17156068,17156077
insert into #SourceDest select 17156066,17156075
insert into #SourceDest select 17156070,17156080
insert into #SourceDest select 17156074,17156087
insert into #SourceDest select 17626991,17626992
insert into #SourceDest select 17626988,17626987
insert into #SourceDest select 17626985,17626986
insert into #SourceDest select 17626971,17626970
insert into #SourceDest select 17626973,17626972
insert into #SourceDest select 16213706,16213716
insert into #SourceDest select 16213705,16213715
insert into #SourceDest select 16213694,16213709
insert into #SourceDest select 16213701,16213713
insert into #SourceDest select 16213702,16213714
insert into #SourceDest select 16213697,16213711
insert into #SourceDest select 16213696,16213710
insert into #SourceDest select 16213700,16213712
insert into #SourceDest select 16213708,16213718
insert into #SourceDest select 16213707,16213717
insert into #SourceDest select 17659808,17659818
insert into #SourceDest select 17659807,17659817
insert into #SourceDest select 17659804,17659814
insert into #SourceDest select 17659805,17659815
insert into #SourceDest select 17659806,17659816
insert into #SourceDest select 16294918,16294924
insert into #SourceDest select 16294922,16294929
insert into #SourceDest select 16294920,16294926
insert into #SourceDest select 16294919,16294925
insert into #SourceDest select 16294921,16294928
insert into #SourceDest select 16262787,16262799
insert into #SourceDest select 16262786,16262798
insert into #SourceDest select 16262771,16262790
insert into #SourceDest select 16262784,16262795
insert into #SourceDest select 16262785,16262797
insert into #SourceDest select 16262773,16262793
insert into #SourceDest select 16262772,16262791
insert into #SourceDest select 16262783,16262794
insert into #SourceDest select 16262789,16262810
insert into #SourceDest select 16262788,16262800
insert into #SourceDest select 17054774,17054785
insert into #SourceDest select 17054769,17054784
insert into #SourceDest select 17054753,17054779
insert into #SourceDest select 17054768,17054783
insert into #SourceDest select 17054756,17054781
insert into #SourceDest select 17054754,17054780
insert into #SourceDest select 17054752,17054778
insert into #SourceDest select 17054765,17054782
insert into #SourceDest select 17054777,17054787
insert into #SourceDest select 16086108,16086113
insert into #SourceDest select 16086112,16086117
insert into #SourceDest select 16086110,16086115
insert into #SourceDest select 16086109,16086114
insert into #SourceDest select 16086111,16086116
insert into #SourceDest select 17054793,17054798
insert into #SourceDest select 17054797,17054802
insert into #SourceDest select 17054795,17054800
insert into #SourceDest select 17054794,17054799
insert into #SourceDest select 17054796,17054801
insert into #SourceDest select 20919376,20919382
insert into #SourceDest select 20919381,20919391
insert into #SourceDest select 20919380,20919390
insert into #SourceDest select 20919377,20919383
insert into #SourceDest select 20919378,20919384
insert into #SourceDest select 20919379,20919385
insert into #SourceDest select 15804860,15804865
insert into #SourceDest select 15804864,15804869
insert into #SourceDest select 15804862,15804867
insert into #SourceDest select 15804861,15804866
insert into #SourceDest select 15804863,15804868
insert into #SourceDest select 16213759,16213769
insert into #SourceDest select 16213758,16213768
insert into #SourceDest select 16213752,16213762
insert into #SourceDest select 16213756,16213766
insert into #SourceDest select 16213757,16213767
insert into #SourceDest select 16213754,16213764
insert into #SourceDest select 16213753,16213763
insert into #SourceDest select 16213755,16213765
insert into #SourceDest select 16213761,16213771
insert into #SourceDest select 16213760,16213770
insert into #SourceDest select 15707411,15707416
insert into #SourceDest select 15707415,15707420
insert into #SourceDest select 15707413,15707418
insert into #SourceDest select 15707412,15707417
insert into #SourceDest select 15707414,15707419
insert into #SourceDest select 16456470,16456480
insert into #SourceDest select 16456469,16456479
insert into #SourceDest select 16456463,16456473
insert into #SourceDest select 16456467,16456477
insert into #SourceDest select 16456468,16456478
insert into #SourceDest select 16456465,16456475
insert into #SourceDest select 16456464,16456474
insert into #SourceDest select 16456466,16456476
insert into #SourceDest select 16456472,16456482
insert into #SourceDest select 16456471,16456481
insert into #SourceDest select 16086201,16086223
insert into #SourceDest select 16086222,16086227
insert into #SourceDest select 16086203,16086225
insert into #SourceDest select 16086202,16086224
insert into #SourceDest select 16086204,16086226
insert into #SourceDest select 17942395,17942420
insert into #SourceDest select 17942394,17942419
insert into #SourceDest select 17942379,17942407
insert into #SourceDest select 17942392,17942418
insert into #SourceDest select 17942382,17942409
insert into #SourceDest select 17942381,17942408
insert into #SourceDest select 17942378,17942406
insert into #SourceDest select 17942383,17942417
insert into #SourceDest select 17942396,17942421
insert into #SourceDest select 19646767,19646776
insert into #SourceDest select 19646766,19646775
insert into #SourceDest select 19646761,19646770
insert into #SourceDest select 19646765,19646774
insert into #SourceDest select 19646763,19646772
insert into #SourceDest select 19646762,19646771
insert into #SourceDest select 19646760,19646769
insert into #SourceDest select 19646764,19646773
insert into #SourceDest select 19646768,19646777
insert into #SourceDest select 17412684,17412708
insert into #SourceDest select 17412681,17412707
insert into #SourceDest select 17412680,17412706
insert into #SourceDest select 17412677,17412704
insert into #SourceDest select 17412676,17412703
insert into #SourceDest select 17412675,17412702
insert into #SourceDest select 17412678,17412705
insert into #SourceDest select 17412686,17412709
insert into #SourceDest select 17412700,17412728
insert into #SourceDest select 17412699,17412725
insert into #SourceDest select 17412691,17412711
insert into #SourceDest select 17412698,17412715
insert into #SourceDest select 17412695,17412713
insert into #SourceDest select 17412693,17412712
insert into #SourceDest select 17412689,17412710
insert into #SourceDest select 17412697,17412714
insert into #SourceDest select 17412701,17412730
insert into #SourceDest select 18679466,18679475
insert into #SourceDest select 18679465,18679474
insert into #SourceDest select 18679456,18679469
insert into #SourceDest select 18679464,18679477
insert into #SourceDest select 18679458,18679471
insert into #SourceDest select 18679457,18679470
insert into #SourceDest select 18679455,18679468
insert into #SourceDest select 18679459,18679472
insert into #SourceDest select 18679467,18679476
insert into #SourceDest select 14416679,14416689
insert into #SourceDest select 14416678,14416688
insert into #SourceDest select 14416672,14416683
insert into #SourceDest select 14416676,14416686
insert into #SourceDest select 14416677,14416687
insert into #SourceDest select 14416674,14416684
insert into #SourceDest select 14416673,14416682
insert into #SourceDest select 14416675,14416685
insert into #SourceDest select 14416681,14416691
insert into #SourceDest select 14416680,14416690
insert into #SourceDest select 16213788,16213793
insert into #SourceDest select 16213792,16213797
insert into #SourceDest select 16213790,16213795
insert into #SourceDest select 16213789,16213794
insert into #SourceDest select 16213791,16213796
insert into #SourceDest select 21412817,21412832
insert into #SourceDest select 21412818,21412833
insert into #SourceDest select 21412831,21412857
insert into #SourceDest select 21412829,21412835
insert into #SourceDest select 21412828,21412834
insert into #SourceDest select 21412830,21412856
insert into #SourceDest select 17412842,17412841
insert into #SourceDest select 17412829,17412828
insert into #SourceDest select 17412812,17412814
insert into #SourceDest select 17412808,17412809
insert into #SourceDest select 17412806,17412807
insert into #SourceDest select 17412803,17412804
insert into #SourceDest select 17412810,17412811
insert into #SourceDest select 17412844,17412843
insert into #SourceDest select 17942346,17942351
insert into #SourceDest select 17942350,17942362
insert into #SourceDest select 17942348,17942353
insert into #SourceDest select 17942347,17942352
insert into #SourceDest select 17942349,17942359
insert into #SourceDest select 17759017,17759029
insert into #SourceDest select 17759021,17759034
insert into #SourceDest select 17759019,17759032
insert into #SourceDest select 17759018,17759031
insert into #SourceDest select 17759020,17759033
insert into #SourceDest select 17759023,17759036
insert into #SourceDest select 17759027,17759042
insert into #SourceDest select 17759025,17759040
insert into #SourceDest select 17759024,17759039
insert into #SourceDest select 17759026,17759041
insert into #SourceDest select 17942320,17942330
insert into #SourceDest select 17942327,17942335
insert into #SourceDest select 17942324,17942333
insert into #SourceDest select 17942322,17942332
insert into #SourceDest select 17942325,17942334
insert into #SourceDest select 18241715,18241726
insert into #SourceDest select 18241723,18241738
insert into #SourceDest select 18241719,18241732
insert into #SourceDest select 18241717,18241728
insert into #SourceDest select 18241720,18241736
insert into #SourceDest select 16086188,16086198
insert into #SourceDest select 16086187,16086197
insert into #SourceDest select 16086181,16086191
insert into #SourceDest select 16086185,16086195
insert into #SourceDest select 16086186,16086196
insert into #SourceDest select 16086183,16086193
insert into #SourceDest select 16086182,16086192
insert into #SourceDest select 16086184,16086194
insert into #SourceDest select 16086190,16086200
insert into #SourceDest select 16086189,16086199
insert into #SourceDest select 21447335,21447353
insert into #SourceDest select 21447339,21447357
insert into #SourceDest select 21447337,21447355
insert into #SourceDest select 21447336,21447354
insert into #SourceDest select 21447338,21447356
insert into #SourceDest select 21447341,21447365
insert into #SourceDest select 21447345,21447369
insert into #SourceDest select 21447343,21447367
insert into #SourceDest select 21447342,21447366
insert into #SourceDest select 21447344,21447368
insert into #SourceDest select 21447347,21447359
insert into #SourceDest select 21447351,21447363
insert into #SourceDest select 21447349,21447361
insert into #SourceDest select 21447348,21447360
insert into #SourceDest select 20681044,20681053
insert into #SourceDest select 20681043,20681052
insert into #SourceDest select 20681038,20681047
insert into #SourceDest select 20681042,20681051
insert into #SourceDest select 20681040,20681049
insert into #SourceDest select 20681039,20681048
insert into #SourceDest select 20681037,20681046
insert into #SourceDest select 20681041,20681050
insert into #SourceDest select 20681045,20681054
insert into #SourceDest select 19374977,19374986
insert into #SourceDest select 19374976,19374985
insert into #SourceDest select 19374971,19374980
insert into #SourceDest select 19374975,19374984
insert into #SourceDest select 19374973,19374982
insert into #SourceDest select 19374972,19374981
insert into #SourceDest select 19374970,19374979
insert into #SourceDest select 19374974,19374983
insert into #SourceDest select 19374988,19374987
insert into #SourceDest select 17626866,17626871
insert into #SourceDest select 17626870,17626878
insert into #SourceDest select 17626868,17626873
insert into #SourceDest select 17626867,17626872
insert into #SourceDest select 17626869,17626874
insert into #SourceDest select 19374944,19374950
insert into #SourceDest select 19374955,19374954
insert into #SourceDest select 19374946,19374952
insert into #SourceDest select 19374945,19374951
insert into #SourceDest select 19374947,19374953
insert into #SourceDest select 19374895,19374896
insert into #SourceDest select 19374899,19374900
insert into #SourceDest select 19374898,19374897
insert into #SourceDest select 19374892,19374891
insert into #SourceDest select 19374893,19374894
insert into #SourceDest select 19374909,19374910
insert into #SourceDest select 19374911,19374912
insert into #SourceDest select 19374907,19374908
insert into #SourceDest select 19374904,19374903
insert into #SourceDest select 19374905,19374906
insert into #SourceDest select 19374957,19374963
insert into #SourceDest select 19374961,19374967
insert into #SourceDest select 19374959,19374965
insert into #SourceDest select 19374958,19374964
insert into #SourceDest select 19374960,19374966
insert into #SourceDest select 19374920,19374927
insert into #SourceDest select 19374919,19374926
insert into #SourceDest select 19374915,19374921
insert into #SourceDest select 19374917,19374924
insert into #SourceDest select 19374918,19374925
insert into #SourceDest select 19374933,19374939
insert into #SourceDest select 19374932,19374938
insert into #SourceDest select 19374928,19374934
insert into #SourceDest select 19374930,19374936
insert into #SourceDest select 19374931,19374937
insert into #SourceDest select 21153977,21153986
insert into #SourceDest select 21153976,21153985
insert into #SourceDest select 21153971,21153980
insert into #SourceDest select 21153975,21153984
insert into #SourceDest select 21153973,21153982
insert into #SourceDest select 21153972,21153981
insert into #SourceDest select 21153970,21153979
insert into #SourceDest select 21153974,21153983
insert into #SourceDest select 21153978,21153987
insert into #SourceDest select 17875202,17875217
insert into #SourceDest select 17875201,17875216
insert into #SourceDest select 17875194,17875208
insert into #SourceDest select 17875200,17875214
insert into #SourceDest select 17875198,17875212
insert into #SourceDest select 17875196,17875210
insert into #SourceDest select 17875193,17875206
insert into #SourceDest select 17875199,17875213
insert into #SourceDest select 17875204,17875219
insert into #SourceDest select 20883463,20883473
insert into #SourceDest select 20883462,20883472
insert into #SourceDest select 20883453,20883466
insert into #SourceDest select 20883458,20883471
insert into #SourceDest select 20883455,20883469
insert into #SourceDest select 20883454,20883467
insert into #SourceDest select 20883452,20883465
insert into #SourceDest select 20883457,20883470
insert into #SourceDest select 20883464,20883474
insert into #SourceDest select 20613686,20613695
insert into #SourceDest select 20613685,20613694
insert into #SourceDest select 20613676,20613689
insert into #SourceDest select 20613680,20613693
insert into #SourceDest select 20613678,20613691
insert into #SourceDest select 20613677,20613690
insert into #SourceDest select 20613675,20613688
insert into #SourceDest select 20613679,20613692
insert into #SourceDest select 20613687,20613696
insert into #SourceDest select 21020922,21020940
insert into #SourceDest select 21020931,21020952
insert into #SourceDest select 21020921,21020939
insert into #SourceDest select 21020930,21020951
insert into #SourceDest select 21020916,21020934
insert into #SourceDest select 21020925,21020943
insert into #SourceDest select 21020920,21020938
insert into #SourceDest select 21020929,21020950
insert into #SourceDest select 21020918,21020936
insert into #SourceDest select 21020927,21020948
insert into #SourceDest select 21020917,21020935
insert into #SourceDest select 21020926,21020947
insert into #SourceDest select 21020915,21020933
insert into #SourceDest select 21020924,21020942
insert into #SourceDest select 21020919,21020937
insert into #SourceDest select 21020928,21020949
insert into #SourceDest select 21020923,21020941
insert into #SourceDest select 21020932,21020962
insert into #SourceDest select 16086266,16086271
insert into #SourceDest select 16086270,16086275
insert into #SourceDest select 16086268,16086273
insert into #SourceDest select 16086267,16086272
insert into #SourceDest select 16086269,16086274
insert into #SourceDest select 18107204,18107245
insert into #SourceDest select 21482032,21482041
insert into #SourceDest select 18107203,18107239
insert into #SourceDest select 21482031,21482040
insert into #SourceDest select 18107188,18107232
insert into #SourceDest select 21482023,21482035
insert into #SourceDest select 18107202,18107238
insert into #SourceDest select 21482030,21482039
insert into #SourceDest select 18107190,18107234
insert into #SourceDest select 21482025,21482037
insert into #SourceDest select 18107189,18107233
insert into #SourceDest select 21482024,21482036
insert into #SourceDest select 18107168,18107231
insert into #SourceDest select 21482022,21482034
insert into #SourceDest select 18107191,18107237
insert into #SourceDest select 21482029,21482038
insert into #SourceDest select 18107205,18107248
insert into #SourceDest select 21482033,21482042
insert into #SourceDest select 18107212,18107260
insert into #SourceDest select 18107211,18107258
insert into #SourceDest select 18107210,18107256
insert into #SourceDest select 18107208,18107254
insert into #SourceDest select 18107207,18107251
insert into #SourceDest select 18107206,18107249
insert into #SourceDest select 18107209,18107255
insert into #SourceDest select 18107213,18107264
insert into #SourceDest select 18464721,18464738
insert into #SourceDest select 18464720,18464736
insert into #SourceDest select 18464715,18464730
insert into #SourceDest select 18464719,18464735
insert into #SourceDest select 18464717,18464732
insert into #SourceDest select 18464716,18464731
insert into #SourceDest select 18464713,18464727
insert into #SourceDest select 18464718,18464734
insert into #SourceDest select 18464722,18464739
insert into #SourceDest select 21223441,21223455
insert into #SourceDest select 21223442,21223753
insert into #SourceDest select 21223450,21223757
insert into #SourceDest select 21223444,21223755
insert into #SourceDest select 21223443,21223754
insert into #SourceDest select 21223445,21223756
insert into #SourceDest select 16456450,16456460
insert into #SourceDest select 16456449,16456459
insert into #SourceDest select 16456443,16456453
insert into #SourceDest select 16456447,16456457
insert into #SourceDest select 16456448,16456458
insert into #SourceDest select 16456445,16456455
insert into #SourceDest select 16456444,16456454
insert into #SourceDest select 16456446,16456456
insert into #SourceDest select 16456452,16456462
insert into #SourceDest select 16456451,16456461
insert into #SourceDest select 14384902,14384914
insert into #SourceDest select 16213582,16213592
insert into #SourceDest select 16213581,16213591
insert into #SourceDest select 16213578,16213588
insert into #SourceDest select 16213579,16213589
insert into #SourceDest select 16213580,16213590
insert into #SourceDest select 16213629,16213655
insert into #SourceDest select 16213628,16213654
insert into #SourceDest select 16213626,16213648
insert into #SourceDest select 16213625,16213647
insert into #SourceDest select 16213599,16213633
insert into #SourceDest select 16213627,16213653
insert into #SourceDest select 16213623,16213634
insert into #SourceDest select 16213624,16213638
insert into #SourceDest select 16213631,17189356
insert into #SourceDest select 16213630,16213656
insert into #SourceDest select 17530492,17530495
insert into #SourceDest select 15674964,15674971
insert into #SourceDest select 15674963,15674970
insert into #SourceDest select 15674960,15674965
insert into #SourceDest select 15674961,15674967
insert into #SourceDest select 15674962,15674969
insert into #SourceDest select 18141262,18141298
insert into #SourceDest select 18141261,18141297
insert into #SourceDest select 18141256,18141289
insert into #SourceDest select 18141260,18141296
insert into #SourceDest select 18141258,18141294
insert into #SourceDest select 18141257,18141290
insert into #SourceDest select 18141255,18141288
insert into #SourceDest select 18141259,18141295
insert into #SourceDest select 18141263,18141299
insert into #SourceDest select 18141282,18141306
insert into #SourceDest select 18141281,18141305
insert into #SourceDest select 18141268,18141304
insert into #SourceDest select 18141266,18141302
insert into #SourceDest select 18141265,18141301
insert into #SourceDest select 18141264,18141300
insert into #SourceDest select 18141267,18141303
insert into #SourceDest select 18141283,18141307
insert into #SourceDest select 17725748,17725754
insert into #SourceDest select 17725747,17725753
insert into #SourceDest select 17725742,17725749
insert into #SourceDest select 17725745,17725751
insert into #SourceDest select 17725746,17725752
insert into #SourceDest select 17725778,17725787
insert into #SourceDest select 17725777,17725786
insert into #SourceDest select 17725772,17725781
insert into #SourceDest select 17725776,17725785
insert into #SourceDest select 17725774,17725783
insert into #SourceDest select 17725773,17725782
insert into #SourceDest select 17725771,17725780
insert into #SourceDest select 17725775,17725784
insert into #SourceDest select 17725779,17725788
insert into #SourceDest select 15561622,15561632
insert into #SourceDest select 14384800,14384826
insert into #SourceDest select 14384797,14384815
insert into #SourceDest select 14384798,14384816
insert into #SourceDest select 14384799,14384817
insert into #SourceDest select 14384871,14384875
insert into #SourceDest select 14384868,14384872
insert into #SourceDest select 14384869,14384873
insert into #SourceDest select 14384870,14384874
insert into #SourceDest select 17758997,17759005
insert into #SourceDest select 17758996,17759004
insert into #SourceDest select 17758995,17759003
insert into #SourceDest select 17758991,17758999
insert into #SourceDest select 17758992,17759000
insert into #SourceDest select 17758993,17759001
insert into #SourceDest select 17758994,17759002
insert into #SourceDest select 17758998,17759006
insert into #SourceDest select 14649152,14649172
insert into #SourceDest select 14649151,14649171
insert into #SourceDest select 14649142,14649169
insert into #SourceDest select 14649141,14649168
insert into #SourceDest select 14649138,14649158
insert into #SourceDest select 14649150,14649170
insert into #SourceDest select 14649139,14649159
insert into #SourceDest select 14649140,14649162
insert into #SourceDest select 14649154,14649174
insert into #SourceDest select 14649153,14649173
insert into #SourceDest select 21223409,21223418
insert into #SourceDest select 21223408,21223417
insert into #SourceDest select 21223403,21223412
insert into #SourceDest select 21223407,21223416
insert into #SourceDest select 21223405,21223414
insert into #SourceDest select 21223404,21223413
insert into #SourceDest select 21223402,21223411
insert into #SourceDest select 21223406,21223415
insert into #SourceDest select 21223410,21223419
insert into #SourceDest select 17659910,17659940
insert into #SourceDest select 17659897,17659942
insert into #SourceDest select 17659896,17659939
insert into #SourceDest select 17659895,17659938
insert into #SourceDest select 17659891,17659912
insert into #SourceDest select 17659892,17659913
insert into #SourceDest select 17659893,17659914
insert into #SourceDest select 17659894,17659915
insert into #SourceDest select 17659911,17659941
insert into #SourceDest select 18797250,18797267
insert into #SourceDest select 18797249,18797266
insert into #SourceDest select 18797245,18797262
insert into #SourceDest select 18797247,18797264
insert into #SourceDest select 18797248,18797265
insert into #SourceDest select 18797256,18797273
insert into #SourceDest select 18797255,18797272
insert into #SourceDest select 18797251,18797268
insert into #SourceDest select 18797253,18797270
insert into #SourceDest select 18797254,18797271
insert into #SourceDest select 18797261,18797281
insert into #SourceDest select 18797260,18797279
insert into #SourceDest select 18797257,18797275
insert into #SourceDest select 18797258,18797276
insert into #SourceDest select 18797259,18797277
insert into #SourceDest select 17156064,17156065
insert into #SourceDest select 21188708,21188714
insert into #SourceDest select 21188713,21188732
insert into #SourceDest select 21188712,21188718
insert into #SourceDest select 21188709,21188715
insert into #SourceDest select 21188710,21188716
insert into #SourceDest select 21188711,21188717
insert into #SourceDest select 14416641,14416665
insert into #SourceDest select 14416639,14416663
insert into #SourceDest select 14416645,14416668
insert into #SourceDest select 14416643,14416667
insert into #SourceDest select 16213802,16213812
insert into #SourceDest select 16213799,16213803
insert into #SourceDest select 16213800,16213809
insert into #SourceDest select 16213801,16213810
insert into #SourceDest select 16119382,16119388
insert into #SourceDest select 21276719,21276729
insert into #SourceDest select 21276718,21276728
insert into #SourceDest select 21276717,21276726
insert into #SourceDest select 21276716,21276725
insert into #SourceDest select 21276712,21276721
insert into #SourceDest select 21276713,21276722
insert into #SourceDest select 21276714,21276723
insert into #SourceDest select 21276715,21276724
insert into #SourceDest select 21276720,21276730
insert into #SourceDest select 18929797,18929802
insert into #SourceDest select 18929796,18929801
insert into #SourceDest select 18929793,18929798
insert into #SourceDest select 18929794,18929799
insert into #SourceDest select 18929795,18929800
insert into #SourceDest select 17088757,17088763
insert into #SourceDest select 17088756,17088762
insert into #SourceDest select 17088751,17088758
insert into #SourceDest select 17088753,17088760
insert into #SourceDest select 17088754,17088761
insert into #SourceDest select 17088744,17088750
insert into #SourceDest select 17088743,17088749
insert into #SourceDest select 20613729,20613733
insert into #SourceDest select 17088739,17088745
insert into #SourceDest select 20613726,20613730
insert into #SourceDest select 17088741,17088747
insert into #SourceDest select 17088742,17088748
insert into #SourceDest select 20613728,20613732
insert into #SourceDest select 17088716,17088727
insert into #SourceDest select 17088714,17088725
insert into #SourceDest select 17088706,17088718
insert into #SourceDest select 17088710,17088721
insert into #SourceDest select 17088712,17088723
insert into #SourceDest select 15406712,15406719
insert into #SourceDest select 15406709,15406713
insert into #SourceDest select 15406710,15406717
insert into #SourceDest select 15406711,15406718
insert into #SourceDest select 15406701,15406705
insert into #SourceDest select 15406698,15406702
insert into #SourceDest select 15406699,15406703
insert into #SourceDest select 15406700,15406704
insert into #SourceDest select 15561597,15561603
insert into #SourceDest select 15561593,15561600
insert into #SourceDest select 15561594,15561601
insert into #SourceDest select 15561595,15561602
insert into #SourceDest select 17725736,17725741
insert into #SourceDest select 17725735,17725740
insert into #SourceDest select 17725728,17725737
insert into #SourceDest select 17725733,17725738
insert into #SourceDest select 17725734,17725739
insert into #SourceDest select 19409010,19409016
insert into #SourceDest select 19408997,19409014
insert into #SourceDest select 19408994,19409011
insert into #SourceDest select 19408995,19409012
insert into #SourceDest select 19408996,19409013
insert into #SourceDest select 19409025,19409043
insert into #SourceDest select 19409024,19409030
insert into #SourceDest select 19409021,19409026
insert into #SourceDest select 19409022,19409027
insert into #SourceDest select 19409023,19409029
insert into #SourceDest select 18831260,18831291
insert into #SourceDest select 18831259,18831280
insert into #SourceDest select 18831258,18831279
insert into #SourceDest select 18831257,18831278
insert into #SourceDest select 18831252,18831274
insert into #SourceDest select 18831253,18831275
insert into #SourceDest select 18831254,18831276
insert into #SourceDest select 18831256,18831277
insert into #SourceDest select 18831261,18831292
insert into #SourceDest select 17908961,17908970
insert into #SourceDest select 17908960,17908969
insert into #SourceDest select 17908959,17908968
insert into #SourceDest select 17908958,17908967
insert into #SourceDest select 17908954,17908963
insert into #SourceDest select 17908955,17908964
insert into #SourceDest select 17908956,17908965
insert into #SourceDest select 17908957,17908966
insert into #SourceDest select 17908962,17908971
insert into #SourceDest select 21276707,21276710
insert into #SourceDest select 19646782,19646785
insert into #SourceDest select 20613724,20613725
insert into #SourceDest select 20411782,20411785
insert into #SourceDest select 20411783,20411786
insert into #SourceDest select 20411775,20411778
insert into #SourceDest select 20411781,20411779
insert into #SourceDest select 20613713,20613714



go

CREATE TABLE #CGTExtractIn
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null)


	CREATE TABLE #CGTNewTransactions
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Flow_No			int		null,
		Transaction_No		numeric(18,0)		null,
		Transaction_Type	char(1)		null,
		Instrument_No		int		null,
		Date_Action		datetime	null,
		Flow_Type		varchar(1)	null,
		Amount			money		null,
		Rate			numeric(14,6)	null,
		Units			numeric(18,4)	null)

	CREATE TABLE #CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)

	CREATE TABLE CGTExtractDetail
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Transaction_No		numeric(18,0)		null,
		Instrument_No		int		null,
		Date			datetime	null,
		Rate			numeric(14,6)	null,
		WAC			numeric(14,6)	null,
		Cumulative_Gain		money		null,
		Gain			money		null,
		Cumulative_Units	numeric(18,4)	null,
		Units			numeric(18,4)	null,
		Amount			money		null,
		WAC_Rollover		smallint	null
	)


	CREATE TABLE #CGTExtractSummary
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Instrument_No		int		null,
		Units_Disposed		numeric(18,4)	null,
		Aggregate_Unit_Cost	numeric(14,6)	null,
		Gross_Proceeds		money		null,
		Net_Gain		money		null,
		Units_Held		numeric(18,4)	null,
		WAC			numeric(14,6)	null
	)


	CREATE TABLE #CGTLossCarryOver
	(	Deal_Xref		int		null,
		Start_Date		datetime	null,
		End_Date		datetime	null,
		Tax_Year		int		null,
		Gain			money		null)

	CREATE TABLE #RE_Sell
	(Instrument_Number   int                null,
	 Rate_Type           char(1)            null,
	 Date                datetime           null,
	 Rate_Date           datetime           null,
	 Buy                 numeric(14,6)      null,
	 Sell                numeric(14,6)      null,
	 Basic               numeric(14,6)      null,
	 Adjusted            numeric(14,6)      null,
	 TimeStamp           varbinary(8)       null)




declare @Start_Date datetime,@End_Date datetime, @Tax_Year int


select @Start_Date = Begin_Date, @End_Date = End_Date, @Tax_Year = Tax_Year
from TaxProcessDates


insert into #CGTExtractIn
select distinct t.Deal_Xref, @Start_Date, Date_Action
from #SourceDest sd, Transactions t
where sd.SourceFlow = t.Flow_No



select *
into #CGTExtractIn_BKP
from #CGTExtractIn

truncate table #CGTExtractIn



while exists (select 1 from #CGTExtractIn_BKP)
begin

set rowcount 1
insert into #CGTExtractIn
select * from #CGTExtractIn_BKP
set rowcount 0

exec CGTExtract


delete #CGTExtractIn_BKP
from #CGTExtractIn_BKP c1, #CGTExtractIn c2
where c1.Deal_Xref = c2.Deal_Xref


insert into CGTExtractDetail
select * from #CGTExtractDetail

truncate table #CGTExtractIn

truncate table #CGTLossCarryOver
truncate table #CGTExtractSummary
truncate table #CGTExtractDetail
truncate table #CGTNewTransactions

end



INSERT INTO #CGTExtractDetail
SELECT * FROM CGTExtractDetail



select NWAC = convert(float,null), *
into #Flows
from #SourceDest


update #Flows
   set NWAC = c.WAC
from #CGTExtractDetail c, Transactions t, #Flows sd
where c.Transaction_No = t.Transaction_No
and t.Flow_No = sd.SourceFlow


select distinct 'delete CGTRollover where Flow_Number =  ' + convert(char(15), SourceFlow) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #Flows

select distinct 'delete CGTRollover where Flow_Number =  ' + convert(char(15), DestFlow) -- + ',null,   ' + convert(char(15),convert(money, WAC))
from #Flows



select distinct 'insert into CGTRollover select ' + convert(char(15), SourceFlow) + ',null,   ' + convert(char(15),convert(money, NWAC))
from #Flows
where NWAC is not null

select distinct 'insert into CGTRollover select ' + convert(char(15), DestFlow) + ',null,   ' + convert(char(15),convert(money, NWAC))
from #Flows
where NWAC is not null


GO
