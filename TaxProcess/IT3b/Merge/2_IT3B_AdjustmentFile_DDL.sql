IF OBJECT_ID('WORKSPACE..IT3B_AdjustmentFile') IS NOT NULL DROP TABLE WORKSPACE..IT3B_AdjustmentFile
GO

CREATE TABLE WORKSPACE..IT3B_AdjustmentFile
(
    Portfolio_Code          Varchar(10)     NULL,
    Deal_Xref               INT             NULL,
    Owner               	INT             NULL,
    Contract_Number         INT             NULL, 
    Deal_Number             INT             NULL,
    Product                 VARCHAR(6)      NULL,
    Product_Desc            VARCHAR(50)     NULL,
    March_Credits           NUMERIC(20,8)   NULL,
    Apr_Credits             NUMERIC(20,8)   NULL,
    May_Credits             NUMERIC(20,8)   NULL,
    June_Credits            NUMERIC(20,8)   NULL,
    Jul_Credits             NUMERIC(20,8)   NULL,
    Aug_Credits             NUMERIC(20,8)   NULL,
    Sep_Credits             NUMERIC(20,8)   NULL,
    Oct_Credits             NUMERIC(20,8)   NULL,
    Nov_Credits             NUMERIC(20,8)   NULL,
    Dec_Credits             NUMERIC(20,8)   NULL,
    Jan_Credits             NUMERIC(20,8)   NULL,
    Feb_Credits             NUMERIC(20,8)   NULL,
    March_Debits            NUMERIC(20,8)   NULL,
    Apr_Debits              NUMERIC(20,8)   NULL,
    May_Debits              NUMERIC(20,8)   NULL,
    June_Debits             NUMERIC(20,8)   NULL,
    Jul_Debits              NUMERIC(20,8)   NULL,
    Aug_Debits              NUMERIC(20,8)   NULL,
    Sep_Debits              NUMERIC(20,8)   NULL,
    Oct_Debits              NUMERIC(20,8)   NULL,
    Nov_Debits              NUMERIC(20,8)   NULL,
    Dec_Debits              NUMERIC(20,8)   NULL,
    Jan_Debits              NUMERIC(20,8)   NULL,
    Feb_Debits              NUMERIC(20,8)   NULL,
    Tot_Exp_Incurred        NUMERIC(20,8)   NULL,
    Tot_Inc_Accured         NUMERIC(20,8)   NULL,
    Opening_Balance         NUMERIC(20,8)   NULL,
    Acc_Start_Date          DATETIME        NULL,
    Closing_Balance         NUMERIC(20,8)   NULL,
    Acc_Close_Date          DATETIME        NULL,
    Foreign_Tax_Paid        NUMERIC(20,8)   NULL
)
GO

