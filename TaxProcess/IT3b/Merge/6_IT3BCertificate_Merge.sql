/**************************************************************************
!  Procedure    : IT3BCertificate_Merge
!  Description  : Merges Existing Taxcertificate tables with that of
!                 data (XL file) received from Ashburton
! ------------------------------------------------------------------------
! Modifications :
! Programmer    Date        Changes
! ________________________________________________________________________
! Kesavan       10 Mar 2014 Creation
!*************************************************************************/

USE IMS_MAIN
GO

IF EXISTS(SELECT 1 FROM dbo.sysobjects WHERE name='IT3BCertificate_Merge' AND type='P')
BEGIN
    DROP PROC dbo.IT3BCertificate_Merge
    PRINT '<<<<<<<<<<<<<<<<<<<IT3BCertificate_Merge DROPPED>>>>>>>>>>>>>>>>>>>>>>>'
END
GO

IF OBJECT_ID('#TaxCertTotal') IS NOT NULL DROP TABLE #TaxCertTotal 
IF OBJECT_ID('#InsertHeader') IS NOT NULL DROP TABLE #InsertHeader
IF OBJECT_ID('#ProdKey') IS NOT NULL DROP TABLE #ProdKey
GO


CREATE PROCEDURE IT3BCertificate_Merge
(	
    @StartDate 	DATETIME, 
	@EndDate 	DATETIME, 
	@TaxYear 	INT,
    @Product    Varchar(5) = 'RMBSP'
)
AS
BEGIN

    IF OBJECT_ID('#InsertHeader') IS NOT NULL DROP TABLE #InsertHeader

    SELECT  
            Certificate_Key = Identity(9), 
            Owner, 
            Contract_Number, 
            Product , 
            Year         = @TaxYear, 
            Start_Date   = @StartDate, 
            End_Date     = @EndDate
    INTO    #InsertHeader 
    FROM    WORKSPACE..IT3B_AdjustmentFile

    CREATE INDEX IH1 ON #InsertHeader(Owner, Product, Contract_Number, Year) 
    CREATE INDEX IH2 ON #InsertHeader(Product) 

    IF OBJECT_ID('#ProdKey') IS NOT NULL DROP TABLE #ProdKey

    SELECT      Product, 
                Certificate_Key = min(Certificate_Key) - 1 , 
                ExistingMax     = convert(int, null)  
    INTO        #ProdKey 
    FROM        #InsertHeader 
    GROUP BY    Product

    UPDATE  #ProdKey 
    SET     ExistingMax = isnull( (SELECT Max(Certificate_Number)  
    FROM 	TaxCertificateHeader  
    WHERE 	Year 	= @TaxYear 
    AND     Product = P.Product),0) 
    FROM	#ProdKey P 

    -- Insert Adjustment IT3B data into Header table
    INSERT INTO TaxCertificateHeader
    (   
            Owner, 
            Product, 
            Contract_Number, 
            Year, 
            Certificate_Number, 
            Start_Date, 
            End_Date, 
            Adhoc
    ) 
    SELECT	Owner, 
            Product, 
            Contract_Number, 
            Year, 
            Certificate_Number = Certificate_Key + (SELECT ExistingMax - Certificate_Key FROM #ProdKey PK where IH.Product = PK.Product), 
            DateAdd(dd,1,Start_Date), -- Start_Date
            End_Date, 
            -1
    FROM	#InsertHeader IH 
    ORDER BY 
            Product -- Order is impotant for correct Cert# generation 

    IF @@ERROR <> 0
    BEGIN
        SELECT 'ERROR Occured while Inserting TaxCertificateHeader'
        GOTO END_RUN
    END
    ELSE
        SELECT 'Inserting TaxCertificateHeader done'

    -- Insert Adjustment IT3B data into Detail table
    INSERT	TaxCertificateDetail  -- Rel 20 
    (
            Certificate_Key,
            Instrument_Class,
            Interest,
            Dividend,
            NonTaxDividends,
            TaxForDividends,
            WithholdingTax,
            Taxable_For_Int,
            Withholding_Tax_For_Int,
            Loc_Div_ToBeTaxed,
            Loc_Div_TaxFree,
            For_Div_TBT_SAWHT_LT,
            For_Div_TBT_SAWHT_GT,
            WHT_TBT_ForDivSA_LT,
            WHT_TBT_ForDivSA_GT,
            TotalMarketValue
    )
    SELECT      T.Certificate_Key, 
                'MM',                   
                Interest                = ROUND(W.Tot_Inc_Accured,2), 
                Dividend                = 0.0, 
                NonTaxDividends         = 0.0, 
                TaxForDividends         = 0.0, 
                WithholdingTax          = 0.0, 
                Taxable_For_Int         = 0.0, 
                Withholding_Tax_For_Int = 0.0,
                Loc_Div_ToBeTaxed       = 0.0, 
                Loc_Div_TaxFree         = 0.0, 
                For_Div_TBT_SAWHT_LT    = 0.0, 
                For_Div_TBT_SAWHT_GT    = 0.0, 
                WHT_TBT_ForDivSA_LT     = 0.0, 
                WHT_TBT_ForDivSA_GT     = 0.0,
                TotalMarketValue        = ROUND(W.Closing_Balance,2)
    FROM        WORKSPACE..IT3B_AdjustmentFile  W
    INNER JOIN  TaxCertificateHeader 	T
    ON          T.Owner             = W.Owner 
    AND         T.Product           = W.Product 
    AND         T.Contract_Number 	= W.Contract_Number 
    AND         T.Adhoc             = -1
    WHERE       T.Year              = @TaxYear 
    AND         T.Start_Date        >= @StartDate

    IF @@ERROR <> 0
    BEGIN
        SELECT 'ERROR Occured while Inserting TaxCertificateDetail'
        GOTO END_RUN
    END
    ELSE
        SELECT 'Inserting TaxCertificateDetail done'

    -- Insert Adjustment IT3B data into TaxCertificateDealXrefs table
    INSERT INTO TaxCertificateDealXrefs 
    (
        Certificate_Key, 
        Deal_Xref
    )
    SELECT DISTINCT
                Certificate_Key,
                Deal_Xref
    FROM        WORKSPACE..IT3B_AdjustmentFile W
    INNER JOIN  TaxCertificateHeader T
    ON          W.Owner             = T.Owner
    AND         W.Contract_Number   = T.Contract_Number
    AND         W.Product           = T.Product
    AND         T.Adhoc             = -1
    AND         T.Year = @TaxYear 
    AND         T.Start_Date >= @StartDate
    WHERE       Convert(Varchar,Certificate_Key) + Convert(Varchar,Deal_Xref) NOT IN
                (
                SELECT  Convert(Varchar,Certificate_Key) + Convert(Varchar,Deal_Xref)
                FROM    TaxCertificateDealXrefs TD
                )

    IF @@ERROR <> 0
    BEGIN
        SELECT 'ERROR Occured while Inserting TaxCertificateDealXrefs'
        GOTO END_RUN
    END
    ELSE
        SELECT 'Inserting TaxCertificateDealXrefs done'

    -- Insert Adjustment IT3B data into TaxCertificateBroker table
    INSERT	TaxCertificateBroker
    (  
            Certificate_Key,
            Deal_Xref,
            Broker_No,
            Broker,
            Brokerage_No,
            Brokerage
    )
    SELECT      T.Certificate_Key, 
                W.Deal_Xref,
                Broker_Number = DP.People_Number,
                Broker = ISNULL(PB.Initials + ' ' + PB.Name,' '),
                Corporate_Number = BC.Corporate_Number,
                Brokerage = ISNULL(PC.Initials + ' ' + PC.Name,' ')
    FROM        WORKSPACE..IT3B_AdjustmentFile	W
    INNER JOIN  TaxCertificateHeader T
    ON          W.Owner             = T.Owner
    AND         W.Contract_Number   = T.Contract_Number
    AND         W.Product           = T.Product
    AND         T.Adhoc             = -1
    AND         T.Year = @TaxYear 
    AND         T.Start_Date >= @StartDate
    INNER JOIN  DealParticipants DP
    ON          DP.Deal_Xref = W.Deal_Xref
    AND         DP.Role_Id = 2 
    INNER JOIN  BrokerCorporate BC
    ON          DP.People_Number = BC.Broker_Number
    AND         BC.End_Date IS NULL
    INNER JOIN  People PB -- Broker Join
    ON          PB.People_Number = DP.People_Number
    AND         PB.Language = 'E'
    INNER JOIN  People PC -- Broker Corporate Join
    ON          PC.People_Number = BC.Corporate_Number
    AND         PC.Language = 'E'
    WHERE       Convert(Varchar,T.Certificate_Key) + Convert(Varchar,W.Deal_Xref) +  Convert(Varchar,DP.People_Number) + Convert(Varchar,BC.Corporate_Number)
    NOT IN      (
                SELECT Convert(Varchar,Certificate_Key) + Convert(Varchar,Deal_Xref) +  Convert(Varchar,Broker_No) + Convert(Varchar,Brokerage_No)
                FROM TaxCertificateBroker
                )

    IF @@ERROR <> 0
    BEGIN
        SELECT 'ERROR Occured while Inserting TaxCertificateBroker'
        GOTO END_RUN
    END
    ELSE
        SELECT 'Inserting TaxCertificateBroker done'


    END_RUN:
END
GO

IF OBJECT_ID('IT3BCertificate_Merge') <> NULL
BEGIN
    PRINT '<<<<<<<<<<<<<<<<<<<IT3BCertificate_Merge CREATED>>>>>>>>>>>>>>>>>>>>>>>'
END
