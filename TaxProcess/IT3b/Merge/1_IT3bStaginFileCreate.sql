
if object_id('WORKSPACE..IT3bStatingFile') is not null
drop table WORKSPACE..IT3bStatingFile
GO
CREATE TABLE WORKSPACE..IT3bStatingFile (ClientCode            varchar(50),
								         ProductDescription    varchar(70),
										 MarchCredits  	       float, 
										 AprilCredits  	       float, 
										 MayCredits    	       float, 
										 JuneCredits   	       float, 
										 JulyCredits   	       float, 
										 AugustCredits 	       float, 
										 SeptemberCredits      float, 
										 OctoberCredits        float, 
										 NovemberCredits       float, 
										 DecemberCredits       float, 
										 JanuaryCredits        float, 
										 FebruaryCredits       float, 
										 MarchDebits           float, 
										 AprilDebits           float, 
										 MayDebits             float, 
										 JuneDebits            float, 
										 JulyDebits            float, 
										 AugustDebits          float, 
										 SeptemberDebits       float, 
										 OctoberDebits         float, 
										 NovemberDebits        float, 
										 DecemberDebits        float, 
										 JanuaryDebits         float, 
										 FebruaryDebits        float, 
										 TotalExpenseIncurred  float, 
										 TotalIncomeAccrued    float, 
										 OpeningBalance        float,
										 AccountStartDate      varchar(20), 
										 ClosingBalance        float,
										 AccountClosingDate    varchar(20),
										 ForeignTaxPaid        float)




GO