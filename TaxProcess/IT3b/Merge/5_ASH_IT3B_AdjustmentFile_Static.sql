USE IMS_MAIN
GO
DELETE FROM WORKSPACE..IT3B_AdjustmentFile WHERE Product_Desc = 'Cash'
GO

UPDATE      WORKSPACE..IT3B_AdjustmentFile
SET         Deal_Xref       =   T2.Deal_Xref,
            Owner           =   T3.Owner,
            Contract_Number =   T3.Contract_Number,
            Product         =   T3.Product
FROM        WORKSPACE..IT3B_AdjustmentFile T1
INNER JOIN  DealNote T2
ON          RTRIM(LTRIM(T1.Portfolio_Code)) = RTRIM(LTRIM(T2.External_Logical_Note_Id))
INNER JOIN  ContractDeal T3
ON          T2.Deal_Xref = T3.Deal_Xref
GO
