USE IMS_MAIN
GO


DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates


UPDATE TaxCertificateHeader SET Start_Date = @Start_Date WHERE Year = @Tax_Year AND Product ='RMBSP' AND Adhoc = -1
GO
