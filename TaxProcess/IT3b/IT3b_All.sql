----- IT3b_All.sql
IF OBJECT_ID('#WHT_Instrument') IS NOT NULL DROP TABLE #WHT_Instrument
IF OBJECT_ID('#WHT_Values') IS NOT NULL DROP TABLE #WHT_Values
IF OBJECT_ID('#LWHTInstrument') IS NOT NULL DROP TABLE #LWHTInstrument
IF OBJECT_ID('#LWHI_Values') IS NOT NULL DROP TABLE #LWHI_Values
IF OBJECT_ID('WORKSPACE..SourceDeals') IS NOT NULL DROP TABLE WORKSPACE..SourceDeals
IF OBJECT_ID('#OwnCnPrd') IS NOT NULL DROP TABLE #OwnCnPrd
IF OBJECT_ID('#TotalMakertValue') IS NOT NULL DROP TABLE #TotalMakertValue
GO

IF OBJECT_ID('IT3b_All') IS NOT NULL
BEGIN
	DROP PROC IT3b_All
	IF OBJECT_ID('IT3b_All') IS NOT NULL
		PRINT '<<<   Error dropping procedure IT3b_All   >>>'
	ELSE
		PRINT '<<<   Successfully dropped procedure IT3b_All   >>>'
END ELSE BEGIN
	PRINT '<<<   Procedure IT3b_All does not exist..   >>>'
END
GO


CREATE PROCEDURE dbo.IT3b_All
( 
    @Start_Date     datetime,  
    @End_Date       datetime, 
    @Owner          int = null, 
    @Product_Code	varchar(5) = null
) 
as                                         
begin 
    /**************************************************************************
    !  Procedure    : IT3b_All 
    !  Description  : Selects Interest Certificate Details for Unit Trusts, Money 
    !                 Market, Guarantee and inserts into Tax- tables
    !  Tables       : ContractDeal
    !                 Deal
    ! ------------------------------------------------------------------------
    ! Modifications :
    ! Programmer    Date        Changes
    ! ________________________________________________________________________
    ! Suiko Alston	13/03	     Used to run batch of it3bs - optimised
    ! Mario Andron	06/02/2003   Added Foreign Interest fields
    !			     Changed Certificate Key Structure 
    !			     Changed Certificate number generation logic
    !			     Changed logic to read from flows to transactions
    !				(Faster!)
    !			     Changed Grouping logic to contract level
    ! Mario Andron  25/03/2003   Changed to create adhoc certificates aswell
    ! Percy Letwaba 29/08/2004   Include Auto Interest Addition Flow_Type
    ! Kesavan       Jan 2013     Rel 24 : Foreign <= Bucket
    ! Kesavan       06/Feb/2015  Rel 26 : REIT + LWHI Changes
    ! Kesavan       22/Jan/2016  SD-34172 : To allow regeneration of Adhoc certificates
    ! Kesavan       10/Feb2016   SWAP-2440 : in specie offset
    !*************************************************************************/
 
--**	--------------------------------------------------- 
	exec DBA_ProcedureLog 'IT3b_All' 
--**	--------------------------------------------------- 
    SET NOCOUNT ON
/*****************************************************************************/ 
-- Force the Start and End date into the same tax year 
/*****************************************************************************/ 
 
DECLARE	@Tax_Year	int 
 
SELECT	@Tax_Year	= min(Tax_Year), 
	@Start_Date	= min(Begin_Date) 
FROM	TaxYearBeginDates 
WHERE	@End_Date	<= End_Date 
 
/*****************************************************************************/ 
-- Fetch relevant deals 
/*****************************************************************************/ 
 
CREATE TABLE #SourceDeals	 
(	Deal_Xref	int		null, 
	Owner		int		null, 
	Product_Code	varchar(5)	null, 
	Contract_Number	int		null, 
	Adhoc		smallint	null) 
 
IF @Owner = null 
begin


  INSERT #SourceDeals	 
  SELECT distinct  
         CD.Deal_Xref, 
         CD.Owner, 
         PG.Product_Code, 
	 Contract_Number, 
	 Adhoc = 0 
  FROM   ContractDeal CD, 
         ProductGroup PG, 
         Deal D 
  WHERE  CD.Product    = PG.Product_Internal 
  AND    PG.Process    = 'AX'                     
  AND    CD.Deal_Xref  = D.Deal_Xref 
  AND    D.Status      NOT IN ('Q', 'A', 'C', 'Z') 
  AND	 NOT EXISTS  
  (	 SELECT 1 
	 FROM	TaxCertificateHeader C 
	 WHERE	C.Owner 					= CD.Owner 
	 AND    C.Product 					= PG.Product_Code 
	 AND	C.Year						= @Tax_Year) 

end
ELSE 
  INSERT #SourceDeals	 
  SELECT distinct  
         CD.Deal_Xref, 
         CD.Owner, 
         PG.Product_Code, 
	 Contract_Number, 
	 Adhoc = 1 
  FROM   ContractDeal CD, 
         ProductGroup PG, 
         Deal D 
  WHERE  CD.Product    = PG.Product_Internal 
  AND    PG.Process    = 'AX'                     
  AND    CD.Deal_Xref  = D.Deal_Xref 
  AND    D.Status      NOT IN ('Q', 'A', 'C', 'Z') 
  AND	 CD.Owner	= @Owner 
  AND 	 PG.Product_Code= @Product_Code 
  AND	 NOT EXISTS  
  (	 SELECT 1 
	 FROM	TaxCertificateHeader C 
	 WHERE	C.Owner 					= CD.Owner 
	 AND    C.Product 					= PG.Product_Code 
	 AND	C.Year						= @Tax_Year
     AND    C.Adhoc = 0) 

create clustered index perfSD1 on #SourceDeals(Deal_Xref) 
 
 
declare @PrevEndDate datetime 
 
 
select @PrevEndDate = End_Date 
from TaxYearBeginDates 
where Tax_Year = @Tax_Year - 1       
 
 
create table #DealStatuses (Deal_Xref int, PreviousStatus char null, Status char null, PreviousDate datetime null, Date Datetime null) 
 
insert #DealStatuses (Deal_Xref) 
select distinct Deal_Xref 
from #SourceDeals	 
 
exec GetDealStatuses @PrevEndDate  , @End_Date 
 
delete #SourceDeals	 
from	#SourceDeals	D, 
	#DealStatuses DS 
where D.Deal_Xref = DS.Deal_Xref 
and   DS.PreviousStatus = DS.Status 
and   DS.Status in ('X','T') 
 
 
/*** Unit Trusts Interest Certificate Details ***/ 
SELECT 	T.Deal_Xref, 
       	T.Flow_No, 
       	Start_Date = T.Date_Action, 
        H.Dist_Date,
       	T.Instrument_No, 
       	convert(money,T.Amount) Amount, 
        Units = convert(money,null),
       	SD.Owner, 
       	SD.Product_Code, 
    	SD.Contract_Number, 
       	NonTaxDividends 	= convert(money, null), 
       	TaxForDividends 	= convert(money, null), 
       	WithholdingTax 		= convert(money, null), 
       	Taxable_For_Int 	= convert(money, null), 
       	Withholding_Tax_For_Int = convert(money, null), 
       	Interest 		= convert(money, null), 
       	Instrument_Class 	= 'UT', 
        Adhoc,
        Loc_Div_ToBeTaxed       = Convert(Money,NULL), -- Rel 24
        Loc_Div_TaxFree         = Convert(Money,NULL), -- Rel 24
        For_Div_TBT_SAWHT_LT    = Convert(Money,NULL), -- Rel 24
        For_Div_TBT_SAWHT_GT    = Convert(Money,NULL), -- Rel 24
        WHT_TBT_ForDivSA_LT     = Convert(Money,NULL), -- Rel 24
        WHT_TBT_ForDivSA_GT     = Convert(Money,NULL), -- Rel 24
        Already_Taxed_Loc_Div_InSpecie  = Convert(Money,NULL) -- Rel 24
INTO   	#TaxCertUT 
FROM   	#SourceDeals            SD, 
	Transactions		T ,
    InstrumentDistributionHistor H
WHERE  	SD.Deal_Xref         	= T.Deal_Xref 
AND    	T.Flow_Type        	= 'D' 
AND    	T.Amount     		> 0 
AND     T.Date_Action = H.Reinvest_Date 
AND     T.Instrument_No  = H.Instrument_Number
AND    	H.Dist_Date		BETWEEN @Start_Date AND @End_Date  
AND    	T.Transaction_Type	= 'D' 

select distinct Owner
into #Owner from #TaxCertUT 

/* Adaptive Server has expanded all '*' elements in the following statement */ select #Owner.Owner into #cursor from #Owner where 1=2

CREATE INDEX TaxCertUT_Ndx1 ON #TaxCertUT (Deal_Xref)
CREATE INDEX TaxCertUT_Ndx2 ON #TaxCertUT (Deal_Xref, Dist_Date)
CREATE INDEX TaxCertUT_Ndx3 ON #TaxCertUT (Owner)

while exists (select 1 from #Owner)
begin

        set rowcount 100/* Adaptive Server has expanded all '*' elements in the following statement */ 
        insert into #cursor select #Owner.Owner from #Owner
        set rowcount 0

        update #TaxCertUT 
        set Units = di.Balance_Units
        from #cursor c, #TaxCertUT t, DistributionInfoHeader dh, DistributionInformation di
        where dh.Deal_Xref = t.Deal_Xref
        and dh.Dist_Info_No = di.Dist_Info_No
        and t.Dist_Date = dh.Dist_Date
        and dh.Instrument_Number = t.Instrument_No
        and di.Reinvest_Date = t.Start_Date
        and c.Owner = t.Owner

        delete #Owner
        from #Owner o1, #cursor c
        where o1.Owner = c.Owner

        truncate table #cursor

        select count(*) ,' Records left for updates of Units ', getdate() from #Owner

end

create table   #CreateBalances 
(Deal_Xref  int      null, 
 Date       datetime null) 
 
INSERT #CreateBalances 
SELECT DISTINCT Deal_Xref, Dist_Date 
FROM #TaxCertUT  
where Units is null
  
select @@rowcount ,' Deal_Xrefs that do not have the required unit balances in DistributionInformation. Balancing executing' 

EXEC Balancing 

select 'Done with first balancing...'
select 'second balancing will follow shortly...'

update #TaxCertUT 
set Units = d.Value
from #TaxCertUT  t, DealInstrumentPosition d, #CreateBalances c
where d.Deal_Xref = c.Deal_Xref
and d.Refreshed = c.Date
and d.Balance_Type = 13 -- Current Units 
and t.Deal_Xref = d.Deal_Xref
and t.Instrument_No = d.Instrument_Number
and t.Dist_Date = d.Refreshed
and Units is null
select @@rowcount,' Records updated in #TaxCertUT '

truncate table #CreateBalances 

UPDATE #TaxCertUT 
SET    Instrument_No    	= IDP.Instrument_Number 
FROM   #TaxCertUT             	TC, 
       InstrumentDistPay    	IDP, 
       Instrument           	I 
WHERE  TC.Instrument_No 	= I.Instrument_Number 
AND    I.Instrument_Type    	= 'CASH' 
AND    TC.Instrument_No 	= IDP.DistPay_Instrument 

SELECT 	TC.Deal_Xref, 
       	TC.Flow_No, 
       	TC.Start_Date, 
       	TC.Instrument_No, 
       	TC.Amount, 
       	TC.Owner, 
       	TC.Product_Code, 
		TC.Contract_Number, 
		--NonTaxDividends	= round((IDH.Already_Taxed_Loc_Div_PreDWT + IDH.TaxFree_Loc_Div_CPU + IDH.Already_Taxed_Loc_Div_CPU + IDH.ToBeTaxed_Loc_Div_CPU ) * TC.Units/100.00,2),  -- Rel 20 VM 08-Apr-2012
        NonTaxDividends	= round((ISNULL(IDH.Already_Taxed_Loc_Div_PreDWT,0) + ISNULL(IDH.TaxFree_Loc_Div_CPU,0) + ISNULL(IDH.ToBeTaxed_Loc_Div_CPU,0) ) * ISNULL(TC.Units,0)/100.00,2),  -- Rel 20 VM 08-Apr-2012
		TaxForDividends = round((ISNULL(IDH.TaxFree_For_Div_CPU,0) + ISNULL(IDH.Already_Taxed_For_Div_CPU,0) + ISNULL(IDH.TBT_ForDiv_ForWHTLessThanSAWHT,0))* ISNULL(TC.Units,0)/100.00 ,2), -- release27 removed ToBeTaxed_For_Div_CPU
		WithholdingTax	= round((ISNULL(IDH.WHT_For_Div_NonSA_AlreadyTaxed,0) + ISNULL(IDH.WHT_ForDivSA_TBT_ForWHTLTSAWHT,0)) * -1 * ISNULL(TC.Units,0)/100.00 ,2),	-- release27 removed WHT_For_Div_SA_ToBeTaxed									   -- Rel 23 Included WHT_ForDivSA_TBT_ForWHTLTSAWHT

		Taxable_For_Int = round( isnull(ISNULL(IDH.TaxFree_For_Int_CPU,0) + ISNULL(IDH.Already_Taxed_For_Int_CPU,0) + 
                                 ISNULL(IDH.TaxFree_For_Oth_Inc_CPU,0) + ISNULL(IDH.Already_Taxed_For_Oth_Inc_CPU,0) + ISNULL(IDH.ToBeTaxed_For_Oth_Inc_CPU,0) +
                                 ISNULL(IDH.ToBeTaxed_For_Int_CPU,0),0) * ISNULL(TC.Units,0)/100.00 ,2),					   -- Rel 20 VM 08-Apr-2012

		Withholding_Tax_For_Int	= round(isnull(ISNULL(IDH.Withholding_Tax_For_Int,0) + ISNULL(IDH.WHT_For_Oth_Inc_CPU,0),0) * -1 * ISNULL(TC.Units,0)/100.00 ,2),																   -- Rel 20 VM 08-Apr-2012

		Interest	= round((ISNULL(IDH.TaxFree_Loc_Int_CPU,0) + ISNULL(IDH.Already_Taxed_Loc_Int_CPU,0) + 
                             ISNULL(IDH.ToBeTaxed_Loc_Int_CPU,0) + ISNULL(IDH.TaxFree_Loc_Oth_Inc_CPU,0) +
                          	ISNULL(IDH.Already_Taxed_Loc_Oth_Inc_CPU,0) + ISNULL(IDH.ToBeTaxed_Loc_Oth_Inc_CPU,0)) * ISNULL(TC.Units,0)/100.00,2),										   -- Rel 20 VM 08-Apr-2012
       	TC.Instrument_Class, 
		Adhoc,
        Loc_Div_ToBeTaxed       = ROUND(ISNULL(IDH.ToBeTaxed_Loc_Div_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                     -- Rel 24
        Loc_Div_TaxFree         = ROUND(ISNULL(IDH.TaxFree_Loc_Div_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                       -- Rel 24
        For_Div_TBT_SAWHT_LT    = ROUND(ISNULL(IDH.TBT_ForDiv_ForWHTLessThanSAWHT,0) * ISNULL(TC.Units,0) / 100.00 , 2),            -- Rel 24
        For_Div_TBT_SAWHT_GT    = ROUND(ISNULL(IDH.ToBeTaxed_For_Div_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                     -- Rel 24
        WHT_TBT_ForDivSA_LT     = ROUND((ISNULL(IDH.WHT_ForDivSA_TBT_ForWHTLTSAWHT,0) * -1) * ISNULL(TC.Units,0) / 100.00 , 2),     -- Rel 24
        WHT_TBT_ForDivSA_GT     = ROUND((ISNULL(IDH.WHT_For_Div_SA_ToBeTaxed,0) * -1) * ISNULL(TC.Units,0) / 100.00 , 2),           -- Rel 24
        Loc_REIT_TBT            = ROUND(ISNULL(IDH.ToBeTaxed_Loc_REIT_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                    -- Rel 26
        Loc_WH_Interest_TBT     = ROUND(ISNULL(IDH.TBT_Loc_WH_Interest_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                   -- Rel 26
        Already_Taxed_Loc_Div_InSpecie = ROUND((ISNULL(IDH.Already_Taxed_Loc_Div_InSpecie,0) + ISNULL(IDH.Already_Taxed_Loc_Div_CPU,0))* ISNULL(TC.Units,0) / 100.00 , 2)      -- Rel 24
INTO	#TaxCert 
FROM    InstrumentDistributionHistor IDH, 
        #TaxCertUT 		TC 
WHERE   TC.Instrument_No        *= IDH.Instrument_Number 
AND     TC.Start_Date           *= IDH.Reinvest_Date 
AND		IDH.Status 		= 'D' 
AND     IDH.Dist_Type 		IN ( 'U','N')   ---- D.M. Tax Run 2004 New Dist Type 'N'. OLB Release. 
AND		TC.Instrument_Class     = 'UT' 
and		TC.Instrument_No not in (Select Instrument_Number from GroupFundInstrumentCategory   where Category_Number = 14) 
and     IDH. Dist_Date < '1 Apr 2012'

INSERT #TaxCert 
SELECT 	TC.Deal_Xref, 
       	TC.Flow_No, 
       	TC.Start_Date, 
       	TC.Instrument_No, 
       	TC.Amount, 
       	TC.Owner, 
       	TC.Product_Code, 
		TC.Contract_Number, 
		NonTaxDividends	= round((ISNULL(IDH.Already_Taxed_Loc_Div_PreDWT,0)) * ISNULL(TC.Units,0)/100.00,2),  -- Rel 20 VM 08-Apr-2012
		TaxForDividends = round((ISNULL(IDH.TaxFree_For_Div_CPU,0) + ISNULL(IDH.Already_Taxed_For_Div_CPU,0) )* ISNULL(TC.Units,0)/100.00 ,2),								   -- Rel 20 VM 08-Apr-2012
		WithholdingTax	= round(ISNULL(IDH.WHT_For_Div_NonSA_AlreadyTaxed,0) * -1 * ISNULL(TC.Units,0)/100.00 ,2),										   -- Rel 20 VM 08-Apr-2012
		Taxable_For_Int = round( isnull(ISNULL(IDH.TaxFree_For_Int_CPU,0) + ISNULL(IDH.Already_Taxed_For_Int_CPU,0) + 
                                 ISNULL(IDH.TaxFree_For_Oth_Inc_CPU,0) + ISNULL(IDH.Already_Taxed_For_Oth_Inc_CPU,0) + ISNULL(IDH.ToBeTaxed_For_Oth_Inc_CPU,0) +
                                 ISNULL(IDH.ToBeTaxed_For_Int_CPU,0),0) * ISNULL(TC.Units,0)/100.00 ,2),					   -- Rel 20 VM 08-Apr-2012

		Withholding_Tax_For_Int	= round(isnull(ISNULL(IDH.Withholding_Tax_For_Int,0) + ISNULL(IDH.WHT_For_Oth_Inc_CPU,0),0) * -1 * ISNULL(TC.Units,0)/100.00 ,2),																   -- Rel 20 VM 08-Apr-2012

		Interest	= round((ISNULL(IDH.TaxFree_Loc_Int_CPU,0) + ISNULL(IDH.Already_Taxed_Loc_Int_CPU,0) + 
                             ISNULL(IDH.ToBeTaxed_Loc_Int_CPU,0) + ISNULL(IDH.TaxFree_Loc_Oth_Inc_CPU,0) +
                          	ISNULL(IDH.Already_Taxed_Loc_Oth_Inc_CPU,0) + ISNULL(IDH.ToBeTaxed_Loc_Oth_Inc_CPU,0)) * ISNULL(TC.Units,0)/100.00,2),			
       	TC.Instrument_Class, 
		Adhoc,
        Loc_Div_ToBeTaxed       = ROUND(ISNULL(IDH.ToBeTaxed_Loc_Div_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                     -- Rel 24
        Loc_Div_TaxFree         = ROUND(ISNULL(IDH.TaxFree_Loc_Div_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                       -- Rel 24
        For_Div_TBT_SAWHT_LT    = ROUND(ISNULL(IDH.TBT_ForDiv_ForWHTLessThanSAWHT,0) * ISNULL(TC.Units,0) / 100.00 , 2),            -- Rel 24
        For_Div_TBT_SAWHT_GT    = ROUND(ISNULL(IDH.ToBeTaxed_For_Div_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                     -- Rel 24
        WHT_TBT_ForDivSA_LT     = ROUND((ISNULL(IDH.WHT_ForDivSA_TBT_ForWHTLTSAWHT,0) * -1) * ISNULL(TC.Units,0) / 100.00 , 2),     -- Rel 24
        WHT_TBT_ForDivSA_GT     = ROUND((ISNULL(IDH.WHT_For_Div_SA_ToBeTaxed,0) * -1) * ISNULL(TC.Units,0) / 100.00 , 2),           -- Rel 24
        Loc_REIT_TBT            = ROUND(ISNULL(IDH.ToBeTaxed_Loc_REIT_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                    -- Rel 26
        Loc_WH_Interest_TBT     = ROUND(ISNULL(IDH.TBT_Loc_WH_Interest_CPU,0) * ISNULL(TC.Units,0) / 100.00 , 2),                   -- Rel 26
        Already_Taxed_Loc_Div_InSpecie = ROUND((ISNULL(IDH.Already_Taxed_Loc_Div_InSpecie,0) + ISNULL(IDH.Already_Taxed_Loc_Div_CPU,0)) * ISNULL(TC.Units,0) / 100.00 , 2)    -- Rel 24
FROM    InstrumentDistributionHistor IDH, 
        #TaxCertUT 		TC 
WHERE   TC.Instrument_No        *= IDH.Instrument_Number 
AND     TC.Start_Date           *= IDH.Reinvest_Date 
AND		IDH.Status 		= 'D' 
AND     IDH.Dist_Type 		IN ( 'U','N')   ---- D.M. Tax Run 2004 New Dist Type 'N'. OLB Release. 
AND		TC.Instrument_Class     = 'UT' 
and		TC.Instrument_No not in (Select Instrument_Number from GroupFundInstrumentCategory   where Category_Number = 14) 
and     IDH. Dist_Date >= '1 Apr 2012'

--- drop table #InstDistHistor_SI 
select I.Instrument_Number,  
	   Reinvest_Date, 
	   sum(I.Already_Taxed_Loc_Div_PreDWT + I.TaxFree_Loc_Div_CPU + I.Already_Taxed_Loc_Div_CPU + I.ToBeTaxed_Loc_Div_CPU) AS Already_Taxed_Loc_Div_PreDWT,   -- Rel 20 VM 08-Apr-2012
	   sum(I.TaxFree_Loc_Int_CPU + I.Already_Taxed_Loc_Int_CPU + I.ToBeTaxed_Loc_Int_CPU)  AS TaxFree_Loc_Int_CPU,		  -- Rel 20 VM 08-Apr-2012 
	   sum(Cents_per_Unit)																   AS Cents_per_Unit,			  -- Rel 20 VM 08-Apr-2012     
	   -- sum(I.TaxFree_For_Div_CPU + I.Already_Taxed_For_Div_CPU + I.ToBeTaxed_For_Div_CPU)  AS TaxFree_For_Div_CPU,		  -- Rel 20 VM 08-Apr-2012    -- Rel 23 Commented
	   --sum(I.TaxFree_For_Div_CPU + I.Already_Taxed_For_Div_CPU + I.ToBeTaxed_For_Div_CPU + I.TBT_ForDiv_ForWHTLessThanSAWHT)  AS TaxFree_For_Div_CPU,		  -- Rel 20 VM 08-Apr-2012    -- Rel 23 Included TBT_ForDiv_ForWHTLessThanSAWHT
	   sum(I.TaxFree_For_Div_CPU + I.Already_Taxed_For_Div_CPU + I.TBT_ForDiv_ForWHTLessThanSAWHT)  AS TaxFree_For_Div_CPU,		 -- release27 removed ToBeTaxed_For_Div_CPU
	   -- sum(I.WHT_For_Div_NonSA_AlreadyTaxed + I.WHT_For_Div_SA_ToBeTaxed)				   AS WHT_For_Div_NonSA_AlreadyTaxed, -- Rel 20 VM 08-Apr-2012 -- Rel 23 Commented
	   --sum(I.WHT_For_Div_NonSA_AlreadyTaxed + I.WHT_For_Div_SA_ToBeTaxed + I.WHT_ForDivSA_TBT_ForWHTLTSAWHT)				   AS WHT_For_Div_NonSA_AlreadyTaxed, -- Rel 20 VM 08-Apr-2012  -- Rel 23 Included WHT_ForDivSA_TBT_ForWHTLTSAWHT
	   sum(I.WHT_For_Div_NonSA_AlreadyTaxed + I.WHT_ForDivSA_TBT_ForWHTLTSAWHT)				   AS WHT_For_Div_NonSA_AlreadyTaxed, -- release27 removed WHT_For_Div_SA_ToBeTaxed
	   sum(I.TaxFree_For_Int_CPU + I.Already_Taxed_For_Int_CPU + I.ToBeTaxed_For_Int_CPU ) AS TaxFree_For_Int_CPU  ,          -- Rel 20 VM 08-Apr-2012
	   sum(Withholding_Tax_For_Int )													   AS Withholding_Tax_For_Int,         -- Rel 20 VM 08-Apr-2012
       SUM(I.ToBeTaxed_Loc_Div_CPU)     AS ToBeTaxed_Loc_Div_CPU,                       -- Rel 24
       SUM(I.TaxFree_Loc_Div_CPU)       AS TaxFree_Loc_Div_CPU,                         -- Rel 24
       SUM(I.TBT_ForDiv_ForWHTLessThanSAWHT)    AS TBT_ForDiv_ForWHTLessThanSAWHT,      -- Rel 24
       SUM(I.ToBeTaxed_For_Div_CPU)             AS ToBeTaxed_For_Div_CPU,               -- Rel 24
       SUM(I.WHT_ForDivSA_TBT_ForWHTLTSAWHT)    AS WHT_ForDivSA_TBT_ForWHTLTSAWHT,      -- Rel 24
       SUM(I.WHT_For_Div_SA_ToBeTaxed)          AS WHT_For_Div_SA_ToBeTaxed,            -- Rel 24
       SUM(I.ToBeTaxed_Loc_REIT_CPU)            AS ToBeTaxed_Loc_REIT_CPU,              -- Rel 26
       SUM(I.TBT_Loc_WH_Interest_CPU)           AS TBT_Loc_WH_Interest_CPU,             -- Rel 26
       SUM(I.Already_Taxed_Loc_Div_InSpecie)    AS Already_Taxed_Loc_Div_InSpecie       -- Rel 24
into   #SI_InstDistHistor_SI 
from   InstrumentDistributionHistor I, GroupFundInstrumentCategory  G 
where  Category_Number = 14 
and	   I.Instrument_Number = G.Instrument_Number 
AND	   Status 		= 'D' 
AND    Dist_Type IN ( 'U','N')  
group by I.Instrument_Number,  I.Reinvest_Date 
 
INSERT #TaxCert 
SELECT 	TC.Deal_Xref, 
       	TC.Flow_No, 
       	TC.Start_Date, 
       	TC.Instrument_No, 
       	TC.Amount, 
       	TC.Owner, 
       	TC.Product_Code, 
		TC.Contract_Number, 
		NonTaxDividends	= round(IDH.Already_Taxed_Loc_Div_PreDWT * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2), 
		TaxForDividends = round((IDH.TaxFree_For_Div_CPU * round(((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0) ,4)) ,2), 
		WithholdingTax	= round(((IDH.WHT_For_Div_NonSA_AlreadyTaxed * -1) * round(((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0) ,4)) ,2), 
		Taxable_For_Int = round((isnull(IDH.TaxFree_For_Int_CPU,0) * round(((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0) ,4)) ,2), 
		Withholding_Tax_For_Int	= round(((isnull(IDH.Withholding_Tax_For_Int,0) * -1) * round(((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0) ,4)) ,2), 
		Interest	= round(IDH.TaxFree_Loc_Int_CPU * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2), 
       	TC.Instrument_Class, 
		Adhoc ,
        Loc_Div_ToBeTaxed       = ROUND(IDH.ToBeTaxed_Loc_Div_CPU * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),                   -- Rel 24
        Loc_Div_TaxFree         = ROUND(IDH.TaxFree_Loc_Div_CPU * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),                     -- Rel 24
        For_Div_TBT_SAWHT_LT    = ROUND(IDH.TBT_ForDiv_ForWHTLessThanSAWHT * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),          -- Rel 24
        For_Div_TBT_SAWHT_GT    = ROUND(IDH.ToBeTaxed_For_Div_CPU * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),                   -- Rel 24
        WHT_TBT_ForDivSA_LT     = ROUND((IDH.WHT_ForDivSA_TBT_ForWHTLTSAWHT * -1) * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),   -- Rel 24
        WHT_TBT_ForDivSA_GT     = ROUND((IDH.WHT_For_Div_SA_ToBeTaxed * -1) * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),         -- Rel 24
        Loc_REIT_TBT            = ROUND(IDH.ToBeTaxed_Loc_REIT_CPU * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),                  -- Rel 26
        Loc_WH_Interest_TBT     = ROUND(IDH.TBT_Loc_WH_Interest_CPU * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2),                 -- Rel 26
        Already_Taxed_Loc_Div_InSpecie = ROUND(IDH.Already_Taxed_Loc_Div_InSpecie * round((TC.Amount * 100.0 / IDH.Cents_per_Unit) / 100.0,4),2)    -- Rel 24
FROM    #SI_InstDistHistor_SI IDH, 
        #TaxCertUT 		TC 
WHERE   TC.Instrument_No        *= IDH.Instrument_Number 
AND     TC.Start_Date           *= IDH.Reinvest_Date 
AND		TC.Instrument_Class     = 'UT' 
and     TC.Instrument_No in (Select Instrument_Number from GroupFundInstrumentCategory   where Category_Number = 14) 
 
 
/*** Money Market Interest Certificate Details ***/ 
 
INSERT #TaxCert 
SELECT 	T.Deal_Xref, 
       	T.Flow_Number,                                  
       	T.Value_Date,                
       	T.Dest_Instrument_Number, 
       	Amount = ISNULL(Amount,0), 
       	SD.Owner,                                  
       	SD.Product_Code, 
        SD.Contract_Number,                                  
       	NonTaxDividends 	= convert(money,0), 
       	TaxForDividends 	= convert(money,0), 
       	WithholdingTax 		= convert(money,0), 
       	Taxable_For_Int 	= convert(money,0), 
       	Withholding_Tax_For_Int = convert(money,0), 
       	Interest 		= T.Amount, 
       	Instrument_Class 	= 'MM', 
        Adhoc,
        Loc_Div_ToBeTaxed       = CONVERT(Money,0), -- Rel 24
        Loc_Div_TaxFree         = CONVERT(Money,0), -- Rel 24
        For_Div_TBT_SAWHT_LT    = CONVERT(Money,0), -- Rel 24
        For_Div_TBT_SAWHT_GT    = CONVERT(Money,0), -- Rel 24
        WHT_TBT_ForDivSA_LT     = CONVERT(Money,0), -- Rel 24
        WHT_TBT_ForDivSA_GT     = CONVERT(Money,0), -- Rel 24
        Loc_REIT_TBT            = CONVERT(Money,0), -- Rel 26
        Loc_WH_Interest_TBT     = CONVERT(Money,0), -- Rel 26
        Already_Taxed_Loc_Div_InSpecie = CONVERT(Money,0)   --  Rel 24
FROM   	Transact		T, 
       	#SourceDeals            SD, 
       	Instrument		I 
WHERE  	SD.Deal_Xref          	= T.Deal_Xref 
AND    	T.Transaction_Type      = 87 
AND    	T.Amount		> 0 
and	T.Dest_Instrument_Number = I.Instrument_Number 
and	I.Instrument_Type 	in ('CALL','FIX','GCAL','NOT','FEED','FSCAL') 
AND    	T.Value_Date		BETWEEN dateadd(dd,1,@Start_Date)  AND dateadd(dd,1,@End_Date) 
 
/*** Auto Interest Addition Certificate Details ***/ 
 
INSERT #TaxCert 
SELECT 	T.Deal_Xref, 
       	T.Flow_No,                                  
       	T.Date_Action,                
       	T.Instrument_No, 
       	Amount = ISNULL(Amount,0), 
       	SD.Owner,                                  
       	SD.Product_Code, 
        SD.Contract_Number,                                  
       	NonTaxDividends 	= convert(money,0), 
       	TaxForDividends 	= convert(money,0), 
       	WithholdingTax 		= convert(money,0), 
       	Taxable_For_Int 	= convert(money,0), 
       	Withholding_Tax_For_Int = convert(money,0), 
       	Interest 		= T.Amount, 
       	Instrument_Class 	= CASE WHEN I.Underlying_Code = 'MM' THEN 'MM' ELSE 'UT' END, 
        Adhoc,
        Loc_Div_ToBeTaxed   = CONVERT(Money,0), -- Rel 24
        Loc_Div_TaxFree     = CONVERT(Money,0), -- Rel 24
        For_Div_TBT_SAWHT_LT= CONVERT(Money,0), -- Rel 24
        For_Div_TBT_SAWHT_GT= CONVERT(Money,0), -- Rel 24
        WHT_TBT_ForDivSA_LT = CONVERT(Money,0), -- Rel 24
        WHT_TBT_ForDivSA_GT = CONVERT(Money,0), -- Rel 24
        Loc_REIT_TBT        = CONVERT(Money,0), -- Rel 26
        Loc_WH_Interest_TBT = CONVERT(Money,0), -- Rel 26
        Already_Taxed_Loc_Div_InSpecie = CONVERT(Money,0)   --  Rel 24
FROM   	Transactions		T, 
	#SourceDeals            SD, 
	Instrument		I 
WHERE  	SD.Deal_Xref         	= T.Deal_Xref 
AND     T.Instrument_No		= I.Instrument_Number 
--AND	I.Underlying_Code	<> 'MM' -- SD-34172 : Intereset application for all instrument types
AND    	T.Flow_Type        	= '^' 
AND    	T.Amount     		> 0 
AND    	T.Date_Action		BETWEEN @Start_Date AND @End_Date  
AND    	T.Transaction_Type	= 'D' 
 
/*** TYEP 2005. Total Makert value should show on the IT3b certificates ***/ 

----- K7 : Rel 24: Below Code included to do balancing in batches
IF OBJECT_ID('#IT3B_DealXref') IS NOT NULL DROP TABLE #IT3B_DealXref

SELECT 
DISTINCT    Deal_Xref, End_Date = @End_Date  
INTO        #IT3B_DealXref 
FROM        #SourceDeals	 

WHILE ((SELECT count(*) FROM #IT3B_DealXref) > 0 )
BEGIN
    TRUNCATE TABLE #CreateBalances

    SET ROWCOUNT 10000
    INSERT #CreateBalances 
    SELECT DISTINCT Deal_Xref, End_Date FROM #IT3B_DealXref
    SET ROWCOUNT 0

    IF OBJECT_ID('#CB') IS NOT NULL DROP TABLE #CB
    /* Adaptive Server has expanded all '*' elements in the following statement */ SELECT #CreateBalances.Deal_Xref, #CreateBalances.Date INTO #CB FROM #CreateBalances

    EXEC Balancing 

    DELETE  #IT3B_DealXref
    FROM    #IT3B_DealXref B,
            #CB L
    WHERE   L.Deal_Xref = B.Deal_Xref

    SELECT count(*) ,' Deal_Xrefs left for Balancing' FROM #IT3B_DealXref
END

IF OBJECT_ID('#IT3B_DealXref') IS NOT NULL DROP TABLE #IT3B_DealXref
----- K7 : Rel 24: Above Code included to do balancing in batches

/*
INSERT #CreateBalances 
SELECT DISTINCT Deal_Xref, @End_Date 
FROM #SourceDeals	 

EXEC Balancing 
*/
/* Abhishek Vaze FR-8017
   Closing balance totals must be instrument balance and not full market value of deal
*/
IF OBJECT_ID('#TotalMakertValue') IS NOT NULL DROP TABLE #TotalMakertValue

CREATE TABLE #TotalMakertValue
(
    Owner               Int         Null, 
    Product_Code        Varchar(5)  Null, 
    Contract_Number     Int         Null,
    TotalMakertValue    Money       Null 
)

-- Change on existing code, line 546 
IF (SELECT Name FROM Tenant) = 'RMB'
BEGIN
        Select distinct Deal_Xref, Instrument_No
		Into #DistinctDealInst
		From #TaxCertUT 

        INSERT INTO #TotalMakertValue
		SELECT SD.Owner, SD.Product_Code, SD.Contract_Number, SUM(ISNULL(Value,0)) TotalMakertValue 
		FROM DealInstrumentPosition DIP, 
		#SourceDeals     SD ,
		#DistinctDealInst DD
	    WHERE    1        =        1 
		AND      DIP.Deal_Xref    =        SD.Deal_Xref 
		AND      DIP.Refreshed             =        @End_Date 
		AND      DIP.Balance_Type =        2 
		AND     DIP.Deal_Xref = DD.Deal_Xref
		AND     DIP.Instrument_Number = DD. Instrument_No
		GROUP BY SD.Owner, SD.Product_Code, SD.Contract_Number 
END
ELSE
 BEGIN
        INSERT INTO #TotalMakertValue
		SELECT SD.Owner, SD.Product_Code, SD.Contract_Number, SUM(ISNULL(Value,0)) TotalMakertValue 
		FROM DealInstrumentPosition DIP, 
		#SourceDeals	    SD 
		WHERE 	1	=	1 
		AND	DIP.Deal_Xref	=	SD.Deal_Xref 
		AND	DIP.Refreshed		=	@End_Date 
		AND	DIP.Balance_Type	=	2 
		GROUP BY SD.Owner, SD.Product_Code, SD.Contract_Number 
END
    
/*** Save onto TaxCertificateHeader and TaxCertificateDetail tables ***/ 
  
SELECT Owner, 
       Product_Code, 
       Contract_Number, 
       Instrument_Class, 
       Interest 		= sum(ISNULL(Interest,0)), 
       NonTaxDividends 		= sum(ISNULL(NonTaxDividends,0)), 
       TaxForDividends 		= sum(ISNULL(TaxForDividends,0)), 
       WithholdingTax 		= sum(ISNULL(WithholdingTax,0)), 
       Taxable_For_Int 		= sum(ISNULL(Taxable_For_Int,0)), 
       Withholding_Tax_For_Int 	= sum(ISNULL(Withholding_Tax_For_Int,0)), 
       Adhoc,
       Loc_Div_ToBeTaxed    = SUM(ISNULL(Loc_Div_ToBeTaxed,0)),       -- Rel 24
       Loc_Div_TaxFree      = SUM(ISNULL(Loc_Div_TaxFree,0)),         -- Rel 24
       For_Div_TBT_SAWHT_LT = SUM(ISNULL(For_Div_TBT_SAWHT_LT,0)),    -- Rel 24
       For_Div_TBT_SAWHT_GT = SUM(ISNULL(For_Div_TBT_SAWHT_GT,0)),    -- Rel 24
       WHT_TBT_ForDivSA_LT  = SUM(ISNULL(WHT_TBT_ForDivSA_LT,0)),     -- Rel 24
       WHT_TBT_ForDivSA_GT  = SUM(ISNULL(WHT_TBT_ForDivSA_GT,0)),     -- Rel 24
       Loc_REIT_TBT         = SUM(ISNULL(Loc_REIT_TBT,0)),            -- Rel 26
       Loc_WH_Interest_TBT  = SUM(ISNULL(Loc_WH_Interest_TBT,0)),     -- Rel 26
       Already_Taxed_Loc_Div_InSpecie = SUM(ISNULL(Already_Taxed_Loc_Div_InSpecie,0)) --  Rel 24
INTO   #TaxCertTotal 
FROM   #TaxCert 
GROUP BY Owner, 
        Product_Code, 
        Contract_Number, 
        Instrument_Class, 
        Adhoc 
 
SELECT  Certificate_Key = Identity(9), 
        Owner, 
        Contract_Number, 
        Product_Code, 
        Year         = @Tax_Year, 
        Start_Date   = @Start_Date, 
        End_Date     = @End_Date, 
        Adhoc 
        into  #InsertHeader 
        FROM  #TaxCertTotal 
        group by  
        Product_Code, 
        Contract_Number, -- Order is impotant for correct Cert# generation 
        Owner, 
        Adhoc 
 
create index IH1 on #InsertHeader(Owner, Product_Code, Contract_Number, Year) 
create index IH2 on #InsertHeader(Product_Code) 
create index TC1 on #TaxCertTotal(Owner, Product_Code, Contract_Number) 

SELECT	Product_Code, 
        min(Certificate_Key) - 1 Certificate_Key, 
        convert(int, null) ExistingMax 
INTO	#ProdKey 
FROM	#InsertHeader 
GROUP BY  
        Product_Code 
 
/*****************************************************************************/ 
-- Don't re-issue numbers that have already been issued 
/*****************************************************************************/ 
--UPDATE	#ProdKey 
--SET	Certificate_Key = Certificate_Key - isnull( 
--(	SELECT 	Max(Certificate_Number)  
--	FROM 	TaxCertificateHeader  
--	WHERE 	Year 	= @Tax_Year 
--	AND	Product = P.Product_Code),0) 
--FROM	#ProdKey P 
 
--BEGIN TRANSACTION 

UPDATE	#ProdKey 
SET	ExistingMax = isnull( (SELECT Max(Certificate_Number)  
	FROM 	TaxCertificateHeader  
	WHERE 	Year 	= @Tax_Year 
	AND	Product = P.Product_Code),0) 
FROM	#ProdKey P 
 
    -- Delete If Tax details already Exists (Start)
    IF OBJECT_ID('#DelCertificates') IS NOT NULL DROP TABLE #DelCertificates

    SELECT      TH.Certificate_Key
    INTO        #DelCertificates
    FROM        TaxCertificateHeader TH
    INNER JOIN  #InsertHeader IH
    ON          TH.Owner            = IH.Owner
    AND         TH.Contract_Number  = IH.Contract_Number
    AND         TH.Product          = IH.Product_Code
    AND         TH.Year             = @Tax_Year
    --AND         TH.Start_Date       = @Start_Date --  SD-34172
    --AND         TH.End_Date         = @End_Date   --  SD-34172 
    AND         TH.Adhoc            = IH.Adhoc

    DELETE      TaxCertificateBroker
    FROM        TaxCertificateBroker TB
    INNER JOIN  #DelCertificates DC
    ON          TB.Certificate_Key = DC.Certificate_Key

    DELETE      TaxCertificateDealXrefs 
    FROM        TaxCertificateDealXrefs TD
    INNER JOIN  #DelCertificates DC
    ON          TD.Certificate_Key = DC.Certificate_Key

    DELETE      TaxCertificateDetail 
    FROM        TaxCertificateDetail TD
    INNER JOIN  #DelCertificates DC
    ON          TD.Certificate_Key = DC.Certificate_Key

    DELETE      TaxCertificateHeader 
    FROM        TaxCertificateHeader TH
    INNER JOIN  #DelCertificates DC
    ON          TH.Certificate_Key = DC.Certificate_Key

    IF OBJECT_ID('#DelCertificates') IS NOT NULL DROP TABLE #DelCertificates

    -- Delete If Tax details already Exists (End)

INSERT into TaxCertificateHeader 
    (
        Owner, 
        Product, 
        Contract_Number, 
        Year, 
        Certificate_Number, 
        Start_Date, 
        End_Date, 
        Adhoc
    ) 
SELECT	Owner, 
        Product_Code, 
        Contract_Number, 
        Year, 
        Certificate_Number = Certificate_Key + (SELECT ExistingMax - Certificate_Key FROM #ProdKey PK where IH.Product_Code = PK.Product_Code), 
        Start_Date, 
        End_Date, 
        Adhoc 
FROM	#InsertHeader IH 
ORDER BY 
        Product_Code -- Order is impotant for correct Cert# generation 
 
INSERT	TaxCertificateDetail  -- Rel 20 
		(
            Certificate_Key,
            Instrument_Class,
            Interest,
            Dividend,
            NonTaxDividends,
            TaxForDividends,
            WithholdingTax,
            Taxable_For_Int,
            Withholding_Tax_For_Int,
            Loc_Div_ToBeTaxed,                 -- Rel 24
            Loc_Div_TaxFree,                   -- Rel 24
            For_Div_TBT_SAWHT_LT,              -- Rel 24
            For_Div_TBT_SAWHT_GT,              -- Rel 24
            WHT_TBT_ForDivSA_LT,               -- Rel 24
            WHT_TBT_ForDivSA_GT,               -- Rel 24
            Loc_Div_AlreadyTaxed_InSpecie,     -- Rel 24
            Loc_Div_WHT,                       -- Rel 24
            For_Div_WHT,                       -- Rel 24
            Loc_REIT_TBT,                      -- Rel 26
            Loc_WH_Interest_TBT                -- Rel 26
        ) 
SELECT      TCH.Certificate_Key, 
            TCT.Instrument_Class, 
            Interest 		= round(sum(TCT.Interest),2), 
            Dividend		= NULL, 
            NonTaxDividends 	= round(sum(ISNULL(NonTaxDividends,0)),2), 
            TaxForDividends 	= round(sum(ISNULL(TaxForDividends,0)),2), 
            WithholdingTax 		= round(sum(ISNULL(WithholdingTax,0)),2), 
            Taxable_For_Int 	= round(sum(ISNULL(Taxable_For_Int,0)),2), 
            Withholding_Tax_For_Int = round(sum(ISNULL(Withholding_Tax_For_Int,0)),2),
            Loc_Div_ToBeTaxed   = round(sum(isnull(Loc_Div_ToBeTaxed,0)),2),                            -- Rel 24
            Loc_Div_TaxFree     = round(sum(isnull(Loc_Div_TaxFree,0)),2),                              -- Rel 24
            For_Div_TBT_SAWHT_LT = round(sum(isnull(For_Div_TBT_SAWHT_LT,0)),2),                        -- Rel 24
            For_Div_TBT_SAWHT_GT = round(sum(isnull(For_Div_TBT_SAWHT_GT,0)),2),                        -- Rel 24
            WHT_TBT_ForDivSA_LT  = round(sum(isnull(WHT_TBT_ForDivSA_LT,0)),2),                         -- Rel 24
            WHT_TBT_ForDivSA_GT  = round(sum(isnull(WHT_TBT_ForDivSA_GT,0)),2),                         -- Rel 24
            Already_Taxed_Loc_Div_InSpecie  = round(sum(isnull(Already_Taxed_Loc_Div_InSpecie,0)),2),   -- Rel 24
            0.0,        -- Loc_Div_WHT,                                                                 -- Rel 24
            0.0,        -- For_Div_WHT                                                                  -- Rel 24
            Loc_REIT_TBT        = round(sum(isnull(Loc_REIT_TBT,0)),2),                                 -- Rel 26
            Loc_WH_Interest_TBT = round(sum(isnull(Loc_WH_Interest_TBT,0)),2)                           -- Rel 26
FROM   	#TaxCertTotal       	TCT, 
      	TaxCertificateHeader 	TCH 
WHERE  	TCT.Owner        	= TCH.Owner 
AND    	TCT.Product_Code 	= TCH.Product 
AND    	TCT.Contract_Number 	= TCH.Contract_Number 
AND     TCH.Year         	= @Tax_Year 
GROUP BY 
        TCH.Certificate_Key, 
       	TCT.Instrument_Class 

/*** Update the total markert value of the investment ***/ 
 
UPDATE TaxCertificateDetail 
SET TotalMarketValue  =   TMV.TotalMakertValue 
FROM TaxCertificateHeader   TCH, 
     TaxCertificateDetail   TCD, 
     #TotalMakertValue       TMV 
WHERE 1=1 
AND	TCH.Certificate_Key	=	TCD.Certificate_Key 
AND	TCH.Owner		=	TMV.Owner 
AND	TCH.Contract_Number	=	TMV.Contract_Number 
AND	TCH.Product		=	TMV.Product_Code 
AND	TCH.Year		=	@Tax_Year 
 
/*** Negative total markert value should show as 0 on the tax certificates. ***/ 
UPDATE TaxCertificateDetail 
SET TotalMarketValue  =  0.00 
WHERE ISNULL(TotalMarketValue,0.00) < 0.00 
 
INSERT	TaxCertificateDealXrefs 
SELECT	Certificate_Key, 
        Deal_Xref 
FROM   	#SourceDeals         	SD, 
        #TaxCertTotal		TD, 
       	TaxCertificateHeader 	TCH 
WHERE  	SD.Owner        	= TCH.Owner 
AND    	SD.Product_Code 	= TCH.Product 
AND     SD.Contract_Number	= TCH.Contract_Number 
AND  	SD.Owner        	= TD.Owner 
AND    	SD.Product_Code 	= TD.Product_Code 
AND     SD.Contract_Number	= TD.Contract_Number 
AND     TCH.Year         	= @Tax_Year 
GROUP BY 
        Certificate_Key, 
        Deal_Xref 
 
INSERT	TaxCertificateBroker  
SELECT	TCH.Certificate_Key, 
        SD.Deal_Xref, 
        Broker_Number = DP.People_Number, 
        ISNULL(min((SELECT P.Initials + ' ' + P.Name FROM People P WHERE BC.Broker_Number = People_Number)),''), 
        Corporate_Number, 
        ISNULL(min((SELECT P.Initials + P.Name FROM People P WHERE BC.Corporate_Number = People_Number)),'') 
FROM    #TaxCertTotal		TD, 
        TaxCertificateHeader	TCH, 
        #SourceDeals		SD, 
        DealParticipants        DP, 
        BrokerCorporate 	BC                                 
WHERE	TD.Owner		= SD.Owner 
AND     TD.Product_Code		= SD.Product_Code 
AND     TD.Contract_Number	= SD.Contract_Number 
AND     TD.Owner		= TCH.Owner 
AND     TD.Product_Code		= TCH.Product 
AND     TD.Contract_Number	= TCH.Contract_Number 
AND     TCH.Year		= @Tax_Year 
AND     SD.Deal_Xref        	= DP.Deal_Xref 
AND     DP.Role_Id          	= 2 
AND     DP.People_Number	= BC.Broker_Number 
GROUP BY 
        TCH.Certificate_Key, 
        SD.Deal_Xref, 
        DP.People_Number, 
        Corporate_Number 

delete IT3bCreditsDebits
from #TaxCert t, IT3bCreditsDebits i
where i.Tax_Year = @Tax_Year
and t.Owner = i.Owner
and t.Product_Code = i.Product_Code
and t.Contract_Number = i.Contract_Number

 INSERT IT3bCreditsDebits
 select h.Certificate_Key,     
        t.Deal_Xref, 
        t.Flow_No, 
        @Tax_Year,
        t.Start_Date, 
        t.Instrument_No, 
        t.Amount, 
        t.Owner, 
        t.Product_Code, 
        t.Contract_Number, 
        t.NonTaxDividends,
        t.TaxForDividends,
        t.WithholdingTax,
        t.Taxable_For_Int,
        t.Withholding_Tax_For_Int,
        t.Interest,
        t.Instrument_Class, 
        t.Adhoc
from #TaxCert t, TaxCertificateHeader h
where h.Year = @Tax_Year
and t.Owner = h.Owner
and t.Product_Code = h.Product
and t.Contract_Number = h.Contract_Number

    -------------- Rel 24
    /*
    IF OBJECT_ID('#LocDivWHT') IS NOT NULL DROP TABLE #LocDivWHT

    SELECT      TCH.Certificate_Key, SUM(Txn.Amount) Amount
    INTO        #LocDivWHT
    FROM        TaxCertificateHeader TCH
    INNER JOIN  TaxCertificateDealXrefs TCD
    ON          TCH.Certificate_Key = TCD.Certificate_Key
    INNER JOIN  #SourceDeals SD
    ON          SD.Deal_Xref = Txn.Deal_Xref
    INNER JOIN  Transactions Txn
    ON          TCD.Deal_Xref = Txn.Deal_Xref
    AND         Txn.Transact_Type = 359
    AND         Txn.Flow_Type = 'Z'
    AND         Txn.Transaction_Type = 'D'
    AND         Txn.Date_Action BETWEEN @Start_Date AND @End_Date
    WHERE       TCH.Year = @Tax_Year
    GROUP BY    TCH.Certificate_Key

    UPDATE      TaxCertificateDetail
    SET         Loc_Div_WHT = ISNULL(LDW.Amount,0) -- * -1 : Rel 26
    FROM        TaxCertificateDetail TCD
    INNER JOIN  #LocDivWHT LDW
    ON          TCD.Certificate_Key = LDW.Certificate_Key
    WHERE       TCD.Instrument_Class = 'UT'

    UPDATE      TaxCertificateDetail
    SET         Loc_Div_WHT = 0,
                For_Div_WHT = 0
    FROM        TaxCertificateDetail TCD
    INNER JOIN  #LocDivWHT LDW
    ON          TCD.Certificate_Key = LDW.Certificate_Key
    WHERE       TCD.Instrument_Class = 'MM'

    IF OBJECT_ID('#LocDivWHT') IS NOT NULL DROP TABLE #LocDivWHT
    */
    -------------- Rel 24
    --------------<<<<< Rel 26 (Start) >>>>> -------------- 
    DECLARE @WHTOwner Int
    DECLARE @WHTContract_Number Int
    DECLARE @WHTProduct Varchar(5)

    IF OBJECT_ID('WORKSPACE..SourceDeals') IS NOT NULL DROP TABLE WORKSPACE..SourceDeals

    /* Adaptive Server has expanded all '*' elements in the following statement */ SELECT #SourceDeals.Deal_Xref, #SourceDeals.Owner, #SourceDeals.Product_Code, #SourceDeals.Contract_Number, #SourceDeals.Adhoc INTO WORKSPACE..SourceDeals FROM #SourceDeals
    CREATE INDEX WK_SD_Ndx1 ON WORKSPACE..SourceDeals (Deal_Xref)

    IF OBJECT_ID('#OwnCnPrd') IS NOT NULL DROP TABLE #OwnCnPrd

    SELECT      DISTINCT CD.Owner, CD.Contract_Number, Product = PG.Product_Code
    INTO        #OwnCnPrd
    FROM        WORKSPACE..SourceDeals WS
    INNER JOIN  ContractDeal CD
    ON          WS.Deal_Xref = CD.Deal_Xref
    INNER JOIN  ProductGroup PG
    ON          CD.Product = PG.Product_Internal
    AND         PG.Process = 'AX'

    WHILE ((SELECT count(*) FROM #OwnCnPrd) > 0 )
    BEGIN

        SET ROWCOUNT 1
        SELECT  @WHTOwner = Owner,                      --  65895
                @WHTContract_Number = Contract_Number,  --  46650
                @WHTProduct = Product                   --  IB
        FROM    #OwnCnPrd
        SET ROWCOUNT 0

        --SELECT ' Processing WHT Values for Owner: ' + Convert(Varchar,@WHTOwner) + ' Contract Number : ' + Convert(Varchar,@WHTContract_Number) + ' Product : ' + @WHTProduct

        IF OBJECT_ID('#WHT_Instrument') IS NOT NULL DROP TABLE #WHT_Instrument

        CREATE TABLE #WHT_Instrument
        (
            Deal_Xref           Int Null,
            Instrument_No       Int Null,
            Instrument_Number   Int Null
        )

        --SELECT      DISTINCT Txn.Deal_Xref, Txn.Instrument_No, IX.Instrument_Number
        --INTO        #WHT_Instrument
        INSERT INTO #WHT_Instrument
        SELECT      DISTINCT Txn.Deal_Xref, Txn.Instrument_No, IX.Instrument_Number
        FROM        TaxCertificateHeader TCH
        INNER JOIN  TaxCertificateDealXrefs TCD
        ON          TCH.Certificate_Key = TCD.Certificate_Key
        INNER JOIN  Transactions Txn
        ON          TCD.Deal_Xref = Txn.Deal_Xref
        AND         Txn.Transact_Type = 359
        AND         Txn.Flow_Type in ('Z')
        INNER JOIN  DistributionInfoHeader DIH
        ON          Txn.Deal_Xref = DIH.Deal_Xref
        AND         DIH.Dist_Date BETWEEN @Start_Date AND @End_Date
        --AND         Txn.Date_Action BETWEEN @Start_Date AND @End_Date
        INNER JOIN  WORKSPACE..SourceDeals SD   --  #SourceDeals SD
        ON          SD.Deal_Xref = Txn.Deal_Xref
        INNER JOIN  ContractDeal CD
        ON          SD.Deal_Xref = CD.Deal_Xref
        AND         CD.Owner = @WHTOwner
        AND         CD.Contract_Number = @WHTContract_Number
        INNER JOIN  ProductGroup PG
        ON          CD.Product = PG.Product_Internal
        AND         PG.Process = 'AX'
        AND         PG.Product_Code = @WHTProduct
        INNER JOIN  InstrumentXref IX
        ON          Txn.Instrument_No = IX.Referenced_Instrument
        AND         IX.Reference_Type = 2   -- Distribution Tax
        WHERE       TCH.Year = @Tax_Year
        AND         Txn.Instrument_No Not in (10998,20998)

        IF OBJECT_ID('#DisInfo_Dates') IS NOT NULL DROP TABLE #DisInfo_Dates

        SELECT      DH.Dist_Info_No,
                    Record_Status_Date = MAX(Record_Status_Date)
        INTO        #DisInfo_Dates 
        FROM        DistributionInfoHeader DH
        INNER JOIN  #WHT_Instrument I
        ON          DH.Instrument_Number = I.Instrument_Number
        AND         DH.Deal_Xref = I.Deal_Xref
        INNER JOIN  InstrumentDistributionHistor IDH
        ON          DH.Instrument_Number = IDH.Instrument_Number
        AND         DH.Dist_Date = IDH.Dist_Date
        AND         IDH.Dist_Date BETWEEN @Start_Date AND @End_Date
        INNER JOIN  DistributionInformation DI
        ON          DH.Dist_Info_No = DI.Dist_Info_No
        INNER JOIN  ContractDeal CD
        ON          DH.Deal_Xref = CD.Deal_Xref
        AND         CD.Owner = @WHTOwner
        AND         CD.Contract_Number = @WHTContract_Number
        INNER JOIN  ProductGroup PG
        ON          CD.Product = PG.Product_Internal
        AND         PG.Process = 'AX'
        AND         PG.Product_Code = @WHTProduct
        INNER JOIN  WORKSPACE..SourceDeals SD
        ON          SD.Deal_Xref = I.Deal_Xref
        GROUP BY    DH.Dist_Info_No

        IF OBJECT_ID('#WHT_Values') IS NOT NULL DROP TABLE #WHT_Values

        SELECT      CD.Owner, CD.Contract_Number, Product = PG.Product_Code, 
                    SUM(ISNULL(DI.WHT_Value,0)) WHT_Value, SUM(ISNULL(DI.REIT_WHT_Value,0)) REIT_WHT_Value
        INTO        #WHT_Values
        FROM        DistributionInfoHeader DH
        INNER JOIN  #WHT_Instrument I
        ON          DH.Instrument_Number = I.Instrument_Number
        AND         DH.Deal_Xref = I.Deal_Xref
        INNER JOIN  InstrumentDistributionHistor IDH
        ON          DH.Instrument_Number = IDH.Instrument_Number
        AND         DH.Dist_Date = IDH.Dist_Date
        --AND         IDH.Reinvest_Date BETWEEN @Start_Date AND @End_Date
        AND         IDH.Dist_Date BETWEEN @Start_Date AND @End_Date
        INNER JOIN  DistributionInformation DI
        ON          DH.Dist_Info_No = DI.Dist_Info_No
        INNER JOIN  ContractDeal CD
        ON          DH.Deal_Xref = CD.Deal_Xref
        AND         CD.Owner = @WHTOwner
        AND         CD.Contract_Number = @WHTContract_Number
        INNER JOIN  ProductGroup PG
        ON          CD.Product = PG.Product_Internal
        AND         PG.Process = 'AX'
        AND         PG.Product_Code = @WHTProduct
        INNER JOIN  WORKSPACE..SourceDeals SD
        ON          SD.Deal_Xref = I.Deal_Xref
        INNER JOIN  #DisInfo_Dates DD
        ON          DI.Dist_Info_No = DD.Dist_Info_No
        AND         DI.Record_Status_Date = DD.Record_Status_Date
        GROUP BY    CD.Owner, CD.Contract_Number, PG.Product_Code

        UPDATE      TaxCertificateDetail
        SET         Loc_Div_WHT = Round(ISNULL(WV.WHT_Value,0),2),
                    REIT_WHT_Tax = Round(ISNULL(WV.REIT_WHT_Value,0),2)
        FROM        TaxCertificateDetail TD
        INNER JOIN  TaxCertificateHeader TH
        ON          TH.Certificate_Key = TD.Certificate_Key
        AND         TH.Year = @Tax_Year
        INNER JOIN  #WHT_Values WV
        ON          TH.Owner = WV.Owner
        AND         TH.Product = WV.Product
        AND         TH.Contract_Number = WV.Contract_Number
        WHERE       TD.Instrument_Class = 'UT'

        UPDATE      TaxCertificateDetail
        SET         REIT_WHT_Tax = 0
        FROM        TaxCertificateDetail TCD
        INNER JOIN  TaxCertificateHeader TCH
        ON          TCH.Certificate_Key = TCD.Certificate_Key
        INNER JOIN  #WHT_Values L
        ON          TCH.Owner = L.Owner
        AND         TCH.Product = L.Product
        AND         TCH.Contract_Number = L.Contract_Number
        AND         TCH.Year = @Tax_Year
        WHERE       TCD.Instrument_Class = 'MM'

        IF OBJECT_ID('#LWHTInstrument') IS NOT NULL DROP TABLE #LWHTInstrument

        CREATE TABLE #LWHTInstrument
        (
            Deal_Xref           Int Null,
            Instrument_No       Int Null,
            Instrument_Number   Int Null
        )

        --SELECT      DISTINCT Txn.Deal_Xref, Txn.Instrument_No, IX.Instrument_Number
        --INTO        #LWHTInstrument
        INSERT INTO #LWHTInstrument (Deal_Xref, Instrument_No, Instrument_Number)
        SELECT      DISTINCT Txn.Deal_Xref, Txn.Instrument_No, IX.Instrument_Number
        FROM        TaxCertificateHeader TCH
        INNER JOIN  TaxCertificateDealXrefs TCD
        ON          TCH.Certificate_Key = TCD.Certificate_Key
        INNER JOIN  Transactions Txn
        ON          TCD.Deal_Xref = Txn.Deal_Xref
        AND         Txn.Transact_Type = 300
        AND         Txn.Flow_Type in ('Z')
        INNER JOIN  DistributionInfoHeader DIH
        ON          Txn.Deal_Xref = DIH.Deal_Xref
        AND         DIH.Dist_Date BETWEEN @Start_Date AND @End_Date
        --AND         Txn.Date_Action BETWEEN @Start_Date AND @End_Date
        INNER JOIN  WORKSPACE..SourceDeals SD
        ON          SD.Deal_Xref = Txn.Deal_Xref
        INNER JOIN  ContractDeal CD
        ON          SD.Deal_Xref = CD.Deal_Xref
        AND         CD.Owner = @WHTOwner
        AND         CD.Contract_Number = @WHTContract_Number
        INNER JOIN  ProductGroup PG
        ON          CD.Product = PG.Product_Internal
        AND         PG.Process = 'AX'
        AND         PG.Product_Code = @WHTProduct
        INNER JOIN  InstrumentXref IX
        ON          Txn.Instrument_No = IX.Referenced_Instrument
        AND         IX.Reference_Type = 2   -- Distribution Tax
        WHERE       TCH.Year = @Tax_Year
        AND         Txn.Instrument_No Not in (10998,20998)

        IF OBJECT_ID('#LWHI_Values') IS NOT NULL DROP TABLE #LWHI_Values

        SELECT      CD.Owner, CD.Contract_Number, Product = PG.Product_Code, 
                    SUM(ISNULL(DI.LWHI_WHT_Value,0)) LWHI_WHT_Value
        INTO        #LWHI_Values
        FROM        DistributionInfoHeader DH
        INNER JOIN  #LWHTInstrument I
        ON          DH.Instrument_Number = I.Instrument_Number
        AND         DH.Deal_Xref = I.Deal_Xref
        INNER JOIN  InstrumentDistributionHistor IDH
        ON          DH.Instrument_Number = IDH.Instrument_Number
        AND         DH.Dist_Date = IDH.Dist_Date
        --AND         IDH.Reinvest_Date BETWEEN @Start_Date AND @End_Date
        AND         IDH.Dist_Date BETWEEN @Start_Date AND @End_Date
        INNER JOIN  DistributionInformation DI
        ON          DH.Dist_Info_No = DI.Dist_Info_No
        INNER JOIN  ContractDeal CD
        ON          DH.Deal_Xref = CD.Deal_Xref
        AND         CD.Owner = @WHTOwner
        AND         CD.Contract_Number = @WHTContract_Number
        INNER JOIN  ProductGroup PG
        ON          CD.Product = PG.Product_Internal
        AND         PG.Process = 'AX'
        AND         PG.Product_Code = @WHTProduct
        INNER JOIN  WORKSPACE..SourceDeals SD
        ON          SD.Deal_Xref = I.Deal_Xref
        GROUP BY    CD.Owner, CD.Contract_Number, PG.Product_Code

        UPDATE      TaxCertificateDetail
        SET         LWHI_WHT_Tax = Round(ISNULL(WV.LWHI_WHT_Value,0),2)
        FROM        TaxCertificateDetail TD
        INNER JOIN  TaxCertificateHeader TH
        ON          TH.Certificate_Key = TD.Certificate_Key
        AND         TH.Year = @Tax_Year
        INNER JOIN  #LWHI_Values WV
        ON          TH.Owner = WV.Owner
        AND         TH.Product = WV.Product
        AND         TH.Contract_Number = WV.Contract_Number
        WHERE       TD.Instrument_Class = 'UT'

        UPDATE      TaxCertificateDetail
        SET         LWHI_WHT_Tax = 0
        FROM        TaxCertificateDetail TCD
        INNER JOIN  TaxCertificateHeader TCH
        ON          TCH.Certificate_Key = TCD.Certificate_Key
        INNER JOIN  #LWHI_Values L
        ON          TCH.Owner = L.Owner
        AND         TCH.Product = L.Product
        AND         TCH.Contract_Number = L.Contract_Number
        AND         TCH.Year = @Tax_Year
        WHERE       TCD.Instrument_Class = 'MM'

        DELETE      #OwnCnPrd
        WHERE       Owner = @WHTOwner
        AND         Contract_Number = @WHTContract_Number
        AND         Product = @WHTProduct

        --SELECT 'Records left for populating WHT Values : ' + Convert(Varchar, Count(*)) FROM  #OwnCnPrd
    END

    UPDATE      TaxCertificateDetail
    SET         Loc_Div_WHT = ISNULL(ABS(Loc_Div_WHT),0) * -1 ,
                REIT_WHT_Tax = ISNULL(ABS(REIT_WHT_Tax),0) * -1,
                LWHI_WHT_Tax = ISNULL(ABS(LWHI_WHT_Tax),0) * -1
    FROM        TaxCertificateDetail TC
    INNER JOIN  TaxCertificateHeader TH
    ON          TH.Certificate_Key = TC.Certificate_Key
    AND         TH.Owner = ISNULL(@Owner,TH.Owner)               --  Fix
    AND         TH.Product = ISNULL(@Product_Code,TH.Product)    --  Fix
    AND         TH.Year = @Tax_Year
    WHERE       TC.Instrument_Class = 'UT'

    IF OBJECT_ID('#WHT_Instrument') IS NOT NULL DROP TABLE #WHT_Instrument
    IF OBJECT_ID('#WHT_Values') IS NOT NULL DROP TABLE #WHT_Values
    IF OBJECT_ID('#LWHTInstrument') IS NOT NULL DROP TABLE #LWHTInstrument
    IF OBJECT_ID('#LWHI_Values') IS NOT NULL DROP TABLE #LWHI_Values
    --------------<<<<< Rel 26 (End) >>>>> -------------- 

    SET NOCOUNT OFF
END 

GO


Grant execute on IT3b_All to testers
GO

IF OBJECT_ID('IT3b_All') IS NULL
	PRINT '<<<   Error creating procedure IT3b_All   >>>'
ELSE
	PRINT '<<<   Successfully created procedure IT3b_All   >>>'
GO
