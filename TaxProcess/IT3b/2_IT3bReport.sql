

SET QUOTED_IDENTIFIER ON


if object_id("WORKSPACE..IT3bReport") is not null
DROP TABLE WORKSPACE..IT3bReport 
if object_id("#Address") is not null
drop table #Address
if object_id("#IT3B") is not null
drop table #IT3B
if object_id("#PeopleTax") is not null
drop table #PeopleTax
if object_id("#RegValue") is not null
drop table #RegValue
GO


DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates

/*****************************************************************************/
-- Fix Duplicate addresses
/*****************************************************************************/

SELECT	PA.People_Number,
	Min(Address_Number) Address_Number
INTO	#Address
FROM	PeopleAddress		PA,
	TaxCertificateHeader	C
WHERE	C.Year			= @Tax_Year
AND	C.Owner			= PA.People_Number
AND	PA.Address_Type		= 'POST'
GROUP BY
	PA.People_Number


CREATE INDEX perf1 on #Address (People_Number)

/*****************************************************************************/
--Return the data
/*****************************************************************************/



SELECT DISTINCT
    CASE
	   WHEN P.Title = 'The H' THEN 'The Honourable'
       ELSE ISNULL(P.Title, '')
    END AS 'Title',
    ISNULL(P.Initials,'') AS 'Initial',
    ISNULL(P.Name,'') AS 'Surname',
    ISNULL(P.First_Names,'') AS 'Name',
    CONVERT(VARCHAR(20), NULL) AS 'ID Number',
    convert(varchar(20), NULL) AS 'Company Registration', 
    convert(varchar(20), NULL) AS 'Tax Number',
    convert(varchar(20), Owner) + '/' + convert(varchar(20),Contract_Number) AS 'Account',
    convert(datetime,null) AS 'DOB',
    ISNULL(PA.Line_1,'') AS 'Address1',
    ISNULL(PA.Line_2,'') AS 'Address2',
    ISNULL(PA.Line_3,'') AS 'Address3',
    ISNULL(PA.Line_4,'') AS 'Address4',
    ISNULL(PA.Line_5,'') AS 'Address5',
    ISNULL(PA.Postal_Code,'') AS 'Postal_Code',
    TCH.Owner,
    TCH.Certificate_Number AS 'Certificate',
    ISNULL(TCH.Product,'') AS 'ProductCode',
    ISNULL(PD.Product_Name,'') AS 'Product',
    ISNULL(TCD.Instrument_Class,'') AS 'Instrument_Class',
    ISNULL(IC.Description,'') AS 'Instrument',
    TCD.Interest AS 'Interest',
    TCD.Taxable_For_Int AS 'Taxable Foreign Interest',
    TCD.Withholding_Tax_For_Int AS 'Withholding tax Interest',
    TCD.NonTaxDividends AS 'NonTaxableDividends',
    TCD.TaxForDividends AS 'TaxableForeignDividends',
    TCD.WithholdingTax AS 'WithholdingTax dividends',
    TCH.Start_Date AS 'FromDate',
    TCH.End_Date AS 'ToDate',
    @TaxYear AS TaxYear,
    TCD.TotalMarketValue,
    Adhoc
INTO #IT3B
FROM
    TaxCertificateHeader    TCH,
    TaxCertificateDetail    TCD,
    ProductDescription      PD,
    InstrumentClass         IC,
    People                  P,
    #Address           	    A,
    PeopleAddress	    PA
WHERE
    P.People_Number  = TCH.Owner  
AND A.People_Number = TCH.Owner
AND A.Address_Number = PA.Address_Number
AND TCH.Certificate_Key    = TCD.Certificate_Key
AND TCH.Product            = PD.Product_Code
AND TCD.Instrument_Class   = IC.Instrument_Class
AND TCH.Year		     = @Tax_Year
AND PD.Language            = 'E'
---AND TCH.Owner      =  69954
--AND Adhoc = CASE @Adhoc WHEN 0 THEN 0 ELSE Adhoc END
ORDER BY
    TCH.Product,
    TCH.Certificate_Number


update #IT3B
set "ID Number" = Reg_Value
FROM PeopleRegistration PR 
where PR.People_Number = #IT3B.Owner 
and Reg_Type = 1



SELECT  People_Number, max(PT.Tax_Number) Tax_Number
into #PeopleTax
FROM PeopleTax PT,
     #IT3B 
WHERE PT.Date_Effective < dateadd(yy,@TaxYear+1,'Mar 1 1899')
AND   PT.People_Number  = #IT3B.Owner
GROUP BY PT.People_Number 
HAVING Date_Effective = (select max(Date_Effective) from PeopleTax PT1 where PT1.People_Number = PT.People_Number)

UPDATE #IT3B 
SET "Tax Number"  =  PT.Tax_Number
FROM #PeopleTax PT
WHERE PT.People_Number   =  #IT3B.Owner

update #IT3B 
set DOB = convert(varchar(11), D.Date,111)
from PeopleDates	D
WHERE	D.People_Number = #IT3B.Owner
AND	D.Date_Type	 = 'B'


select PR.People_Number, max(Reg_Value) Reg_Value
into #RegValue
from #IT3B A,
      PeopleRegistration PR
where A.Owner = PR.People_Number
and   Reg_Type in (3,12)
group by PR.People_Number

update #IT3B 
set "Company Registration" = Reg_Value
from #RegValue RV
where RV.People_Number = #IT3B.Owner


update #IT3B 
set "Company Registration" = stuff("Company Registration", 12,12,'')
where ascii(substring("Company Registration",12,1)) not between 33 and 126


select
Title
,Initial,
Surname,
Name,
"ID Number" ,
"Company Registration",
"Tax Number" ,
Account,
convert(varchar(11),DOB,111) DOB,
Address1,
Address2,
Address3,
Address4,
Address5,
Postal_Code,
Owner,
Certificate,
ProductCode,
Product,
Instrument_Class,
Instrument,
Interest,
"Taxable Foreign Interest",
"Withholding tax Interest",
NonTaxableDividends,
TaxableForeignDividends,
"WithholdingTax dividends",
FromDate,
ToDate,
TaxYear,
TotalMarketValue,
Adhoc
INTO WORKSPACE..IT3bReport 
from #IT3B 




SET QUOTED_IDENTIFIER OFF



