SET FLUSHMESSAGE ON
go

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates


print ' Deleting IT3B Tax Certificates, Adhoc  = 1 and Year', @Tax_Year

declare @TaxYear int
select @TaxYear = @Tax_Year

select count(*), 'Certificates to be deleted'
from TaxCertificateHeader where Year = @TaxYear

print 'TaxCertificateBroker'

delete TaxCertificateBroker
from TaxCertificateBroker TCB, TaxCertificateHeader TCH
where TCH.Certificate_Key = TCB.Certificate_Key
and TCH.Year = @TaxYear
print  'Deleted TaxCertificateBroker %1!', @@rowcount -- 755



print 'TaxCertificateDealXrefs'

delete TaxCertificateDealXrefs
from TaxCertificateDealXrefs TCD, TaxCertificateHeader TCH
where TCH.Certificate_Key = TCD.Certificate_Key
and TCH.Year = @TaxYear
print  'Deleted TaxCertificateDealXrefs %1!', @@rowcount -- 730

print 'TaxCertificateDetail'

delete TaxCertificateDetail
from TaxCertificateDetail TCD, TaxCertificateHeader TCH
where TCH.Certificate_Key = TCD.Certificate_Key
and TCH.Year = @TaxYear
print  'Deleted TaxCertificateDetail %1!', @@rowcount -- 493

print 'TaxCertificateHeader'

delete TaxCertificateHeader
from TaxCertificateHeader where Year = @TaxYear 
print  'Deleted TaxCertificateHeader %1!', @@rowcount


exec IT3b_All   @Start_Date,
           @End_Date 
go