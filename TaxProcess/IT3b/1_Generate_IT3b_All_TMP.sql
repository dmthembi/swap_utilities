SET FLUSHMESSAGE ON
go


create table #data (Owner int)

insert into #data select 23124


select  Certificate_Key into #CertificateKey
from TaxCertificateHeader h, #data d
where h.Owner = d.Owner 
and h.Year = 2017


delete TaxCertificateBroker
where Certificate_Key in (select distinct Certificate_Key from #CertificateKey)


delete TaxCertificateDealXrefs
where Certificate_Key in (select distinct Certificate_Key from #CertificateKey)

delete TaxCertificateDetail
where Certificate_Key in (select distinct Certificate_Key from #CertificateKey)


delete TaxCertificateHeader
where Certificate_Key in (select distinct Certificate_Key from #CertificateKey)


DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year 
from TaxProcessDates


print ' Deleting IT3B Tax Certificates, Adhoc  = 1 and Year', @Tax_Year

declare @TaxYear int
select @TaxYear = @Tax_Year


exec IT3b_All   @Start_Date,
           @End_Date 
go