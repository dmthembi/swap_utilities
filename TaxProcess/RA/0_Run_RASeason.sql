

if object_id('#TaxYear') is not null 
drop table #TaxYear
go



DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates


select @Tax_Year
select @Bi_Annual

if(@Bi_Annual = 1)
begin
exec RASeason @Tax_Year, 'Y'
end
else
begin
exec RASeason @Tax_Year
end

go