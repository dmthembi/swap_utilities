

if object_id('WORKSPACE..RADetail') is not null
drop table WORKSPACE..RADetail
go
DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates

if(@Bi_Annual = 1)
begin
 select 'BI_Annual RA Certificates generation for'
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end
 else
 begin
 select 'Annual RA Certificates  generation for '
 select 'Start_Date ' + convert(varchar(11),@Start_Date) 
 select 'End_Date ' +   convert(varchar(11),@End_Date)
 end

exec RACertsGenerateNew @Tax_Year
go


go