--select * into RAContributionDetailData from WORKSPACE..RAContributionDetailData


if object_id('#TaxYear') is not null 
drop table #TaxYear
go

declare @Tax_Year int
select @Tax_Year = 2016

select  Tax_Year =  @Tax_Year into #TaxYear
go

if object_id('#Deals') is not null
drop table #Deals
if object_id('#del') is not null
drop table #del
if object_id('#data') is not null
drop table #data

go

select Deal_Xref, 
        Owner, 
       Contract_Number, 
       Amount    = sum(isnull(Amount,0)),
       DOScheduleAmount  = sum(isnull(DOScheduleAmount,0)),  
       DIDAmount  = sum(isnull(DIDAmount,0)),   
       DIDDAmount = sum(isnull(DIDDAmount,0))   
into #Deals
from WORKSPACE..RAContributionDetailData
where Tax_Year = 2016
and  isnull(Transfer_Type,0) not in (1,3,4)
group by Deal_Xref, Owner, Contract_Number

select *
into #del
from #Deals
where 1=2

if ((SELECT Name FROM Tenant) = 'RMB')
begin
insert into #del
select *
from #Deals
where DOScheduleAmount + DIDAmount + DIDDAmount = 0
end

if ((SELECT Name FROM Tenant) <> 'RMB')
begin
insert into #del
select *
from #Deals
where 1=1 --DOScheduleAmount + DIDAmount + DIDDAmount = 0
and Owner in (511417,525909,377891,466221,522855,467577,478889,467578,480706)
end



select TransactType = convert(varchar(30),null), w.*
into #data
from WORKSPACE..RAContributionDetailData w, #del d
where w.Contract_Number = d.Contract_Number
and w.Tax_Year = (select Tax_Year from #TaxYear)

update #data
set TransactType = tt.Long_Description
from #data d, TransactionType tt
where d.Transact_Type = tt.Transaction_Type 




delete WORKSPACE..RAContributionDetailData
from WORKSPACE..RAContributionDetailData w, #del d
where w.Contract_Number = d.Contract_Number
and w.Tax_Year = (select Tax_Year from #TaxYear)

delete RAContributionCert
from RAContributionCert r,  #del d
where r.Contract_Number = d.Contract_Number
and r.Year = (select Tax_Year from #TaxYear)

go

