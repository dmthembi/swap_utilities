-- RASeason 2014

if object_id('WORKSPACE..RADetail') is not null
drop table WORKSPACE..RADetail

if object_id('RACertsGenerateNew') is not null
drop proc RACertsGenerateNew
go

create procedure RACertsGenerateNew @TaxYear int
as
begin
/**
   drop table #RADataTEMP
     drop table #RAData
     drop table #DealStatuses
     drop table #RAContrCert
drop table #TaxYearBeginDates
drop table #RADataTEMP1
drop table #Transact
     drop table #BadData
     drop table #RABackup
drop table #RADataTEMPDeals
drop table #Collections
drop  table #SumCollections
drop table  WORKSPACE..RADetail
**/


   --declare @TaxYear int
   --select @TaxYear = 2015


   select *
   into #TaxYearBeginDates
   from TaxYearBeginDates
   where Tax_Year = @TaxYear


   select *, TotalInitFee = convert(float,null)
   into #RADataTEMP
   from WORKSPACE..RAContributionDetailData
   where Tax_Year = @TaxYear


    delete  #RADataTEMP
    where Transfer_Type in (1,3,4)
    SELECT      @@rowcount, ' Section 14 Transfers Deals deleted'


                            declare  @EndDate datetime 
                            declare @StartDate datetime

                            select @StartDate = Begin_Date, @EndDate = End_Date
                            from TaxYearBeginDates
                            where Tax_Year = @TaxYear

                                declare @PrevEndDate datetime
                                select @PrevEndDate  = dateadd(dd,-1,@StartDate)

                                create table #DealStatuses 
                                (
                                 Deal_Xref int,
                                 PreviousStatus char null, 
                                 Status char null, 
                                 PreviousDate datetime null, 
                                 Date Datetime null
                                )

                                insert #DealStatuses (Deal_Xref)
                                select  distinct Deal_Xref
                                from   #RADataTEMP

                                exec GetDealStatuses @PrevEndDate ,@EndDate 


                                delete #RADataTEMP
                                from   #RADataTEMP D,
                                       #DealStatuses DS
                                where   D.Deal_Xref = DS.Deal_Xref
                                and     DS.PreviousStatus = DS.Status
                                and     DS.Status in ('X','T')
                                select @@rowcount,'Records that went into X and T status the previous Tax Year deleted'

create index idx_1 on #RADataTEMP(Deal_Xref)


select distinct Deal_Xref, Date_Action
into #RADataTEMPDeals
from #RADataTEMP


select t.Deal_Xref, t.Value_Date, Amount = sum(t.Amount)
into #Transact
from #RADataTEMPDeals r inner join Transact t on r.Deal_Xref = t.Deal_Xref and r.Date_Action = t.Value_Date
where  t.Transaction_Type = 206
group by t.Deal_Xref, t.Value_Date


select count(*),' Records found in #Transact' from #Transact


update #RADataTEMP
set TotalInitFee = t.Amount
from #RADataTEMP r inner join #Transact t on r.Deal_Xref = t.Deal_Xref and r.Date_Action = t.Value_Date



    select  Owner ,
            Contract_Number, 
            Date_Action, 
            Product,
            Transact_Type,
            Amount = sum(isnull(Amount,0.00)),
            DOScheduleAmount  = convert(money,null),
            DIDAmount = convert(money,null),
            DIDDAmount  = convert(money,null),
            Updated = convert(bit,0),
            TotalInitFee = isnull(TotalInitFee,0.00),
            Reversal_Amount
    into #RADataTEMP1
    from #RADataTEMP
    group by Owner ,
            Contract_Number, 
            Date_Action,
            Product,
            Transact_Type,
            isnull(TotalInitFee,0.00),
            Reversal_Amount



    select  Owner ,
            Contract_Number, 
            Date_Action, 
            Product,
            Transact_Type,
            Amount = sum(isnull(Amount,0.00)),
            DOScheduleAmount  = convert(money,null),
            DIDAmount = convert(money,null),
            DIDDAmount  = convert(money,null),
            Updated = convert(bit,0),
            TotalInitFee = sum(isnull(TotalInitFee,0.00)),
            Reversal_Amount
    into #RAData
    from #RADataTEMP1
    group by Owner ,
            Contract_Number, 
            Date_Action,
            Product,
            Transact_Type,
            Reversal_Amount





select distinct Deal_Xref, Contract_Number, Date_Action, Transact_Type,  DOScheduleAmount ,DIDAmount ,DIDDAmount 
into #Collections
from #RADataTEMP




select Contract_Number, Date_Action, Transact_Type, DOScheduleAmount= sum( DOScheduleAmount), DIDAmount= sum(DIDAmount), DIDDAmount = sum(DIDDAmount)
into #SumCollections
from #Collections
group by Contract_Number, Date_Action, Transact_Type


update #RAData
set DOScheduleAmount = s.DOScheduleAmount,
    DIDAmount = s.DIDAmount,
    DIDDAmount = s.DIDDAmount
from #RAData r inner join #SumCollections s
          on r.Contract_Number = s.Contract_Number
          and r.Date_Action = s.Date_Action
          and r.Transact_Type = s.Transact_Type


    UPDATE      #RAData
    SET         Reversal_Amount = Amount
    WHERE       Updated = 0
    and         Reversal_Amount > 0
    and         Transact_Type = 369

    

    UPDATE      #RAData
    SET         Amount = Amount - Reversal_Amount,
                Updated = 1
    WHERE       Updated = 0
    and         Reversal_Amount > 0
    and         Transact_Type = 369





-- ----1



update #RAData
set Updated = 1
where  Updated = 0
and Amount = DOScheduleAmount





update #RAData
set Updated = 1
where  Updated = 0
and Amount = DIDAmount



update #RAData
set Updated = 1
where  Updated = 0
and Amount = DIDDAmount



--**

--2**  


-- Less than 10% varience.....
 
update #RAData
set Amount = DIDAmount,
    Updated = 1
where Updated = 0
and  isnull(DIDAmount,0) <> 0
--and   round(DIDAmount,2) = round(DIDDAmount,2 ) 
and  abs((1- Amount /DIDAmount))*100 < 10


update #RAData
set Amount = DIDAmount,
    Updated = 1
where Updated = 0
and  isnull(DIDAmount,0) <> 0
--and   round(DIDAmount,2) = round(DOScheduleAmount ,2) 
and  abs((1- Amount /DIDAmount))*100 < 10


update #RAData
set Amount = DIDDAmount,
    Updated = 1
where Updated = 0
and  isnull(DIDDAmount,0) <> 0
--and   round(DIDDAmount,2) = round(DOScheduleAmount,2)  
and  abs((1- Amount /DIDDAmount))*100 < 10


--**
update #RAData
set Amount = DIDAmount,
    Updated = 1
where Updated = 0
and  isnull(DIDAmount,0) <> 0
--and   round(DIDAmount,2) = round(DIDDAmount,2 ) 
and  abs((1- (Amount+TotalInitFee) /DIDAmount))*100 < 10


update #RAData
set Amount = DIDAmount,
    Updated = 1
where Updated = 0
and  isnull(DIDAmount,0) <> 0
--and   round(DIDAmount,2) = round(DOScheduleAmount ,2) 
and  abs((1- (Amount+TotalInitFee) /DIDAmount))*100 < 10

update #RAData
set Amount = DIDDAmount,
    Updated = 1
where Updated = 0
and  isnull(DIDDAmount,0) <> 0
--and   round(DIDDAmount,2) = round(DOScheduleAmount,2)  
and  abs((1- (Amount+TotalInitFee) /DIDDAmount))*100 < 10


--3**
update #RAData
set Amount = DOScheduleAmount,
    Updated = 1
where Updated = 0
and round(DOScheduleAmount,2) = round(Amount + TotalInitFee,2)

update #RAData
set Amount = DIDAmount,
    Updated = 1
where Updated = 0
and round(DIDAmount,2) = round(Amount + TotalInitFee,2)

update #RAData
set Amount = DIDDAmount,
    Updated = 1
where Updated = 0
and round(DIDDAmount,2) = round(Amount + TotalInitFee,2)

update #RAData
set Amount = DOScheduleAmount,
    Updated = 1
where Updated = 0
and abs(round(DOScheduleAmount,2) - round(Amount + TotalInitFee,2)) < 0.1


update #RAData
set Amount = DIDDAmount,
    Updated = 1
where Updated = 0
and abs(Amount - DIDAmount) < 0.0001


select *
into WORKSPACE..RADetail
from #RADataTEMP


truncate table WORKSPACE..RAContributionDetail


--bcp WORKSPACE..RADetail out RADetailIMS.csv -SIMSQA2 -Usystem_application -Psystem_application -c -t,

    insert into WORKSPACE..RAContributionDetail(Tax_Year,Contract_Number, Product ,Owner,Date_Action, Amount)
    select  (select Tax_Year from #TaxYearBeginDates),
            Contract_Number,
            Product,
            Owner, 
            Date_Action, 
            Sum(Amount)
    from #RAData
	where Amount > 0
    group by   
            Contract_Number,
            Product,
            Owner, 
            Date_Action

    select  Year = (select Tax_Year from #TaxYearBeginDates),
            Owner, 
            Product, 
            Contract_Number,  
            Amount = sum(Amount)
    into #RAContrCert
    from #RAData
	where Amount > 0
    group by  Owner, Product, Contract_Number



delete RAContributionCert
where Year = @TaxYear

/**
    delete RAContributionCert
    from RAContributionCert ra inner join #RAContrCert r
    on ra.Year = r.Year
    and ra.Owner = r.Owner
    and ra.Product = r.Product
    and ra.Contract_Number = r.Contract_Number
**/


    insert into RAContributionCert ( Year,Owner,Product,Contract_Number, Contribution)
    select Year,Owner,Product,Contract_Number, Amount
    from #RAContrCert r

   -- where r.Contribution > 0

end
go