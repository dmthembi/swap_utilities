
if object_id('WORKSPACE..RAContributionDetail') is not null
drop table WORKSPACE..RAContributionDetail
GO

DECLARE @Start_Date datetime,
        @End_Date   datetime,
        @Tax_Year    int,
		@Bi_Annual   bit


	
select  @Start_Date  = Begin_Date     , @End_Date =   End_Date  , @Tax_Year = Tax_Year , @Bi_Annual = Bi_Annual
from TaxProcessDates



select @Tax_Year

select P.Title Designation,
       P.Initials,
       P.First_Names  'ClientFirstName',
       Name 'ClientSurname',
       Line_1,
       Line_2,
       Line_3,
       Line_4,
       Line_5,
       Postal_Code,
       Reg_Value ID_Number ,
       Tax_Number = isnull((SELECT max(Tax_Number) FROM PeopleTax PT WHERE PT.Date_Effective < dateadd(yy,2004 + 1,'Mar 1 1899') and PT.People_Number = RA.Owner GROUP BY People_Number HAVING Date_Effective = max(Date_Effective)),'')  ,
       convert(varchar(20),RA.Owner) + '/' + convert(varchar(20),RA.Contract_Number) AccountNumber,
       --Product_Name Nameoffund, 
       CASE RA.Product
       WHEN 'ILRA' THEN 'Investec Investment Linked Retirement Annuity Fund'
       WHEN  'IRAF' THEN 'Investec Investment Linked Retirement Annuity Fund'
       WHEN  'INVRA' THEN 'Investec Investment Linked Retirement Annuity Fund'
       END Nameoffund,
       RA.Year TaxYear,
       '01 March 2016' PeriodFrom,
       '28 feb 2017'    PeriodTo,
       Contribution 
into WORKSPACE..RAContributionDetail
from RAContributionCert RA,
     People             P,
     PeopleAddress      PA,
     ProductDescription  PD,
     PeopleRegistration  PR
where RA.Year             = @Tax_Year
and   RA.Owner            *=     P.People_Number
and   PA.People_Number    =*     P.People_Number
and   PA.Address_Type     =     'POST'
and   RA.Product          *=     PD.Product_Code
and   PD.Language         =     'E'
and    PR.Reg_Type      =     1
and    RA.Owner *= PR.People_Number
GO


--select * from WORKSPACE..RAContributionDetail
