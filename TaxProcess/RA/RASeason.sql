drop proc RASeason
go


CREATE PROCEDURE dbo.RASeason @Tax_Year INT = null, @BiAnnual  char = 'N' 
AS
--*******************************************************************************
--* Author - Dale Mthembi
--* Date   - 
--* Descr  - 
--* 
--* ------------------------------------------------------------------
--*  Changes
--*  Change BY             Date              Changes
--*  ----------            --------------    -------------
--*  
--* 
--* ******************************************************************************
BEGIN
	-----------------------------------------------------
	EXEC DBA_ProcedureLog 'RASeason'
	-----------------------------------------------------





select '*** Start ***',getdate()

if @Tax_Year is null
BEGIN
select @Tax_Year = Tax_Year FROM TaxYearBeginDates WHERE getdate() BETWEEN Begin_Date  AND  End_Date
END

SELECT      Tax_Year,
            Begin_Date,       
            End_Date  
INTO        #TaxDates
FROM        TaxYearBeginDates 
WHERE       Tax_Year = @Tax_Year


if upper(@BiAnnual) = 'Y'
begin
    update #TaxDates
    set End_Date = dateadd(dd,-datepart(dd,End_Date),( dateadd(mm,-5,End_Date))) --- This will take it to 31 Aug 200Y
end



--1.Get All the Deals where Product in ('IRAF','ILRA','INVRA')
        SELECT      CD.Owner,
                    CD.Deal_Xref, 
                    CD.Deal_Number,
                    CD.Contract_Number, 
                    CD.Product,
                    D.Investment_Type
        INTO        #Deals
        FROM        ContractDeal CD
        INNER JOIN  Deal D
        ON          CD.Deal_Xref    =   D.Deal_Xref
        AND         CD.Product      IN  ('IRAF','ILRA','INVRA','RA')
        SELECT      @@rowcount, ' Active Deals found -- IRAF,ILRA,INVRA'



        CREATE unique INDEX idx_HashDeals_Deal_Xref ON #Deals(Deal_Xref)


--2. Now we take normal, expected transactions that are in the expected tax year..
    SELECT      TD.Tax_Year,
                Transact_Type       =   ISNULL(T.Transact_Type,0),
                Transfer_Type       =   convert(int,null),
                D.Investment_Type,
                D.Owner,
                D.Contract_Number, 
                D.Deal_Number,
                D.Product,
                D.Deal_Xref, 
                T.Date_Action, 
                T.Amount,
                T.Instrument_No,
                DOScheduleAmount    =   CONVERT(MONEY,NULL),
                DIDAmount           =   CONVERT(MONEY,null),
                DIDDAmount          =   CONVERT(MONEY,null),
                Reversal_Amount     =   CONVERT(MONEY,0.00),
                Month1              =   CONVERT(INT,NULL),
                Year1               =   CONVERT(INT,NULL)
    INTO        #Transactions
    FROM        #Deals D (index idx_HashDeals_Deal_Xref)
    INNER JOIN  Transactions T 
    ON          T.Deal_Xref         =   D.Deal_Xref
    INNER JOIN #TaxDates TD 
    ON T.Date_Action       >= TD.Begin_Date
    AND T.Date_Action      <= TD.End_Date
    where       T.Flow_Type         =   'P'
    AND         T.Transaction_Type  =   'D'

    SELECT      @@rowcount, 'Transactions found'

  CREATE INDEX idx_HashTrans ON #Transactions(Deal_Xref,Date_Action) 






--3. Delete the previours years data.....

    delete RAPreviousTaxYearData
    from RAPreviousTaxYearData r inner join  #TaxDates t
    on r.Tax_Year = t.Tax_Year
    inner join #Deals d on r.Deal_Xref = d.Deal_Xref



    insert into RAPreviousTaxYearData( Tax_Year,Reson,Transaction_Description,Deposit_Date,Deal_Xref,Date_Action,Instrument_No)
     select t.Tax_Year,
          Reson = convert(varchar(100),'Data whose Transactions are in this Tax Year but deposit in the last tax year.'), 
          Transaction_Description = tt.Long_Description,
          didd.Deposit_Date, 
          t.Deal_Xref,
          t.Date_Action,
          t.Instrument_No
    from #Transactions t inner join DealInvestmentDetail did on t.Deal_Xref = did.Deal_Xref
                                                           and t.Date_Action = did.Process_Date 
        inner join DealInvestmentDepositDetail didd on did.DealInvestmentDetail_Id = didd.DealInvestmentDetail_Id
       inner join TransactionType tt on tt.Transaction_Type = t.Transact_Type
    WHERE       didd.Deposit_Date <  (SELECT Begin_Date FROM #TaxDates)


  



    delete #Transactions
    from #Transactions t inner join DealInvestmentDetail did on t.Deal_Xref = did.Deal_Xref
                                                           and t.Date_Action = did.Process_Date 
        inner join DealInvestmentDepositDetail didd on did.DealInvestmentDetail_Id = didd.DealInvestmentDetail_Id
    WHERE       didd.Deposit_Date <  (SELECT Begin_Date FROM #TaxDates)
   select @@rowcount,' Transactions delete from #Transactions that belong to the previous tax year.'

 


--4. Transactions written in the new tax year but belong to the current tax year.... 


  select didd.DealInvestmentDetail_Id, Deposit_Date = dateadd(dd,1-datepart(dd,Deposit_Date),Deposit_Date) , totalDepositAmount = sum(Deposit_Amount)
  into #DealInvestmentDepositDetail
  from  #Deals d inner join DealInvestmentDepositDetail didd on d.Deal_Xref = didd.Deal_Xref
   WHERE       didd.Deposit_Date >=  (SELECT Begin_Date FROM #TaxDates)
   and         didd.Deposit_Date <=  (SELECT End_Date FROM #TaxDates)
   group by didd.DealInvestmentDetail_Id, dateadd(dd,1-datepart(dd,Deposit_Date),Deposit_Date)





    INSERT      #Transactions
    SELECT      (SELECT Tax_Year FROM #TaxDates),
                Transact_Type       =   ISNULL(t.Transact_Type,0),
                did.Transfer_Type,
                d.Investment_Type,
                d.Owner,
                d.Contract_Number, 
                d.Deal_Number,
                d.Product,
                d.Deal_Xref, 
                Date_Action = did.Process_Date, 
                t.Amount,
                t.Instrument_No,
                DOScheduleAmount    =   CONVERT(MONEY,NULL),
                DIDAmount           =   CONVERT(MONEY,null),
                DIDDAmount          =   CONVERT(MONEY,null),
                Reversal_Amount     =   CONVERT(MONEY,0.00),
                Month1              =   CONVERT(INT,NULL),
                Year1               =   CONVERT(INT,NULL)
    FROM        #Deals d inner join DealInvestmentDetail did on d.Deal_Xref = did.Deal_Xref
                inner join #DealInvestmentDepositDetail didd on did.DealInvestmentDetail_Id = didd.DealInvestmentDetail_Id
                inner join Transactions t on d.Deal_Xref = t.Deal_Xref
                                         and did.Process_Date = t.Date_Action
    WHERE        t.Date_Action > (SELECT End_Date FROM #TaxDates)
    and         t.Transaction_Type = 'D'
    and         t.Flow_Type = 'P'

    if upper(@BiAnnual) = 'Y'
    begin
        delete #Transactions
        where Date_Action >= (select End_Date from #TaxDates)
        select @@rowcount ,' Transactions delete due to limitations on BI Anual Run'
    end




    -- to be modified into delete later....
    truncate table RABackDatedTrans

    insert into RABackDatedTrans
    SELECT       Tax_Year = (SELECT Tax_Year FROM #TaxDates),
                Transact_Type       =   ISNULL(t.Transact_Type,0),
                d.Investment_Type,
                d.Owner,
                d.Contract_Number, 
                d.Deal_Number,
                d.Product,
                d.Deal_Xref, 
                Date_Action = didd.Deposit_Date, 
                t.Amount,
                t.Instrument_No,
                DOScheduleAmount    =   CONVERT(MONEY,NULL),
                DIDAmount           =   CONVERT(MONEY,null),
                DIDDAmount          =   CONVERT(MONEY,null),
                Reversal_Amount     =   CONVERT(MONEY,0.00),
                Month1              =   CONVERT(INT,NULL),
                Year1               =   CONVERT(INT,NULL)

    FROM        #Deals d inner join DealInvestmentDetail did on d.Deal_Xref = did.Deal_Xref
                inner join DealInvestmentDepositDetail didd on did.DealInvestmentDetail_Id = didd.DealInvestmentDetail_Id
                inner join Transactions t on d.Deal_Xref = t.Deal_Xref
                                         and did.Process_Date = t.Date_Action
    WHERE       didd.Deposit_Date >=  (SELECT Begin_Date FROM #TaxDates)
    and         didd.Deposit_Date <=  (SELECT End_Date FROM #TaxDates)
    and         t.Date_Action > (SELECT End_Date FROM #TaxDates)
    -- and         did.BusinessType_Id in (2,8) -- Group RA, Addition
    and         t.Transaction_Type = 'D'
    and         t.Flow_Type = 'P'


--5.Update The:
--            #Transactions DOScheduleAmount from DebitOrderSchedule (Amount)
--            DIDAmount from DealInvestmentDetail (Dep_Investment_Amount)
--            DIDDAmount from DealInvestmentDepositDetail (Deposit_Amount)




        UPDATE      #Transactions
        SET         DOScheduleAmount  =   DOS.Amount
        FROM        #Transactions T 
        INNER JOIN  DebitOrderSchedule DOS
        ON          T.Deal_Xref         = DOS.Deal_Xref
        AND         DOS.Date            = T.Date_Action
        select @@rowcount , 'records updated from DebitOrderSchedule table'




        UPDATE      #Transactions
        SET         DIDAmount     =   DID.Dep_Investment_Amount,
                    Transfer_Type  = DID.Transfer_Type
        FROM        #Transactions T 
        INNER JOIN  DealInvestmentDetail DID
        ON          T.Deal_Xref     = DID.Deal_Xref
        AND         T.Date_Action   = DID.Process_Date
        select @@rowcount , 'records updated from DealInvestmentDetail table'


        select Deal_Xref, Date_Action
        into #TransDIDD
        from #Transactions
        group by Deal_Xref, Date_Action



        SELECT      DID.Deal_Xref, 
                    DID.DealInvestmentDetail_Id,  
                    DID.Process_Date,
                    DID.BusinessType_Id ,
                    TotalAmount = SUM(DIDD.Deposit_Amount)
        INTO        #diddTotal
        FROM        #TransDIDD T 
        INNER JOIN  DealInvestmentDetail DID
        ON          T.Deal_Xref     = DID.Deal_Xref
        AND         T.Date_Action   = DID.Process_Date
        INNER JOIN  DealInvestmentDepositDetail DIDD
        ON          DID.DealInvestmentDetail_Id = DIDD.DealInvestmentDetail_Id
        AND         DIDD.Deposit_Date           >=  (SELECT Begin_Date FROM #TaxDates)
        AND         DIDD.Deposit_Date           <=  (SELECT End_Date FROM #TaxDates) 
        GROUP BY    DID.Deal_Xref , DID.DealInvestmentDetail_Id, DID.Process_Date, DID.BusinessType_Id




        SELECT      DID.Deal_Xref, 
                    DID.DealInvestmentDetail_Id,  
                    DIDD.Deposit_Date,
                    DID.BusinessType_Id ,
                    TotalAmount = SUM(DIDD.Deposit_Amount)
        INTO        #diddTotal_Bad_Data
        FROM        #TransDIDD T 
        INNER JOIN  DealInvestmentDetail DID
        ON          T.Deal_Xref     = DID.Deal_Xref
        INNER JOIN  DealInvestmentDepositDetail DIDD
        ON          DID.DealInvestmentDetail_Id = DIDD.DealInvestmentDetail_Id
        AND         DIDD.Deposit_Date           >=  (SELECT Begin_Date FROM #TaxDates)
        AND         DIDD.Deposit_Date           <=  (SELECT End_Date FROM #TaxDates)
        AND         DIDD.Deposit_Date           = T.Date_Action
        GROUP BY    DID.Deal_Xref , DID.DealInvestmentDetail_Id, DIDD.Deposit_Date, DID.BusinessType_Id 




        SELECT      DIDD.Deal_Xref, 
                    DIDD.DealInvestmentDetail_Id,  
                    DIDD.Deposit_Date,
                    TotalAmount = SUM(DIDD.Deposit_Amount)
        INTO        #diddTotal2
        FROM        #TransDIDD T 
        INNER JOIN  DealInvestmentDepositDetail DIDD
        ON          T.Deal_Xref = DIDD.Deal_Xref
        AND         T.Date_Action  = DIDD.Deposit_Date
        WHERE       DIDD.Deposit_Date           >=  (SELECT Begin_Date FROM #TaxDates)
        AND         DIDD.Deposit_Date           <=  (SELECT End_Date FROM #TaxDates) 
              GROUP BY    DIDD.Deal_Xref , DIDD.DealInvestmentDetail_Id, DIDD.Deposit_Date



        UPDATE      #Transactions
        SET         DIDDAmount    = D.TotalAmount,
                    Transfer_Type = DID.Transfer_Type
        FROM        #Transactions T 
        INNER JOIN  DealInvestmentDetail DID
        ON          T.Deal_Xref     = DID.Deal_Xref
        AND         T.Date_Action   = DID.Process_Date
        INNER JOIN  #diddTotal D 
        ON          DID.DealInvestmentDetail_Id = D.DealInvestmentDetail_Id
        AND        T.Date_Action = D.Process_Date
        WHERE      ((DID.BusinessType_Id = 2) and (T.Transact_Type = 367 ))



     DELETE #diddTotal_Bad_Data
     FROM #diddTotal_Bad_Data b inner join #diddTotal d
     on b.DealInvestmentDetail_Id = d.DealInvestmentDetail_Id



        UPDATE      #Transactions
        SET         DIDDAmount    = D.TotalAmount,
                    Transfer_Type = DID.Transfer_Type
        FROM        #Transactions T 
        INNER JOIN  DealInvestmentDetail DID
        ON          T.Deal_Xref     = DID.Deal_Xref
        INNER JOIN  #diddTotal_Bad_Data D 
        ON          DID.DealInvestmentDetail_Id = D.DealInvestmentDetail_Id
        AND        T.Date_Action = D.Deposit_Date
        and        T.DIDDAmount is null





--6.Get the reversals from the Transactions Table and upate in #Transactions (Reversal_Amount)
        SELECT      D.Owner,
                    D.Contract_Number,
                    D.Product,
                    D.Deal_Xref, 
                    T.Date_Action, 
                    Amount = sum(T.Amount),
                    Month1              =   CONVERT(INT,NULL),
                    Year1               =   CONVERT(INT,NULL)
        INTO        #Reversal
        FROM        Transactions T
        INNER JOIN  #Deals D
        ON          T.Deal_Xref         = D.Deal_Xref
        AND         T.Flow_Type         = '9'
        AND         T.Transaction_Type  = 'S'
        INNER JOIN  #TaxDates TD
        ON          T.Date_Action BETWEEN TD.Begin_Date AND TD.End_Date
        GROUP BY    D.Owner,
                    D.Contract_Number, 
                    D.Product,
                    D.Deal_Xref, 
                    T.Date_Action
        SELECT      @@rowcount, 'Reversals found'




        UPDATE      #Transactions
        SET         Reversal_Amount = R.Amount 
        FROM        #Transactions T, 
                    #Reversal R
        WHERE       T.Deal_Xref = R.Deal_Xref
        AND         datepart(yy,T.Date_Action)  = datepart(yy,R.Date_Action)
        AND         datepart(mm,T.Date_Action)  = datepart(mm,R.Date_Action)
        AND         T.Transact_Type          = 369                           --Debit Order
        AND         T.Date_Action  = ( SELECT MAX(Date_Action)
                                        FROM #Transactions T1
                                        WHERE   datepart(yy,T.Date_Action)  = datepart(yy,T1.Date_Action)
                                        AND     datepart(mm,T.Date_Action)  = datepart(mm,T1.Date_Action)
                                        AND     T.Deal_Xref = T1.Deal_Xref 
                                        AND     T1.Transact_Type          = 369)



--7. Data for RA Certificate Generation....

   delete WORKSPACE..RAContributionDetailData
   from WORKSPACE..RAContributionDetailData r inner join #Transactions t
   on r.Deal_Xref = t.Deal_Xref
   inner join #TaxDates td on r.Tax_Year = td.Tax_Year




    INSERT INTO WORKSPACE..RAContributionDetailData
    SELECT  (SELECT Tax_Year FROM #TaxDates),
            Transact_Type,
            Transfer_Type,
            Investment_Type,
            Owner,
            Contract_Number,
            Deal_Number,
            Product,
            Deal_Xref,
            Date_Action,
            Amount,
            Instrument_No,
            DOScheduleAmount,
            DIDAmount,
            DIDDAmount,
            Reversal_Amount
    FROM #Transactions





    UPDATE      #Transactions
    SET         Reversal_Amount = Amount
    WHERE       Reversal_Amount > 0

    UPDATE      #Transactions
    SET         Amount = Amount - Reversal_Amount


    delete  #Transactions
    where Transfer_Type in (1,3,4)
    SELECT      @@rowcount, ' Section 14 Transfers Deals deleted'

   --truncate table WORKSPACE..RAContributionDetail 



--select * from WORKSPACE..RAContributionDetail


   delete WORKSPACE..RAContributionDetail
   from WORKSPACE..RAContributionDetail w,  #Transactions t, #TaxDates td
   where w.Deal_Xref = t.Deal_Xref
   and w.Tax_Year = td.Tax_Year


    insert into WORKSPACE..RAContributionDetail --( Deal_Xref, Owner , Date_Action, Initial_Amount)
    select  (SELECT Tax_Year FROM #TaxDates),
            Deal_Xref, 
            Contract_Number,
            Owner,
            Date_Action,
            Product,
            sum(Amount)
    from #Transactions
    group by  Deal_Xref, 
            Owner, 
            Date_Action,
            Investment_Type



    select distinct Deal_Xref, Date_Action, DOScheduleAmount,DIDAmount,DIDDAmount
    into #Totals
    from #Transactions


    update WORKSPACE..RAContributionDetail
    set Amount = t.DOScheduleAmount
    from WORKSPACE..RAContributionDetail w, #Totals t
    where w.Deal_Xref = t.Deal_Xref
    and w.Date_Action = t.Date_Action
    and DOScheduleAmount = DIDAmount
    and DOScheduleAmount is not null

    update WORKSPACE..RAContributionDetail
    set Amount = t.DIDAmount
    from WORKSPACE..RAContributionDetail w, #Totals t
    where w.Deal_Xref = t.Deal_Xref
    and w.Date_Action = t.Date_Action
    and DOScheduleAmount <> DIDAmount
    and DIDAmount is not null






    INSERT      #Transactions
    SELECT      (select Tax_Year from #TaxDates),
                Transact_Type       =   NULL,
                null,
                D.Investment_Type,
                D.Owner,
                D.Contract_Number, 
                D.Deal_Number,
                D.Product,
                D.Deal_Xref, 
                Date_Action = NULL, 
                Amount              =   NULL,
                -1,                                         -- Instrument_Number
                DOScheduleAmount    =   CONVERT(MONEY,NULL),
                DIDAmount           =   CONVERT(MONEY,null),
                DIDDAmount          =   CONVERT(MONEY,null),
                Reversal_Amount     =   CONVERT(MONEY,0.00),
                Month1              =   CONVERT(INT,NULL),
                Year1               =   CONVERT(INT,NULL)
    FROM        #Deals D
    WHERE NOT EXISTS (SELECT 1 FROM #Transactions T WHERE D.Deal_Xref = T.Deal_Xref)
      

  -- exec RASARSDetailGenerate   --This is the electronic file crap...
    

    --11.Get the Total Value per Contract.
    --Get the IFAs.
    /* Adaptive Server has expanded all '*' elements in the following statement */ SELECT              Broker_Number = DP.People_Number,  
                        T.Tax_Year, T.Transact_Type, T.Transfer_Type, T.Investment_Type, T.Owner, T.Contract_Number, T.Deal_Number, T.Product, T.Deal_Xref, T.Date_Action, T.Amount, T.Instrument_No, T.DOScheduleAmount, T.DIDAmount, T.DIDDAmount, T.Reversal_Amount, T.Month1, T.Year1                                                                                    
    INTO                #data1
    FROM                DealParticipants DP 
    RIGHT OUTER JOIN    #Transactions T
    ON                  DP.Deal_Xref    = T.Deal_Xref
    WHERE               DP.Role_Id      = 2

    CREATE INDEX idx_Hashdata1 ON #data1(Broker_Number)

    --Get the Broker Corporate.
    SELECT      BC.Corporate_Number, 
                Next_DO_Date                = CONVERT(DATETIME,NULL), 
                Next_DO_Amount              = CONVERT(MONEY,NULL),
                Current_Investment_Value    = CONVERT(MONEY,NULL),  
                Value_Date                  = CONVERT(DATETIME,NULL),
                Previous_Total_Contribution = CONVERT(MONEY,NULL),
                D.Broker_Number,
                D.Owner,
                D.Contract_Number,
                D.Deal_Number,
                D.Product,
                D.Deal_Xref,
                Amount                      = SUM(Amount)
    INTO        #data2
    FROM        #data1 D 
    INNER JOIN  BrokerCorporate BC 
    ON          D.Broker_Number = BC.Broker_Number
    WHERE       CURRENT_DATE() BETWEEN ISNULL(BC.Start_Date,'01 JAN 1900') and ISNULL(BC.End_Date,'31 DEC 9999')
    GROUP BY    BC.Corporate_Number, 
                D.Broker_Number,
                D.Owner,
                D.Contract_Number,
                D.Deal_Number,
                D.Product,
                D.Deal_Xref

    CREATE INDEX idx_Hashdata2 ON #data2(Deal_Xref) 




    select distinct Deal_Xref , Owner, Contract_Number, Product,  Deal_Number into #DealsBKP from #data2



    --Get the Amount from WORKSPACE..IMS4Balances
    SELECT      W.Deal_Xref, 
                Refreshed, 
                Total_Amount = SUM(Value)
    INTO        #CurrentInvestmentValue
    FROM        WORKSPACE..IMS4Balances W 
    INNER JOIN  #DealsBKP D 
    ON          W.Deal_Xref     = D.Deal_Xref
    WHERE       W.Balance_Type  = 2
    GROUP BY    W.Deal_Xref, Refreshed

    CREATE INDEX idx_HashCIV ON #CurrentInvestmentValue(Deal_Xref)

   -- select * into CIB from #CurrentInvestmentValue



    SELECT      DISTINCT Deal_Xref
    INTO        #DistDeals
    FROM        #Transactions

   
    SELECT      T.Deal_Xref, 
                Next_Date       = MAX(DIFD.Next_Date), 
                Gross_Amount    = SUM(Gross_Amount)
    INTO        #NextDO
    FROM        #DistDeals T 
    INNER JOIN  DealInstrumentFlows DIF 
    ON          T.Deal_Xref     = DIF.Deal_Xref
    INNER JOIN  DealInstrumentFlowsDetail DIFD 
    ON          DIF.Flow_Number = DIFD.Flow_Number
    WHERE       DIF.Flow_Type   = 'P'
    AND         DIF.Number_Of_Occurances > 0
    AND         DIF.Remaining_Occurances > 0
    AND         DIFD.Direction  = 'S'
    AND         DIFD.Flow_Flag  = 'Y'
    GROUP BY    T.Deal_Xref 

    CREATE INDEX idx_HashNextDO ON #NextDO(Deal_Xref)

    UPDATE      #NextDO
    SET         Gross_Amount        =   D.Debit_Amount
    FROM        #NextDO N
    INNER JOIN  Deal D
    ON          N.Deal_Xref         =   D.Deal_Xref
    AND         D.Investment_Type   =   'R'


    UPDATE      #data2
    SET         D.Next_DO_Date      = ND.Next_Date,
                D.Next_DO_Amount    = ND.Gross_Amount
    FROM        #data2 D 
    INNER JOIN  #NextDO ND 
    ON          D.Deal_Xref         = ND.Deal_Xref




    SELECT      Corporate_Name          = CONVERT(VARCHAR(50),NULL),
                Corporate_Number,
                IFA_Name                = CONVERT(VARCHAR(50),NULL),
                IFA_Number              = Broker_Number,
                Investor_Name           = CONVERT(VARCHAR(50),NULL),
                Investor_Number         = Owner,
                Product,
                Contract_Number,
                Deal_Number,
                Deal_Xref,
                Value_Date,
                Next_DO_Date,
                Next_DO_Amount,
                Previous_Total_Contribution,
                Current_Investment_Value,
                Contribution_MarchToDec = CONVERT(MONEY,NULL),
                Contribution_JanToFeb   = CONVERT(MONEY,NULL)
    INTO        #Report
    FROM        #data2

    -- Get the deals not picked up....
    delete #DealsBKP
    from #DealsBKP d inner join #Report r
    on d.Deal_Xref = r.Deal_Xref



    select      Corporate_Name                  = CONVERT(VARCHAR(50),NULL),
                Corporate_Number                = convert(INT,0),
                IFA_Name                        = CONVERT(VARCHAR(50),NULL),
                IFA_Number                      = dp.People_Number, --Broker_Number,
                Investor_Name                   = CONVERT(VARCHAR(50),NULL),
                Investor_Number                 = d.Owner,
                d.Product,
                d.Contract_Number,
                d.Deal_Number,
                d.Deal_Xref,
                Value_Date                      = convert(DATETIME,NULL),
                Next_DO_Date                    = convert(DATETIME,NULL),
                Next_DO_Amount                  = convert(MONEY,NULL),
                Previous_Total_Contribution     = convert(MONEY,NULL),
                Current_Investment_Value        = convert(MONEY,NULL),
                Contribution_MarchToDec         = CONVERT(MONEY,NULL),
                Contribution_JanToFeb           = CONVERT(MONEY,NULL)
      INTO #DealNotPickedUp
      from #DealsBKP d inner join DealParticipants dp
      on d.Deal_Xref    =   dp.Deal_Xref
      where dp.Role_Id = 2

      update #DealNotPickedUp
      set Corporate_Number = bc.Corporate_Number
      from #DealNotPickedUp d inner join BrokerCorporate bc
      on d.IFA_Number = bc.Broker_Number
      where bc.End_Date is null


    /* Adaptive Server has expanded all '*' elements in the following statement */ insert into #Report
    select #DealNotPickedUp.Corporate_Name, #DealNotPickedUp.Corporate_Number, #DealNotPickedUp.IFA_Name, #DealNotPickedUp.IFA_Number, #DealNotPickedUp.Investor_Name, #DealNotPickedUp.Investor_Number, #DealNotPickedUp.Product, #DealNotPickedUp.Contract_Number, #DealNotPickedUp.Deal_Number, #DealNotPickedUp.Deal_Xref, #DealNotPickedUp.Value_Date, #DealNotPickedUp.Next_DO_Date, #DealNotPickedUp.Next_DO_Amount, #DealNotPickedUp.Previous_Total_Contribution, #DealNotPickedUp.Current_Investment_Value, #DealNotPickedUp.Contribution_MarchToDec, #DealNotPickedUp.Contribution_JanToFeb 
    from #DealNotPickedUp


   UPDATE      #Report
    SET         Current_Investment_Value    = C.Total_Amount,
                Value_Date                  = C.Refreshed
    FROM        #Report D 
    INNER JOIN  #CurrentInvestmentValue C 
    ON          D.Deal_Xref                 = C.Deal_Xref



    -- This will update Previous total Contribution amount at contract level, will repeat the amount at Deal_Xref level
  --  UPDATE      #Report
  --  SET         Previous_Total_Contribution = RA.Contribution
  --  FROM        #Report R 
  --  INNER JOIN  RAContributionCert RA 
  --  ON          R.Contract_Number = RA.Contract_Number
  --  WHERE       RA.Year = @Tax_Year -1 --2013


    select distinct Deal_Xref, Date_Action, Amount
    into #PrevYrData
    from WORKSPACE..RAContributionDetail RA, #TaxDates TD
    where RA.Tax_Year = TD.Tax_Year - 1

    select Deal_Xref, Amount = sum(Amount)
    into #PrevYrContribution
    from #PrevYrData
    group by Deal_Xref


    update #Report
    set Previous_Total_Contribution =  PY.Amount
    from #Report R, #PrevYrContribution PY
    where R.Deal_Xref = PY.Deal_Xref




    UPDATE      #Report
    SET         Corporate_Name      = P.Name
    FROM        #Report R 
    INNER JOIN  People P 
    ON          R.Corporate_Number  = P.People_Number 

    UPDATE      #Report
    SET         IFA_Name        = LTRIM(RTRIM(P.First_Names + ' ' + P.Name))
    FROM        #Report R 
    INNER JOIN  People P 
    ON          R.IFA_Number    = P.People_Number

      
    UPDATE      #Report
    SET         Investor_Name       = ltrim(rtrim(P.Title + ' ' + P.First_Names + ' ' + P.Name))
    FROM        #Report R 
    INNER JOIN  People P 
    ON          R.Investor_Number   = P.People_Number    

    --select * from #Transactions   
 
    SELECT      Deal_Xref, 
                Total_Amount = SUM(DIDAmount)
    INTO        #TotalsPreJan
    FROM        #Totals
    WHERE       Date_Action <= (SELECT dateadd(dd,-datepart(dd,End_Date),dateadd(mm,-1,End_Date)) FROM #TaxDates) --'31 Dec 2013'
    GROUP BY    Deal_Xref


    -- select dateadd(mm,-2,End_Date) from #TaxDates Syed to put the logic...

    UPDATE      #Report
    SET         Contribution_MarchToDec = T.Total_Amount
    FROM        #Report R 
    INNER JOIN  #TotalsPreJan T 
    ON          R.Deal_Xref             = T.Deal_Xref

    SELECT      Deal_Xref, 
                Total_Amount    = SUM(DIDAmount)
    INTO        #TotalsPostDec
    FROM        #Totals
    WHERE       Date_Action     > (SELECT dateadd(dd,-datepart(dd,End_Date),dateadd(mm,-1,End_Date)) FROM #TaxDates)--'31 Dec 2013'
    GROUP BY    Deal_Xref

    UPDATE      #Report
    SET         Contribution_JanToFeb = T.Total_Amount
    FROM        #Report R 
    INNER JOIN  #TotalsPostDec T 
    ON          R.Deal_Xref = T.Deal_Xref

 


    delete RAContributionReport
    from RAContributionReport ra inner join #Report r
    on ra.Deal_Xref = r.Deal_Xref

    /* Adaptive Server has expanded all '*' elements in the following statement */ INSERT      RAContributionReport
    SELECT      #Report.Corporate_Name, #Report.Corporate_Number, #Report.IFA_Name, #Report.IFA_Number, #Report.Investor_Name, #Report.Investor_Number, #Report.Product, #Report.Contract_Number, #Report.Deal_Number, #Report.Deal_Xref, #Report.Value_Date, #Report.Next_DO_Date, #Report.Next_DO_Amount, #Report.Previous_Total_Contribution, #Report.Current_Investment_Value, #Report.Contribution_MarchToDec, #Report.Contribution_JanToFeb 
    FROM        #Report

select '***************************The End*************************** ',getdate()

    --SELECT * FROM RAContributionReport
END
GO
