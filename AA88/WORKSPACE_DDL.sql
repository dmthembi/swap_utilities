CREATE TABLE AA88Staging(
TransactionNumber_3320      VARCHAR(10)  NULL,
EmployeeSurname_3030        VARCHAR(120) NULL,
FirstNames_3040             VARCHAR(90)  NULL,
RSAIDNumber_3060            VARCHAR(13)  NULL,
PassportNumber_3070         VARCHAR(16)  NULL,
IncomeTaxNumber_3100        VARCHAR(10)  NULL,
SWAPPeopleNo_3160           VARCHAR(25)  NULL,
IssueDate_3321              VARCHAR(8)   NULL,
StartDate_3322              VARCHAR(8)   NULL,
EndDate_3323                VARCHAR(8)   NULL,
MonthlyDeduction_3324       MONEY        NULL,
AmountDue_3325              MONEY        NULL,
PaymentRefNumber_3326       VARCHAR(19)  NULL,
CancellationDate_3330       VARCHAR(8)   NULL,
CancelValue_3331            MONEY        NULL,
CancelReason_3332           VARCHAR(90)  NULL,
TransactionStatusCode_3351  INT          NULL
)

 