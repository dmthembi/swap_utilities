﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.IO;
using System.Configuration;

namespace ImportAA88Data
{
    class AA88Data
    {
        AA88RecordList aa88RecordList;
        string conString;
        string dataSource;
        string port;
        string UID;
        string PWD;
        string Database;
        string AA88File;
        string InsertCommand;
        StreamReader file;
        


        public AA88Data()
        {
            conString = ConfigurationManager.ConnectionStrings["OLEDBconnectionString"].ToString();
            dataSource = ConfigurationManager.AppSettings["dataSource"].ToString();
            port = ConfigurationManager.AppSettings["port"].ToString();
            UID = ConfigurationManager.AppSettings["UID"].ToString();
            PWD = ConfigurationManager.AppSettings["PWD"].ToString();
            Database = ConfigurationManager.AppSettings["Database"].ToString();
            conString = String.Format(conString, dataSource, UID, PWD);

            AA88File = ConfigurationManager.AppSettings["AA88File"].ToString();
            InsertCommand = ConfigurationManager.AppSettings["InsertCommand"].ToString();

            file = new StreamReader(AA88File);

            Console.WriteLine("Generating List from File {0}", AA88File);
            aa88RecordList = ContainerClasses.getAA88RecordList(file);
            file.Close();
            Console.WriteLine();
            Console.WriteLine("Truncating staging table....");
            TruncateStaging();
            DateTime start = DateTime.Now;
            Console.WriteLine("sending data to the database....");
            TransferToStaging();
            DateTime end = DateTime.Now;
            Console.WriteLine("Done....");
            
            Console.ReadLine();

        }

        private void TruncateStaging()
        {
            OleDbConnection conn = new OleDbConnection(conString);
            conn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = conn;
            cmd.CommandText = "truncate table AA88Staging";

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        private void TransferToStaging()
        {
            string bulkInsetCommand = string.Empty;
            string tmpInsertCommand;
            OleDbConnection conn = new OleDbConnection(conString);
            conn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = conn;
            foreach (AA88Record row in aa88RecordList)
            {
                tmpInsertCommand = string.Format(InsertCommand, row.TransactionNumber_3320 == null ? "null" : row.TransactionNumber_3320,
                   row.EmployeeSurname_3030 == null ? "null" : row.EmployeeSurname_3030,
                  row.FirstNames_3040 == null ? "null" : row.FirstNames_3040,
                  row.RSAIDNumber_3060 == null ? "null" : row.RSAIDNumber_3060,
                  row.PassportNumber_3070 == null ? "null" : row.PassportNumber_3070,
                  row.IncomeTaxNumber_3100 == null ? "null" : row.IncomeTaxNumber_3100,
                  row.SWAPPeopleNo_3160 == null ? "null" : row.SWAPPeopleNo_3160,
                  row.IssueDate_3321 == null ? "null" : row.IssueDate_3321,
                  row.StartDate_3322 == null ? "null" : row.StartDate_3322,
                  row.EndDate_3323 == null ? "null" : row.EndDate_3323,
                  row.MonthlyDeduction_3324.Equals(null) ? "null" : row.MonthlyDeduction_3324.ToString(),
                  row.AmountDue_3325.Equals(null) ? "null" : row.AmountDue_3325.ToString(),
                  row.PaymentRefNumber_3326 == null ? "null" : row.PaymentRefNumber_3326,
                  row.CancellationDate_3330 == null ? "null" : row.CancellationDate_3330,
                  row.CancelValue_3331.Equals(null) ? "null" : row.CancelValue_3331.ToString(),
                  row.CancelReason_3332 == null ? "null" : row.CancelReason_3332,
                  row.TransactionStatusCode_3351 == null ? "null" : row.TransactionStatusCode_3351);

                bulkInsetCommand += tmpInsertCommand + " ";


            }

            cmd.CommandText = bulkInsetCommand;
            cmd.ExecuteNonQuery();

            conn.Close();

        }

    }


}
