﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace ImportAA88Data
{
    class ContainerClasses
    {
        public static string ConvertToString(string[] strArray)
        {
            string str = string.Empty;

            foreach (string field in strArray)
            {
                str += field;
                str += "|";
            }
            str = str.Substring(0, str.Length - 1);
            return str;
        }


        public static AA88RecordList getAA88RecordList(StreamReader file)
        {

            AA88RecordList aA88RecordList = new AA88RecordList();
            string line;
            int counter = 0;
            string[] lineArray;

            Console.Write("Line Number:          , Position ");
            while ((line = file.ReadLine()) != null)
            {
                counter++;
                if (counter == 1)
                    continue;      //skip first record....

                
                Console.SetCursorPosition(14, Console.CursorTop);
                Console.Write(counter);
                line = line.Replace("\"","");
                lineArray = line.Split(',');
                AA88Record aaRecord = createAARecord(lineArray);
                aA88RecordList.Add(aaRecord);
            }


            return aA88RecordList;
        }

        private static  AA88Record createAARecord(string[] lineArray)
        {
            int code;
            string value;
            decimal decimalValue;
            DateTime dateValue  = DateTime.Now;
            bool isInt;
            bool noError;
            int position = 0;
            AA88Record newRecord = new AA88Record();
            newRecord.hasError = false;
           // foreach(string code in lineArray)
            while (position < lineArray.Count()) 
            {
                
                Console.SetCursorPosition(35, Console.CursorTop);
                Console.Write(position);
               // if (position % 2 == 1)  //Code is always in odd position...
               isInt = int.TryParse(lineArray[position], out code);
               if (code == 9999)
                   break; //end of record...
               position++;
               value = lineArray[position];
               if (isInt)
               {
                                   
                   switch(code)
                   {
                       case 3320:
                           {
                               newRecord.TransactionNumber_3320 = "'" + value + "'";
                               break;
                           }
                       case 3030:
                           {
                               newRecord.EmployeeSurname_3030 = "'" + value + "'";
                               break;
                           }
                       case 3040:
                           {
                               newRecord.FirstNames_3040 = "'" + value + "'";
                               break;
                           }
                       case 3060:
                           {
                               newRecord.RSAIDNumber_3060 = "'" + value + "'";
                               break;
                           }
                       case 3070:
                           {
                               newRecord.PassportNumber_3070 = "'" + value + "'";
                               break;
                           }
                       case 3100:
                           {
                               newRecord.IncomeTaxNumber_3100 = "'" + value + "'";
                               break;
                           }
                       case 3160:
                           {
                               newRecord.SWAPPeopleNo_3160 = "'" + value + "'";
                               break;
                           }
                       case 3321:
                           {
                               if (value.Length != 8)
                               {
                                   noError = false;
                               }
                               else
                               {
                                   value = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);
                                   noError = DateTime.TryParse(value, out dateValue);
                               }

                               if (noError)
                               {
                                   newRecord.IssueDate_3321 = "'" + dateValue.ToString("yyyy MMM dd") + "'";
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;
                           }
                       case 3322:
                           {
                               if (value.Length != 8)
                               {
                                   noError = false;
                               }
                               else
                               {
                                   value = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);
                                   noError = DateTime.TryParse(value, out dateValue);
                               }

                               if (noError)
                               {
                                   newRecord.StartDate_3322 = "'" + dateValue.ToString("yyyy MMM dd") + "'";
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;


                           }
                       case 3323:
                           {
                               if (value.Length != 8)
                               {
                                   noError = false;
                               }
                               else
                               {
                                   value = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);
                                   noError = DateTime.TryParse(value, out dateValue);
                               }

                               if (noError)
                               {
                                   newRecord.EndDate_3323 = "'" + dateValue.ToString("yyyy MMM dd") + "'";
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;

                           }
                       case 3324:
                           {
                               noError = Decimal.TryParse(value, out decimalValue);
                               if (noError)
                               {
                                   newRecord.MonthlyDeduction_3324 = decimalValue;
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;
                           }
                       case 3325:
                           {
                               noError = Decimal.TryParse(value, out decimalValue);
                               if (noError)
                               {
                                   newRecord.AmountDue_3325 = decimalValue;
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;
                           }
                       case 3326:
                           {
                               newRecord.PaymentRefNumber_3326 = "'" + value + "'";
                               break;
                           }
                       case 3330:
                           {
                               if (value.Length != 8)
                               {
                                   noError = false;
                               }
                               else
                               {
                                   value = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);
                                   noError = DateTime.TryParse(value, out dateValue);
                               }

                               if (noError)
                               {
                                   newRecord.CancellationDate_3330 = "'" + dateValue.ToString("yyyy MMM dd") + "'";
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;
                               
                           }
                       case 3331:
                           {
                               noError = Decimal.TryParse(value, out decimalValue);
                               if (noError)
                               {
                                   newRecord.CancelValue_3331 = decimalValue;
                               }
                               else
                               {
                                   newRecord.hasError = true;
                                   position++;
                                   continue;
                               }
                               break;
                           }
                       case 3332:
                           {
                               newRecord.CancelReason_3332 = "'" + value + "'";
                               break;
                           }
                       case 3351:
                           {
                               newRecord.TransactionStatusCode_3351 =  value ;
                               break;
                           }

                       default:
                           {
                               newRecord.hasError = true;
                               break;
                           }
                       
                   }  //switch
                    
               } //if
               else
               {
                   newRecord.hasError = true;
               }
                            
                position++;

            }// while

                return newRecord;

        }


    }

    public class AA88Record
    {
        public string TransactionNumber_3320 { get; set; }
        public string EmployeeSurname_3030 { get; set; }
        public string FirstNames_3040 { get; set; }
        public string RSAIDNumber_3060 { get; set; }
        public string PassportNumber_3070 { get; set; }
        public string IncomeTaxNumber_3100 { get; set; }
        public string SWAPPeopleNo_3160 { get; set; }
        public string IssueDate_3321 { get; set; }
        public string StartDate_3322 { get; set; }
        public string EndDate_3323 { get; set; }
        public decimal MonthlyDeduction_3324 { get; set; }
        public decimal AmountDue_3325 { get; set; }
        public string PaymentRefNumber_3326 { get; set; }
        public string CancellationDate_3330 { get; set; }
        public decimal CancelValue_3331 { get; set; }
        public string CancelReason_3332 { get; set; }
        public string TransactionStatusCode_3351 { get; set; }
        public bool hasError { get; set; }
    }

    public class AA88RecordList : List<AA88Record>
    {
        public AA88RecordList() { }
        public AA88RecordList(DataTable dtAA88RecordList)
            : base()
        {

        }

        public void Add(string  _TransactionNumber_3320, string _EmployeeSurname_3030  ,string  _FirstNames_3040           ,string  _RSAIDNumber_3060        ,string  _PassportNumber_3070 ,
                       string  _IncomeTaxNumber_3100  ,string   _SWAPPeopleNo_3160     ,string  _IssueDate_3321            ,string  _StartDate_3322          ,string _EndDate_3323  ,
                       decimal _MonthlyDeduction_3324 ,decimal  _AmountDue_3325        ,string  _PaymentRefNumber_3326     ,string    _CancellationDate_3330 ,
                       decimal _CancelValue_3331      ,string   _CancelReason_3332     ,string  _TransactionStatusCode_3351 )
        {
            Add(new AA88Record()
            {
                TransactionNumber_3320 = _TransactionNumber_3320,
                EmployeeSurname_3030 = _EmployeeSurname_3030,
                FirstNames_3040 = _FirstNames_3040,
                RSAIDNumber_3060 = _RSAIDNumber_3060,
                PassportNumber_3070 = _PassportNumber_3070,
                IncomeTaxNumber_3100 = _IncomeTaxNumber_3100,
                SWAPPeopleNo_3160 = _SWAPPeopleNo_3160,
                IssueDate_3321 = _IssueDate_3321,
                StartDate_3322 = _StartDate_3322,
                EndDate_3323 = _EndDate_3323,
                MonthlyDeduction_3324 = _MonthlyDeduction_3324,
                AmountDue_3325 = _AmountDue_3325,
                PaymentRefNumber_3326 = _PaymentRefNumber_3326,
                CancellationDate_3330 = _CancellationDate_3330,
                CancelValue_3331 = _CancelValue_3331,
                CancelReason_3332 = _CancelReason_3332,
                TransactionStatusCode_3351 = _TransactionStatusCode_3351 
            });
        }
    }

}
